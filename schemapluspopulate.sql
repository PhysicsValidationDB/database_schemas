--
-- this table contains the reference to the entry in the inspire database:
-- we use the inspire API to extract info like author, Journal etc from inspire
-- using the ReferenceUpload java application 
--
CREATE TABLE INSPIREREFERENCE(
   INSPIREID  INTEGER NOT NULL PRIMARY KEY 
);
ALTER TABLE INSPIREREFERENCE OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON  INSPIREREFERENCE TO G4VALWRITER;
GRANT SELECT ON INSPIREREFERENCE  TO G4VALREADER;
--
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES ( 54182);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES ( 54183);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES ( 66422);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES ( 83895);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES ( 84030);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES ( 89128);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES ( 96272);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (108684);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (129342);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (180991);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (180999);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (182157);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (194031);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (197272);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (201929);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (216708);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (227712);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (230030);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (232412);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (285052);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (287791);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (313140);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (320447);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (323473);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (336815);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (365223);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (378192);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (407319);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (414336);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (431203);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (447317);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (457688);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (530755);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (532743);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (533861);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (561173);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (561787);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (599498);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (658042);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (686664);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (694016);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (695147);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (786183);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (813159);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (825244);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (826544);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (830148);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (857228);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (886780);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (1079585);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (1337955);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (892536);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (246551);
INSERT INTO PUBLIC.INSPIREREFERENCE (INSPIREID) VALUES (1473668);

---
--- The following table for reference 
--- (e.g. data extracted from the inspire database is cashed here to guarantee fast access)
--
CREATE TABLE REFERENCE(
   REFID      SERIAL PRIMARY KEY,
   INSPIREID  INTEGER,
   AUTHORS    CHARACTER VARYING(100)[],  -- list of authors 
   TITLE      TEXT,                      -- title of the paper
   JOURNAL    CHARACTER VARYING(100),    -- name of the scientific journal
   ERN        CHARACTER VARYING(100),    -- electronic-resource-num
   PAGES      CHARACTER VARYING(50),     -- page numbers
   VOLUME     CHARACTER VARYING(50),     -- journal volume
   YEA        INTEGER,                   -- year of publication
   ABSTR      TEXT,                      -- abstract
   KEYWORDS   CHARACTER VARYING(100)[],  -- keywords 
   LINKURL    TEXT                       -- link to the e.g. inspire url for this paper 
);
ALTER TABLE REFERENCE OWNER TO g4valwriter;
GRANT SELECT, UPDATE, INSERT, DELETE ON  REFERENCE TO g4valwriter;
GRANT SELECT ON REFERENCE TO g4valreader;

INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(54182,'{"Carter, A.A.","Riley, K.F.","Tapper, R.J.","Bugg, D.V.","Gilmore, R.S.","Knight, K.M.","Salter, D.C.","Stafford, G.H.","Wilson, E.J.N.","Davies, J.D.","Dowell, J.D.","Hattersley, P.M.","Homer, R.J.","O''dell, A.W."}','Pion-Nucleon Total Cross Sections from 0.5 to 2.65 GeV/c','Phys.Rev.','10.1103/PhysRev.168.1457','1457-1465','168',1968,'Total cross sections of π+ and π− mesons on protons and deuterons have been measured in a transmission experiment to relative accuracies of ±0.2% over the laboratory momentum range 0.46-2.67 GeV/c. The systematic error is estimated to be about ±0.5% over most of the range, increasing to about ±2% near both ends. Data have been obtained at momentum intervals of 25-50 MeV/c with a momentum resolution of ±0.6%. No new structure is observed in the π±p total cross sections, but results differ in several details from previous experiments. From 1-2 GeV/c, where systematic erros are the smallest, the total cross section of π− mesons on deuterons is found to be consistently higher than that of π+ mesons by (1.3±0.3)%; about half of this difference may be understood in terms of Coulomb-barrier effects. The πd and πN total cross sections are used to check the validity of the Glauber theory. Substantial disagreements (up to 2 mb) are observed, and the conclusion is drawn that the Glauber theory is inadequate in this momentum range.','{}','https://inspirehep.net/record/54182');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(54183,'{"Bugg, D.V.","Gilmore, R.S.","Knight, K.M.","Salter, D.C.","Stafford, G.H.","Wilson, E.J.N.","Davies, J.D.","Dowell, J.D.","Hattersley, P.M.","Homer, R.J.","O''dell, A.W.","Carter, A.A.","Tapper, R.J.","Riley, K.F."}','Kaon-Nucleon Total Cross Sections from 0.6 to 2.65 GeV/c','Phys.Rev.','10.1103/PhysRev.168.1466','1466-1475','168',1968,'Total cross sections of K+ and K− mesons on protons and deuterons have been measured in a transmission experiment over the range of laboratory momentum 0.6-2.65 GeV/c. Measurements have been made on K− at 58 momenta at intervals of 25-50 MeV/c; the experimental accuracy is better than 1% above 700 MeV/c, and the momentum resolution of the beam is ±0.6%. Structure is observed in the total cross sections suggesting or confirming Y1∗ resonances at masses of 1665, 1768, 1905, 2020, 2250, and 2455 MeV/c2 and Y0∗ resonances at masses of 1695, 1819, 1870, 2100, and 2340 MeV/c2. The K+ measurements are less extensive, and are concentrated in the momentum range below 1.5 GeV/c; the experimental errors are typically ±0.2 mb. Structure previously reported in the K+p and K+d total cross sections near a laboratory momentum of 1.2 GeV/c is confirmed. Total cross sections of K+ and K− on carbon have been measured at a number of momenta with an accuracy of about ±2%.','{}','https://inspirehep.net/record/54183');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(66422,'{"Dicello, J.F.","Igo, G."}','Proton Total Reaction Cross Sections in the 10-20-MeV Range: Calcium-40 and Carbon-12','Phys.Rev.','10.1103/PhysRevC.2.488','488-499','C2',1970,'Total reaction cross sections for protons on Ca40 have been measured at 15 energies between 10.3 and 21.6 MeV by the beam-attenuation method. The total reaction cross section for Ca40 rises sharply at low energies, reaches a maximum value around 13 MeV, and reaches a minimum value around 16 MeV. The rise at the lower energies is a result of the Coulomb barrier. The dip at 16 MeV is probably associated with the (p,n) threshold for Ca40. A comparison is made between the present experimental values and preliminary optical-model predictions based on available elastic-scattering data and polarization data. The variation in the reaction cross section is also compared with the integrated partial cross sections for elastic scattering. Total reaction cross sections for protons on carbon have been measured at ten energies between 9.88 and 19.48 MeV. Resonances in the total reaction cross section are observed in the neighborhood of 10.4 and 13.8 MeV. Variations of 200 mb are seen in the cross sections with changes in energy of the incident protons of about 200 keV. A comparison is made of the present total reaction cross sections and the integrated partial cross sections for elastic scattering and inelastic scattering to the first excited state of C12.','{}','https://inspirehep.net/record/66422');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(83895,'{"Wilkin, Colin","Cox, C.R.","Domingo, J.J.","Gabathuler, K.","Pedroni, E.","Rohlin, J.","Schwaller, P.","Tanner, N.W."}','A comparison of pi+ and pi- total cross-sections of light nuclei near the 3-3 resonance','Nucl.Phys.','10.1016/0550-3213(73)90242-3','61-85','B62',1973,'The total cross sections of 4 He, 6 Li, 7 Li, 9 Be, 12 C and 32 S for positive and negative pions have been measured in the energy range 80 to 260 MeV in a transmission experiment. Coulomb corrections were applied using real parts of the forward nuclear amplitudes as determined from dispersion relations. At the lower energies there remain large residual differences between the π + and π − scattering on the isoscalar nuclei. These can be largely understood in terms of the Coulomb distortion.','{"pi+ nucleus: interaction","interaction: pi+ nucleus","pi+ light nucleus: interaction","interaction: pi+ light nucleus","pi- nucleus: interaction","interaction: pi- nucleus","pi- light nucleus: interaction","interaction: pi- light nucleus","helium","lithium","beryllium","carbon","sulfur","pi+: total cross section","total cross section: pi+","pi-: total cross section","total cross section: pi-","correction: coulomb scattering","coulomb scattering: correction","absorption","counters and detectors: experimental results","cern cycl","0.1-0.3 gev"}','https://inspirehep.net/record/83895');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(84030,'{"Allardyce, B.W.","Batty, C.J.","Baugh, D.J.","Friedman, E.","Heymann, G.","Cage, M.E.","Pyle, G.J.","Squier, G.T.A.","Clough, A.S.","Jackson, D.F.","Murugesu, S.","Rajaratnam, V."}','Pion reaction cross-sections and nuclear sizes','Nucl.Phys.','10.1016/0375-9474(73)90049-3','1-51','A209',1973,'Measurements have been made of the reaction cross sections of π− and π+ mesons for the nuclei C, Ca, Ni, Sn and Pb at beam momenta of 0.71, 0.84, 1.00, 1.36, 1.58 and 2.00 GeV/c. Additionally measurements have been made for Al, 120Sn, Ho and 208Pb at momenta of 0.84, 1.00, 1.36 and 1.58 GeV/c. The ratio of π− to π+ reaction cross sections has been analysed in terms of neutron density distributions using the Watson multiple-scattering theory. The effects of second-order contributions and of the various choices of wave equation have been considered. It is found that the distributions of neutrons and protons in heavy nuclei must have very similar radial parameters. Good agreement is obtained between the measured and calculated values for the reaction cross sections, without any adjustment of parameters.','{"nuclear reaction","pi- nucleus: interaction","interaction: pi- nucleus","pi+ nucleus: interaction","interaction: pi+ nucleus","aluminum","carbon","calcium","nickel","tin","lead","holmium","multiple scattering","n: density matrix","nucleus: form factor","form factor: nucleus","pi+: total cross section","total cross section: pi+","pi-: total cross section","total cross section: pi-","counters and detectors: experimental results","nimrod ps","0.6-1.9 gev"}','https://inspirehep.net/record/84030');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(89128,'{"Clough, A.S.","Turner, G.K.","Allardyce, B.W.","Batty, C.J.","Baugh, D.J.","McDonald, W.John","Riddle, R.A.J.","Watson, L.H.","Cage, M.E.","Pyle, G.J.","Squier, G.T.A."}','Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV','Nucl.Phys.','10.1016/0550-3213(74)90134-5','15-28','B76',1974,'Total cross sections for π+ and π− mesons on 6Li,7Li, 9Be and C have been measured in the energy range from 90 to 860 MeV, and for oxygen from 88 to 228 MeV. The results of calculations with Kisslinger and local optical model potentials, using different sets of parameters, are compared with the data for 6Li, C and O.','{}','https://inspirehep.net/record/89128');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(96272,'{"Singer, P."}','Emission of particles following muon capture in intermediate and heavy nuclei','Springer Tracts Mod.Phys.','','39-87','71',1974,'','{"muon nucleus: interaction","interaction: muon nucleus","muon: capture","capture: muon","meson: atom","n: emission","emission: n","photon: emission","emission: photon","emission: particle","particle: emission","review","interpretation of experiments"}','https://inspirehep.net/record/96272');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(108684,'{"Carroll, A.S.","Chiang, I.-H.","Dover, C.B.","Kycia, T.F.","Li, K.K.","Mazur, P.O.","Michael, D.N.","Mockett, P.M.","Rahm, D.C.","Rubinstein, R."}','Pion-Nucleus Total Cross-Sections in the (3,3) Resonance Region','Phys.Rev.','10.1103/PhysRevC.14.635','635-638','C14',1976,'The results of total cross section measurements are presented for π± on targets of natural Li, C, Al, Fe, Sn, and Pb in the region of 65-320 MeV laboratory kinetic energy. The data are fitted with a simple phenomenological model, which allows one to extract the A dependence of the peak energy and width which characterize the pion-nucleus interaction. NUCLEAR REACTIONS π± on natural Li, C, Al, Fe, Sn, Pb, E=60−320 MeV; measured total σ in transmission experiment.','{"pi- nucleus: interaction","interaction: pi- nucleus","pi+ nucleus: interaction","interaction: pi+ nucleus","lithium","carbon","aluminum","iron","tin","lead","Delta(1232)","total cross section: energy dependence","energy dependence: total cross section","parametrization","dependence: mass number","mass number: dependence","experimental results","Brookhaven PS","0.06-0.32 GeV-kin"}','https://inspirehep.net/record/108684');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(129342,'{"Baba, K.","Endo, I.","Fujisaki, M.","Kadota, S.","Sumi, Y.","Fujii, S.","Murata, Y.","Noguchi, S.","Murakami, A."}','Quasifree Pion Photoproduction From Carbon Above 300-{MeV}','Nucl.Phys.','10.1016/0375-9474(78)90466-9','292-310','A306',1978,'The momentum spectra of charged pions produced in the reaction 12 C( γ , π ± ) have been measured for incident photon energies in the interval between 300 and 850 MeV in steps of 50 MeV. Pions with relatively high momenta have been detected by a magnetic spectrometer set at the lab angles 28.4° and 44.2°. All these spectra exhibit a clear singly peaked structure. Detailed features of the structure are quantitatively investigated and compared with a plane-wave impulse-approximation calculation. Calculated results are found to reproduce the spectral shape very well and the absolute magnitude is also rather well fitted by introducing the pion absorption effect properly.','{"PHOTON LIGHT NUCLEUS: INCLUSIVE REACTION","CARBON","PI+: FINAL STATE","FINAL STATE: PI+","PI-: FINAL STATE","FINAL STATE: PI-","PI+: PHOTOPRODUCTION","PHOTOPRODUCTION: PI+","PI-: PHOTOPRODUCTION","PHOTOPRODUCTION: PI-","PI+: MOMENTUM SPECTRUM","PI-: MOMENTUM SPECTRUM","APPROXIMATION: PLANE WAVE IMPULSE","PI: ABSORPTION","ABSORPTION: PI","MAGNETIC SPECTROMETER: EXPERIMENTAL RESULTS","EXPERIMENTAL RESULTS: MAGNETIC SPECTROMETER","TOKYO ES","PHOTON CARBON-12 --> PI+ ANYTHING","PHOTON CARBON-12 --> PI- ANYTHING","0.30-0.85 GEV"}','https://inspirehep.net/record/129342');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(180991,'{"Schumacher, R.A.","Adams, G.S.","Ingham, D.R.","Matthews, J.L.","Sapp, W.W.","Turley, R.S.","Owens, R.O.","Roberts, B.L."}','CU (GAMMA, P) X REACTION AT E (GAMMA) = 150-MEV AND 300-MEV','Phys.Rev.','10.1103/PhysRevC.25.2269','2269-2277','C25',1982,'Inclusive photoproton cross sections for the reaction Cu(γ,p)X have been measured for a photon energy of 300 MeV at proton angles 45°, 90°, and 135°, and for 150 MeV at 45°. The data are compared with an intranuclear-cascade calculation and with Ni(π±,p) data. The angular distribution is analyzed to obtain an estimate of the number of nucleons involved in the interaction. NUCLEAR REACTIONS Cu(γ,p)X, E=150 MeV, θ=45°, E=300 MeV, θ=45°,90°,135°; measured σ(Ep,θ); intranuclear cascade analysis.','{"PHOTON NUCLEUS: NUCLEAR REACTION","PHOTON NUCLEUS: INCLUSIVE REACTION","COPPER","P: PHOTOPRODUCTION","PHOTOPRODUCTION: P","MODEL: INTRANUCLEAR CASCADE","DIFFERENTIAL CROSS SECTION: ANGULAR DEPENDENCE","ANGULAR DEPENDENCE: DIFFERENTIAL CROSS SECTION","DIFFERENTIAL CROSS SECTION: ENERGY DEPENDENCE","ENERGY DEPENDENCE: DIFFERENTIAL CROSS SECTION","MODEL: INTERACTION","INTERACTION: MODEL","MODEL: ISOBAR","COUNTERS AND DETECTORS: EXPERIMENTAL RESULTS","PHOTON COPPER --> P ANYTHING","0.15: 0.30 GEV: 45: 90: 135: 150 DEGREES"}','https://inspirehep.net/record/180991');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(180999,'{"Marlow, Daniel R.","Barnes, P.D.","Colella, N.J.","Dytman, S.A.","Eisenstein, R.A.","Grace, R.","Takeutchi, F.","Wharton, W.R.","Bart, S.","Hancock, D.","Hackenburg, R.","Hungerford, E.","Mayes, W.","Pinsky, Lawrence S.","Williams, T.","Chrien, R.","Palevsky, H.","Sutter, R."}','KAON SCATTERING FROM C AND CA AT 800-MEV/C','Phys.Rev.','10.1103/PhysRevC.25.2619','2619-2637','C25',1982,'Results from K± elastic and inelastic scattering from C12 and Ca40 are reported. The data were all taken at an incident momentum of 800 MeV/c over an angular range from 2° to 38°. The elastic data are compared to first-order optical model calculations in coordinate and momentum space; good qualitative agreement is obtained. The inelastic data (from C12 only) are compared to distorted-wave Born approximation calculations, and good agreement is found if "realistic" inelastic transition densities are used. Although a first-order optical potential description does not describe the data fully, there are strong indications of the increased penetrability of K+ over K− in this energy range. NUCLEAR REACTIONS C12(K±,K±)C12, Ca40(K±,K±)Ca40, E=442 MeV (800 MeV/c), measured σ(θ) for elastic and inelastic scattering, compared to optical model and DWBA calculations, deduced optical potential parameters; θ=2°−38°, Δθ=1°.','{"K+ LIGHT NUCLEUS: ELASTIC SCATTERING","ELASTIC SCATTERING: K+ LIGHT NUCLEUS","K- LIGHT NUCLEUS: ELASTIC SCATTERING","ELASTIC SCATTERING: K- LIGHT NUCLEUS","K+ LIGHT NUCLEUS: INELASTIC SCATTERING","INELASTIC SCATTERING: K+ LIGHT NUCLEUS","K- LIGHT NUCLEUS: INELASTIC SCATTERING","INELASTIC SCATTERING: K- LIGHT NUCLEUS","CARBON","K+ NUCLEUS: ELASTIC SCATTERING","ELASTIC SCATTERING: K+ NUCLEUS","K- NUCLEUS: ELASTIC SCATTERING","ELASTIC SCATTERING: K- NUCLEUS","K+ NUCLEUS: INELASTIC SCATTERING","INELASTIC SCATTERING: K+ NUCLEUS","K- NUCLEUS: INELASTIC SCATTERING","INELASTIC SCATTERING: K- NUCLEUS","CALCIUM","DIFFERENTIAL CROSS SECTION: ANGULAR DEPENDENCE","ANGULAR DEPENDENCE: DIFFERENTIAL CROSS SECTION","POTENTIAL: OPTICAL","APPROXIMATION: DISTORTED WAVE BORN","SPECTROMETER: EXPERIMENTAL RESULTS","BROOKHAVEN PS","TABLES","0.442 GEV: 2-38 DEGREES"}','https://inspirehep.net/record/180999');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(182157,'{"Madey, R.","Vilaithong, T.","Anderson, B.D.","Knudson, J.N.","Witten, T.R.","Baldwin, A.R.","Waterman, F.M."}','NEUTRONS FROM NUCLEAR CAPTURE OF NEGATIVE PIONS','Phys.Rev.','10.1103/PhysRevC.25.3050','3050-3067','C25',1982,'We measured the energy spectra and yields of neutrons above 1.2 MeV from nuclear capture of negative pions slowing down and stopping in C, N, O, Al, Cu, Ta, and Pb targets. Each neutron spectrum was decomposed into an evaporation portion and a direct portion. The number of direct neutrons per stopped pion is substantially constant for all the targets with a mean value of 1.74±0.28, whereas the yield of evaporation neutrons increases by an order of magnitude from about 0.7 for carbon to about 7 for lead. The total kinetic energy carried away by the neutrons is substantially constant also for all the targets with a mean value of 76.7±2.0 MeV. The kinetic energy carried away by evaporation neutrons increases with mass number with an associated decrease in the kinetic energy carried away by direct neutrons. We obtained the nuclear temperature (in MeV) of the first residual nucleus formed in the evaporation process for each target: Pb (1.4), Ta (1.4), Cu (1.9), A1 (2.3), O (2.2), N (2.2), and C (2.1). The spectrum from a light target displays a shoulder in the region around 60 MeV which is consistent with a two-nucleon absorption mechanism. The direct neutron spectra are characterized by a rising yield as the neutron energy decreases in the region from 30 MeV down to about 10 MeV. The data are compared with previous measurements and with the predictions of intranuclear cascade and preequilibrium calculations. UCLEAR REACTIONS C, N, O, Al, Cu, Ta, Pb (π−,xn), Eπ−≈0 MeV. Measured differential energy spectra and yields. Deduced nuclear temperature. Compared results to intranuclear cascade and preequilibrium model calculations.','{"PI- LIGHT NUCLEUS: NUCLEAR REACTION","PI- LIGHT NUCLEUS: INCLUSIVE REACTION","PI- NUCLEUS: NUCLEAR REACTION","PI- NUCLEUS: INCLUSIVE REACTION","CARBON","NITROGEN","OXYGEN","ALUMINUM","COPPER","TANTALUM","LEAD","PI-: CAPTURE","CAPTURE: PI-","N: HADROPRODUCTION","HADROPRODUCTION: N","N: MULTIPLE PRODUCTION","MULTIPLE PRODUCTION: N","ENERGY SPECTRUM: (N)","(N): ENERGY SPECTRUM","PRODUCTION: YIELD","YIELD: PRODUCTION","DEPENDENCE: MASS NUMBER","MASS NUMBER: DEPENDENCE","ENERGY DEPENDENCE","NUCLEAR PROPERTIES: TEMPERATURE","NUCLEUS: CASCADE","TABLES","COUNTERS AND DETECTORS: EXPERIMENTAL RESULTS","PI- LIGHT NUCLEUS --> (N)N ANYTHING","PI- NUCLEUS --> (N)N ANYTHING","0.125 GEV"}','https://inspirehep.net/record/182157');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(194031,'{"Bayukov, Yu.D.","Gavrilov, V.B.","Goryainov, N.A.","Grishuk, Yu.G.","Gushchin, O.B.","Degtyarenko, P.V.","Leksin, G.A.","Fedorov, V.B.","Shvartsman, B.B.","Shevchenko, S.V.","Shuvalov, S.M."}','PRODUCTION CROSS-SECTIONS OF PROTONS WITH ENERGIES OF 70-MeV TO 230-MeV IN REACTIONS p A ---> p x AT 1.9-GeV/c, pi+ A ---> p x AT 1-GeV/c TO 6-GeV/c AND pi- A ---> p x AT 1.4-GeV/c AND 5-GeV/c','','','','',1983,'','{}','https://inspirehep.net/record/194031');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(197272,'{"Shibata, T.A.","Nakai, K.","Enyo, H.","Sasaki, S.","Sekimoto, M.","Arai, I.","Nakayama, K.","Ichimaru, K.","Nakamura-Yokota, H.","Chiba, R."}','PARTICLE PRODUCTION IN THE TARGET RAPIDITY REGION FROM HADRON NUCLEUS REACTIONS AT SEVERAL GEV','Nucl.Phys.','10.1016/0375-9474(83)90244-0','525-558','A408',1983,'Highly inelastic processes in hadron-nucleus reactions at several GeV have been studied by measuring multi-particle emission in the target-rapidity region. Events with no leading particle(s) but with high multiplicities were observed up to 4 GeV. Proton spectra from such events were well reproduced with a single-moving-source model, which implied possible formation of a local source. The number of nucleons involved in the source was estimated to be (3–5)A 1 3 from the source velocity and the multiplicity of emitted protons. In those processes the incident energy flux seemed to be deposited totally or mostly (>62;75%) in the target nucleus to form the local source. The cross sections for the process were about 30% of the geometrical cross sections, with little dependence on incident energies up to 4 GeV and no dependence on projectiles (pions or protons). The E 0 parameter in the invariant-cross-section formula E d 3 σ /d p 3 = A exp (− E / E 0 ) for protons from the source increases with incident energy from 1 to 4 GeV/ c , but seems to saturate above 10 GeV at a value E 0 = 60–70 MeV. Three components in the emitted nucleon spectra were observed which would correspond to three stages of the reaction process: primary, pre-equilibrium and equilibrium.','{"p nucleus: inelastic scattering","inelastic scattering: p nucleus","pi+ nucleus: inelastic scattering","inelastic scattering: pi+ nucleus","hadron nucleus: inclusive reaction","aluminum","copper","tin","lead","multiple production","nucleon: hadroproduction","hadroproduction: nucleon","charged particle: multiplicity","multiplicity: spectrum","spectrum: multiplicity","differential cross section: energy dependence","energy dependence: differential cross section","differential cross section: angular dependence","angular dependence: differential cross section","energy spectrum: (nucleon)","(nucleon): energy spectrum","yield: (pi)","correlation: (2p)","forward scattering: correlation","nuclear reaction: space-time","experimental results","KEK PS","tables","1-4: 12 GeV"}','https://inspirehep.net/record/197272');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(201929,'{"Mcgill, J.A.","Hoffmann, G.W.","Barlett, M.L.","Fergerson, R.W.","Milner, E.C.","Chrien, R.E.","Sutter, R.J.","Kozlowski, T.","Stearns, R.L."}','PROTON + NUCLEUS INCLUSIVE (P, P'') SCATTERING AT 800-MeV','Phys.Rev.','10.1103/PhysRevC.29.204','204-208','C29',1984,'800 MeV inclusive (p,p′) data, spanning the momentum range from that of the quasi-elastic peak to outgoing proton momenta of about 350MeVc, are reported for the target nuclei H1,H2, C12, Ca40, and Pb208. The data are consistent with simple ideas concerning quasi-free π production, and an angle and momentum integration of d2σdΩdp across the entire quasi-free region accounts for most of the p + nucleus total reaction cross section. NUCLEAR REACTIONS (p,p′) on H1, H2, C12, Ca40, Pb208; Ep=800 MeV, θL=5°−30°; measured d2σdΩdp for quasi-elastic and quasi-free pion production.]','{"P P: INCLUSIVE REACTION","P DEUTERON: INCLUSIVE REACTION","P LIGHT NUCLEUS: INCLUSIVE REACTION","CARBON","P NUCLEUS: INCLUSIVE REACTION","CALCIUM","LEAD","PI: HADROPRODUCTION","HADROPRODUCTION: PI","DIFFERENTIAL CROSS SECTION: ANGULAR DEPENDENCE","ANGULAR DEPENDENCE: DIFFERENTIAL CROSS SECTION","MOMENTUM SPECTRUM: (P)","MODEL: ISOBAR","TOTAL CROSS SECTION","MAGNETIC SPECTROMETER: HRS","EXPERIMENTAL RESULTS","LAMPF LINAC","0.8 GEV: 5-30 DEGREES"}','https://inspirehep.net/record/201929');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(216708,'{"Enyo, H.","Sasaki, S.","Nagae, T.","Tokushuku, K.","Sano, M.","Sekimoto, M.","Chiba, J.","Ichimaru, K.","Mori, T.","Katsumi, T.","Yokota, H.","Chiba, R.","Nakai, K."}','Analyses of Particle Production in Hadron - Nucleus Reactions at Several {GeV} With a Two Moving Source Model','Phys.Lett.','10.1016/0370-2693(85)90107-8','1','B159',1985,'Spectra of protons and pions emitted in the target-rapidity region in hadron-nucleus reactions at 3 and 4 GeV c were observed in wide momentum- and angular-acceptance measurements. Fitting to the data with the two-moving-source model well reproduced experimental proton spectra with the slope parameters of 50–60 MeV for one component and 110–125 MeV for another. The slope parameters for pions were 120–165 MeV, larger than those for protons. Energy depositions of GeV particles were estimated by integrating energies carried away through the particle production','{"hadron nucleus: nuclear reaction","p nucleus: inclusive reaction","pi nucleus: inclusive reaction","aluminum","lead","p: hadroproduction","hadroproduction: p","pi-: hadroproduction","hadroproduction: pi-","pi+: hadroproduction","hadroproduction: pi+","energy spectrum: (hadron)","(hadron): energy spectrum","multiplicity: spectrum","spectrum: multiplicity","model: nuclear reaction","spectrometer: experimental results","KEK PS","3: 4 GeV/c"}','https://inspirehep.net/record/216708');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(227712,'{"Bayukov, Yu.D.","Gavrilov, V.B.","Goryainov, N.A.","Grishuk, Yu.G.","Gushchin, O.B.","Degtyarenko, P.V.","Leksin, G.A.","Fedorov, V.B.","Shvartsman, B.B.","Shevchenko, S.V.","Shuvalov, S.M."}','ANGULAR DEPENDENCES OF INCLUSIVE NUCLEON PRODUCTION IN NUCLEAR REACTIONS AT HIGH-ENERGIES AND SEPARATION OF CONTRIBUTIONS FROM QUASIFREE AND DEEP INELASTIC NUCLEAR PROCESSES','Sov.J.Nucl.Phys.','','116-121','42',1985,'','{"P NUCLEUS: INCLUSIVE REACTION","P LIGHT NUCLEUS: INCLUSIVE REACTION","PI+ NUCLEUS: INCLUSIVE REACTION","PI+ LIGHT NUCLEUS: INCLUSIVE REACTION","PI- NUCLEUS: INCLUSIVE REACTION","PI- LIGHT NUCLEUS: INCLUSIVE REACTION","CARBON","COPPER","LEAD","URANIUM","P: EMISSION","EMISSION: P","ANGULAR DEPENDENCE","P: YIELD","DEEP INELASTIC SCATTERING","EXPERIMENTAL RESULTS","1-9 GEV/C (P): 1-6 GEV/C (PI+): 1.4-5.0 GEV/C (PI-)"}','https://inspirehep.net/record/227712');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(230030,'{"Abramov, V.V.","Baldin, B.Yu.","Buzulutskov, A.F.","Glebov, V.Yu.","Dyshkant, A.S.","Efimov, A.O.","Evdokimov, V.N.","Krinitsyn, A.N.","Kryshkin, V.I.","Mutafian, M.I.","Podstavkov, V.M.","Ronzhin, A.I.","Sulyaev, R.M.","Turchanovich, L.K.","Vasilchenko, V.G.","Volkov, A.A.","Zmushko, V.V."}','High $p_T$ Deuteron and Anti-deuteron Production in $p p$ and $p$ a Collisions at 70-{GeV}','Sov.J.Nucl.Phys.','','845','45',1987,'','{"P P: INCLUSIVE REACTION","P LIGHT NUCLEUS: INCLUSIVE REACTION","P LIGHT NUCLEUS: NUCLEAR REACTION","BERYLLIUM","P NUCLEUS: INCLUSIVE REACTION","P NUCLEUS: NUCLEAR REACTION","COPPER","LEAD","DEUTERON: HADROPRODUCTION","HADROPRODUCTION: DEUTERON","DEUTERON: ANTINUCLEUS","TRITIUM: HADROPRODUCTION","HADROPRODUCTION: TRITIUM","P: HADROPRODUCTION","HADROPRODUCTION: P","DIFFERENTIAL CROSS SECTION: TRANSVERSE MOMENTUM","TRANSVERSE MOMENTUM: DIFFERENTIAL CROSS SECTION","CROSS SECTION: RATIO","DEPENDENCE: MASS NUMBER","MASS NUMBER: DEPENDENCE","YIELD: (TRITIUM)","MODEL: FUSION","MAGNETIC SPECTROMETER: EXPERIMENTAL RESULTS","SERPUKHOV PS","70 GEV/C"}','https://inspirehep.net/record/230030');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(232412,'{"Mcgaughey, P.L.","Bol, K.D.","Clover, M.R.","De Vries, R.M.","Digiacomo, N.J.","Kapustinsky, J.S.","Sondheim, W.E.","Smith, G.R.","Sunier, J.W.","Yariv, Y.","Buenerd, M.","Chauvin, J.","Lebrun, D.","Martin, P.","Dousse, J.C."}','Dynamics of Low-energy Anti-proton Annihilation in Nuclei as Inferred From Inclusive Proton and Pion Measurements','Phys.Rev.Lett.','10.1103/PhysRevLett.56.2156','2156-2159','56',1986,'The cross sections for the production of charged pions and protons from the annihilation of 608-MeV/c antiprotons on C12, Y89, and U238 are presented. The sources of pion and proton emission are inferred from the rapidity distributions of the data. The results are compared to and seen to be in good agreement with intranuclear-cascade calculations.','{"ANTI-P NUCLEUS: INCLUSIVE REACTION","ANTI-P LIGHT NUCLEUS: INCLUSIVE REACTION","ANTI-P NUCLEUS: NUCLEAR REACTION","ANTI-P LIGHT NUCLEUS: NUCLEAR REACTION","ANTI-P: ANNIHILATION","ANNIHILATION: ANTI-P","CARBON","YTTRIUM","URANIUM","PI+: HADROPRODUCTION","HADROPRODUCTION: PI+","P: HADROPRODUCTION","HADROPRODUCTION: P","DIFFERENTIAL CROSS SECTION: SLOPE","SLOPE: DIFFERENTIAL CROSS SECTION","MOMENTUM SPECTRUM","MODEL: INTRANUCLEAR CASCADE","DIFFERENTIAL CROSS SECTION: RAPIDITY","RAPIDITY: DIFFERENTIAL CROSS SECTION","MAGNETIC SPECTROMETER: EXPERIMENTAL RESULTS","CERN LEAR","0.608 GEV/C"}','https://inspirehep.net/record/232412');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(285052,'{"Franz, J.","Koncz, P.","Rossle, E.","Sauerwein, C.","Schmitt, H.","Schmoll, K.","Ero, J.","Fodor, Z.","Kecskemeti, J.","Kovacs, Z.","Seres, Z."}','Neutron Induced Production of Protons, Deuterons and Tritons on Copper and Bismuth','Nucl.Phys.','10.1016/0375-9474(90)90360-X','774-802','A510',1990,'Inclusive cross sections for production of protons, deuterons and tritons by neutrons in the energy range of 300–580 MeV on copper and bismuth have been measured at five angles between 54° and 164°. The systematic dependence of the invariant cross sections on incident energy and emission angle are evaluated. For the study of the mass-number dependence earlier data on carbon are included. The results are discussed on the basis of different models, like quasi-two-body sealing or moving-source model.','{"n nucleus: nuclear reaction","n nucleus: inclusive reaction","copper","bismuth","p: hadroproduction","hadroproduction: p","deuteron: hadroproduction","hadroproduction: deuteron","tritium: hadroproduction","hadroproduction: tritium","yield: ratio","differential cross section: energy dependence","energy dependence: differential cross section","differential cross section: slope","slope: differential cross section","dependence: mass number","mass number: dependence","model: nuclear reaction","scaling","experimental results","PSI Cycl","0.30-0.58 GeV: 54-164 degrees"}','https://inspirehep.net/record/285052');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(287791,'{"Buchle, R.","Franz, J.","Koncz, P.","Marx, M.","Rossle, E.","Sauerwein, C.","Schmitt, H.","Schmoll, K.","Ero, J.","Fodor, Z.","Kecskemeti, J.","Kovacs, Z.","Seres, Z."}','Neutron Induced Pion Production on Carbon, Copper and Bismuth at Intermediate-energies','Nucl.Phys.','10.1016/0375-9474(90)90272-N','541-570','A515',1990,'Inclusive production cross sections of charged pions on carbon, copper and bismuth by neutrons in the energy range of 300–580 MeV have been measured from 54° to 164°. The invariant cross sections can be expressed by Full-size image (<1 K) for the high-energy part of the pion spectra. The slope parameter exhibits a systematic variation with neutron energy and emission angle for the three targets. The dependence of the pion production on the target mass number varies strongly with pion energy and emission angle. The production cross sections are compared with the model of quasi-two-body scaling, the moving-source model and with intranuclear cascade calculations.','{"n light nucleus: inclusive reaction","carbon","n nucleus: inclusive reaction","copper","bismuth","pi: hadroproduction","hadroproduction: pi","differential cross section: slope","slope: differential cross section","differential cross section: momentum dependence","momentum dependence: differential cross section","energy dependence","angular dependence","dependence: mass number","mass number: dependence","model: nuclear reaction","model: scaling","intranuclear cascade","experimental results","PSI Cycl","0.30-0.58 GeV: 54-164 degrees"}','https://inspirehep.net/record/287791');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(313140,'{"Ryckbosch, D.","Van Hoorebeke, L.","Vyver, R.Van De","De Smet, F.","Adler, J-O.","Nilsson, D.","Schroder, B.","Zorro, R."}','Determination of the absorption mechanism in photon-induced pre-equilibrium reactions','Phys.Rev.','10.1103/PhysRevC.42.444','444-447','C42',1990,'Spectra of protons and deuterons emitted in the photodisintegration of Al27 and Canat have been measured with the tagged photon technique. The experimental data are compared to the results of calculations using the hybrid model for pre-equilibrium reactions. For the first time it is possible to determine the relevant initial doorway configuration, i.e., a 2p-1h doorway, appropriate for quasideuteron absorption.','{}','https://inspirehep.net/record/313140');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(320447,'{"Gavrishchuk, O.P.","Moroz, N.S.","Peresedov, V.P.","Zolin, L.S.","Belyaev, I.M.","Lobanov, V.V."}','Charged pion backward production in 15-GeV - 65-GeV proton nucleus collisions','Nucl.Phys.','10.1016/0375-9474(91)90037-7','589-596','A523',1991,'The differential cross sections of π − and π + meson production at a laboratory angle of 159° in collisions of 15–65 GeV protons with Be, C, Al, Ti, Mo and W targets are measured. The data are presented in the tables for Lorentz-invariant cross sections over the momentum range of pions from 0.25 to 0.95 GeV/ c . The slopes (“temperatures”) of a cumulative part of the pion spectra (the pion kinetic energy is >0.35 GeV) increase by 15–20% with changing A from 9 up to 184. Some discrepancy in the E -dependence of the temperature of the cumulative pion spectra is observed in the high-energy region studied, namely the temperature at 15–65 GeV, taking its slow rise over this range into account, contradicts that at 400 GeV.','{"p nucleus: nuclear reaction","p nucleus: inclusive reaction","beryllium","carbon","aluminum","titanium","molybdenum","tungsten","pi+: hadroproduction","hadroproduction: pi+","pi-: hadroproduction","hadroproduction: pi-","differential cross section: energy dependence","energy dependence: differential cross section","magnetic spectrometer: experimental results","Serpukhov PS","p nucleus --> pi- anything","p nucleus --> pi+ anything","15-65 GeV: 159 degrees"}','https://inspirehep.net/record/320447');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(323473,'{"Akiba, Y.","Beavis, D.","Bloomer, M.A.","Bond, P.D.","Chasman, C.","Chen, Z.","Chu, Y.Y.","Cole, B.A.","Costales, J.B.","Crawford, H.J.","Cumming, J.B.","Debbe, R.","Engelage, J.","Fung, S.Y.","Gushue, S.","Hamagaki, H.","Hansen, O.","Hayano, R.S.","Hayashi, S.","Homma, S.","Huang, H.Z.","Ikeda, Y.","Juricic, I.","Kang, J.","Katcoff, S.","Kaufman, S.","Kimura, K.","Kitamura, K.","Kurita, K.","Ledoux, R.J.","Levine, M.J.","Miake, Y.","Morse, R.J.","Moskowitz, B.","Nagamiya, S.","Olness, J.","Parsons, C.G.","Remsberg, L.P.","Sakurai, H.","Sarabura, M.","Stankus, P.","Steadman, S.G.","Stephans, G.S.F.","Sugitate, T.","Tannenbaum, M.J.","van Dijk, J.H.","Videbaek, F.","Vient, M.","Vincent, P.","Vutsadakis, V.","Wegner, H.E.","Woodruff, D.S.","Wu, Y.D.","Zajc, W.","Abbott, T."}','Measurement of particle production in proton induced reactions at 14.6-GeV/c','Phys.Rev.','10.1103/PhysRevD.45.3906','3906-3920','D45',1992,'Particle production in proton-induced reactions at 14.6 GeV/c on Be, Al, Cu, and Au targets has been systematically studied using the E-802 spectrometer at the BNL-Alternating Gradient Synchrotron. Particles are measured in the angular range from 5° to 58° and identified up to momenta of 5, 3.5, and 8 GeV/c for pions, kaons, and protons, respectively. Mechanisms for particle production are discussed in comparison with heavy-ion-induced reactions at the same incident energy per nucleon.','{"p nucleus: inclusive reaction","nucleus nucleus: inclusive reaction","beryllium","aluminum","copper","gold","scattering: heavy ion","heavy ion: scattering","hadron: hadroproduction","hadroproduction: hadron","yield: (p K+ pi+)","dependence: mass number","mass number: dependence","rapidity spectrum","differential cross section","magnetic spectrometer: experimental results","Brookhaven PS","14.6 GeV/c: 5-58 degrees"}','https://inspirehep.net/record/323473');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(336815,'{"Dover, Carl B.","Gutsche, T.","Maruyama, M.","Faessler, Amand"}','The Physics of nucleon - anti-nucleon annihilation','Prog.Part.Nucl.Phys.','10.1016/0146-6410(92)90004-L','87-174','29',1992,'The nucleon-antinucleon (NN̄) annihilation process provides a fertile testing ground for microscopic hadron exchange and/or quark models. We review a variety of such approaches here, including models which treat the underlying quark-gluon degrees of freedom explicitly. A first principles calculation of NN̄ annihilation in the framework of non-perturbative quantum chromodynamics (QCD) is still beyond our reach, so we adopt a more phenomenological approach, in which we try various annihilation topologies combined with different prescriptions for the dependence of the effective quark-antiquark (QQ̄) creation/destruction operator on spin, flavor and color. The NN̄ system offers a rich ensemble of annihilation channels, whose relative branching ratios BR provide strong constraints on dynamical models. Recent experiments at LEAR show that values of BR display a significant dependence on L , the NN̄ relative orbital angular momentum, spin S and isospin I . These dynamical selection rules (DSR), i.e. the suppression of transitions allowed à priori by conservation of the quantum numbers { J , π , C , G }, provide key signatures of the annihilation mechanism, and suggest dynamical content beyond simple statistical or SU(2)/SU(3) flavor symmetry models. We investigate to what extent the observed DSR enable us to unravel the quark dynamics of the annihilation process.','{"review","nucleon antinucleon: annihilation","annihilation: nucleon antinucleon","quantum chromodynamics: nonperturbative","model: annihilation","annihilation: model","cross section: momentum dependence","nucleon antinucleon: branching ratio","quark gluon: interaction","interaction: quark gluon","potential: optical","meson: exchange","exchange: meson","potential: absorption","absorption: potential","baryon: exchange","exchange: baryon","quark: selection rule","numerical calculations: interpretation of experiments","bibliography"}','https://inspirehep.net/record/336815');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(365223,'{"Wise, J.E.","Braunstein, M.R.","Hoeibraten, S.","Kohler, M.D.","Kriss, B.J.","Ouyang, J.","Peterson, R.J.","McGill, J.A.","Morris, C.L.","Seestrom, S.J.","Whitton, R.M.","Zumbro, J.D.","Edwards, C.M.","Williams, A.L."}','Quasifree pion scattering at 500-MeV','Phys.Rev.','10.1103/PhysRevC.48.1840','1840-1848','C48',1993,'Non-charge-exchange inclusive cross sections have been measured at 500 MeV incident pion energy at quasifree scattering kinematics for both positive and negative pion charges. Peaks identified with quasifree knockout are seen at momentum transfers from 314 to 724 MeV/c. The widths and sizes of the peaks seen are consistent with the knockout of single nucleons from the nuclear surface. The data are consistent with no softening of the pion quasifree response at high momentum transfer, in contrast to the result seen in 500-MeV pion charge exchange data.','{"pi+ nucleus: inelastic scattering","inelastic scattering: pi+ nucleus","pi- nucleus: inelastic scattering","inelastic scattering: pi- nucleus","carbon","calcium","zirconium","lead","Fermi gas","nucleon: emission","emission: nucleon","charge exchange","differential cross section: energy loss","energy loss: differential cross section","differential cross section: angular dependence","angular dependence: differential cross section","effect: shadowing","dependence: mass number","mass number: dependence","dependence: momentum transfer","magnetic spectrometer: experimental results","LAMPF Linac","0.5"}','https://inspirehep.net/record/365223');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(378192,'{"Whitmore, J.J.","Persi, F.","Toothacker, W.S.","Elcombe, P.A.","Hill, J.C.","Neale, W.W.","Walker, W.D.","Kowald, W.","Lucas, P.","Voyvodic, L.","Ammar, R.","Coppage, D.","Davis, R.","Gress, J.","Kanekal, S.","Kwak, N.","Bishop, J.M.","Biswas, N.N.","Cason, N.M.","Kenney, V.P.","Mattingly, M.C.K.","Ruchti, R.C.","Shephard, W.D."}','Inclusive charged pion production in hadron nucleus interactions at 100-GeV/c and 320-GeV/c','Z.Phys.','10.1007/BF01560238','199-227','C62',1994,'An experiment has been performed with the Fermilab 30-inch bubble chamber and Downstream Particle Identifier to study inclusive charged pion production in the high energy interactions of π±,K+,p and\(\bar p\) with thin foils of magnesium, silver and gold. The laboratory rapidity and transverse momentum distributions are presented separately for π+ and π− production. Comparisons are made with data from hadron-proton interactions and theA dependence of the cross sections in the different kinematic regions is discussed. We investigate the dependence of the cross sections on the number of observed protons ejected from the nucleus. By using our π−A data from two different beam energies, we study the energy dependence of these spectra. Comparisons are made with the VENUS string model Monte Carlo.','{"hadron nucleus: inclusive reaction","pi","K","p","anti-p","magnesium","silver","gold","pi: hadroproduction","hadroproduction: pi","p: hadroproduction","hadroproduction: p","rapidity spectrum","transverse momentum: spectrum","spectrum: transverse momentum","dependence: mass number","mass number: dependence","string model","bubble chamber: experimental results","Batavia PS","hadron nucleus --> pi anything","100: 320 G"}','https://inspirehep.net/record/378192');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(407319,'{"Michel, R.","Gloris, M.","Lange, H.J.","Leya, I.","Luepke, M.","Herpers, U.","Dittrich-Hannen, B.","Roesel, R.","Schiekel, T.","Filges, D.","Dragovitsch, P.","Suter, M.","Hofmann, H.J.","Woelfli, W.","Baur, H.","Wieler, R.","Kubik, P.W."}','Nuclide production by proton induced reactions on elements (6 <= Z <= 29) in the energy range from 800-MeV to 2600-MeV','Nucl.Instrum.Meth.','10.1016/0168-583X(95)00566-8','183-222','B103',1995,'In the course of a systematic investigation of proton-induced reactions for p energies between 800 and 2600 MeV, the target elements O, Mg, Al, Si, Ca, Ti, V, Mn, Fe, Co, Ni, Cu, Zr, Rh, Nb, Ba and Au were irradiated with 800 MeV protons at LAMPF/Los Alamos National Laboratory, and with 1200, 1600 and 2600 MeV protons at Laboratoire National Saturne/Saclay. The 1600 MeV irradiations covered in addition the target elements C, N, Rb, Sr, Y. The study was designed to measure production cross sections of radionuclides by γ-spectrometry and by accelerator mass spectrometry and of stable rare gas isotopes by conventional mass spectrometry. A detailed analysis of secondary particle fields was performed for targets of different thicknesses. Corrections for interferences by secondaries were made on the basis of secondary particle spectra as calculated by the code HET in the form of the HERMES code system and experimental and theoretical excitation functions of p- and n-induced reactions. Here, about 700 cross sections for the production of radionuclides from target elements Z ≤ 29 (Cu) by more than 200 reactions are presented. In addition, cross sections for the production of stable He and Ne isotopes from iron at a proton energy of 600 MeV are given. Together with earlier work of our group, there now exists a consistent set of excitation functions from threshold energies up to 2600 MeV. A comparison of the new data with earlier measurements from other authors exhibited a considerable lack of reliability for many of the earlier data. On the basis of the new data, the quality of existing semiempirical formulas for the calculation of spallation cross sections is discussed. In a more physical approach, the production of residual nuclides is calculated in the framework of an INC/E model using Monte Carlo techniques for energies between 100 MeV and 5 GeV and compared with the experimental results.','{"p nucleus: nuclear reaction","fission","nuclide: hadroproduction","hadroproduction: nuclide","cross section","radioactivity","p: emission","emission: p","n: emission","emission: n","experimental results","LAMPF Linac","Saclay PS","bibliography","0.8-2.6 GeV/c"}','https://inspirehep.net/record/407319');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(414336,'{"Kormanyos, C.M.","Peterson, R.J.","Shepard, J.R.","Wise, J.E.","Bart, S.","Chrien, R.E.","Lee, L.","Clausen, B.L.","Piekarewicz, J.","Barakat, M.B.","Hungerford, E.V.","Michael, R.A.","Hicks, K.H.","Kishimoto, T."}','Quasielastic K+ scattering','Phys.Rev.','10.1103/PhysRevC.51.669','669-679','C51',1995,'K+-nucleus double-differential cross sections in the quasielastic region have been measured using a kaon beam with an incident laboratory momentum of 705 MeV/c. Data are presented for D, C, Ca, and Pb at fixed three-momentum transfers of 290, 390, and 480 MeV/c. Measurements of the effective number of nucleons are compared with calculations predicted from a semiclassical attenuation model based on the eikonal approximation, and the long nuclear mean free path expected for positive kaons is found, even for lead. An exclusive (K,K’p) spectrum for C at 480 MeV/c is shown. The characteristics of the exclusive data suggest that protons with zero orbital angular momentum have been detected in coincidence. The inclusive data are described well by local density random phase approximation calculations which include isoscalar N-N correlations in the σ-ω model.','{}','https://inspirehep.net/record/414336');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(431203,'{"Saunders, A.","Hoeibraten, S.","Kraushaar, J.J.","Kriss, B.J.","Peterson, R.J.","Ristinen, R.A.","Brack, J.T.","Hofman, G.","Gibson, E.F.","Morris, C.L."}','Reaction and total cross-sections for low-energy pi+ and pi- on isospin zero nuclei','Phys.Rev.','10.1103/PhysRevC.53.1745','1745-1752','C53',1996,'Reaction and total cross sections for π+ and π− on targets of H2, Li6, C, Al, Si, S, and Ca have been measured for beam energies from 42 to 65 MeV. The cross sections are proportional to the target mass at 50 MeV, consistent with transparency to these projectiles. The cross sections are compared to theoretical calculations. © 1996 The American Physical Society.','{"pi+ nucleus: nuclear reaction","pi- nucleus: nuclear reaction","deuterium","lithium","carbon","aluminum","silicon","sulfur","calcium","total cross section: energy dependence","energy dependence: total cross section","dependence: mass number","mass number: dependence","experimental results","TRIUMF Cycl","0.042-0.065 GeV"}','https://inspirehep.net/record/431203');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(447317,'{"Amsler, Claude"}','Proton - anti-proton annihilation and meson spectroscopy with the crystal barrel','Rev.Mod.Phys.','10.1103/RevModPhys.70.1293','1293-1340','70',1998,'This report reviews the achievements of the Crystal Barrel experiment at the Low Energy Antiproton Ring (LEAR) at CERN. During seven years of operation Crystal Barrel has collected very large statistical samples in pbarp annihilation, especially at rest and with emphasis on final states with high neutral multiplicity. The measured rates for annihilation into various two-body channels and for electromagnetic processes have been used to test simple models for the annihilation mechanism based on the quark internal structure of hadrons. From three-body annihilations three scalar mesons, a0(1450), f0(1370) and f0(1500) have been established in various decay modes. One of them, f0(1500), may be identified with the expected ground state scalar glueball.','{"review: experimental results","anti-p p: annihilation","at rest","partial wave","meson: hadroproduction","meson: multiple production","meson: hadron spectroscopy","photon: associated production","meson: hadronic decay","meson: radiative decay","gauge boson: search for","spin: parity","meson: multiplicity","postulated particle: a0(1450)","f0(1370)","f0(1500)","postulated particle: f2(1565)","f1(1420)","glueball","four-pi-detector","CERN LEAR","bibliography","0 GeV"}','https://inspirehep.net/record/447317');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(457688,'{"Friedman, E.","Gal, A.","Weiss, R.","Aclander, J.","Alster, J.","Mardor, I.","Mardor, Y.","May-Tal Beck, S.","Piasetzky, E.","Yavin, A.I.","Bart, S.","Chrien, R.E.","Pile, P.H.","Sawafta, R.","Sutter, R.J.","Barakat, M.","Johnston, K.","Krauss, R.A.","Seyfarth, H.","Stearns, R.L."}','K+ nucleus reaction and total cross-sections: New analysis of transmission experiments','Phys.Rev.','10.1103/PhysRevC.55.1304','1304-1311','C55',1997,'The attenuation cross sections measured in transmission experiments at the alternating-gradient synchrotron for K+ on 6Li, C, Si, and Ca at pL = 488, 531, 656, and 714 MeV/c are reanalyzed in order to derive total (σT) and reaction (σR) cross sections. The effect of plural (Molière) scattering is properly accounted for, leading to revised values of σT. We demonstrate the model dependence of these values, primarily due to the choice of K+ nuclear optical potential used to generate the necessary Coulomb-nuclear and nuclear elastic corrections. Values of σR are also derived, for the first time, from the same data and exhibit a remarkable degree of model independence. The derived values of σT and σR exceed those calculated by the first-order tρ optical potential for C, Si, and Ca, but not for 6Li, particularly at 656 and 714 MeV/c where the excess is 10–25%. Relative to 6Li, this excess is found to be nearly energy independent and its magnitude of 15–25% is not reproduced by any nuclear medium effect studied so far.','{"K+ nucleus: nuclear reaction","K+ nucleus: elastic scattering","elastic scattering: K+ nucleus","lithium","carbon","silicon","calcium","total cross section: energy dependence","energy dependence: total cross section","potential: optical","effect: multiple scattering","multiple scattering: effect","Klein-Gordon equation","K nucleon: interaction","interaction: K nucleon","nuclear matter: effect","interpretation of experiments","0.488-0.714 GeV"}','https://inspirehep.net/record/457688');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(530755,'{"Gelderloos, C. J.","Brack, J. T.","Holcomb, M. D.","Keilman, M. V.","Mercer, D. J.","Peterson, R. J.","Ristinen, R. A.","Saunders, A."}','Reaction and total cross-sections for 400-MeV 500-MeV pi- on nuclei','Phys.Rev.','10.1103/PhysRevC.62.024612','024612','C62',2000,'Attenuation measurements of reaction and total cross sections have been made for π− beams at 410, 464, and 492 MeV on targets of CD2, 6Li, C, Al, S, Ca, Cu, Zr, Sn, and Pb. These results are assisted by and compared to predictions from a recent eikonal optical model. Calculations with this model, which does not include pion absorption, agree with recent elastic scattering data, but are significantly below our measured reaction and total cross sections.','{"pi- nucleus: nuclear reaction","pi- nucleus: elastic scattering","pi-: absorption","cross section: angular dependence","energy dependence","dependence: mass number","approximation: eikonal","model: optical","experimental results","LAMPF Linac","0.4-0.5 GeV"}','https://inspirehep.net/record/530755');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(532743,'{"von Egidy, T.","Figuera, P.","Galin, J.","Goldenbaum, F.","Golubeva, E.S.","Hasinoff, M.","Hilscher, D.","Ilinov, A.S.","Jahnke, U.","Krause, M.","Kurcewicz, W.","Ledoux, X.","Lott, B.","Maier, L.","Manrique de Lara, M.","Pausch, G.","Pienkowski, L.","Quednau, B.","Schott, W.","Schroder, W.","Toke, J."}','Neutrons produced by 1.22-GeV anti-proton interactions with nuclei','Eur.Phys.J.','10.1007/s100500070106','197-204','A8',2000,'Inclusive neutron energy spectra were measuredb y time of flight using 1.22 GeV antiprotons from LEAR, CERN, as projectiles andtargets from natural Al, Cu, Ag, Ho, Ta, Au, Pb, Bi, U. The sum of two Maxwellian distributions was fitted to the spectra d2 σ/(dΩdE) obtainedat several forward andbac kwardangles yielding neutron multiplicities M i andslop e or temperature parameters T i for the low-energy (evaporative, i = 1) andhigh-energy (pre-equilibrium, i = 2) parts, respectively. M 1 increases with A, proportional to the nuclear volume, and M 2 is growing with A 1/3, proportional to the nuclear radius. The T parameters are nearly independent of A. The results are comparedwith previous multiplicity measurements with a 4π neutron detector, intranuclear cascade calculations and neutron spectra from stoppedan tiproton annihilation on nuclei. With the measuredproton spectra also the ratio of emitted neutrons to protons was determined for Au.','{"anti-p nucleus: nuclear reaction","anti-p nucleus: inclusive reaction","anti-p nucleus: annihilation","n: hadroproduction","n: energy spectrum","differential cross section: angular dependence","differential cross section: energy dependence","n: multiplicity","dependence: mass number","model: multiple production","temperature","intranuclear cascade","fission","p: hadroproduction","yield: (n p)","scintillation counter: experimental results","CERN LEAR","1.22 GeV"}','https://inspirehep.net/record/532743');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(533861,'{"Kurosawa, T.","Nakao, N.","Nakamura, T.","Iwase, H.","Sato, H.","Uwamino, Y.","Fukumura, A."}','Neutron yields from thick C, Al, Cu, and Pb targets bombarded by 400 MeV/nucleon Ar, Fe, Xe and 800 MeV/nucleon Si ions','Phys.Rev.','10.1103/PhysRevC.62.044615','044615','C62',2000,'The angular and energy distributions of neutrons produced by 400 MeV/nucleon Ar, Fe, and Xe, and 800 MeV/nucleon Si ions stopping in thick C, Al, Cu, and Pb targets were measured using the Heavy-Ion Medical Accelerator in Chiba of the National Institute of Radiological Science (NIRS), Japan. The neutron spectra in the forward direction have broad peaks which are located at about 60 to 70 % of the incident particle energy per nucleon due to breakup and knock-on processes, and spread up to almost twice as much as the projectile energy per nucleon. The resultant spectra were integrated over energy to produce neutron angular distributions. The total neutron yields for each system were obtained by integrating over the angular distributions, and we could estimate the total yields using a simple formula. The experimental results are compared with the calculations using the heavy-ion collision Monte Carlo code, and the calculated results rather agree with the measured results. The phenomenological hybrid analysis, based on the moving source model and the Gaussian fitting of the breakup and knock-on processes, could also well represent the measured thick target neutron spectra.','{}','https://inspirehep.net/record/533861');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(561173,'{"Chemakin, I.","Cianciolo, V.","Cole, B.A.","Fernow, Richard C.","Frawley, Anthony D.","Gilkes, M.","Gushue, S.","Hartouni, Edward P.","Hiejima, H.","Justice, M.","Kang, J.H.","Kirk, H.G.","Maeda, N.","McGrath, R.L.","Mioduszewski, S.","Morrison, Douglas R.O.","Moulson, M.","Namboodiri, M.N.","Palmer, R.B.","Rai, G.","Read, K.","Remsberg, L.","Rosati, M.","Shin, Y.","Soltz, R.A.","Sorensen, S.","Thomas, J.H.","Torun, Y.","Winter, D.L.","Yang, X.","Zajc, W.A.","Zhang, Y."}','Inclusive soft pion production from 12.3-Gev/c and 17.5-GeV/c protons on Be, Cu and Au','Phys.Rev.','10.1103/PhysRevC.65.024904','024904','C65',2002,'Differential cross-sections are presented for the inclusive production of charged pions in the momentum range 0.1 to 1.2 GeV/c in interactions of 12.3 and 17.5 GeV/c protons with Be, Cu, and Au targets. The measurements were made by Experiment 910 at the Alternating Gradient Synchrotron in Brookhaven National Laboratory. The cross-sections are presented as a function of pion total momentum and production polar angle $\theta$ with respect to the beam.','{"p nucleus: inclusive reaction","beryllium","copper","gold","pi-: hadroproduction","pi+: hadroproduction","pi: multiple production","differential cross section: momentum dependence","angular distribution","magnetic spectrometer: experimental results","Brookhaven PS","12.3: 17.5 GeV/c"}','https://inspirehep.net/record/561173');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(561787,'{"Fujii, Y.","Hashimoto, O.","Nakagawa, T.","Sato, Y.","Takahashi, T.","Brack, J. T.","Gelderloos, C. J.","Keilman, M. V.","Peterson, R. J.","Itoh, M.","Sakaguchi, H.","Takeda, H.","Aoki, K.","Hotchi, H.","Noumi, H.","Ohta, Y.","Outa, H.","Sekimoto, M.","Youn, M.","Ajimura, S.","Kishimoto, T.","Bhang, H.","Park, H.","Sawafta, R."}','Quasielastic pi- nucleus scattering at 950-MeV/c','Phys.Rev.','10.1103/PhysRevC.64.034608','034608','C64',2001,'Quasielastic scattering cross sections have been measured with a 950MeV/c π− beam on targets of 2H, 6Li, C, Ca, Zr, and 208Pb, over a range of three-momentum transfers from 350 through 650MeV/c. Results for carbon are compared to a finite-nucleus continuum random-phase approximation calculation including distortions. The pion spectra at our lowest range of momentum transfers show less scalar/isoscalar correlation than predicted.','{"pi- nucleus: inelastic scattering","pi- nucleus: nuclear reaction","deuterium","lithium","carbon","calcium","zirconium","lead","pi- nucleon: interaction","spin: isospin","differential cross section: angular dependence","dependence: momentum transfer","energy loss","dependence: mass number","random phase approximation","spectrometer: superconductivity","experimental results","KEK PS","0.950 GeV/c"}','https://inspirehep.net/record/561787');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(599498,'{"Yashima, H.","Uwamino, Y.","Sugita, H.","Nakamura, T.","Ito, S.","Fukumura, A."}','Projectile dependence of radioactive spallation products induced in copper by high-energy heavy ions','Phys.Rev.','10.1103/PhysRevC.66.044607','044607','C66',2002,'Irradiation experiments were performed at the HIMAC (heavy-ion medical accelerator in Chiba) facility, National Institute of Radiological Sciences. The radioactive spallation products in a thick Cu target were obtained for 230 and 100 MeV/nucleon Ne, C, He, and p ions. The gamma-ray spectra from irradiated Cu pieces inserted into the composite Cu target were measured with a high-purity Ge detector. From the gamma-ray spectra, we obtained the variation of radioactive yields with depth in the Cu target and the mass-yield distribution of nuclides produced on the surface. The results show that the cross sections strongly depend on the projectile mass and, consequently, on the mass number difference between Cu and the produced nuclides. The most important feature of the present data is the extraction of the energy dependence of the cross sections. The measured cross sections were fitted to a modified form of Rudstam’s semiempirical formula in order to evaluate the cross sections of unmeasured nuclides. These estimated cross sections were added to the experimental yields for each mass number and the total mass yield and isobaric charge distributions, and the slope of the mass-yield distribution were obtained.','{}','https://inspirehep.net/record/599498');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(658042,'{"Iwamoto, Y.","Shigyo, N.","Satoh, D.","Kunieda, S.","Watanabe, T.","Ishimoto, S.","Tenzou, H.","Maehata, K.","Ishibashi, K.","Nakamoto, T.","Numajiri, M.","Meigo, S.","Takada, H."}','Measurement of pion induced neutron-production double-differential cross sections on Fe and Pb at 870-MeV and 2.1-GeV','Phys.Rev.','10.1103/PhysRevC.70.024602','024602','C70',2004,'Neutron-production double-differential cross sections for 870MeVπ+ and π− and 2.1GeVπ+ mesons incident on iron and lead targets were measured with NE213 liquid scintillators by time-of-flight technique. NE213 liquid scintillators 12.7cm in diameter and 12.7cm thick were placed in directions of 15, 30, 60, 90, 120, and 150°. The typical flight path length was 1.5m. Neutron detection efficiencies were evaluated by calculation results of SCINFUL and CECIL codes. The experimental results were compared with JAERI quantum molecular dynamics code. For the meson incident reactions, adoption of NN in-medium effects was slightly useful for reproducing 870MeVπ+-incident neutron yields at neutron energies of 10–30MeV, as was the case for proton incident reactions. The π− incident reaction generates more neutrons than π+ incidence as the number of nucleons in targets decrease.','{"pi+ nucleus: inclusive reaction","pi- nucleus: inclusive reaction","iron","n: hadroproduction","n: energy spectrum","differential cross section: angular dependence","quantum molecular dynamics","scintillation counter: liquid","fast logic: time-of-flight","experimental results","KEK PS","0.87-2.10 GeV"}','https://inspirehep.net/record/658042');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(686664,'{"Auce, A.","Ingemarsson, A.","Johansson, R.","Lantz, M.","Tibell, G.","Carlson, R.F.","Shachno, M.J.","Cowley, A.A.","Hillhouse, G.C.","Jacobs, N.M.","Stander, J.A.","van Zyl, J.J.","Fortsch, S.V.","Lawrie, J.J.","Smit, F.D.","Steyn, G.F."}','Reaction cross sections for protons on C-12, Ca-40, Zr-90 and Pb-208 at energies between 80-MeV and 180-MeV','Phys.Rev.','10.1103/PhysRevC.71.064606','064606','C71',2005,'Results of reaction cross-section measurements on C12,Ca40,Zr90, and Pb208 at incident proton energies between 80 and 180 MeV and for Ni58 at 81 MeV are presented. The experimental procedure is described, and the results are compared with earlier measurements and predictions using macroscopic and microscopic models.','{}','https://inspirehep.net/record/686664');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(694016,'{"Alt, C.","Anticic, T.","Baatar, B.","Barna, D.","Bartke, J.","Betev, L.","Biakowska, H.","Blume, C.","Boimska, B.","Botje, M.","Bracinik, J.","Buncic, P.","Cerny, V.","Christakoglou, P.","Chvala, O.","Dinkelaker, P.","Dolejsi, J.","Eckardt, V.","Fischer, H.G.","Flierl, D.","Fodor, Z.","Foka, P.","Friese, V.","Gazdzicki, M.","Grebieszkow, K.","Hohne, C.","Kadija, K.","Karev, A.","Kliemant, M.","Kniege, S.","Kolesnikov, V.I.","Kornas, E.","Korus, R.","Kowalski, M.","Kraus, I.","Kreps, M.","van Leeuwen, M.","Lungwitz, B.","Makariev, M.","Malakhov, A.I.","Mateev, M.","Melkumov, G.L.","Mitrovski, M.","Mrowczynski, S.","Palla, G.","Panayotov, D.","Petridis, A.","Renfordt, R.","Rybczynski, M.","Rybicki, A.","Sandoval, A.","Schmitz, N.","Schuster, T.","Seyboth, P.","Sikler, F.","Skrzypczak, E.","Stefanek, G.","Stock, R.","Strobele, H.","Susa, T.","Sziklai, J.","Szymanski, P.","Trubnikov, V.","Varga, D.","Vassiliou, M.","Veres, G.I.","Vesztergombi, G.","Vranic, D.","Wenig, S.","Wetzler, A.","Wlodarczyk, Z.","Yoo, I.K."}','Inclusive production of charged pions in p+p collisions at 158-GeV/c beam momentum','Eur.Phys.J.','10.1140/epjc/s2005-02391-9','343-381','C45',2006,'New results on the production of charged pions in p+p interactions are presented. The data come from a sample of 4.8 million inelastic events obtained with the NA49 detector at the CERN SPS at 158 GeV/c beam momentum. Pions are identified by energy loss measurement in a large TPC tracking system which covers a major fraction of the production phase space. Inclusive invariant cross sections are given on a grid of nearly 300 bins per charge over intervals from 0 to 2 GeV/c in transverse momentum and from 0 to 0.85 in Feynman x. The results are compared to existing data in overlapping energy ranges.','{"p p: inclusive reaction","pi: hadroproduction","yield: (pi+ pi-)","differential cross section: transverse momentum","x-dependence","rapidity spectrum","mass spectrum: transverse","energy dependence","magnetic spectrometer: experimental results","CERN SPS","p p --> pi anything","158 GeV/c"}','https://inspirehep.net/record/694016');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(695147,'{"Catanesi, M.G.","Muciaccia, M.T.","Radicioni, E.","Simone, S.","Edgecock, R.","Ellis, Malcolm","Robbins, S.","Soler, F.J.P.","Gossling, C.","Mass, M.","Bunyatov, S.","Chukanov, A.","Dedovitch, D.","Elagin, A.","Gostkin, M.","Guskov, A.","Khartchenko, D.","Klimov, O.","Krasnoperov, A.","Kustov, D.","Nikolaev, K.","Popov, B.","Serdiouk, V.","Tereshchenko, V.","Zhemchugov, A.","Di Capua, E.","Vidal-Sitjes, G.","Artamonov, A.","Arce, P.","Giani, S.","Gilardoni, Simone S.","Gorbunov, P.","Grant, A.","Grossheim, A.","Gruber, P.","Ivanchenko, V.","Kayis-Topaksu, A.","Linssen, L.","Panman, J.","Papadopoulos, I.","Pasternak, J.","Tcherniaev, E.","Tsukerman, I.","Veenhof, R.","Wiebusch, C.","Zucchelli, P.","Blondel, A.","Borghi, S.","Campanelli, M.","Cervera-Villanueva, A.","Morone, M.C.","Prior, G.","Schroeter, R.","Kato, I.","Nakaya, T.","Nishikawa, K.","Ueda, S.","Ableev, V.","Gastaldi, U.","Mills, Geoffrey B.","Graulich, J.S.","Gregoire, G.","Bonesini, M.","Calvi, M.","De Min, A.","Ferri, F.","Paganoni, M.","Paleari, F.","Kirsanov, M.","Bagulya, A.","Grichine, V.","Polukhina, N.","Palladino, V.","Coney, L.","Schmitz, D.","Barr, G.","De Santo, A.","Pattison, C.","Zuber, K.","Bobisut, F.","Gibin, D.","Guglielmi, A.","Laveder, M.","Menegolli, A.","Mezzetto, M.","Dumarchez, J.","Troquereau, S.","Vannucci, F.","Ammosov, V.","Gapienko, V.","Koreshev, V.","Semak, A.","Sviridov, Yu.","Zaets, V.","Dore, U.","Orestano, D.","Pasquali, M.","Pastore, F.","Tonazzo, A.","Tortora, L.","Booth, C.","Buttar, C.","Hodgson, P.","Howlett, L.","Bogomilov, M.","Chizhov, M.","Kolev, D.","Tsenov, R.","Piperov, Stefan","Temnikov, P.","Apollonio, M.","Chimenti, P.","Giannini, G.","Santin, G.","Hayato, Y.","Ichikawa, A.","Kobayashi, T.","Burguet-Castell, J.","Gomez-Cadenas, J.J.","Novella, P.","Sorel, M.","Tornero, A."}','Measurement of the production cross-section of positive pions in p-Al collisions at 12.9-GeV/c','Nucl.Phys.','10.1016/j.nuclphysb.2005.10.016','1-45','B732',2006,'A precision measurement of the double-differential production cross-section, ${{d^2 \sigma^{\pi^+}}}/{{d p d\Omega}}$, for pions of positive charge, performed in the HARP experiment is presented. The incident particles are protons of 12.9 GeV/c momentum impinging on an aluminium target of 5% nuclear interaction length. The measurement of this cross-section has a direct application to the calculation of the neutrino flux of the K2K experiment. After cuts, 210000 secondary tracks reconstructed in the forward spectrometer were used in this analysis. The results are given for secondaries within a momentum range from 0.75 GeV/c to 6.5 GeV/c, and within an angular range from 30 mrad to 210 mrad. The absolute normalization was performed using prescaled beam triggers counting protons on target. The overall scale of the cross-section is known to better than 6%, while the average point-to-point error is 8.2%.','{"p nucleus: inclusive reaction","aluminum","pi+: hadroproduction","differential cross section: momentum dependence","angular dependence","parametrization","neutrino/mu: secondary beam","neutrino: path length","neutrino: flux","neutrino: energy","numerical calculations: Monte Carlo","KEK PS","magnetic spectrometer: experimental results","CERN Lab","p aluminum --> pi+ anything","12.9 GeV/c"}','https://inspirehep.net/record/695147');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(786183,'{"Radicioni, E.","Edgecock, R.","Ellis, Malcolm","Soler, F.J.P.","Gossling, C.","Bunyatov, S.","Krasnoperov, A.","Popov, B.","Serdiouk, V.","Tereschenko, V.","Di Capua, E.","Vidal-Sitjes, G.","Artamonov, A.","Giani, S.","Gilardoni, S.","Gorbunov, P.","Grant, A.","Grossheim, A.","Ivanchenko, A.","Ivanchenko, V.","Kayis-Topaksu, A.","Panman, J.","Papadopoulos, I.","Tcherniaev, E.","Tsukerman, I.","Veenhof, R.","Wiebusch, C.","Zucchelli, P.","Blondel, A.","Borghi, S.","Morone, M.C.","Prior, G.","Schroeter, R.","Meurer, C.","Gastaldi, U.","Mills, G.B.","Graulich, J.S.","Gregoire, G.","Bonesini, M.","Ferri, F.","Kirsanov, M.","Bagulya, A.","Grichine, V.","Polukhina, N.","Palladino, V.","Coney, L.","Schmitz, D.","Barr, G.","De Santo, A.","Bobisut, F.","Gibin, D.","Guglielmi, A.","Mezzetto, M.","Dumarchez, J.","Dore, U.","Orestano, D.","Pastore, F.","Tonazzo, A.","Tortora, L.","Booth, C.","Howlett, L.","Skoro, G.","Bogomilov, M.","Chizhov, M.","Kolev, D.","Tsenov, R.","Piperov, Stefan","Temnikov, P.","Apollonio, M.","Chimenti, P.","Giannini, G.","Burguet-Castell, J.","Cervera-Villanueva, A.","Gomez-Cadenas, J.J.","Martin-Albo, J.","Novella, P.","Sorel, M.","Tornero, A.","Catanesi, M.G."}','Large-angle production of charged pions with 3-12.9-GeV/c incident protons on nuclear targets','Phys.Rev.','10.1103/PhysRevC.77.055207','055207','C77',2008,'Measurements of the double-differential charged pion production cross-section in the range of momentum 100 MeV/c &lt; p &lt; 800 MeV/c and angle 0.35 &lt; \theta &lt; 2.15 rad in proton-beryllium, proton-carbon, proton-aluminium, proton-copper, proton-tin, proton-tantalum and proton-lead collisions are presented. The data were taken with the large acceptance HARP detector in the T9 beam line of the CERN PS. The pions were produced by proton beams in a momentum range from 3 GeV/c to 12.9 GeV/c hitting a target with a thickness of 5% of a nuclear interaction length.','{}','https://inspirehep.net/record/786183');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(813159,'{"Apollonio, M.","Artamonov, A.","Bagulya, A.","Barr, G.","Blondel, A.","Bobisut, F.","Bogomilov, M.","Bonesini, M.","Booth, C.","Borghi, S.","Bunyatov, S.","Burguet-Castell, J.","Catanesi, M.G.","Cervera-Villanueva, A.","Chimenti, P.","Coney, L.","Di Capua, E.","Dore, U.","Dumarchez, J.","Edgecock, R.","Ellis, M.","Ferri, F.","Gastaldi, U.","Giani, S.","Giannini, G.","Gibin, D.","Gilardoni, S.","Gorbunov, P.","Gossling, C.","Gomez-Cadenas, J.J.","Grant, A.","Graulich, J.S.","Gregoire, G.","Grichine, V.","Grossheim, A.","Guglielmi, A.","Howlett, L.","Ivanchenko, A.","Ivanchenko, V.","Kayis-Topaksu, A.","Kirsanov, M.","Kolev, D.","Krasnoperov, A.","Martin-Albo, J.","Meurer, C.","Mezzetto, M.","Mills, G.B.","Morone, M.C.","Novella, P.","Orestano, D.","Palladino, V.","Panman, J.","Papadopoulos, I.","Pastore, F.","Piperov, Stefan","Polukhina, N.","Popov, B.","Prior, G.","Radicioni, E.","Schmitz, D.","Schroeter, R.","Skoro, G","Sorel, M.","Tcherniaev, E.","Temnikov, P.","Tereschenko, V.","Tonazzo, A.","Tortora, L.","Tsenov, R.","Tsukerman, I.","Vidal-Sitjes, G.","Wiebusch, C.","Zucchelli, P."}','Forward production of charged pions with incident pi+- on nuclear targets measured at the CERN PS','Nucl.Phys.','10.1016/j.nuclphysa.2009.01.080','118-192','A821',2009,'Measurements of the double-differential $\pi^{\pm}$ production cross-section in the range of momentum $0.5 \GeVc \leq p \le 8.0 \GeVc$ and angle $0.025 \rad \leq \theta \le 0.25 \rad$ in interactions of charged pions on beryllium, carbon, aluminium, copper, tin, tantalum and lead are presented. These data represent the first experimental campaign to systematically measure forward pion hadroproduction. The data were taken with the large acceptance HARP detector in the T9 beam line of the CERN PS. Incident particles, impinging on a 5% nuclear interaction length target, were identified by an elaborate system of beam detectors. The tracking and identification of the produced particles was performed using the forward spectrometer of the HARP detector. Results are obtained for the double-differential cross-sections $ {{\mathrm{d}^2 \sigma}}/{{\mathrm{d}p\mathrm{d}\Omega}} $ mainly at four incident pion beam momenta (3 \GeVc, 5 \GeVc, 8 \GeVc and 12 \GeVc). The measurements are compared with the GEANT4 and MARS Monte Carlo simulation','{"pi nucleus: inclusive reaction","pi+: hadroproduction","pi-: hadroproduction","small-angle","differential cross section: momentum dependence","angular dependence","mass number: dependence","forward spectrometer","CERN Lab","numerical calculations: Monte Carlo","3: 5: 8: 12 GeV/c"}','https://inspirehep.net/record/813159');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(825244,'{"Apollonio, M.","Artamonov, A.","Bagulya, A.","Barr, G.","Blondel, A.","Bobisut, F.","Bogomilov, M.","Bonesini, M.","Booth, C.","Borghi, S.","Bunyatov, S.","Burguet-Castell, J.","Catanesi, M.G.","Cervera-Villanueva, A.","Chimenti, P.","Coney, L.","Di Capua, E.","Dore, U.","Dumarchez, J.","Edgecock, R.","Ellis, M.","Ferri, F.","Gastaldi, U.","Giani, S.","Giannini, G.","Gibin, D.","Gilardoni, S.","Gorbunov, P.","Gossling, C.","Gomez-Cadenas, J.J.","Grant, A.","Graulich, J.S.","Gregoire, G.","Grichine, V.","Grossheim, A.","Guglielmi, A.","Howlett, L.","Ivanchenko, A.","Ivanchenko, V.","Kayis-Topaksu, A.","Kirsanov, M.","Kolev, D.","Krasnoperov, A.","Martin-Albo, J.","Meurer, C.","Mezzetto, M.","Mills, G.B.","Morone, M.C.","Novella, P.","Orestano, D.","Palladino, V.","Panman, J.","Papadopoulos, I.","Pastore, F.","Piperov, S.","Polukhina, N.","Popov, B.","Prior, G.","Radicioni, E.","Schmitz, D.","Schroeter, R.","Skoro, G.","Sorel, M.","Tcherniaev, E.","Temnikov, P.","Tereschenko, V.","Tonazzo, A.","Tortora, L.","Tsenov, R.","Tsukerman, I.","Vidal-Sitjes, G.","Wiebusch, C.","Zucchelli, P."}','Large-angle production of charged pions with incident pion beams on nuclear targets','Phys.Rev.','10.1103/PhysRevC.80.065207','065207','C80',2009,'Measurements of the double-differential pi+/- production cross-section in the range of momentum 100 MeV/c <= p <= 800 MeV/c and angle 0.35 rad <= theta <= 2.15 rad using pi+/- beams incident on beryllium, aluminium, carbon, copper, tin, tantalum and lead targets are presented. The data were taken with the large acceptance HARP detector in the T9 beam line of the CERN PS. The secondary pions were produced by beams in a momentum range from 3 GeV/c to 12.9 GeV/c hitting a solid target with a thickness of 5% of a nuclear interaction length. The tracking and identification of the produced particles was performed using a small-radius cylindrical time projection chamber (TPC) placed inside a solenoidal magnet. Incident particles were identified by an elaborate system of beam detectors. Results are obtained for the double-differential cross-sections d2sigma/dpdtheta at six incident beam momenta. Data at 3 GeV/c, 5 GeV/c, 8 GeV/c, and 12 GeV/c are available for all targets while additional data at 8.9 GeV/c and 12.9 GeV/c were taken in positive particle beams on Be and Al targets, respectively. The measurements are compared with several generators of GEANT4 and the MARS Monte Carlo simulation.','{"pi nucleus: inclusive reaction","pi: hadroproduction","production: wide-angle","numerical calculations: Monte Carlo","drift chamber: time projection","differential cross section: momentum dependence","angular dependence","GEANT","lead","beryllium","aluminum","tantalum","copper","carbon","tin","HARP","CERN Lab","tables","pi nucleus --> pi anything","3.0-12.9 GeV/c"}','https://inspirehep.net/record/825244');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(826544,'{"Artamonov, A.","Bagulya, A.","Barr, G.","Blondel, A.","Bobisut, F.","Bogomilov, M.","Bonesini, M.","Booth, C.","Borghi, S.","Bunyatov, S.","Burguet-Castell, J.","Catanesi, M.G.","Cervera-Villanueva, A.","Chimenti, P.","Coney, L.","Di Capua, E.","Dore, U.","Dumarchez, J.","Edgecock, R.","Ellis, M.","Ferri, F.","Gastaldi, U.","Giani, S.","Giannini, G.","Gibin, D.","Gilardoni, S.","Gorbunov, P.","Gossling, C.","Gomez-Cadenas, J.J.","Grant, A.","Graulich, J.S.","Gregoire, G.","Grichine, V.","Grossheim, A.","Guglielmi, A.","Howlett, L.","Ivanchenko, A.","Ivanchenko, V.","Kayis-Topaksu, A.","Kirsanov, M.","Kolev, D.","Krasnoperov, A.","Martin-Albo, J.","Meurer, C.","Mezzetto, M.","Mills, G.B.","Morone, M.C.","Novella, P.","Orestano, D.","Palladino, V.","Panman, J.","Papadopoulos, I.","Pastore, F.","Piperov, Stefan","Polukhina, N.","Popov, B.","Prior, G.","Radicioni, E.","Schmitz, D.","Schroeter, R.","Serdiouk, V.","Skoro, G","Sorel, M.","Tcherniaev, E.","Temnikov, P.","Tereschenko, V.","Tonazzo, A.","Tortora, L.","Tsenov, R.","Tsukerman, I.","Vidal-Sitjes, G.","Wiebusch, C.","Zucchelli, P.","Apollonio, M."}','Forward production of charged pions with incident protons on nuclear targets at the CERN PS','Phys.Rev.','10.1103/PhysRevC.80.035208','035208','C80',2009,'Measurements of the double-differential charged pion production cross-section in the range of momentum 0.5 GeV/c &lt; p &lt; 8.0 GeV/c and angle 0.025 rad &lt; theta &lt;0.25 rad in collisions of protons on beryllium, carbon, nitrogen, oxygen, aluminium, copper, tin, tantalum and lead are presented. The data were taken with the large acceptance HARP detector in the T9 beam line of the CERN PS. Incident particles were identified by an elaborate system of beam detectors. The data were taken with thin targets of 5% of a nuclear interaction length. The tracking and identification of the produced particles was performed using the forward system of the HARP experiment. Results are obtained for the double-differential cross section mainly at four incident proton beam momenta (3 GeV/c, 5 GeV/c, 8 GeV/c and 12 GeV/c). Measurements are compared with the GEANT4 and MARS Monte Carlo generators. A global parametrization is provided as an approximation of all the collected datasets which can serve as a tool for quick yields estimates.','{"p: nucleus: interaction","pi: hadroproduction","CERN Lab","differential cross section: parametrization","momentum dependence","angular dependence","mass number: dependence","forward detector","Monte Carlo: GEANT","beryllium","tantalum","aluminum","nitrogen","copper","oxygen","carbon","tin","tables","p nucleus --> pi anything","3.0-12.9 GeV/c"}','https://inspirehep.net/record/826544');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(830148,'{"Apollonio, M.","Artamonov, A.","Bagulya, A.","Barr, G.","Blondel, A.","Bobisut, F.","Bogomilov, M.","Bonesini, M.","Booth, C.","Borghi, S.","Bunyatov, S.","Burguet-Castell, J.","Catanesi, M.G.","Cervera-Villanueva, A.","Chimenti, P.","Coney, L.","Di Capua, E.","Dore, U.","Dumarchez, J.","Edgecock, R.","Ellis, M.","Ferri, F.","Gastaldi, U.","Giani, S.","Giannini, G.","Gibin, D.","Gilardoni, S.","Gorbunov, P.","Gossling, C.","Gomez-Cadenas, J.J.","Grant, A.","Graulich, J.S.","Gregoire, G.","Grichine, V.","Grossheim, A.","Guglielmi, A.","Howlett, L.","Ivanchenko, A.","Ivanchenko, V.","Kayis-Topaksu, A.","Kirsanov, M.","Kolev, D.","Krasnoperov, A.","Martin-Albo, J.","Meurer, C.","Mezzetto, M.","Mills, G.B.","Morone, M.C.","Novella, P.","Orestano, D.","Palladino, V.","Panman, J.","Papadopoulos, I.","Pastore, F.","Piperov, Stefan","Polukhina, N.","Popov, B.","Prior, G.","Radicioni, E.","Schmitz, D.","Schroeter, R.","Skoro, G","Sorel, M.","Tcherniaev, E.","Temnikov, P.","Tereschenko, V.","Tonazzo, A.","Tortora, L.","Tsenov, R.","Tsukerman, I.","Vidal-Sitjes, G.","Wiebusch, C.","Zucchelli, P."}','Comparison of large-angle production of charged pions with incident protons on cylindrical long and short targets','Phys.Rev.','10.1103/PhysRevC.80.065204','065204','C80',2009,'The HARP collaboration has presented measurements of the double-differential pi+/pi- production cross-section in the range of momentum 100 MeV/c <= p 800 MeV/c and angle 0.35 rad <= theta <= 2.15 rad with proton beams hitting thin nuclear targets. In many applications the extrapolation to long targets is necessary. In this paper the analysis of data taken with long (one interaction length) solid cylindrical targets made of carbon, tantalum and lead is presented. The data were taken with the large acceptance HARP detector in the T9 beam line of the CERN PS. The secondary pions were produced by beams of protons with momenta 5 GeV/c, 8 GeV/c and 12 GeV/c. The tracking and identification of the produced particles were performed using a small-radius cylindrical time projection chamber (TPC) placed inside a solenoidal magnet. Incident protons were identified by an elaborate system of beam detectors. Results are obtained for the double-differential yields per target nucleon d2 sigma / dp dtheta. The measurements are compared with predictions of the MARS and GEANT4 Monte Carlo simulations.','{"p nucleus: inclusive reaction","production: wide-angle","pi+: hadroproduction","pi-: hadroproduction","Monte Carlo: GEANT: MARS","differential cross section: momentum dependence","angular dependence","mass number: dependence","tantalum","carbon","lead","drift chamber: time projection","CERN Lab: experimental results","5: 8: 12 GeV/c"}','https://inspirehep.net/record/830148');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(857228,'{"Apollonio, M.","Artamonov, A.","Bagulya, A.","Barr, G.","Blondel, A.","Bobisut, F.","Bogomilov, M.","Bonesini, M.","Booth, C.","Borghi, S.","Bunyatov, S.","Burguet-Castell, J.","Catanesi, M.G.","Cervera-Villanueva, A.","Chimenti, P.","Coney, L.","Di Capua, E.","Dore, U.","Dumarchez, J.","Edgecock, R.","Ellis, M.","Ferri, F.","Gastaldi, U.","Giani, S.","Giannini, G.","Gibin, D.","Gilardoni, S.","Gorbunov, P.","Gossling, C.","Gomez-Cadenas, J.J.","Grant, A.","Graulich, J.S.","Gregoire, G.","Grichine, V.","Grossheim, A.","Guglielmi, A.","Howlett, L.","Ivanchenko, A.","Ivanchenko, V.","Kayis-Topaksu, A.","Kirsanov, M.","Kolev, D.","Krasnoperov, A.","Martin-Albo, J.","Meurer, C.","Mezzetto, M.","Mills, G.B.","Morone, M.C.","Novella, P.","Orestano, D.","Palladino, V.","Panman, J.","Papadopoulos, I.","Pastore, F.","Piperov, Stefan","Polukhina, N.","Popov, B.","Prior, G.","Radicioni, E.","Schmitz, D.","Schroeter, R.","Skoro, G","Sorel, M.","Tcherniaev, E.","Temnikov, P.","Tereschenko, V.","Tonazzo, A.","Tortora, L.","Tsenov, R.","Tsukerman, I.","Vidal-Sitjes, G.","Wiebusch, C.","Zucchelli, P."}','Measurements of forward proton production with incident protons and charged pions on nuclear targets at the CERN Proton Synchroton','Phys.Rev.','10.1103/PhysRevC.82.045208','045208','C82',2010,'Measurements of the double-differential proton production cross-section in the range of momentum 0.5 GeV/c < p < 8.0 GeV/c and angle 0.05 rad < \theta < 0.25 rad in collisions of charged pions and protons on beryllium, carbon, aluminium, copper, tin, tantalum and lead are presented. The data were taken with the large acceptance HARP detector in the T9 beam line of the CERN Proton Synchrotron. Incident particles were identified by an elaborate system of beam detectors and impinged on a target of 5 % of a nuclear interaction length. The tracking and identification of the produced particles was performed using the forward spectrometer of the HARP experiment. Results are obtained for the double-differential cross-sections mainly at four incident beam momenta (3 GeV/c, 5 GeV/c, 8 GeV/c and 12 GeV/c). Measurements are compared with predictions of the GEANT4 and MARS Monte Carlo generators.','{"beryllium","tantalum","aluminum","copper","carbon","lead","Monte Carlo: GEANT","CERN Lab","HARP","tables","p nucleus: inclusive reaction","pi nucleus: inclusive reaction","tin","p: hadroproduction","small-angle","p: yield","differential cross section: angular dependence","mass number: dependence","momentum dependence","forward spectrometer","experimental results","3.0-12.9 GeV/c"}','https://inspirehep.net/record/857228');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(886780,'{"Abgrall, N","Aduszkiewicz, A","Andrieu, B","Anticic, T","Antoniou, N","Argyriades, J","Asryan, A G","Baatar, B","Blondel, A","Blumer, J","Bogusz, M","Boldizsar, L","Bravar, A","Brooks, W","Brzychczyk, J","Bubak, A","Bunyatov, S A","Busygina, O","Cetner, T","Choi, K -U","Christakoglou, P","Chung, P","Czopowicz, T","Davis, N","Diakonos, F","Di Luise, S","Dominik, W","Dumarchez, J","Engel, R","Ereditato, A","Esposito, L S","Feofilov, G A","Fodor, Z","Ferrero, A","Fulop, A","Garrido, X","Gazdzicki, M","Golubeva, M","Grebieszkow, K","Grzeszczuk, A","Guber, F","Hakobyan, H","Hasegawa, T","Igolkin, S","Ivanov, A S","Ivanov, Y","Ivashkin, A","Kadija, K","Kapoyannis, A","Katrynska, N N","Kielczewska, D","Kikola, D","Kim, J -H","Kirejczyk, M","Kisiel, J","Kobayashi, T","Kochebina, O","Kolesnikov, V I","Kolev, D","Kondratiev, V P","Korzenev, A","Kowalski, S","Kuleshov, S","Kurepin, A","Lacey, R","Lagoda, J","Laszlo, A","Lyubushkin, V V","Mackowiak, M","Majka, Z","Malakhov, A I","Marchionni, A","Marcinek, A","Maris, I","Marin, V","Matulewicz, T","Matveev, V","Melkumov, G L","Meregaglia, A","Messina, M","Mrowczynski, St","Murphy, S","Nakadaira, T","Naumenko, P A","Nishikawa, K","Palczewski, T","Palla, G","Panagiotou, A D","Peryt, W","Petukhov, O","Planeta, R.","Pluta, J","Popov, B A","Posiadala, M","Pu lawski, S","Rauch, W","Ravonel, M","Renfordt, R","Robert, A","Rohrich, D","Rondio, E","Rossi, B","Roth, M","Rubbia, A","Rybczynski, M","Sadovsky, A","Sakashita, K","Sekiguchi, T","Seyboth, P","Shibata, M","Sissakian, A N","Skrzypczak, E","Slodkowski, M.","Sorin, A S","Staszel, P","Stefanek, G","Stepaniak, J","Strabel, C","Strobele, H","Susa, T","Szaik, P","Szuba, M","Tada, M","Taranenko, A","Tsenov, R","Ulrich, R","Unger, M","Vassiliou, M","Vechernin, V V","Vesztergombi, G","Wilczek, A","lodarczyk, Z W","Wojtaszek, A","Yi, J -G","Yoo, I -K","Zipper, W"}','Measurements of Cross Sections and Charged Pion Spectra in Proton-Carbon Interactions at 31 GeV/c','Phys.Rev.','10.1103/PhysRevC.84.034604','034604','C84',2011,'Interaction cross sections and charged pion spectra in p+C interactions at 31 GeV/c were measured with the large acceptance NA61/SHINE spectrometer at the CERN SPS. These data are required to improve predictions of the neutrino flux for the T2K long baseline neutrino oscillation experiment in Japan. A set of data collected during the first NA61/SHINE run in 2007 with an isotropic graphite target with a thickness of 4% of a nuclear interaction length was used for the analysis. The measured p+C inelastic and production cross sections are 257.2 +- 1.9 +- 8.9 mb and 229.3 +- 1.9 +- 9.0 mb, respectively. Inclusive production cross sections for negatively and positively charged pions are presented as a function of laboratory momentum in 10 intervals of the laboratory polar angle covering the range from 0 up to 420 mrad. The spectra are compared with predictions of several hadron production models.','{"p nucleus: inelastic scattering","p nucleus: inclusive reaction","carbon","pi: hadroproduction","pi+: momentum spectrum","pi-: momentum spectrum","pi+ pi-: ratio","differential cross section: measured","momentum dependence","angular dependence","data analysis method","CERN SPS","experimental results","approx. 31 GeV/c"}','https://inspirehep.net/record/886780');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(1079585,'{"Abgrall, N.","Aduszkiewicz, A.","Anticic, T.","Antoniou, N.","Argyriades, J.","Baatar, B.","Blondel, A.","Blumer, J.","Bogusz, M.","Boldizsar, L.","Bravar, A.","Brooks, W.","Brzychczyk, J.","Bubak, A.","Bunyatov, S.A.","Busygina, O.","Cetner, T.","Choi, K.U.","Christakoglou, P.","Czopowicz, T.","Davis, N.","Diakonos, F.","Di Luise, S.","Dominik, W.","Dumarchez, J.","Engel, R.","Ereditato, A.","Esposito, L.S.","Feofilov, G.A.","Fodor, Z.","Ferrero, A.","Fulop, A.","Garrido, X.","Gazdzicki, M.","Golubeva, M.","Grebieszkow, K.","Grzeszczuk, A.","Guber, F.","Haesler, A.","Hakobyan, H.","Hasegawa, T.","Idczak, R.","Ivanov, Y.","Ivashkin, A.","Kadija, K.","Kapoyannis, A.","Katrynska, N.","Kielczewska, D.","Kikola, D.","Kim, J.H.","Kirejczyk, M.","Kisiel, J.","Kobayashi, T.","Kochebina, O.","Kolesnikov, V.I.","Kolev, D.","Kondratiev, V.P.","Korzenev, A.","Kowalski, S.","Krasnoperov, A.","Kuleshov, S.","Kurepin, A.","Lacey, R.","Lagoda, J.","Laszlo, A.","Lyubushkin, V.V.","Mackowiak-Pawlowska, M.","Majka, Z.","Malakhov, A.I.","Marchionni, A.","Marcinek, A.","Maris, I.","Marin, V.","Matulewicz, T.","Matveev, V.","Melkumov, G.L.","Meregaglia, A.","Messina, M.","Mrowczynski, St.","Murphy, S.","Nakadaira, T.","Nishikawa, K.","Palczewski, T.","Palla, G.","Panagiotou, A.D.","Paul, T.","Peryt, W.","Petukhov, O.","Planeta, R.","Pluta, J.","Popov, B.A.","Posiadala, M.","Pulawski, S.","Rauch, W.","Ravonel, M.","Renfordt, R.","Robert, A.","Rohrich, D.","Rondio, E.","Rossi, B.","Roth, M.","Rubbia, A.","Rybczynski, M.","Sadovsky, A.","Sakashita, K.","Sekiguchi, T.","Seyboth, P.","Shibata, M.","Skrzypczak, E.","Slodkowski, M.","Staszel, P.","Stefanek, G.","Stepaniak, J.","Strabel, C.","Strobele, H.","Susa, T.","Szaflik, P.","Szuba, M.","Tada, M.","Taranenko, A.","Tereshchenko, V.","Tsenov, R.","Turko, L.","Ulrich, R.","Unger, M.","Vassiliou, M.","Veberic, D.","Vechernin, V.V.","Vesztergombi, G.","Wilczek, A.","Wlodarczyk, Z.","Wojtaszek-Szwarc, A.","Yi, J.G.","Yoo, I.K.","Zambelli, L.","Zipper, W."}','Measurement of Production Properties of Positively Charged Kaons in Proton-Carbon Interactions at 31 GeV/c','Phys.Rev.','10.1103/PhysRevC.85.035210','035210','C85',2012,'Spectra of positively charged kaons in p+C interactions at 31 GeV/c were measured with the NA61/SHINE spectrometer at the CERN SPS. The analysis is based on the full set of data collected in 2007 with a graphite target with a thickness of 4% of a nuclear interaction length. Interaction cross sections and charged pion spectra were already measured using the same set of data. These new measurements in combination with the published ones are required to improve predictions of the neutrino flux for the T2K long baseline neutrino oscillation experiment in Japan. In particular, the knowledge of kaon production is crucial for precisely predicting the intrinsic electron neutrino component and the high energy tail of the T2K beam. The results are presented as a function of laboratory momentum in 2 intervals of the laboratory polar angle covering the range from 20 up to 240 mrad. The kaon spectra are compared with predictions of several hadron production models. Using the published pion results and the new kaon data, the K+/\pi+ ratios are computed.','{"K+: hadroproduction","differential cross section: measured","neutrino: secondary beam","CERN SPS","p nucleus: inclusive reaction","carbon","yield: (K+ pi+)","momentum dependence","angular dependence","magnetic spectrometer","experimental results","p carbon --> K+ anything","31 GeV/c"}','https://inspirehep.net/record/1079585');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(1337955,'{"Baker, W. F.","Cool, R. L.","Jenkins, E. W.","Kycia, T. F.;","Lindenbaum, S. J.","Love, W. A.","Luers, D.","Niederer, J. A.","Ozaki, S.","Read, A. L.","Russell, J. J.","Yuan, L. C."}','Particle Production by 10-30 Bev Protons Incident on Al and Be','Phys.Rev.Lett.','10.1103/PhysRevLett.7.101','101-104','7',1961,'','{}','https://inspirehep.net/record/1337955');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(892536,'{"Bellettini, G.","Cocconi, G.","Diddens, A.N.","Lillethun, E.","Matthiae, G.","Scanlon, J.P.","Wetherell, A.M."}','Proton-nuclei cross sections at 20 GeV','Nucl.Phys.','10.1016/0029-5582(66)90267-7','609-624','79',1966,'Measurements of the differential cross section of 20 GeV protons scattered elastically and quasi-elastically by a series of nuclei, ranging from Li to U, are presented. The total and the elastic cross sections are also given.','{}','https://inspirehep.net/record/892536');
insert into public.reference (AUTHORS,TITLE,KEYWORDS,LINKURL) values('{"NA49"}','Detailed analysis of soft hadronic interactions','{"NA49","p+p interactions","Pion production","Proton and Anti-proton production","Neutron production","Kaon production","p+C interactions","Carbon","Kaon s-dependence in p+p interactions","proton and pion 1/√s-dependence in p+C interactions"}','https://spshadrons.web.cern.ch/spshadrons/');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(1473668,'{"Aliaga, L.","Kordosky, M.","Golan, T.","Altinok, O.","Bellantoni, L.","Bercellie, A.","Betancourt, M.","Bravar, A.","Budd, H.","Carneiro, M.F.","Diaz, G.A.","Endress, E.","Felix, J.","Fields, L.","Fine, R.","Gago, A.M.","Galindo, R.","Gallagher, H.","Gran, R.","Harris, D.A.","Higuera, A.","Hurtado, K.","Kiveni, M.","Kleykamp, J.","Le, T.","Maher, E.","Mann, W.A.","Marshall, C.M.","Martinez Caicedo, D.A.","McFarland, K.S.","McGivern, C.L.","McGowan, A.M.","Messerly, B.","Miller, J.","Mislivec, A.","Morfin, J.G.","Mousseau, J.","Naples, D.","Nelson, J.K.","Norrick, A.","Nuruzzaman","Paolone, V.","Park, J.","Patrick, C.E.","Perdue, G.N.","Ransome, R.D.","Ray, H.","Ren, L.","Rimal, D.","Rodrigues, P.A.","Ruterbories, D.","Schellman, H.","Solano Salinas, C.J.","Sanchez Falero, S.","Tice, B.G.","Valencia, E.","Walton, T.","Wolcott, J.","Wospakrik, M.","Zhang, D."}','Neutrino Flux Predictions for the NuMI Beam','','','','',2016,'Knowledge of the neutrino flux produced by the Neutrinos at the Main Injector (NuMI) beamline is essential to the neutrino oscillation and neutrino interaction measurements of the MINERvA, MINOS+, NOvA and MicroBooNE experiments at Fermi National Accelerator Laboratory. We have produced a flux prediction which uses all available and relevant hadron production data, incorporating measurements of particle production off of thin targets as well as measurements of particle yields from a spare NuMI target exposed to a 120 GeV proton beam. The result is the most precise flux prediction achieved for a neutrino beam in the one to tens of GeV energy region. We have also compared the prediction to in situ measurements of the neutrino flux and find good agreement.','{"* Automatic Keywords *","neutrino: flux","neutrino: beam","neutrino: interaction","neutrino: oscillation","p: beam","hadron: production","particle: yield","accelerator","MINERvA","MINOS","NOvA"}','https://inspirehep.net/record/1473668');
INSERT INTO PUBLIC.REFERENCE (INSPIREID,AUTHORS,TITLE,JOURNAL,ERN, PAGES,VOLUME,YEA,ABSTR,KEYWORDS,LINKURL) VALUES(246551,'{"Ero, J.","Fodor, Z.","Franz, J.","Kecskemeti, J.","Koncz, P.","Kovacs, Z.","Rossle, E.","Sauerwein, C.","Schmitt, H.","Seres, Z.","Woolverton, H.L."}','PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON CARBON BY INTERMEDIATE-ENERGY NEUTRONS','Nucl.Phys.','10.1016/0375-9474(87)90053-4','733-758','A472',1987,'Inclusive cross sections for production of protons, deuterons and tritons on carbon by neutrons in the energy range of 300–580 MeV were determined at nine angles between 51° and 165°. The invariant cross sections are well represented by an exponential fit, E d σ d 3 p ∝ exp (−Bp 2 ) . The slope parameters exhibit a systematic dependence on incident energy and emission angle. The results are discussed on the basis of the model of quasi-two-body scaling.','{"N LIGHT NUCLEUS: NUCLEAR REACTION","N LIGHT NUCLEUS: INCLUSIVE REACTION","CARBON","P: HADROPRODUCTION","HADROPRODUCTION: P","DEUTERON: HADROPRODUCTION","HADROPRODUCTION: DEUTERON","TRITIUM: HADROPRODUCTION","HADROPRODUCTION: TRITIUM","DIFFERENTIAL CROSS SECTION: ENERGY DEPENDENCE","ENERGY DEPENDENCE: DIFFERENTIAL CROSS SECTION","DIFFERENTIAL CROSS SECTION: PARAMETRIZATION","PARAMETRIZATION: DIFFERENTIAL CROSS SECTION","DIFFERENTIAL CROSS SECTION: ANGULAR DEPENDENCE","ANGULAR DEPENDENCE: DIFFERENTIAL CROSS SECTION","MODEL: SCALING","EXPERIMENTAL RESULTS","SIN CYCL","0.30-0.58 GEV: 51-165 DEGREES"}','https://inspirehep.net/record/246551');

---
--- stuff representing the data (e.g. 1D/2d Histograms, 1D/2D Graphs)
---
CREATE TABLE DATATYPES(
DTYPE INTEGER PRIMARY KEY,
DESCRIPTION CHARACTER VARYING(50) UNIQUE
);
ALTER TABLE DATATYPES OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON  DATATYPES TO G4VALWRITER;
GRANT SELECT ON  DATATYPES TO G4VALREADER;
INSERT INTO PUBLIC.DATATYPES (DTYPE,DESCRIPTION) VALUES (1, '1D Histogram with variable binning and errors in y');
INSERT INTO PUBLIC.DATATYPES (DTYPE,DESCRIPTION) VALUES (2, '1D Histogram, total stat+sys errors in y');
INSERT INTO PUBLIC.DATATYPES (DTYPE,DESCRIPTION) VALUES (1000, '1D Datapoint set');
INSERT INTO PUBLIC.DATATYPES (DTYPE,DESCRIPTION) VALUES (1001, '1D Datapoint set with total stat+sys errors in y');
INSERT INTO PUBLIC.DATATYPES (DTYPE,DESCRIPTION) VALUES (10000,'1D Energy values for monoenergegitc beams');
---
--- Table representing the data (e.g. 1D/2d Histograms, 1D/2D Graphs)
---
CREATE TABLE DATATABLE(
       DTID           SERIAL PRIMARY KEY,
       DTYPE          INTEGER  REFERENCES DATATYPES(DTYPE),
       title          CHARACTER VARYING(100),
       npoints        INTEGER,
       NBINS          INTEGER[],
       axis_title     CHARACTER VARYING(100)[],
       val            REAL[],
       err_stat_plus  REAL[],
       err_stat_minus REAL[],
       err_sys_plus   REAL[],
       err_sys_minus  REAL[],
       bin_min        REAL[],
       bin_max        REAL[]
);
ALTER TABLE DATATABLE OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON  DATATABLE TO G4VALWRITER;
GRANT SELECT ON  DATATABLE TO G4VALREADER;
--
-- Name: scores; Type: TABLE; Schema: public; Owner: g4valwriter; Tablespace: 
--
CREATE TABLE SCORES (
    SCOREID  SERIAL PRIMARY KEY,
    SCORE CHARACTER VARYING(10),
    STYPE CHARACTER VARYING(10), 
    UNIQUE (SCORE,STYPE)
);
ALTER TABLE SCORES OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON SCORES  TO G4VALWRITER;
GRANT SELECT ON  SCORES TO G4VALREADER;
INSERT INTO PUBLIC.SCORES ( SCORE,STYPE) VALUES ( 'passed','expert');
INSERT INTO PUBLIC.SCORES ( SCORE,STYPE) VALUES ( 'NA','NA');
--
--  dictionary table defining the ACCESS of a test result
--
CREATE TABLE ACCESS (
    ACCESSID  SERIAL PRIMARY KEY,
    ACCESS CHARACTER VARYING(50) NOT NULL UNIQUE
);
ALTER TABLE ACCESS OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON ACCESS  TO G4VALWRITER;
GRANT SELECT ON  ACCESS TO G4VALREADER;
INSERT INTO PUBLIC.ACCESS (ACCESS) VALUES ('public');                  -- visible to everyone
INSERT INTO PUBLIC.ACCESS (ACCESS) VALUES ('internal');                -- visible only to geant 4 caloborators
INSERT INTO PUBLIC.ACCESS (ACCESS) VALUES ('temporary');               -- to be deleted

CREATE TABLE MCTOOL(
   MCID SERIAL  PRIMARY KEY,
   MCNAME       CHARACTER VARYING(20) NOT NULL UNIQUE
); 


ALTER TABLE  MCTOOL OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON MCTOOL  TO G4VALWRITER;
GRANT SELECT ON MCTOOL TO G4VALREADER;

insert into PUBLIC.MCTOOL(MCNAME) VALUES ('Experiment');
insert into PUBLIC.MCTOOL(MCNAME) VALUES ('Geant4');
insert into PUBLIC.MCTOOL(MCNAME) VALUES ('Genie');

CREATE TABLE MCDETAIL(
   MCDTID       SERIAL PRIMARY KEY,
   MCTID        INTEGER REFERENCES MCTOOL(MCID),
   VERSIONTAG   CHARACTER VARYING(50) NOT NULL,
   MODEL        CHARACTER VARYING(50) NOT NULL,
   UNIQUE (MCTID,VERSIONTAG,MODEL)
); 

ALTER TABLE   MCDETAIL OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON MCDETAIL  TO G4VALWRITER;
GRANT SELECT ON MCDETAIL TO G4VALREADER;

insert into PUBLIC.MCDETAIL(MCTID,VERSIONTAG,MODEL) VALUES (1,'NA','NA');
insert into PUBLIC.MCDETAIL(MCTID,VERSIONTAG,MODEL) VALUES (2,'10.1.p02','FTFP_BERT');

CREATE TABLE PARTICLE(
   PDGID INTEGER PRIMARY KEY,   -- PDG ID
   PNAME  CHARACTER VARYING(50) UNIQUE
);
ALTER TABLE  PARTICLE OWNER TO g4valwriter;
GRANT SELECT, UPDATE, INSERT, DELETE ON PARTICLE  TO g4valwriter;
GRANT SELECT ON PARTICLE  TO g4valreader;
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('B+',521);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('B-',-521);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('B0',511);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('Bc+',541);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('Bc-',-541);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('Bs0',531);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('D+',411);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('D-',-411);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('D0',421);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('Ds+',431);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('Ds-',-431);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('He3',1000020030);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('J/psi',443);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1440)+',12212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1440)0',12112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1520)+',2124);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1520)0',1214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1535)+',22212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1535)0',22112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1650)+',32212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1650)0',32112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1675)+',2216);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1675)0',2116);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1680)+',12216);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1680)0',12116);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1700)+',22124);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1700)0',21214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1710)+',42212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1710)0',42112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1720)+',32124);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1720)0',31214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1900)+',42124);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1900)0',41214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1990)+',12218);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(1990)0',12118);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(2090)+',52214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(2090)0',52114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(2190)+',2128);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(2190)0',1218);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(2220)+',100002210);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(2220)0',100002110);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(2250)+',100012210);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('N(2250)0',100012110);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('Upsilon',553);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('a0(1450)+',10211);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('a0(1450)-',-10211);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('a0(1450)0',10111);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('a0(980)+',9000211);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('a0(980)-',-9000211);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('a0(980)0',9000111);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('a1(1260)+',20213);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('a1(1260)-',-20213);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('a1(1260)0',20113);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('a2(1320)+',215);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('a2(1320)-',-215);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('a2(1320)0',115);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('alpha',1000020040);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_B0',-511);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_Bs0',-531);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_D0',-421);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_He3',-1000020030);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1440)+',-12212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1440)0',-12112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1520)+',-2124);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1520)0',-1214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1535)+',-22212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1535)0',-22112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1650)+',-32212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1650)0',-32112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1675)+',-2216);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1675)0',-2116);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1680)+',-12216);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1680)0',-12116);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1700)+',-22124);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1700)0',-21214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1710)+',-42212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1710)0',-42112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1720)+',-32124);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1720)0',-31214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1900)+',-42124);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1900)0',-41214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1990)+',-12218);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(1990)0',-12118);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(2090)+',-52214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(2090)0',-52114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(2190)+',-2128);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(2190)0',-1218);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(2220)+',-100002210);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(2220)0',-100002110);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(2250)+',-100012210);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_N(2250)0',-100012110);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_alpha',-1000020040);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_b_quark',-5);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_c_quark',-4);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_d_quark',-1);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_dd1_diquark',-1103);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1600)+',-32214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1600)++',-32224);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1600)-',-31114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1600)0',-32114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1620)+',-2122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1620)++',-2222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1620)-',-1112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1620)0',-1212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1700)+',-12214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1700)++',-12224);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1700)-',-11114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1700)0',-12114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1900)+',-12122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1900)++',-12222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1900)-',-11112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1900)0',-11212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1905)+',-2126);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1905)++',-2226);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1905)-',-1116);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1905)0',-1216);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1910)+',-22122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1910)++',-22222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1910)-',-21112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1910)0',-21212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1920)+',-22214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1920)++',-22224);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1920)-',-21114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1920)0',-22114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1930)+',-12126);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1930)++',-12226);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1930)-',-11116);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1930)0',-11216);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1950)+',-2218);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1950)++',-2228);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1950)-',-1118);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta(1950)0',-2118);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta+',-2214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta++',-2224);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta-',-1114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_delta0',-2114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_deuteron',-1000010020);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_k(1460)0',-100311);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_k0_star(1430)0',-10311);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_k1(1270)0',-10313);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_k1(1400)0',-20313);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_k2(1770)0',-10315);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_k2_star(1430)0',-315);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_k2_star(1980)0',-100315);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_k3_star(1780)0',-317);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_k_star(1410)0',-100313);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_k_star(1680)0',-30313);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_k_star0',-313);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_kaon0',-311);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_lambda',-3122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_lambda(1405)',-13122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_lambda(1520)',-3124);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_lambda(1600)',-23122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_lambda(1670)',-33122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_lambda(1690)',-13124);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_lambda(1800)',-43122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_lambda(1810)',-53122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_lambda(1820)',-3126);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_lambda(1830)',-13126);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_lambda(1890)',-23124);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_lambda(2100)',-3128);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_lambda(2110)',-23126);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_lambda_b',-5122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_lambda_c+',-4122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_neutron',-2112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_nu_e',-12);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_nu_mu',-14);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_nu_tau',-16);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_omega-',-3334);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_omega_b-',-5332);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_omega_c0',-4332);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_proton',-2212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_s_quark',-3);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sd0_diquark',-3101);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sd1_diquark',-3103);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1385)+',-3224);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1385)-',-3114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1385)0',-3214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1660)+',-13222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1660)-',-13112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1660)0',-13212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1670)+',-13224);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1670)-',-13114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1670)0',-13214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1750)+',-23222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1750)-',-23112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1750)0',-23212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1775)+',-3226);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1775)-',-3116);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1775)0',-3216);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1915)+',-13226);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1915)-',-13116);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1915)0',-13216);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1940)+',-23224);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1940)-',-23114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(1940)0',-23214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(2030)+',-3228);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(2030)-',-3118);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma(2030)0',-3218);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma+',-3222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma-',-3112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma0',-3212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma_b+',-5222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma_b-',-5112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma_b0',-5212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma_c+',-4212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma_c++',-4222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_sigma_c0',-4112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_ss1_diquark',-3303);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_su0_diquark',-3201);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_su1_diquark',-3203);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_t_quark',-6);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_triton',-1000010030);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_u_quark',-2);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_ud0_diquark',-2101);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_ud1_diquark',-2103);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_uu1_diquark',-2203);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi(1530)-',-3314);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi(1530)0',-3324);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi(1690)-',-23314);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi(1690)0',-23324);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi(1820)-',-13314);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi(1820)0',-13324);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi(1950)-',-33314);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi(1950)0',-33324);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi(2030)-',-13316);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi(2030)0',-13326);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi-',-3312);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi0',-3322);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi_b-',-5132);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi_b0',-5232);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi_c+',-4232);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('anti_xi_c0',-4132);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('b1(1235)+',10213);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('b1(1235)-',-10213);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('b1(1235)0',10113);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('b_quark',5);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('c_quark',4);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('d_quark',1);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('dd1_diquark',1103);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1600)+',32214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1600)++',32224);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1600)-',31114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1600)0',32114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1620)+',2122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1620)++',2222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1620)-',1112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1620)0',1212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1700)+',12214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1700)++',12224);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1700)-',11114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1700)0',12114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1900)+',12122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1900)++',12222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1900)-',11112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1900)0',11212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1905)+',2126);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1905)++',2226);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1905)-',1116);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1905)0',1216);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1910)+',22122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1910)++',22222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1910)-',21112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1910)0',21212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1920)+',22214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1920)++',22224);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1920)-',21114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1920)0',22114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1930)+',12126);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1930)++',12226);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1930)-',11116);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1930)0',11216);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1950)+',2218);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1950)++',2228);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1950)-',1118);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta(1950)0',2118);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta+',2214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta++',2224);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta-',1114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('delta0',2114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('deuteron',1000010020);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('e+',-11);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('e-',11);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('eta',221);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('eta(1295)',100221);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('eta(1405)',9020221);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('eta(1475)',100331);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('eta2(1645)',10225);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('eta2(1870)',10335);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('eta_prime',331);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('etac',441);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('f0(1370)',30221);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('f0(1500)',9030221);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('f0(1710)',10331);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('f0(500)',9000221);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('f0(980)',9010221);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('f1(1285)',20223);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('f1(1420)',20333);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('f2(1270)',225);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('f2(1810)',9030225);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('f2(2010)',9060225);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('f2_prime(1525)',335);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('gamma',22);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('geantino',0);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('gluon',21);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('h1(1170)',10223);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('h1(1380)',10333);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k(1460)+',100321);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k(1460)-',-100321);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k(1460)0',100311);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k0_star(1430)+',10321);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k0_star(1430)-',-10321);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k0_star(1430)0',10311);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k1(1270)+',10323);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k1(1270)-',-10323);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k1(1270)0',10313);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k1(1400)+',20323);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k1(1400)-',-20323);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k1(1400)0',20313);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k2(1770)+',10325);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k2(1770)-',-10325);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k2(1770)0',10315);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k2_star(1430)+',325);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k2_star(1430)-',-325);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k2_star(1430)0',315);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k2_star(1980)+',100325);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k2_star(1980)-',-100325);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k2_star(1980)0',100315);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k3_star(1780)+',327);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k3_star(1780)-',-327);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k3_star(1780)0',317);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k_star(1410)+',100323);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k_star(1410)-',-100323);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k_star(1410)0',100313);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k_star(1680)+',30323);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k_star(1680)-',-30323);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k_star(1680)0',30313);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k_star+',323);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k_star-',-323);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('k_star0',313);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('kaon+',321);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('kaon-',-321);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('kaon0',311);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('kaon0L',130);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('kaon0S',310);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('lambda',3122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('lambda(1405)',13122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('lambda(1520)',3124);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('lambda(1600)',23122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('lambda(1670)',33122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('lambda(1690)',13124);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('lambda(1800)',43122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('lambda(1810)',53122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('lambda(1820)',3126);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('lambda(1830)',13126);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('lambda(1890)',23124);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('lambda(2100)',3128);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('lambda(2110)',23126);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('lambda_b',5122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('lambda_c+',4122);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('mu+',-13);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('mu-',13);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('neutron',2112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('nu_e',12);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('nu_mu',14);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('nu_tau',16);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('omega',223);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('omega(1420)',100223);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('omega(1650)',30223);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('omega-',3334);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('omega3(1670)',227);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('omega_b-',5332);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('omega_c0',4332);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('phi',333);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('phi(1680)',100333);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('phi3(1850)',337);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('pi(1300)+',100211);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('pi(1300)-',-100211);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('pi(1300)0',100111);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('pi+',211);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('pi-',-211);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('pi0',111);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('pi2(1670)+',10215);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('pi2(1670)-',-10215);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('pi2(1670)0',10115);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('proton',2212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('rho(1450)+',100213);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('rho(1450)-',-100213);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('rho(1450)0',100113);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('rho(1700)+',30213);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('rho(1700)-',-30213);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('rho(1700)0',30113);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('rho+',213);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('rho-',-213);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('rho0',113);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('rho3(1690)+',217);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('rho3(1690)-',-217);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('rho3(1690)0',117);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('s_quark',3);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sd0_diquark',3101);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sd1_diquark',3103);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1385)+',3224);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1385)-',3114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1385)0',3214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1660)+',13222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1660)-',13112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1660)0',13212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1670)+',13224);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1670)-',13114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1670)0',13214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1750)+',23222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1750)-',23112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1750)0',23212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1775)+',3226);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1775)-',3116);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1775)0',3216);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1915)+',13226);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1915)-',13116);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1915)0',13216);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1940)+',23224);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1940)-',23114);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(1940)0',23214);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(2030)+',3228);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(2030)-',3118);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma(2030)0',3218);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma+',3222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma-',3112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma0',3212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma_b+',5222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma_b-',5112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma_b0',5212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma_c+',4212);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma_c++',4222);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('sigma_c0',4112);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('ss1_diquark',3303);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('su0_diquark',3201);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('su1_diquark',3203);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('t_quark',6);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('tau+',-15);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('tau-',15);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('triton',1000010030);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('u_quark',2);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('ud0_diquark',2101);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('ud1_diquark',2103);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('uu1_diquark',2203);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi(1530)-',3314);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi(1530)0',3324);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi(1690)-',23314);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi(1690)0',23324);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi(1820)-',13314);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi(1820)0',13324);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi(1950)-',33314);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi(1950)0',33324);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi(2030)-',13316);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi(2030)0',13326);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi-',3312);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi0',3322);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi_b-',5132);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi_b0',5232);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi_c+',4232);
INSERT INTO PUBLIC.PARTICLE(PNAME,PDGID) VALUES ('xi_c0',4132);

CREATE TABLE MATERIAL(
   MID       SERIAL PRIMARY KEY,
   Z         INTEGER,                                 -- z
   MNAME     CHARACTER VARYING(50) NOT NULL UNIQUE,   -- name of material
   DENSITY   REAL                  NOT NULL,          -- density in g/cm^3
   ION       REAL                  NOT NULL,          -- ionization potential in eV
   NCOMP     INTEGER,                                 -- number of compounds
   COMP      INTEGER[],                               -- which compound
   FRAC      REAL[]                                   -- fraction of compound
);
ALTER TABLE  MATERIAL OWNER TO g4valwriter;
GRANT SELECT, UPDATE, INSERT, DELETE ON MATERIAL  TO g4valwriter;
GRANT SELECT ON MATERIAL TO g4valreader;
---
--- Elements:
---
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (1,'H',8.3748e-05,19.2,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (2,'He',0.000166322,41.8,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (3,'Li',0.534,40,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (4,'Be',1.848,63.7,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (5,'B',2.37,76,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (6,'C',2,81,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (7,'N',0.0011652,82,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (8,'O',0.00133151,95,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (9,'F',0.00158029,115,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (10,'Ne',0.000838505,137,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (11,'Na',0.971,149,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (12,'Mg',1.74,156,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (13,'Al',2.699,166,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (14,'Si',2.33,173,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (15,'P',2.2,173,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (16,'S',2,180,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (17,'Cl',0.00299473,174,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (18,'Ar',0.00166201,188,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (19,'K',0.862,190,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (20,'Ca',1.55,191,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (21,'Sc',2.989,216,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (22,'Ti',4.54,233,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (23,'V',6.11,245,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (24,'Cr',7.18,257,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (25,'Mn',7.44,272,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (26,'Fe',7.874,286,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (27,'Co',8.9,297,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (28,'Ni',8.902,311,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (29,'Cu',8.96,322,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (30,'Zn',7.133,330,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (31,'Ga',5.904,334,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (32,'Ge',5.323,350,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (33,'As',5.73,347,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (34,'Se',4.5,348,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (35,'Br',0.0070721,343,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (36,'Kr',0.00347832,352,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (37,'Rb',1.532,363,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (38,'Sr',2.54,366,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (39,'Y',4.469,379,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (40,'Zr',6.506,393,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (41,'Nb',8.57,417,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (42,'Mo',10.22,424,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (43,'Tc',11.5,428,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (44,'Ru',12.41,441,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (45,'Rh',12.41,449,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (46,'Pd',12.02,470,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (47,'Ag',10.5,470,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (48,'Cd',8.65,469,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (49,'In',7.31,488,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (50,'Sn',7.31,488,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (51,'Sb',6.691,487,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (52,'Te',6.24,485,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (53,'I',4.93,491,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (54,'Xe',0.00548536,482,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (55,'Cs',1.873,488,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (56,'Ba',3.5,491,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (57,'La',6.154,501,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (58,'Ce',6.657,523,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (59,'Pr',6.71,535,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (60,'Nd',6.9,546,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (61,'Pm',7.22,560,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (62,'Sm',7.46,574,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (63,'Eu',5.243,580,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (64,'Gd',7.9004,591,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (65,'Tb',8.229,614,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (66,'Dy',8.55,628,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (67,'Ho',8.795,650,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (68,'Er',9.066,658,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (69,'Tm',9.321,674,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (70,'Yb',6.73,684,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (71,'Lu',9.84,694,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (72,'Hf',13.31,705,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (73,'Ta',16.654,718,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (74,'W',19.3,727,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (75,'Re',21.02,736,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (76,'Os',22.57,746,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (77,'Ir',22.42,757,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (78,'Pt',21.45,790,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (79,'Au',19.32,790,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (80,'Hg',13.546,800,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (81,'Tl',11.72,810,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (82,'Pb',11.35,823,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (83,'Bi',9.747,823,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (84,'Po',9.32,830,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (85,'At',9.32,825,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (86,'Rn',0.00900662,794,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (87,'Fr',1,827,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (88,'Ra',5,826,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (89,'Ac',10.07,841,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (90,'Th',11.72,847,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (91,'Pa',15.37,878,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (92,'U',18.95,890,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (93,'Np',20.25,902,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (94,'Pu',19.84,921,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (95,'Am',13.67,934,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (96,'Cm',13.51,939,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (97,'Bk',14,952,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (98,'Cf',10,966,Null,Null,Null);
---
--- Compounds
---
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'A-150_TISSUE',1.127,65.1,6,'{1,6,7,8,9,20}','{0.101327,0.7755,0.035057,0.0523159,0.017422,0.018378}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'ACETONE',0.7899,64.2,3,'{1,6,8}','{0.104122,0.620405,0.275473}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'ACETYLENE',0.0010967,58.2,2,'{1,6}','{0.077418,0.922582}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'ADENINE',1.35,71.4,3,'{1,6,7}','{0.037294,0.44443,0.518276}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'ADIPOSE_TISSUE_ICRP',0.92,63.2,13,'{1,6,7,8,11,12,15,16,17,19,20,26,30}','{0.119477,0.63724,0.00797,0.232333,0.0005,2e-05,0.00016,0.00073,0.00119,0.00032,2e-05,2e-05,2e-05}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'AIR',0.00120479,85.7,4,'{6,7,8,18}','{0.000124,0.755268,0.231781,0.012827}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'ALANINE',1.42,71.9,4,'{1,6,7,8}','{0.0791899,0.404439,0.157213,0.359159}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'Al_2O_3',3.97,145.2,2,'{8,13}','{0.470749,0.529251}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'AMBER',1.1,63.2,3,'{1,6,8}','{0.10593,0.788974,0.105096}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'AMMONIA',0.000826019,53.7,2,'{1,7}','{0.177547,0.822453}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'ANILINE',1.0235,66.2,3,'{1,6,7}','{0.075759,0.773838,0.150403}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'ANTHRACENE',1.283,69.5,2,'{1,6}','{0.05655,0.94345}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'B-100_BONE',1.45,85.9,6,'{1,6,7,8,9,20}','{0.0654709,0.536944,0.0215,0.032085,0.167411,0.176589}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'BAKELITE',1.25,72.4,3,'{1,6,8}','{0.057441,0.774591,0.167968}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'BARIUM_FLUORIDE',4.89,375.9,2,'{9,56}','{0.21672,0.78328}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'BARIUM_SULFATE',4.5,285.7,3,'{8,16,56}','{0.274212,0.137368,0.58842}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'BENZENE',0.87865,63.4,2,'{1,6}','{0.077418,0.922582}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'BERYLLIUM_OXIDE',3.01,93.2,2,'{4,8}','{0.36032,0.63968}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'BGO',7.13,534.1,3,'{8,32,83}','{0.154126,0.17482,0.671054}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'BLOOD_ICRP',1.06,75.2,14,'{1,6,7,8,11,12,14,15,16,17,19,20,26,30}','{0.101866,0.10002,0.02964,0.759414,0.00185,4e-05,3e-05,0.00035,0.00185,0.00278,0.00163,6e-05,0.00046,1e-05}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'BONE_COMPACT_ICRU',1.85,91.9,8,'{1,6,7,8,12,15,16,20}','{0.063984,0.278,0.027,0.410016,0.002,0.07,0.002,0.147}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'BONE_CORTICAL_ICRP',1.85,106.4,9,'{1,6,7,8,12,15,16,20,30}','{0.047234,0.14433,0.04199,0.446096,0.0022,0.10497,0.00315,0.20993,0.0001}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'BORON_CARBIDE',2.52,84.7,2,'{5,6}','{0.78261,0.21739}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'BORON_OXIDE',1.812,99.6,2,'{5,8}','{0.310551,0.689449}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'BRAIN_ICRP',1.03,73.3,13,'{1,6,7,8,11,12,15,16,17,19,20,26,30}','{0.110667,0.12542,0.01328,0.737723,0.00184,0.00015,0.00354,0.00177,0.00236,0.0031,9e-05,5e-05,1e-05}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'BUTANE',0.00249343,48.3,2,'{1,6}','{0.173408,0.826592}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'N-BUTYL_ALCOHOL',0.8098,59.9,3,'{1,6,8}','{0.135978,0.648171,0.215851}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'C-552',1.76,86.8,5,'{1,6,8,9,14}','{0.02468,0.501611,0.004527,0.465209,0.003973}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CADMIUM_TELLURIDE',6.2,539.3,2,'{48,52}','{0.468355,0.531645}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CADMIUM_TUNGSTATE',7.9,468.3,3,'{8,48,74}','{0.177644,0.312027,0.510329}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CALCIUM_CARBONATE',2.8,136.4,3,'{6,8,20}','{0.120003,0.479554,0.400443}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CALCIUM_FLUORIDE',3.18,166,2,'{9,20}','{0.486659,0.513341}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CALCIUM_OXIDE',3.3,176.1,2,'{8,20}','{0.285299,0.714701}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CALCIUM_SULFATE',2.96,152.3,3,'{8,16,20}','{0.470095,0.235497,0.294408}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CALCIUM_TUNGSTATE',6.062,395,3,'{8,20,74}','{0.22227,0.139202,0.638528}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CARBON_DIOXIDE',0.00184212,85.,2,'{6,8}','{0.272916,0.727084}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CARBON_TETRACHLORIDE',1.594,166.3,2,'{6,17}','{0.078083,0.921917}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CELLULOSE_CELLOPHANE',1.42,77.6,3,'{1,6,8}','{0.062162,0.444462,0.493376}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CELLULOSE_BUTYRATE',1.2,74.6,3,'{1,6,8}','{0.067125,0.545403,0.387472}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CELLULOSE_NITRATE',1.49,87,4,'{1,6,7,8}','{0.029216,0.271296,0.121276,0.578212}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CERIC_SULFATE',1.03,76.7,5,'{1,7,8,16,58}','{0.107596,0.0008,0.874976,0.014627,0.002001}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CESIUM_FLUORIDE',4.115,440.7,2,'{9,55}','{0.125069,0.874931}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CESIUM_IODIDE',4.51,553.1,2,'{53,55}','{0.488451,0.511549}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CHLOROBENZENE',1.1058,89.1,3,'{1,6,17}','{0.044772,0.640254,0.314974}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CHLOROFORM',1.4832,156,3,'{1,6,17}','{0.008443,0.100613,0.890944}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CONCRETE',2.3,135.2,10,'{1,6,8,11,12,13,14,19,20,26}','{0.01,0.001,0.529107,0.016,0.002,0.033872,0.337021,0.013,0.044,0.014}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'CYCLOHEXANE',0.779,56.4,2,'{1,6}','{0.143711,0.856289}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'1,2-DICHLOROBENZENE',1.3048,106.5,3,'{1,6,17}','{0.027425,0.490233,0.482342}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'DICHLORODIETHYL_ETHER',1.2199,103.3,4,'{1,6,8,17}','{0.0563811,0.335942,0.111874,0.495802}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'1,2-DICHLOROETHANE',1.2351,111.9,3,'{1,6,17}','{0.04074,0.242746,0.716514}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'DIETHYL_ETHER',0.71378,60,3,'{1,6,8}','{0.135978,0.648171,0.215851}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'N,N-DIMETHYL_FORMAMIDE',0.9487,66.6,4,'{1,6,7,8}','{0.096523,0.492965,0.191625,0.218887}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'DIMETHYL_SULFOXIDE',1.1014,98.6,4,'{1,6,8,16}','{0.077403,0.307467,0.204782,0.410348}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'ETHANE',0.00125324,45.4,2,'{1,6}','{0.201115,0.798885}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'ETHYL_ALCOHOL',0.7893,62.9,3,'{1,6,8}','{0.131269,0.521437,0.347294}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'ETHYL_CELLULOSE',1.13,69.3,3,'{1,6,8}','{0.090027,0.585182,0.324791}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'ETHYLENE',0.00117497,50.7,2,'{1,6}','{0.143711,0.856289}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'EYE_LENS_ICRP',1.1,73.3,4,'{1,6,7,8}','{0.099269,0.19371,0.05327,0.653751}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'FERRIC_OXIDE',5.2,227.3,2,'{8,26}','{0.300567,0.699433}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'FERROBORIDE',7.15,261,2,'{5,26}','{0.162174,0.837826}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'FERROUS_OXIDE',5.7,248.6,2,'{8,26}','{0.222689,0.777311}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'FERROUS_SULFATE',1.024,76.4,7,'{1,7,8,11,16,17,26}','{0.108259,2.7e-05,0.878636,2.2e-05,0.012968,3.4e-05,5.4e-05}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'FREON-12',1.12,143,3,'{6,9,17}','{0.099335,0.314247,0.586418}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'FREON-12B2',1.8,284.9,3,'{6,9,35}','{0.057245,0.181096,0.761659}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'FREON-13',0.95,126.6,3,'{6,9,17}','{0.114983,0.545621,0.339396}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'FREON-13B1',1.5,210.5,3,'{6,9,35}','{0.080659,0.382749,0.536592}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'FREON-13I1',1.8,293.5,3,'{6,9,53}','{0.061309,0.290924,0.647767}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'GADOLINIUM_OXYSULFIDE',7.44,493.3,3,'{8,16,64}','{0.084528,0.08469,0.830782}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'GALLIUM_ARSENIDE',5.31,384.9,2,'{31,33}','{0.482019,0.517981}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'GEL_PHOTO_EMULSION',1.2914,74.8,5,'{1,6,7,8,16}','{0.08118,0.41606,0.11124,0.38064,0.01088}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'Pyrex_Glass',2.23,134,6,'{5,8,11,13,14,19}','{0.0400639,0.539561,0.0281909,0.011644,0.377219,0.00332099}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'GLASS_LEAD',6.22,526.4,5,'{8,14,22,33,82}','{0.156453,0.080866,0.008092,0.002651,0.751938}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'GLASS_PLATE',2.4,145.4,4,'{8,11,14,20}','{0.4598,0.0964411,0.336553,0.107205}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'GLUCOSE',1.54,77.2,3,'{1,6,8}','{0.071204,0.363652,0.565144}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'GLUTAMINE',1.46,73.3,4,'{1,6,7,8}','{0.0689651,0.410926,0.191681,0.328427}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'GLYCEROL',1.2613,72.6,3,'{1,6,8}','{0.0875539,0.391262,0.521184}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'GUANINE',1.58,75,4,'{1,6,7,8}','{0.033346,0.39738,0.463407,0.105867}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'GYPSUM',2.32,129.7,4,'{1,8,16,20}','{0.023416,0.557572,0.186215,0.232797}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'N-HEPTANE',0.68376,54.4,2,'{1,6}','{0.160937,0.839063}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'N-HEXANE',0.6603,54,2,'{1,6}','{0.163741,0.836259}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'KAPTON',1.42,79.6,4,'{1,6,7,8}','{0.026362,0.691133,0.07327,0.209235}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'LANTHANUM_OXYBROMIDE',6.28,439.7,3,'{8,35,57}','{0.068138,0.340294,0.591568}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'LANTHANUM_OXYSULFIDE',5.86,421.2,3,'{8,16,57}','{0.0936,0.093778,0.812622}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'LEAD_OXIDE',9.53,766.7,2,'{8,82}','{0.071682,0.928318}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'LITHIUM_AMIDE',1.178,55.5,3,'{1,3,7}','{0.087783,0.302262,0.609955}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'LITHIUM_CARBONATE',2.11,87.9,3,'{3,6,8}','{0.187871,0.16255,0.649579}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'LITHIUM_FLUORIDE',2.635,94,2,'{3,9}','{0.267585,0.732415}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'LITHIUM_HYDRIDE',0.82,36.5,2,'{1,3}','{0.126797,0.873203}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'LITHIUM_IODIDE',3.494,485.1,2,'{3,53}','{0.051858,0.948142}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'LITHIUM_OXIDE',2.013,73.6,2,'{3,8}','{0.46457,0.53543}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'LITHIUM_TETRABORATE',2.44,94.6,3,'{3,5,8}','{0.082085,0.25568,0.662235}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'LUNG_ICRP',1.05,75.3,13,'{1,6,7,8,11,12,15,16,17,19,20,26,30}','{0.101278,0.10231,0.02865,0.757072,0.00184,0.00073,0.0008,0.00225,0.00266,0.00194,9e-05,0.00037,1e-05}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'M3_WAX',1.05,67.9,5,'{1,6,8,12,20}','{0.114318,0.655824,0.0921831,0.134792,0.002883}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'MAGNESIUM_CARBONATE',2.958,118,3,'{6,8,12}','{0.142455,0.569278,0.288267}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'MAGNESIUM_FLUORIDE',3,134.3,2,'{9,12}','{0.609883,0.390117}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'MAGNESIUM_OXIDE',3.58,143.8,2,'{8,12}','{0.396964,0.603036}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'MAGNESIUM_TETRABORATE',2.53,108.3,3,'{5,8,12}','{0.240837,0.62379,0.135373}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'MERCURIC_IODIDE',6.36,684.5,2,'{53,80}','{0.55856,0.44144}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'METHANE',0.000667151,41.7,2,'{1,6}','{0.251306,0.748694}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'METHANOL',0.7914,67.6,3,'{1,6,8}','{0.125822,0.374852,0.499326}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'MIX_D_WAX',0.99,60.9,5,'{1,6,8,12,22}','{0.13404,0.77796,0.03502,0.038594,0.014386}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'MS20_TISSUE',1,75.1,6,'{1,6,7,8,12,17}','{0.081192,0.583442,0.017798,0.186381,0.130287,0.0009}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'MUSCLE_SKELETAL_ICRP',1.04,75.3,13,'{1,6,7,8,11,12,15,16,17,19,20,26,30}','{0.100637,0.10783,0.02768,0.754773,0.00075,0.00019,0.0018,0.00241,0.00079,0.00302,3e-05,4e-05,5e-05}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'MUSCLE_STRIATED_ICRU',1.04,74.7,9,'{1,6,7,8,11,12,15,16,19}','{0.101997,0.123,0.035,0.729003,0.0008,0.0002,0.002,0.005,0.003}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'MUSCLE_WITH_SUCROSE',1.11,74.3,4,'{1,6,7,8}','{0.0982341,0.156214,0.035451,0.710101}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'MUSCLE_WITHOUT_SUCROSE',1.07,74.2,4,'{1,6,7,8}','{0.101969,0.120058,0.035451,0.742522}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'NAPHTHALENE',1.145,68.4,2,'{1,6}','{0.062909,0.937091}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'NITROBENZENE',1.19867,75.8,4,'{1,6,7,8}','{0.040935,0.585374,0.113773,0.259918}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'NITROUS_OXIDE',0.00183094,84.9,2,'{7,8}','{0.636483,0.363517}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'NYLON-8062',1.08,64.3,4,'{1,6,7,8}','{0.103509,0.648416,0.0995361,0.148539}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'NYLON-6/6',1.14,63.9,4,'{1,6,7,8}','{0.097976,0.636856,0.123779,0.141389}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'NYLON-6/10',1.14,63.2,4,'{1,6,7,8}','{0.107062,0.680449,0.099189,0.1133}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'NYLON-11_RILSAN',1.425,61.6,4,'{1,6,7,8}','{0.115476,0.720818,0.0764169,0.0872889}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'OCTANE',0.7026,54.7,2,'{1,6}','{0.158821,0.841179}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'PARAFFIN',0.93,55.9,2,'{1,6}','{0.148605,0.851395}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'N-PENTANE',0.6262,53.6,2,'{1,6}','{0.167635,0.832365}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'PHOTO_EMULSION',3.815,331,8,'{1,6,7,8,16,35,47,53}','{0.0141,0.072261,0.01932,0.066101,0.00189,0.349103,0.474105,0.00312}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'PLASTIC_SC_VINYLTOLUENE',1.032,64.7,2,'{1,6}','{0.085,0.915}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'PLUTONIUM_DIOXIDE',11.46,746.5,2,'{8,94}','{0.118055,0.881945}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POLYACRYLONITRILE',1.17,69.6,3,'{1,6,7}','{0.0569829,0.679055,0.263962}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POLYCARBONATE',1.2,73.1,3,'{1,6,8}','{0.055491,0.755751,0.188758}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POLYCHLOROSTYRENE',1.3,81.7,3,'{1,6,17}','{0.061869,0.696325,0.241806}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POLYETHYLENE',0.94,57.42,2,'{1,6}','{0.143711,0.856289}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'MYLAR',1.4,78.7,3,'{1,6,8}','{0.041959,0.625016,0.333025}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'PLEXIGLASS',1.19,74,3,'{1,6,8}','{0.080538,0.599848,0.319614}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POLYOXYMETHYLENE',1.425,77.4,3,'{1,6,8}','{0.067135,0.400017,0.532848}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POLYPROPYLENE',0.9,56.5,2,'{1,6}','{0.143711,0.856289}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POLYSTYRENE',1.06,68.7,2,'{1,6}','{0.077418,0.922582}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'TEFLON',2.2,99.1,2,'{6,9}','{0.240183,0.759817}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POLYTRIFLUOROCHLOROETHYLENE',2.1,120.7,3,'{6,9,17}','{0.20625,0.489354,0.304395}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POLYVINYL_ACETATE',1.19,73.7,3,'{1,6,8}','{0.070245,0.558066,0.371689}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POLYVINYL_ALCOHOL',1.3,69.7,3,'{1,6,8}','{0.091517,0.545298,0.363185}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POLYVINYL_BUTYRAL',1.12,67.2,3,'{1,6,8}','{0.092802,0.680561,0.226637}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POLYVINYL_CHLORIDE',1.3,108.2,3,'{1,6,17}','{0.04838,0.38436,0.56726}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POLYVINYLIDENE_CHLORIDE',1.7,134.3,3,'{1,6,17}','{0.020793,0.247793,0.731414}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POLYVINYLIDENE_FLUORIDE',1.76,88.8,3,'{1,6,9}','{0.03148,0.375141,0.593379}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POLYVINYL_PYRROLIDONE',1.25,67.7,4,'{1,6,7,8}','{0.081616,0.648407,0.126024,0.143953}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POTASSIUM_IODIDE',3.13,431.9,2,'{19,53}','{0.235528,0.764472}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'POTASSIUM_OXIDE',2.32,189.9,2,'{8,19}','{0.169852,0.830148}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'PROPANE',0.00187939,47.1,2,'{1,6}','{0.182855,0.817145}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'lPROPANE',0.43,52,2,'{1,6}','{0.182855,0.817145}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'N-PROPYL_ALCOHOL',0.8035,61.1,3,'{1,6,8}','{0.134173,0.599595,0.266232}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'PYRIDINE',0.9819,66.2,3,'{1,6,7}','{0.06371,0.759217,0.177073}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'RUBBER_BUTYL',0.92,56.5,2,'{1,6}','{0.143711,0.856289}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'RUBBER_NATURAL',0.92,59.8,2,'{1,6}','{0.118371,0.881629}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'RUBBER_NEOPRENE',1.23,93,3,'{1,6,17}','{0.05692,0.542646,0.400434}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'SILICON_DIOXIDE',2.32,139.2,2,'{8,14}','{0.532565,0.467435}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'SILVER_BROMIDE',6.473,486.6,2,'{35,47}','{0.425537,0.574463}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'SILVER_CHLORIDE',5.56,398.4,2,'{17,47}','{0.247368,0.752632}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'SILVER_HALIDES',6.47,487.1,3,'{35,47,53}','{0.422895,0.573748,0.003357}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'SILVER_IODIDE',6.01,543.5,2,'{47,53}','{0.459458,0.540542}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'SKIN_ICRP',1.1,72.7,13,'{1,6,7,8,11,12,15,16,17,19,20,26,30}','{0.100588,0.22825,0.04642,0.619002,7e-05,6e-05,0.00033,0.00159,0.00267,0.00085,0.00015,1e-05,1e-05}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'SODIUM_CARBONATE',2.532,125,3,'{6,8,11}','{0.113323,0.452861,0.433815}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'SODIUM_IODIDE',3.667,452,2,'{11,53}','{0.153373,0.846627}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'SODIUM_MONOXIDE',2.27,148.8,2,'{8,11}','{0.258143,0.741857}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'SODIUM_NITRATE',2.261,114.6,3,'{7,8,11}','{0.164795,0.56472,0.270485}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'STILBENE',0.9707,67.7,2,'{1,6}','{0.067101,0.932899}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'SUCROSE',1.5805,77.5,3,'{1,6,8}','{0.064779,0.42107,0.514151}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'TERPHENYL',1.234,71.7,2,'{1,6}','{0.044543,0.955457}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'TESTES_ICRP',1.04,75,13,'{1,6,7,8,11,12,15,16,17,19,20,26,30}','{0.104166,0.09227,0.01994,0.773884,0.00226,0.00011,0.00125,0.00146,0.00244,0.00208,0.0001,2e-05,2e-05}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'TETRACHLOROETHYLENE',1.625,159.2,2,'{6,17}','{0.144856,0.855144}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'THALLIUM_CHLORIDE',7.004,690.3,2,'{17,81}','{0.147822,0.852178}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'TISSUE_SOFT_ICRP',1,72.3,13,'{1,6,7,8,11,12,15,16,17,19,20,26,30}','{0.104472,0.23219,0.02488,0.630238,0.00113,0.00013,0.00133,0.00199,0.00134,0.00199,0.00023,5e-05,3e-05}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'TISSUE_SOFT_ICRU-4',1,74.9,4,'{1,6,7,8}','{0.101172,0.111,0.026,0.761828}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'TISSUE-METHANE',0.00106409,61.2,4,'{1,6,7,8}','{0.101869,0.456179,0.035172,0.40678}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'TISSUE-PROPANE',0.00182628,59.5,4,'{1,6,7,8}','{0.102672,0.56894,0.035022,0.293366}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'TITANIUM_DIOXIDE',4.26,179.5,2,'{8,22}','{0.400592,0.599408}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'TOLUENE',0.8669,62.5,2,'{1,6}','{0.08751,0.91249}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'TRICHLOROETHYLENE',1.46,148.1,3,'{1,6,17}','{0.007671,0.182831,0.809498}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'TRIETHYL_PHOSPHATE',1.07,81.2,4,'{1,6,8,15}','{0.082998,0.395628,0.351334,0.17004}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'TUNGSTEN_HEXAFLUORIDE',2.4,354.4,2,'{9,74}','{0.382723,0.617277}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'URANIUM_DICARBIDE',11.28,752,2,'{6,92}','{0.091669,0.908331}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'URANIUM_MONOCARBIDE',13.63,862,2,'{6,92}','{0.048036,0.951964}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'URANIUM_OXIDE',10.96,720.6,2,'{8,92}','{0.118502,0.881498}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'UREA',1.323,72.8,4,'{1,6,7,8}','{0.067131,0.199999,0.466459,0.266411}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'VALINE',1.23,67.7,4,'{1,6,7,8}','{0.0946409,0.512644,0.119565,0.27315}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'VITON',1.8,98.6,3,'{1,6,9}','{0.009417,0.280555,0.710028}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'WATER',1,75.,2,'{1,8}','{0.111894,0.888106}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'WATER_VAPOR',0.000756182,71.6,2,'{1,8}','{0.111894,0.888106}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'XYLENE',0.87,61.8,2,'{1,6}','{0.094935,0.905065}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'GRAPHITE',1.7,78.,1,'{6}','{1}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'lH2',0.0708,21.8,1,'{1}','{1}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'lAr',1.396,188,1,'{18}','{1}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'lKr',2.418,352,1,'{36}','{1}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'lXe',2.953,482,1,'{54}','{1}');
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (null,'PbWO4',8.28,0,3,'{8,82,74}','{0.140637,0.455366,0.403998}');
---
--- some Isotopes:
---
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (3,'Li6',0.534,40,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (3,'Li7',0.534,40,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (50,'Sn120',7.31,488,Null,Null,Null);
INSERT INTO PUBLIC.MATERIAL(Z,MNAME,DENSITY,ION,NCOMP,COMP,FRAC) VALUES (82,'Pb208',11.35,823,Null,Null,Null);
---
CREATE TABLE OBSERVABLE(
   OID SERIAL PRIMARY KEY,
   ONAME  CHARACTER VARYING(50) UNIQUE
);
ALTER TABLE  OBSERVABLE OWNER TO g4valwriter;
GRANT SELECT, UPDATE, INSERT, DELETE ON OBSERVABLE TO g4valwriter;
GRANT SELECT ON OBSERVABLE TO g4valreader;

insert into PUBLIC.OBSERVABLE(ONAME) VALUES ('differential cross section dsig/dO dT');
insert into PUBLIC.OBSERVABLE(ONAME) VALUES ('momentum spectrum');
insert into PUBLIC.OBSERVABLE(ONAME) VALUES ('Cross Section');
insert into PUBLIC.OBSERVABLE(ONAME) VALUES ('average Multiplicity');
insert into PUBLIC.OBSERVABLE(ONAME) VALUES ('average transverse momentum');
insert into PUBLIC.OBSERVABLE(ONAME) VALUES ('Neutron Yield');
insert into PUBLIC.OBSERVABLE(ONAME) VALUES ('Reaction Cross Section');
insert into PUBLIC.OBSERVABLE(ONAME) VALUES ('differential cross section dsig / dtheta dp');
insert into PUBLIC.OBSERVABLE(ONAME) VALUES ('Lorentz-inv. cross section E(d3sig/dp3)');



CREATE TABLE REACTION(
   RID SERIAL PRIMARY KEY,
   RNAME  CHARACTER VARYING(50) UNIQUE
);
ALTER TABLE  REACTION OWNER TO g4valwriter;
GRANT SELECT, UPDATE, INSERT, DELETE ON REACTION  TO g4valwriter;
GRANT SELECT ON REACTION TO g4valreader;

insert into PUBLIC.REACTION(RNAME) VALUES ('particle production');
insert into PUBLIC.REACTION(RNAME) VALUES ('capture');
insert into PUBLIC.REACTION(RNAME) VALUES ('scattering');
insert into PUBLIC.REACTION(RNAME) VALUES ('nuclear interaction');
---
--- The following table should evolve as we will deal with test beam setups etc. 
---
/*
CREATE TABLE BEAM(
   BID      INTEGER PRIMARY KEY,
   PARTICLE INTEGER REFERENCES PARTICLE(PDGID),     
   ENERGY   REAL                              -- kinetic Energy in MeV
);
ALTER TABLE  BEAM OWNER TO g4valwriter;
GRANT SELECT, UPDATE, INSERT, DELETE ON BEAM  TO g4valwriter;
GRANT SELECT ON BEAM TO g4valreader;
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (1,2112,542);      ----- Franz Neutron beam 
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (2,2112,477);      ----- Franz Neutron beam 
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (3,2112,425);      ----- Franz Neutron beam 
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (4,2112,383);      ----- Franz Neutron beam 
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (5,2112,347.7);    ----- Franz Neutron beam 
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (6,2112,317.4);    ----- Franz Neutron beam 
---
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (7,2212,158000);   ----- NA49 158GeV proton beam
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (8,-211,0);        ----- Madey beam of stopped pion-
---
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (9,211,89);          ----- Clough et al beam  pion+
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (10,211,115);        ----- Clough et al beam  pion+
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (11,211,128);        ----- Clough et al beam  pion+
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (12,211,156);        ----- Clough et al beam  pion+
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (13,211,187);        ----- Clough et al beam  pion+
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (14,211,229);        ----- Clough et al beam  pion+
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (15,211,259);        ----- Clough et al beam  pion+
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (16,211,289);        ----- Clough et al beam  pion+
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (17,211,337);        ----- Clough et al beam  pion+
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (18,211,409);        ----- Clough et al beam  pion+
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (19,211,482);        ----- Clough et al beam  pion+
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (20,211,567);        ----- Clough et al beam  pion+
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (21,211,696);        ----- Clough et al beam  pion+
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (22,211,855);        ----- Clough et al beam  pion+
---
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (23,-211,89);         ----- Clough et al beam  pion-
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (24,-211,115);        ----- Clough et al beam  pion-
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (25,-211,128);        ----- Clough et al beam  pion-
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (26,-211,156);        ----- Clough et al beam  pion-
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (27,-211,187);        ----- Clough et al beam  pion-
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (28,-211,229);        ----- Clough et al beam  pion-
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (29,-211,259);        ----- Clough et al beam  pion-
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (30,-211,289);        ----- Clough et al beam  pion-
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (31,-211,337);        ----- Clough et al beam  pion-
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (32,-211,409);        ----- Clough et al beam  pion-
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (33,-211,482);        ----- Clough et al beam  pion-
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (34,-211,567);        ----- Clough et al beam  pion-
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (35,-211,696);        ----- Clough et al beam  pion-
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (36,-211,855);        ----- Clough et al beam  pion-
---
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (37,2212,3000);        ----- Harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (38,2212,5000);        ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (39,2212,8000);        ----- Harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (40,2212,8900);        ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (41,2212,12000);       ----- Harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (42,211,3000);         ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (43,211,5000);         ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (44,211,8000);         ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (45,211,12000);        ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (46,-211,3000);        ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (47,-211,5000);        ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (48,-211,8000);        ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (49,-211,12000);       ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (50,2212,1000);        ----- Harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (51,2212,1400);        ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (52,2212,2000);        ----- Harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (53,2212,6000);        ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (54,2212,6250);        ----- Harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (55,2212,6500);        ----- Harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (56,2212,7000);        ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (57,2212,7500);        ----- Harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (58,2212,8250);        ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (59,2212,8500);        ----- Harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (60,2212,9000);        ----- Harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (61,211,1000);         ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (62,211,1400);         ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (63,211,2000);         ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (64,211,4000);         ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (65,211,6000);         ----- harp
INSERT INTO PUBLIC.BEAM(BID,PARTICLE,ENERGY) VALUES (66,-211,1400);        ----- harp
*/
---
--- The following table shows evolve as we will deal with test beam setups etc. 
---
CREATE TABLE BEAM(
   BID          INTEGER PRIMARY KEY,
   BNAME        CHARACTER VARYING(50),                -- name of the Beam or flux file  
   REFID        INTEGER REFERENCES REFERENCE(REFID),  -- reference where beam is described
   PARTICLEIDS  INTEGER[],                            -- Particle Content of Beam (each references PARTICLE(PDGID))
   MEANENERGY   REAL[],                               -- kinetic Energy in MeV for each component
   DATATABLEIDS INTEGER[]                             -- pointer to histograms with fluxes/particle contribution each REFERENCES DATATABLE(DTID),
                                                      -- can be null  
);
ALTER TABLE  BEAM OWNER TO g4valwriter;
GRANT SELECT, UPDATE, INSERT, DELETE ON BEAM  TO g4valwriter;
GRANT SELECT ON BEAM TO g4valreader;
INSERT INTO PUBLIC.BEAM(BID,BNAME,REFID,PARTICLEIDS,MEANENERGY)  VALUES (1,'SIN Neutron beam',20,'{2112}','{542}');     ----- Franz Neutron beam 
INSERT INTO PUBLIC.BEAM(BID,BNAME,REFID,PARTICLEIDS,MEANENERGY)  VALUES (2,'SIN Neutron beam',20,'{2112}','{477}');     ----- Franz Neutron beam 
INSERT INTO PUBLIC.BEAM(BID,BNAME,REFID,PARTICLEIDS,MEANENERGY)  VALUES (3,'SIN Neutron beam',20,'{2112}','{425}');     ----- Franz Neutron beam 
INSERT INTO PUBLIC.BEAM(BID,BNAME,REFID,PARTICLEIDS,MEANENERGY)  VALUES (4,'SIN Neutron beam',20,'{2112}','{383}');     ----- Franz Neutron beam 
INSERT INTO PUBLIC.BEAM(BID,BNAME,REFID,PARTICLEIDS,MEANENERGY)  VALUES (5,'SIN Neutron beam',20,'{2112}','{347.7}');   ----- Franz Neutron beam 
INSERT INTO PUBLIC.BEAM(BID,BNAME,REFID,PARTICLEIDS,MEANENERGY)  VALUES (6,'SIN Neutron beam',20,'{2112}','{317.4}');   ----- Franz Neutron beam 
---
INSERT INTO PUBLIC.BEAM(BID,BNAME,REFID,PARTICLEIDS,MEANENERGY)  VALUES (7,'CERN 158 GeV proton beam',41,'{2212}','{158000}');  ----- NA49 158GeV proton beam
INSERT INTO PUBLIC.BEAM(BID,BNAME,REFID,PARTICLEIDS,MEANENERGY)  VALUES (8,'Nevis Laboratories synchrocyclotron',7,'{-211}','{0}');       ----- Madey beam of stopped pion-
---
INSERT INTO PUBLIC.BEAM(BID,BNAME,REFID,PARTICLEIDS)  VALUES ( 9,'Nimrod Rutherford/sec. beam',6,'{211}');        ----- Clough et al beam  pion+
---
INSERT INTO PUBLIC.BEAM(BID,BNAME,REFID,PARTICLEIDS)  VALUES (23,'Nimrod Rutherford/sec. beam',6,'{-211}');        ----- Clough et al beam  pion-
---
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (37,'CERN PS','{2212}','{3000}');       ----- Harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (38,'CERN PS','{2212}','{5000}');       ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (39,'CERN PS','{2212}','{8000}');       ----- Harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (40,'CERN PS','{2212}','{8900}');       ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (41,'CERN PS','{2212}','{12000}');      ----- Harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (42,'CERN PS','{211}','{3000}');        ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (43,'CERN PS','{211}','{5000}');        ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (44,'CERN PS','{211}','{8000}');        ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (45,'CERN PS','{211}','{12000}');       ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (46,'CERN PS','{-211}','{3000}');       ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (47,'CERN PS','{-211}','{5000}');       ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (48,'CERN PS','{-211}','{8000}');       ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (49,'CERN PS','{-211}','{12000}');      ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (50,'CERN PS','{2212}','{1000}');       ----- Harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (51,'CERN PS','{2212}','{1400}');       ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (52,'CERN PS','{2212}','{2000}');       ----- Harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (53,'CERN PS','{2212}','{6000}');       ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (54,'CERN PS','{2212}','{6250}');       ----- Harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (55,'CERN PS','{2212}','{6500}');       ----- Harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (56,'CERN PS','{2212}','{7000}');       ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (57,'CERN PS','{2212}','{7500}');       ----- Harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (58,'CERN PS','{2212}','{8250}');       ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (59,'CERN PS','{2212}','{8500}');       ----- Harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (60,'CERN PS','{2212}','{9000}');       ----- Harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (61,'CERN PS','{211}','{1000}');        ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (62,'CERN PS','{211}','{1400}');        ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (63,'CERN PS','{211}','{2000}');        ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (64,'CERN PS','{211}','{4000}');        ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (65,'CERN PS','{211}','{6000}');        ----- harp
INSERT INTO PUBLIC.BEAM(BID,BNAME,PARTICLEIDS,MEANENERGY)  VALUES (66,'CERN PS','{-211}','{1400}');       ----- harp
---
--- Now add the NuMI neutrino flux files
---

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,BIN_MIN,BIN_MAX) VALUES (232,1,'numu_fhc','{200}','{"neutrino energy (GeV)"," #nu/m^{2}/10^{6}POT/0.5 GeV"}','{0,2.57268,6.5321,16.9721,25.1453,33.1235,40.7319,42.7611,34.1954,20.4086,11.0596,6.78507,4.86896,3.94903,3.34018,2.90956,2.5465,2.28787,2.04961,1.85345,1.69827,1.53642,1.41473,1.2813,1.18419,1.0729,0.988757,0.906115,0.842301,0.760841,0.694909,0.619462,0.578754,0.531736,0.475759,0.440441,0.402552,0.371215,0.340375,0.316818,0.290653,0.266663,0.248852,0.228189,0.211295,0.193518,0.181962,0.168424,0.151371,0.136651,0.123746,0.112641,0.102157,0.0978681,0.0939489,0.0896216,0.0863609,0.083713,0.0778946,0.0754906,0.074479,0.0710892,0.0684686,0.0656285,0.0630613,0.060361,0.0591554,0.0570593,0.0546286,0.0534604,0.0538198,0.0523749,0.0526842,0.0508142,0.0509157,0.0513228,0.0528484,0.0519899,0.0513883,0.0521457,0.0528112,0.0530977,0.052726,0.050804,0.0472128,0.0470125,0.0445495,0.0435043,0.041248,0.0403854,0.0397383,0.0377318,0.0372685,0.0330514,0.0333536,0.0331484,0.0323664,0.0306515,0.0299567,0.0288373,0.0252492,0.0228493,0.0214966,0.0214054,0.0204719,0.0188888,0.0171903,0.0168044,0.0151954,0.0128583,0.0122657,0.0110181,0.00987062,0.00860311,0.0074285,0.00643831,0.00612104,0.0056251,0.00548342,0.005348,0.00492845,0.00471218,0.00432594,0.00444651,0.00371671,0.003608,0.00344085,0.00320035,0.00328527,0.00295966,0.00272372,0.00259608,0.00252757,0.00260708,0.00248182,0.00226274,0.00200334,0.00191054,0.00190403,0.00172433,0.00162464,0.00176832,0.00152054,0.00130456,0.00136153,0.00130553,0.00109509,0.00120415,0.000782535,0.000973128,0.000970957,0.000863651,0.000856531,0.000697046,0.000788551,0.000707275,0.000618628,0.000495731,0.000508559,0.00052077,0.000488449,0.000436796,0.000384988,0.000381566,0.000304227,0.000348949,0.00030981,0.000278037,0.000287612,0.000187706,0.000270415,0.000195838,0.000251095,0.000177407,0.000206108,0.000196183,0.000162436,0.00013071,0.000151658,0.00018873,0.000113568,0.000123293,0.000124276,8.74646e-05,0.000104067,6.43681e-05,6.58933e-05,7.45094e-05,5.98641e-05,4.79666e-05,4.26609e-05,3.71747e-05,4.82427e-05,1.36327e-05,2.24881e-05,2.78917e-05,3.31587e-05,2.72674e-05,3.84084e-05,4.13537e-05}',
'{0,0.350952,0.673438,1.27415,1.79693,2.42111,2.84533,2.70913,2.22865,1.76663,1.08774,0.579456,0.395314,0.333253,0.29198,0.256603,0.223991,0.199597,0.178813,0.160832,0.146273,0.129843,0.118528,0.106019,0.0981789,0.0879123,0.0811143,0.0734427,0.0677211,0.0612885,0.0556397,0.0498119,0.0465579,0.0430023,0.038119,0.0357784,0.0326242,0.0307699,0.0287312,0.0271813,0.0257387,0.0245712,0.023842,0.022635,0.0212063,0.0204245,0.0203929,0.0202827,0.0192149,0.0184629,0.0179727,0.0175124,0.0168151,0.0157063,0.0144337,0.0133978,0.0119558,0.0109317,0.00987732,0.00926077,0.00896187,0.00843304,0.00810675,0.00765039,0.0072913,0.00686654,0.0068391,0.00666456,0.006298,0.00627476,0.0066237,0.00669188,0.00715303,0.00743847,0.00798091,0.00859227,0.00969454,0.0100799,0.0107363,0.0113574,0.0119387,0.0123752,0.0123491,0.0118762,0.0110498,0.011002,0.0104797,0.01032,0.00959253,0.00944182,0.00928298,0.0087988,0.00873341,0.0076722,0.00773475,0.00765541,0.00747829,0.00714262,0.00693098,0.00648448,0.00565239,0.00498176,0.00461015,0.00465617,0.00431676,0.00388347,0.00345284,0.00329029,0.00288394,0.00242193,0.00235522,0.00212353,0.00198295,0.00193312,0.00190604,0.00187355,0.0018613,0.00171014,0.00169053,0.00164802,0.00147262,0.00144312,0.00133995,0.00134343,0.00116917,0.00114048,0.0010715,0.00096154,0.00104205,0.00086849,0.000855772,0.000781233,0.000768566,0.000783756,0.000700795,0.000697625,0.000611315,0.000582407,0.000541873,0.000513105,0.000458556,0.000488977,0.000430046,0.000370528,0.000394966,0.000380301,0.000308217,0.000346307,0.000226431,0.000268442,0.000252723,0.000236529,0.00022041,0.00019335,0.000185528,0.000169954,0.000165651,0.000125678,0.000128638,0.00012421,0.000125136,0.000106582,9.8576e-05,9.52779e-05,7.67668e-05,8.82048e-05,8.32471e-05,8.12784e-05,8.24813e-05,5.50877e-05,8.46936e-05,6.53466e-05,8.46726e-05,6.58143e-05,7.60785e-05,7.18624e-05,5.95557e-05,4.79351e-05,5.56621e-05,6.95147e-05,4.22199e-05,4.52253e-05,4.55094e-05,3.21162e-05,3.80313e-05,2.41056e-05,2.40961e-05,2.71408e-05,2.18279e-05,1.74437e-05,1.56187e-05,1.3763e-05,1.77373e-05,5.02307e-06,8.4546e-06,1.03077e-05,1.21706e-05,9.97847e-06,1.41075e-05,1.53032e-05}',
'{0,0.350952,0.673438,1.27415,1.79693,2.42111,2.84533,2.70913,2.22865,1.76663,1.08774,0.579456,0.395314,0.333253,0.29198,0.256603,0.223991,0.199597,0.178813,0.160832,0.146273,0.129843,0.118528,0.106019,0.0981789,0.0879123,0.0811143,0.0734427,0.0677211,0.0612885,0.0556397,0.0498119,0.0465579,0.0430023,0.038119,0.0357784,0.0326242,0.0307699,0.0287312,0.0271813,0.0257387,0.0245712,0.023842,0.022635,0.0212063,0.0204245,0.0203929,0.0202827,0.0192149,0.0184629,0.0179727,0.0175124,0.0168151,0.0157063,0.0144337,0.0133978,0.0119558,0.0109317,0.00987732,0.00926077,0.00896187,0.00843304,0.00810675,0.00765039,0.0072913,0.00686654,0.0068391,0.00666456,0.006298,0.00627476,0.0066237,0.00669188,0.00715303,0.00743847,0.00798091,0.00859227,0.00969454,0.0100799,0.0107363,0.0113574,0.0119387,0.0123752,0.0123491,0.0118762,0.0110498,0.011002,0.0104797,0.01032,0.00959253,0.00944182,0.00928298,0.0087988,0.00873341,0.0076722,0.00773475,0.00765541,0.00747829,0.00714262,0.00693098,0.00648448,0.00565239,0.00498176,0.00461015,0.00465617,0.00431676,0.00388347,0.00345284,0.00329029,0.00288394,0.00242193,0.00235522,0.00212353,0.00198295,0.00193312,0.00190604,0.00187355,0.0018613,0.00171014,0.00169053,0.00164802,0.00147262,0.00144312,0.00133995,0.00134343,0.00116917,0.00114048,0.0010715,0.00096154,0.00104205,0.00086849,0.000855772,0.000781233,0.000768566,0.000783756,0.000700795,0.000697625,0.000611315,0.000582407,0.000541873,0.000513105,0.000458556,0.000488977,0.000430046,0.000370528,0.000394966,0.000380301,0.000308217,0.000346307,0.000226431,0.000268442,0.000252723,0.000236529,0.00022041,0.00019335,0.000185528,0.000169954,0.000165651,0.000125678,0.000128638,0.00012421,0.000125136,0.000106582,9.8576e-05,9.52779e-05,7.67668e-05,8.82048e-05,8.32471e-05,8.12784e-05,8.24813e-05,5.50877e-05,8.46936e-05,6.53466e-05,8.46726e-05,6.58143e-05,7.60785e-05,7.18624e-05,5.95557e-05,4.79351e-05,5.56621e-05,6.95147e-05,4.22199e-05,4.52253e-05,4.55094e-05,3.21162e-05,3.80313e-05,2.41056e-05,2.40961e-05,2.71408e-05,2.18279e-05,1.74437e-05,1.56187e-05,1.3763e-05,1.77373e-05,5.02307e-06,8.4546e-06,1.03077e-05,1.21706e-05,9.97847e-06,1.41075e-05,1.53032e-05}',
'{-0.5,0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99}',
'{0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99,99.5}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,BIN_MIN,BIN_MAX) VALUES (233,1,'numubar_fhc','{200}','{"neutrino energy (GeV)"," #nu/m^{2}/10^{6}POT/0.5 GeV"}','{0,1.18001,1.70358,1.67247,1.768,1.77342,1.78248,1.70537,1.61004,1.46013,1.34207,1.29089,1.24436,1.20582,1.12898,1.06604,0.972489,0.911417,0.819305,0.740326,0.67632,0.616516,0.554209,0.510262,0.452763,0.410963,0.373273,0.337387,0.303872,0.271809,0.239525,0.219168,0.195296,0.17695,0.161226,0.145433,0.125517,0.116903,0.108555,0.0949622,0.0871153,0.0808685,0.0703825,0.0628248,0.0586383,0.0529007,0.0461846,0.0434702,0.0404942,0.0366842,0.0330834,0.0311727,0.0267126,0.0245618,0.02312,0.0209426,0.0200144,0.0183736,0.0173779,0.0157021,0.0150053,0.0139086,0.0129454,0.0124031,0.0113694,0.01046,0.010488,0.0101671,0.00865235,0.00797364,0.00863543,0.00703883,0.00732776,0.00683083,0.00933398,0.00604794,0.00579274,0.00529108,0.00445162,0.00451514,0.00360658,0.00396321,0.00337285,0.00341973,0.00336851,0.00285991,0.00291622,0.0028289,0.0026177,0.00288349,0.00253547,0.00219857,0.00236423,0.00224046,0.002305,0.00212155,0.00209673,0.00191884,0.00201021,0.00182269,0.00153807,0.0016069,0.00135921,0.0014426,0.0012286,0.00145636,0.00122315,0.00132124,0.00118907,0.00101884,0.00115272,0.000869833,0.000834733,0.000682615,0.000697644,0.000666361,0.000715068,0.000610581,0.000519174,0.000868696,0.000478149,0.000447694,0.000452018,0.000410329,0.000261847,0.000367987,0.000345309,0.000251598,0.00025464,0.000213141,0.000245878,0.00038092,0.000282933,0.000192922,0.000230176,0.00017662,0.000141891,0.000106594,0.00012177,0.000169524,8.08563e-05,0.000162434,0.000103901,9.21105e-05,0.000141575,0.000114485,0.000153919,7.71236e-05,5.28933e-05,0.000126048,9.94781e-05,7.36464e-05,2.7913e-05,3.28362e-05,8.01253e-05,6.4877e-05,5.62119e-05,4.21734e-05,7.12872e-06,1.41892e-05,3.56471e-05,7.22127e-06,0,7.27701e-06,0,0,7.037e-06,7.08305e-06,0,6.98071e-06,0,0,6.75809e-06,0,6.68455e-06,0,0,1.33391e-05,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{0,0.194264,0.2688,0.238751,0.224363,0.212639,0.201814,0.187307,0.181868,0.187451,0.18088,0.156444,0.131195,0.117654,0.105046,0.097615,0.0876929,0.083201,0.0746653,0.0663992,0.0594021,0.0544525,0.0498387,0.0443732,0.0402769,0.0361362,0.0314662,0.0293166,0.0271118,0.0238397,0.0224134,0.0198656,0.0181378,0.0156886,0.0154827,0.0140377,0.012886,0.0119717,0.0114077,0.0102152,0.0102646,0.0101064,0.00876583,0.00814561,0.00820225,0.00807607,0.00722005,0.00718403,0.00677416,0.00739901,0.00665829,0.00655121,0.00611115,0.00536819,0.00461553,0.00417559,0.00380581,0.00343093,0.00311052,0.0027014,0.00268677,0.0021492,0.00195167,0.00194913,0.00178173,0.00157657,0.0015803,0.00152007,0.00128064,0.0012399,0.00136778,0.00113791,0.00120935,0.0011386,0.00330772,0.000989075,0.0010678,0.00100671,0.000931805,0.00108804,0.000916457,0.00106598,0.000878768,0.000926846,0.000922602,0.000794956,0.000803336,0.000776776,0.000662834,0.000836046,0.000681862,0.000623487,0.000659134,0.000615072,0.000644163,0.000584486,0.000525568,0.0005294,0.000503176,0.000454676,0.000375316,0.000398539,0.000317496,0.00034804,0.000291386,0.000352078,0.000303116,0.000328629,0.000275232,0.000248173,0.000272485,0.000220186,0.000236734,0.000201204,0.000213572,0.000220528,0.000229691,0.000220679,0.000180766,0.00024287,0.000165725,0.000155664,0.000165196,0.000151155,9.55755e-05,0.000133687,0.000111147,9.41893e-05,8.29054e-05,6.83367e-05,8.32545e-05,0.000137888,9.01739e-05,6.78915e-05,8.64109e-05,6.51495e-05,5.12589e-05,3.8919e-05,3.76724e-05,6.30918e-05,2.70502e-05,5.55174e-05,3.23664e-05,3.22676e-05,5.95713e-05,6.9075e-05,6.47799e-05,3.67343e-05,2.03589e-05,7.76323e-05,4.37289e-05,3.79665e-05,1.0533e-05,9.65978e-06,6.28811e-05,6.17999e-05,6.29927e-05,6.15954e-05,2.5156e-06,6.36894e-06,1.08226e-05,2.69593e-06,0,2.72568e-06,0,0,2.66166e-06,2.65303e-06,0,2.6147e-06,0,0,2.49937e-06,0,2.47217e-06,0,0,4.99632e-06,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{0,0.194264,0.2688,0.238751,0.224363,0.212639,0.201814,0.187307,0.181868,0.187451,0.18088,0.156444,0.131195,0.117654,0.105046,0.097615,0.0876929,0.083201,0.0746653,0.0663992,0.0594021,0.0544525,0.0498387,0.0443732,0.0402769,0.0361362,0.0314662,0.0293166,0.0271118,0.0238397,0.0224134,0.0198656,0.0181378,0.0156886,0.0154827,0.0140377,0.012886,0.0119717,0.0114077,0.0102152,0.0102646,0.0101064,0.00876583,0.00814561,0.00820225,0.00807607,0.00722005,0.00718403,0.00677416,0.00739901,0.00665829,0.00655121,0.00611115,0.00536819,0.00461553,0.00417559,0.00380581,0.00343093,0.00311052,0.0027014,0.00268677,0.0021492,0.00195167,0.00194913,0.00178173,0.00157657,0.0015803,0.00152007,0.00128064,0.0012399,0.00136778,0.00113791,0.00120935,0.0011386,0.00330772,0.000989075,0.0010678,0.00100671,0.000931805,0.00108804,0.000916457,0.00106598,0.000878768,0.000926846,0.000922602,0.000794956,0.000803336,0.000776776,0.000662834,0.000836046,0.000681862,0.000623487,0.000659134,0.000615072,0.000644163,0.000584486,0.000525568,0.0005294,0.000503176,0.000454676,0.000375316,0.000398539,0.000317496,0.00034804,0.000291386,0.000352078,0.000303116,0.000328629,0.000275232,0.000248173,0.000272485,0.000220186,0.000236734,0.000201204,0.000213572,0.000220528,0.000229691,0.000220679,0.000180766,0.00024287,0.000165725,0.000155664,0.000165196,0.000151155,9.55755e-05,0.000133687,0.000111147,9.41893e-05,8.29054e-05,6.83367e-05,8.32545e-05,0.000137888,9.01739e-05,6.78915e-05,8.64109e-05,6.51495e-05,5.12589e-05,3.8919e-05,3.76724e-05,6.30918e-05,2.70502e-05,5.55174e-05,3.23664e-05,3.22676e-05,5.95713e-05,6.9075e-05,6.47799e-05,3.67343e-05,2.03589e-05,7.76323e-05,4.37289e-05,3.79665e-05,1.0533e-05,9.65978e-06,6.28811e-05,6.17999e-05,6.29927e-05,6.15954e-05,2.5156e-06,6.36894e-06,1.08226e-05,2.69593e-06,0,2.72568e-06,0,0,2.66166e-06,2.65303e-06,0,2.6147e-06,0,0,2.49937e-06,0,2.47217e-06,0,0,4.99632e-06,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{-0.5,0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99}',
'{0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99,99.5}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,BIN_MIN,BIN_MAX) VALUES (234,1,'nue_fhc','{200}','{"neutrino energy (GeV)"," #nu/m^{2}/10^{6}POT/0.5 GeV"}','{0,0.0775396,0.202694,0.32261,0.384936,0.415845,0.381665,0.357537,0.307985,0.256419,0.212067,0.170515,0.142452,0.11139,0.0876123,0.0689416,0.0558481,0.0465502,0.0390126,0.0367891,0.0353903,0.0300351,0.027342,0.0258104,0.0231747,0.0212863,0.0205337,0.0186439,0.0180845,0.0167449,0.0150738,0.0134763,0.0136713,0.0136898,0.0142884,0.013224,0.0124072,0.0114532,0.00981529,0.010291,0.0101557,0.00891199,0.00918022,0.00852991,0.00787444,0.00746946,0.00758879,0.00710387,0.00753641,0.00679443,0.00622631,0.0059886,0.00585607,0.00519887,0.00468746,0.00513224,0.0044658,0.00432316,0.00415913,0.00384305,0.0035593,0.00377682,0.00365884,0.00375971,0.0030368,0.00285379,0.0029184,0.00264386,0.00235853,0.00200272,0.0028182,0.00212354,0.00205581,0.00201882,0.0021231,0.00174873,0.0015336,0.00126266,0.00120746,0.00111192,0.00115663,0.00079433,0.000646952,0.000706009,0.000742218,0.000616233,0.000585403,0.000481287,0.000390283,0.000435671,0.000494574,0.000438768,0.000428916,0.000398605,0.00019154,0.00022869,0.000321152,0.000289188,0.000187977,0.000207156,0.000136391,0.000110156,0.000248373,0.00018762,0.000163848,0.000196,0.000119494,0.000108251,0.000117566,9.11782e-05,0.000103836,8.11306e-05,5.91998e-05,8.89622e-05,5.12442e-05,5.32611e-05,1.75594e-05,3.68979e-05,4.44303e-05,4.87566e-05,4.31313e-05,3.37471e-05,4.83693e-05,3.87892e-05,8.34261e-06,3.98277e-05,1.67792e-05,2.75683e-05,7.64075e-06,4.37892e-06,2.53851e-05,1.22523e-05,1.60035e-05,3.9933e-06,1.23376e-05,1.62416e-05,1.20982e-05,1.97418e-05,1.56355e-05,1.14933e-05,7.68674e-06,7.66896e-06,7.51072e-06,3.73896e-06,7.49393e-06,7.22168e-06,0,0,3.63495e-06,1.07065e-05,1.06619e-05,1.41711e-05,3.53571e-06,1.05242e-05,0,3.46546e-06,6.9049e-06,0,0,3.38788e-06,0,3.34527e-06,3.3241e-06,0,3.29049e-06,6.52276e-06,0,0,0,9.54838e-06,3.15252e-06,0,6.23927e-06,3.09875e-06,6.16383e-06,1.53264e-05,3.0543e-06,6.06891e-06,3.00794e-06,0,5.95054e-06,0,8.81786e-06,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{0,0.011495,0.0165328,0.0218186,0.0240538,0.0264195,0.0239007,0.0212301,0.020112,0.0220619,0.0205681,0.012916,0.00834516,0.00639695,0.00475514,0.00383098,0.00301367,0.00284419,0.00238352,0.00238715,0.0026272,0.00203892,0.00192625,0.00186348,0.0017375,0.00166409,0.00177109,0.00165527,0.00154831,0.00149289,0.00140552,0.00129112,0.00128872,0.00134452,0.00137289,0.00127258,0.00128194,0.00113292,0.00107117,0.00110589,0.00114353,0.000989847,0.0010221,0.00097866,0.000964613,0.000919696,0.000893586,0.000898436,0.0010478,0.00095098,0.000847228,0.000898087,0.000879691,0.000844766,0.000774423,0.000898713,0.00072018,0.000745754,0.00072214,0.000626826,0.000570095,0.00071618,0.000644054,0.000682817,0.000555741,0.000543483,0.00058984,0.000499898,0.000500954,0.000378757,0.000609059,0.00040082,0.000395982,0.00040289,0.000385147,0.000281267,0.000291946,0.000207875,0.000203023,0.000200949,0.000199901,0.000135994,0.000110169,0.000114821,0.000121565,9.98557e-05,8.69867e-05,6.76296e-05,6.67601e-05,8.13406e-05,7.48411e-05,7.61499e-05,7.84823e-05,6.6394e-05,5.94782e-05,5.72239e-05,5.58482e-05,4.16432e-05,2.86685e-05,4.69573e-05,3.85899e-05,2.12832e-05,3.93866e-05,3.45443e-05,2.70091e-05,4.33386e-05,2.4054e-05,2.56374e-05,2.55962e-05,2.05121e-05,2.50707e-05,2.2606e-05,1.50477e-05,2.25461e-05,1.02208e-05,1.06029e-05,4.75231e-06,7.77985e-06,1.15798e-05,1.16243e-05,9.94681e-06,9.21114e-06,1.11665e-05,1.37711e-05,2.24498e-06,1.20304e-05,5.31116e-06,1.1431e-05,3.29121e-06,1.50737e-06,5.23801e-06,3.0385e-06,5.41033e-06,2.41811e-06,4.22967e-06,3.79375e-06,3.66822e-06,4.69917e-06,4.90821e-06,5.04591e-06,3.55618e-06,2.496e-06,3.02512e-06,2.02447e-06,3.46695e-06,2.53743e-06,0,0,1.96815e-06,6.48325e-06,4.67879e-06,5.70859e-06,1.91442e-06,4.32853e-06,0,1.87638e-06,3.73867e-06,0,0,1.83437e-06,0,1.8113e-06,1.79984e-06,0,1.78164e-06,3.53176e-06,0,0,0,5.16999e-06,1.70694e-06,0,3.37827e-06,1.67782e-06,3.33741e-06,8.29851e-06,1.65376e-06,3.28602e-06,1.62865e-06,0,3.22193e-06,0,4.77444e-06,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{0,0.011495,0.0165328,0.0218186,0.0240538,0.0264195,0.0239007,0.0212301,0.020112,0.0220619,0.0205681,0.012916,0.00834516,0.00639695,0.00475514,0.00383098,0.00301367,0.00284419,0.00238352,0.00238715,0.0026272,0.00203892,0.00192625,0.00186348,0.0017375,0.00166409,0.00177109,0.00165527,0.00154831,0.00149289,0.00140552,0.00129112,0.00128872,0.00134452,0.00137289,0.00127258,0.00128194,0.00113292,0.00107117,0.00110589,0.00114353,0.000989847,0.0010221,0.00097866,0.000964613,0.000919696,0.000893586,0.000898436,0.0010478,0.00095098,0.000847228,0.000898087,0.000879691,0.000844766,0.000774423,0.000898713,0.00072018,0.000745754,0.00072214,0.000626826,0.000570095,0.00071618,0.000644054,0.000682817,0.000555741,0.000543483,0.00058984,0.000499898,0.000500954,0.000378757,0.000609059,0.00040082,0.000395982,0.00040289,0.000385147,0.000281267,0.000291946,0.000207875,0.000203023,0.000200949,0.000199901,0.000135994,0.000110169,0.000114821,0.000121565,9.98557e-05,8.69867e-05,6.76296e-05,6.67601e-05,8.13406e-05,7.48411e-05,7.61499e-05,7.84823e-05,6.6394e-05,5.94782e-05,5.72239e-05,5.58482e-05,4.16432e-05,2.86685e-05,4.69573e-05,3.85899e-05,2.12832e-05,3.93866e-05,3.45443e-05,2.70091e-05,4.33386e-05,2.4054e-05,2.56374e-05,2.55962e-05,2.05121e-05,2.50707e-05,2.2606e-05,1.50477e-05,2.25461e-05,1.02208e-05,1.06029e-05,4.75231e-06,7.77985e-06,1.15798e-05,1.16243e-05,9.94681e-06,9.21114e-06,1.11665e-05,1.37711e-05,2.24498e-06,1.20304e-05,5.31116e-06,1.1431e-05,3.29121e-06,1.50737e-06,5.23801e-06,3.0385e-06,5.41033e-06,2.41811e-06,4.22967e-06,3.79375e-06,3.66822e-06,4.69917e-06,4.90821e-06,5.04591e-06,3.55618e-06,2.496e-06,3.02512e-06,2.02447e-06,3.46695e-06,2.53743e-06,0,0,1.96815e-06,6.48325e-06,4.67879e-06,5.70859e-06,1.91442e-06,4.32853e-06,0,1.87638e-06,3.73867e-06,0,0,1.83437e-06,0,1.8113e-06,1.79984e-06,0,1.78164e-06,3.53176e-06,0,0,0,5.16999e-06,1.70694e-06,0,3.37827e-06,1.67782e-06,3.33741e-06,8.29851e-06,1.65376e-06,3.28602e-06,1.62865e-06,0,3.22193e-06,0,4.77444e-06,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{-0.5,0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99}',
'{0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99,99.5}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,BIN_MIN,BIN_MAX) VALUES (235,1,'nuebar_fhc','{200}','{"neutrino energy (GeV)"," #nu/m^{2}/10^{6}POT/0.5 GeV"}','{0,0.0129121,0.0219059,0.022345,0.0270454,0.0206952,0.0206763,0.0210683,0.0188609,0.0195614,0.0185201,0.0182437,0.0164469,0.0160847,0.0152656,0.0138283,0.0147048,0.0129192,0.0128393,0.0127927,0.0107389,0.00957394,0.0101244,0.00923199,0.0091913,0.00899241,0.00724768,0.00762504,0.00708308,0.00807749,0.00740003,0.0059597,0.00537327,0.00567002,0.00575276,0.00475583,0.00491462,0.00452102,0.00399899,0.00487352,0.0036554,0.00434604,0.00363463,0.00342958,0.00342735,0.00294077,0.00279051,0.00228456,0.00260351,0.00238169,0.00278298,0.00236442,0.00190751,0.00254691,0.00216188,0.00157714,0.00171705,0.00164746,0.00127368,0.00157493,0.00135743,0.00112223,0.00135349,0.000842483,0.00107939,0.000982836,0.0010423,0.000886089,0.000810529,0.000736294,0.000885551,0.000469231,0.000749287,0.00051529,0.000731263,0.000761928,0.000572661,0.000460623,0.000588579,0.00044352,0.00038409,0.000576195,0.00024075,0.000156094,0.000305398,0.000208994,0.000291708,0.000201887,0.000178733,0.000222806,0.000184698,8.79994e-05,0.00021117,0.000200549,0.000204128,7.06619e-05,0.00014312,0.000129,0.000119392,0.000137417,0.000105404,0.000125417,7.56934e-05,3.24167e-05,4.32329e-05,9.81423e-05,5.34789e-05,1.05401e-05,0,1.05244e-05,1.02776e-05,2.00808e-05,1.03077e-05,0,0,2.96637e-05,2.9594e-05,0,0,0,8.50997e-06,0,9.19937e-06,9.60656e-06,0,9.18215e-06,9.11599e-06,0,0,0,8.83777e-06,8.78209e-06,1.71809e-05,0,0,8.77629e-06,0,0,0,0,8.44274e-06,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{0,0.00489602,0.00230251,0.00340763,0.00353409,0.00225129,0.00242798,0.00284614,0.00216477,0.00262813,0.00241226,0.00223887,0.00179088,0.00194966,0.0016961,0.0013506,0.00158668,0.00132056,0.00131719,0.00134199,0.00103965,0.000822471,0.00109797,0.000854197,0.000797043,0.0010259,0.000662683,0.000860067,0.000750209,0.00091333,0.000790002,0.000660166,0.00063779,0.000569237,0.000605071,0.000469308,0.000473822,0.000483555,0.000459008,0.000512486,0.000399647,0.000436097,0.000430833,0.000349779,0.000326786,0.000356537,0.000326569,0.000243707,0.000289557,0.000267203,0.000365024,0.000261132,0.000271317,0.00038592,0.000303008,0.000209467,0.000243773,0.000209322,0.000172434,0.00024018,0.000194408,0.000145867,0.00021007,0.000114565,0.000164167,0.000155838,0.000178018,0.000143565,0.000135689,0.000123765,0.000161731,8.09627e-05,0.000138734,0.000110125,0.000147121,0.000123927,0.000107473,9.56998e-05,0.000110379,9.44076e-05,6.3291e-05,0.000125524,6.71003e-05,3.38674e-05,5.83321e-05,4.39327e-05,5.0846e-05,4.52974e-05,3.44483e-05,6.36851e-05,3.97295e-05,3.51836e-05,5.30098e-05,3.25322e-05,6.38117e-05,2.49659e-05,2.99082e-05,2.81422e-05,2.95184e-05,2.66249e-05,2.11651e-05,4.48368e-05,2.41202e-05,1.39046e-05,1.454e-05,3.24527e-05,2.27005e-05,4.51291e-06,0,4.48282e-06,4.4005e-06,5.19852e-06,4.39053e-06,0,0,1.26642e-05,9.68795e-06,0,0,0,3.72027e-06,0,2.96692e-06,4.12523e-06,0,3.91929e-06,3.88342e-06,0,0,0,3.7723e-06,3.74853e-06,4.48522e-06,0,0,3.78927e-06,0,0,0,0,3.64526e-06,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{0,0.00489602,0.00230251,0.00340763,0.00353409,0.00225129,0.00242798,0.00284614,0.00216477,0.00262813,0.00241226,0.00223887,0.00179088,0.00194966,0.0016961,0.0013506,0.00158668,0.00132056,0.00131719,0.00134199,0.00103965,0.000822471,0.00109797,0.000854197,0.000797043,0.0010259,0.000662683,0.000860067,0.000750209,0.00091333,0.000790002,0.000660166,0.00063779,0.000569237,0.000605071,0.000469308,0.000473822,0.000483555,0.000459008,0.000512486,0.000399647,0.000436097,0.000430833,0.000349779,0.000326786,0.000356537,0.000326569,0.000243707,0.000289557,0.000267203,0.000365024,0.000261132,0.000271317,0.00038592,0.000303008,0.000209467,0.000243773,0.000209322,0.000172434,0.00024018,0.000194408,0.000145867,0.00021007,0.000114565,0.000164167,0.000155838,0.000178018,0.000143565,0.000135689,0.000123765,0.000161731,8.09627e-05,0.000138734,0.000110125,0.000147121,0.000123927,0.000107473,9.56998e-05,0.000110379,9.44076e-05,6.3291e-05,0.000125524,6.71003e-05,3.38674e-05,5.83321e-05,4.39327e-05,5.0846e-05,4.52974e-05,3.44483e-05,6.36851e-05,3.97295e-05,3.51836e-05,5.30098e-05,3.25322e-05,6.38117e-05,2.49659e-05,2.99082e-05,2.81422e-05,2.95184e-05,2.66249e-05,2.11651e-05,4.48368e-05,2.41202e-05,1.39046e-05,1.454e-05,3.24527e-05,2.27005e-05,4.51291e-06,0,4.48282e-06,4.4005e-06,5.19852e-06,4.39053e-06,0,0,1.26642e-05,9.68795e-06,0,0,0,3.72027e-06,0,2.96692e-06,4.12523e-06,0,3.91929e-06,3.88342e-06,0,0,0,3.7723e-06,3.74853e-06,4.48522e-06,0,0,3.78927e-06,0,0,0,0,3.64526e-06,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{-0.5,0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99}',
'{0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99,99.5}');

INSERT INTO PUBLIC.BEAM(BID,BNAME,REFID,PARTICLEIDS,MEANENERGY,DATATABLEIDS)  VALUES (67,'Fermilab NuMI BEAM forward horn current',54,'{14,-14,12,-12}','{4373,6540,5400,9761}','{232,233,234,235}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,BIN_MIN,BIN_MAX) VALUES (236,1,'numu','{200}','{"neutrino energy (GeV)"," #nu/m^{2}/10^{6}POT/0.5 GeV"}','{0,1.26023,1.6942,1.77732,1.88063,1.90189,1.95862,1.9001,1.81664,1.72529,1.65072,1.64261,1.7014,1.74801,1.80224,1.75573,1.72822,1.65379,1.57022,1.4705,1.36647,1.277,1.1718,1.08288,0.997897,0.918685,0.832191,0.760202,0.676686,0.642563,0.573875,0.535315,0.479052,0.444652,0.397291,0.365809,0.329912,0.31148,0.284505,0.263747,0.239432,0.221306,0.199026,0.184623,0.165959,0.155209,0.138949,0.129217,0.113346,0.108104,0.0961868,0.0882935,0.0769612,0.0731971,0.0658168,0.0663442,0.0638,0.0639769,0.0577577,0.0592572,0.0550511,0.0520964,0.0529578,0.050784,0.0469345,0.0457078,0.0455539,0.0436619,0.0414635,0.0410883,0.0408574,0.0393252,0.0412633,0.0413533,0.0424639,0.0411949,0.0429448,0.0429993,0.0420907,0.0448438,0.0450531,0.043256,0.0428603,0.0434414,0.0414464,0.0406804,0.0383021,0.0401964,0.0357288,0.0382498,0.0367162,0.0332527,0.0333865,0.0323092,0.0302014,0.0297875,0.0291151,0.0270744,0.0260975,0.0247956,0.0247284,0.022539,0.0210947,0.019913,0.0191215,0.0161958,0.0164381,0.0146308,0.0150564,0.0121959,0.0121095,0.010349,0.00977076,0.00881675,0.00756091,0.00603199,0.00612847,0.00516865,0.00494874,0.00450658,0.0051287,0.00477131,0.00406467,0.00385226,0.00377114,0.00376889,0.00345954,0.00318169,0.00309731,0.00284603,0.00284945,0.00254173,0.00227711,0.00229475,0.00214682,0.00218894,0.00180831,0.00178438,0.00180278,0.00150815,0.00165498,0.00132036,0.00135599,0.00126981,0.00137066,0.00120127,0.00116601,0.000761156,0.00092547,0.000737544,0.000864603,0.000990537,0.000742364,0.000586839,0.000524582,0.000622198,0.000567248,0.000554118,0.000480055,0.000365485,0.000422634,0.000352553,0.000438517,0.000293756,0.000286717,0.000378069,0.000349481,0.000304363,0.000294548,0.000189889,0.000201434,0.000194722,0.000145763,0.000129728,0.000130976,0.000130396,0.000137201,0.000162305,0.000108147,0.000140496,7.34238e-05,5.29814e-05,8.54313e-05,7.16084e-05,7.80628e-05,7.11643e-05,5.90759e-05,5.1197e-05,5.13478e-05,5.06742e-05,3.70379e-05,4.35614e-05,1.89099e-05,2.51344e-05,1.24883e-05,6.23071e-06,0,6.26213e-06,0,0}',
'{0,0.204234,0.294409,0.257589,0.239069,0.230145,0.225357,0.203569,0.192386,0.207498,0.207518,0.183943,0.174033,0.176193,0.177428,0.168609,0.162763,0.155856,0.145404,0.135713,0.125308,0.116759,0.105637,0.0987849,0.0892814,0.081829,0.0741132,0.0679015,0.06017,0.056852,0.0498803,0.0470792,0.0415944,0.0402779,0.0347748,0.033294,0.0289861,0.027567,0.0265808,0.0248695,0.0230515,0.0215248,0.0204206,0.0195126,0.0180531,0.0182307,0.0172722,0.0169844,0.0161597,0.0162571,0.0156941,0.0151657,0.0137534,0.0125931,0.0108579,0.0101772,0.00911833,0.00869143,0.00742525,0.00751888,0.00684037,0.00644048,0.00637387,0.00596021,0.0055821,0.00540626,0.00533003,0.00527387,0.00510602,0.0051899,0.00536124,0.00533361,0.00589017,0.00632536,0.00711517,0.00737648,0.00836955,0.00875223,0.00913008,0.0101996,0.0111891,0.0107377,0.0106087,0.0109792,0.0102389,0.010095,0.0095317,0.0100774,0.00882666,0.0095656,0.00927802,0.00837521,0.00828727,0.00812576,0.00749887,0.00732632,0.00709579,0.0064495,0.00616677,0.00578071,0.00570298,0.00511264,0.00477503,0.00435247,0.0041861,0.0033776,0.00337782,0.00288485,0.00291669,0.00232897,0.00233267,0.00196169,0.00194504,0.00194322,0.00194544,0.00171915,0.00191215,0.00163053,0.00160581,0.00146199,0.00156776,0.00143271,0.00130937,0.00119452,0.00113321,0.00113884,0.00104755,0.00105539,0.00099961,0.000910406,0.000943763,0.000816891,0.000677801,0.000691372,0.000702156,0.000640817,0.000572852,0.000598864,0.000547255,0.000516267,0.000521948,0.000445364,0.000405688,0.00036877,0.000387921,0.000344067,0.00035138,0.000234379,0.000265946,0.000209413,0.000240064,0.000272217,0.000246133,0.000152624,0.000149935,0.00017573,0.000159073,0.000140118,0.000140311,8.32411e-05,0.000107131,7.74115e-05,0.000105487,7.82522e-05,7.48754e-05,9.11043e-05,8.31182e-05,7.38662e-05,6.57455e-05,5.52577e-05,5.07988e-05,6.40157e-05,5.31859e-05,4.72259e-05,4.79612e-05,4.77387e-05,5.06485e-05,5.94791e-05,3.99507e-05,5.14873e-05,2.7288e-05,1.9423e-05,3.12414e-05,2.61816e-05,2.85271e-05,2.6006e-05,2.19138e-05,1.87495e-05,1.88346e-05,1.85179e-05,1.35789e-05,1.59364e-05,6.91698e-06,9.46978e-06,4.5992e-06,2.26131e-06,0,2.33954e-06,0,0}',
'{0,0.204234,0.294409,0.257589,0.239069,0.230145,0.225357,0.203569,0.192386,0.207498,0.207518,0.183943,0.174033,0.176193,0.177428,0.168609,0.162763,0.155856,0.145404,0.135713,0.125308,0.116759,0.105637,0.0987849,0.0892814,0.081829,0.0741132,0.0679015,0.06017,0.056852,0.0498803,0.0470792,0.0415944,0.0402779,0.0347748,0.033294,0.0289861,0.027567,0.0265808,0.0248695,0.0230515,0.0215248,0.0204206,0.0195126,0.0180531,0.0182307,0.0172722,0.0169844,0.0161597,0.0162571,0.0156941,0.0151657,0.0137534,0.0125931,0.0108579,0.0101772,0.00911833,0.00869143,0.00742525,0.00751888,0.00684037,0.00644048,0.00637387,0.00596021,0.0055821,0.00540626,0.00533003,0.00527387,0.00510602,0.0051899,0.00536124,0.00533361,0.00589017,0.00632536,0.00711517,0.00737648,0.00836955,0.00875223,0.00913008,0.0101996,0.0111891,0.0107377,0.0106087,0.0109792,0.0102389,0.010095,0.0095317,0.0100774,0.00882666,0.0095656,0.00927802,0.00837521,0.00828727,0.00812576,0.00749887,0.00732632,0.00709579,0.0064495,0.00616677,0.00578071,0.00570298,0.00511264,0.00477503,0.00435247,0.0041861,0.0033776,0.00337782,0.00288485,0.00291669,0.00232897,0.00233267,0.00196169,0.00194504,0.00194322,0.00194544,0.00171915,0.00191215,0.00163053,0.00160581,0.00146199,0.00156776,0.00143271,0.00130937,0.00119452,0.00113321,0.00113884,0.00104755,0.00105539,0.00099961,0.000910406,0.000943763,0.000816891,0.000677801,0.000691372,0.000702156,0.000640817,0.000572852,0.000598864,0.000547255,0.000516267,0.000521948,0.000445364,0.000405688,0.00036877,0.000387921,0.000344067,0.00035138,0.000234379,0.000265946,0.000209413,0.000240064,0.000272217,0.000246133,0.000152624,0.000149935,0.00017573,0.000159073,0.000140118,0.000140311,8.32411e-05,0.000107131,7.74115e-05,0.000105487,7.82522e-05,7.48754e-05,9.11043e-05,8.31182e-05,7.38662e-05,6.57455e-05,5.52577e-05,5.07988e-05,6.40157e-05,5.31859e-05,4.72259e-05,4.79612e-05,4.77387e-05,5.06485e-05,5.94791e-05,3.99507e-05,5.14873e-05,2.7288e-05,1.9423e-05,3.12414e-05,2.61816e-05,2.85271e-05,2.6006e-05,2.19138e-05,1.87495e-05,1.88346e-05,1.85179e-05,1.35789e-05,1.59364e-05,6.91698e-06,9.46978e-06,4.5992e-06,2.26131e-06,0,2.33954e-06,0,0}',
'{-0.5,0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99}',
'{0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99,99.5}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,BIN_MIN,BIN_MAX) VALUES (237,1,'numubar','{200}','{"neutrino energy (GeV)"," #nu/m^{2}/10^{6}POT/0.5 GeV"}','{0,2.32864,6.25619,16.0002,22.95,29.9316,36.1717,36.4453,28.2484,16.2189,8.36204,4.95442,3.39086,2.56585,2.08784,1.74572,1.46005,1.2336,1.07991,0.967436,0.87012,0.742388,0.680907,0.611291,0.557863,0.498813,0.458315,0.41748,0.366968,0.331689,0.295811,0.267799,0.246969,0.222799,0.197898,0.180388,0.16127,0.1467,0.128982,0.120322,0.107828,0.0990447,0.0884847,0.0775478,0.0712567,0.06502,0.061399,0.0567506,0.0472413,0.0450734,0.0395679,0.0399078,0.0345991,0.0304799,0.0298679,0.0307108,0.0273375,0.0245225,0.0213034,0.0210049,0.0178209,0.0176523,0.015917,0.0156011,0.0138286,0.0132639,0.0128411,0.0120399,0.0122118,0.0105882,0.0113714,0.00981063,0.00850859,0.00799719,0.00779621,0.00668593,0.0062713,0.00621651,0.00545273,0.00499728,0.00434392,0.00406847,0.00408786,0.00382218,0.00389077,0.00394874,0.00375069,0.00367789,0.00255179,0.00329578,0.00279767,0.00244189,0.0023659,0.00269112,0.00254411,0.00224309,0.00218838,0.0019572,0.00161793,0.00159798,0.00174074,0.00202082,0.0018499,0.00160751,0.00136398,0.00143431,0.00115889,0.00145397,0.00139629,0.00132168,0.00121122,0.00101406,0.0010429,0.00111527,0.000874329,0.000798792,0.000756622,0.000819432,0.000457881,0.000525877,0.000435187,0.00054257,0.00113583,0.000289107,0.000434407,0.000346553,0.000438086,0.00032786,0.000375259,0.0002229,0.000311765,0.000257336,0.000189327,0.000184442,0.000379652,0.000129924,0.000141786,0.000146865,9.26825e-05,0.000109545,3.41406e-05,0.000178636,0.000105416,0.000100919,0.000103629,5.19358e-05,8.4478e-05,9.82103e-05,0.000101109,3.30909e-05,6.53732e-05,7.86398e-05,4.71905e-05,1.61775e-05,0,1.63829e-05,0,4.9346e-05,3.24196e-05,0,1.50814e-05,0,3.14713e-05,0,0,0,0,0,0,1.51744e-05,1.50828e-05,0,0,0,0,0,0,1.34646e-05,1.43661e-05,0,1.3221e-05,0,1.40743e-05,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{0,0.329954,0.595403,1.17841,1.67535,2.16515,2.48493,2.3142,1.9944,1.55961,0.918012,0.479545,0.295648,0.220771,0.175103,0.148329,0.123841,0.104766,0.0910875,0.078229,0.0721867,0.0615282,0.0554626,0.0492842,0.0444997,0.04056,0.0361556,0.0336224,0.0288799,0.0255976,0.0245897,0.0222994,0.0204015,0.0188519,0.0167448,0.0150904,0.0140729,0.013049,0.0123024,0.0121815,0.0112699,0.0105285,0.0103702,0.00964923,0.00923201,0.00875765,0.00878994,0.00854115,0.00765322,0.00813302,0.00724599,0.00778255,0.00696912,0.00595729,0.00597852,0.00591824,0.00518976,0.00431735,0.00366341,0.00340343,0.00315814,0.00303928,0.00256508,0.00261938,0.00223083,0.00193647,0.00211589,0.00177976,0.00185974,0.00164013,0.00193283,0.00160292,0.00135891,0.00135901,0.00117599,0.0010781,0.00109872,0.00108725,0.00103584,0.00108104,0.00107924,0.00112852,0.00108007,0.000996405,0.000983986,0.000977552,0.000878309,0.000991901,0.000659734,0.000760166,0.000688627,0.000757861,0.000629309,0.000709393,0.000637451,0.000574319,0.000599469,0.000474008,0.000417705,0.000361459,0.000422944,0.000501537,0.000412661,0.000415453,0.00032167,0.000344408,0.000272144,0.000346815,0.000352548,0.000352538,0.000271132,0.000237155,0.000287041,0.000308269,0.000265509,0.000288009,0.000230143,0.000254906,0.000158071,0.000173407,0.000143967,0.00016998,0.000321253,8.77098e-05,0.000153817,0.000132085,0.000161797,0.000110689,0.000135283,8.03389e-05,0.000122629,9.2589e-05,7.1772e-05,6.81592e-05,0.000109807,4.61774e-05,5.36389e-05,5.34308e-05,3.59021e-05,4.45701e-05,1.21101e-05,6.40906e-05,3.82691e-05,3.55989e-05,3.07887e-05,1.8789e-05,3.02416e-05,3.50203e-05,3.65922e-05,1.16847e-05,2.33932e-05,2.92983e-05,1.89734e-05,5.79658e-06,0,6.00083e-06,0,1.85096e-05,1.28453e-05,0,5.27302e-06,0,1.18427e-05,0,0,0,0,0,0,5.5916e-06,5.55785e-06,0,0,0,0,0,0,5.05429e-06,5.29377e-06,0,4.96286e-06,0,5.18622e-06,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{0,0.329954,0.595403,1.17841,1.67535,2.16515,2.48493,2.3142,1.9944,1.55961,0.918012,0.479545,0.295648,0.220771,0.175103,0.148329,0.123841,0.104766,0.0910875,0.078229,0.0721867,0.0615282,0.0554626,0.0492842,0.0444997,0.04056,0.0361556,0.0336224,0.0288799,0.0255976,0.0245897,0.0222994,0.0204015,0.0188519,0.0167448,0.0150904,0.0140729,0.013049,0.0123024,0.0121815,0.0112699,0.0105285,0.0103702,0.00964923,0.00923201,0.00875765,0.00878994,0.00854115,0.00765322,0.00813302,0.00724599,0.00778255,0.00696912,0.00595729,0.00597852,0.00591824,0.00518976,0.00431735,0.00366341,0.00340343,0.00315814,0.00303928,0.00256508,0.00261938,0.00223083,0.00193647,0.00211589,0.00177976,0.00185974,0.00164013,0.00193283,0.00160292,0.00135891,0.00135901,0.00117599,0.0010781,0.00109872,0.00108725,0.00103584,0.00108104,0.00107924,0.00112852,0.00108007,0.000996405,0.000983986,0.000977552,0.000878309,0.000991901,0.000659734,0.000760166,0.000688627,0.000757861,0.000629309,0.000709393,0.000637451,0.000574319,0.000599469,0.000474008,0.000417705,0.000361459,0.000422944,0.000501537,0.000412661,0.000415453,0.00032167,0.000344408,0.000272144,0.000346815,0.000352548,0.000352538,0.000271132,0.000237155,0.000287041,0.000308269,0.000265509,0.000288009,0.000230143,0.000254906,0.000158071,0.000173407,0.000143967,0.00016998,0.000321253,8.77098e-05,0.000153817,0.000132085,0.000161797,0.000110689,0.000135283,8.03389e-05,0.000122629,9.2589e-05,7.1772e-05,6.81592e-05,0.000109807,4.61774e-05,5.36389e-05,5.34308e-05,3.59021e-05,4.45701e-05,1.21101e-05,6.40906e-05,3.82691e-05,3.55989e-05,3.07887e-05,1.8789e-05,3.02416e-05,3.50203e-05,3.65922e-05,1.16847e-05,2.33932e-05,2.92983e-05,1.89734e-05,5.79658e-06,0,6.00083e-06,0,1.85096e-05,1.28453e-05,0,5.27302e-06,0,1.18427e-05,0,0,0,0,0,0,5.5916e-06,5.55785e-06,0,0,0,0,0,0,5.05429e-06,5.29377e-06,0,4.96286e-06,0,5.18622e-06,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{-0.5,0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99}',
'{0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99,99.5}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,BIN_MIN,BIN_MAX) VALUES (238,1,'nue','{200}','{"neutrino energy (GeV)"," #nu/m^{2}/10^{6}POT/0.5 GeV"}','{0,0.017619,0.024468,0.0247072,0.0289681,0.0261259,0.0244944,0.0248429,0.0243176,0.0210516,0.0260835,0.0218616,0.0216677,0.0257614,0.0250689,0.0211863,0.0206424,0.0185665,0.0186202,0.0195591,0.0176432,0.0187612,0.0169269,0.0158311,0.0139587,0.0138259,0.0140874,0.0131638,0.013787,0.0115823,0.0112263,0.0106683,0.00992776,0.00934423,0.0112915,0.00980136,0.00906855,0.0088649,0.00836086,0.00855646,0.00861397,0.0075687,0.00644012,0.00586709,0.00672339,0.00673427,0.00561338,0.00748926,0.00558542,0.00527728,0.0050243,0.0052199,0.00473437,0.00449716,0.00429554,0.00339322,0.00426814,0.00463139,0.00498407,0.00311544,0.0039933,0.00352805,0.00317487,0.00314783,0.00388983,0.00282322,0.00296346,0.00284603,0.00286399,0.00179767,0.00292871,0.00203388,0.00195052,0.00213542,0.00143981,0.00200258,0.00116572,0.00148393,0.00125753,0.00108996,0.00103306,0.00104339,0.000956211,0.00104021,0.000711324,0.000547198,0.00106189,0.000405144,0.000431938,0.000365312,0.000362286,0.000217029,0.000228015,0.000346128,0.000493943,0.000213753,0.000331967,0.000136696,0.000163226,0.000148376,0.000145039,0.000146929,0.000181975,0.000138491,0.000128308,8.98766e-05,6.79387e-05,6.74499e-05,8.78074e-05,0.000100908,8.1297e-05,7.87907e-05,9.56768e-05,0.000120745,3.02172e-05,0.000117826,0.000121184,7.47081e-05,5.40806e-05,6.53994e-05,6.22275e-05,5.22332e-05,7.69965e-05,6.37437e-05,1.86202e-05,4.78746e-05,8.28044e-06,8.22929e-06,1.75539e-05,0,3.69685e-05,9.41226e-06,2.59588e-05,9.00975e-06,1.77911e-05,1.76797e-05,0,8.70525e-06,2.61598e-05,0,0,0,0,0,0,0,0,0,8.2834e-06,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{0,0.00499385,0.00351855,0.00289774,0.00332006,0.00328824,0.00221934,0.00239638,0.00252919,0.00251999,0.00301711,0.00252578,0.0021833,0.00235975,0.00209622,0.00231704,0.0021818,0.00202819,0.00154212,0.00194561,0.00177098,0.00173812,0.0016482,0.00143773,0.0014341,0.00146578,0.00150746,0.00133619,0.00127013,0.00116554,0.00115352,0.00104626,0.00100327,0.00105308,0.00120712,0.0012533,0.000992634,0.00103589,0.000972166,0.000972652,0.00111757,0.00088289,0.000731497,0.000684856,0.000747258,0.000896305,0.000681229,0.00119248,0.000745512,0.000772463,0.000730882,0.000688841,0.000693236,0.000717333,0.000646536,0.000452225,0.000700039,0.000736584,0.000945012,0.000542876,0.00067378,0.000575655,0.000632956,0.000558551,0.000819598,0.000629469,0.000553955,0.000530597,0.000562864,0.000319054,0.00061111,0.000391678,0.000411569,0.000412945,0.000280138,0.000419522,0.000204191,0.000310362,0.00022679,0.000187095,0.000184833,0.000169585,0.000144665,0.000167298,0.000105182,8.78842e-05,0.000185563,8.30886e-05,6.62672e-05,6.51182e-05,7.38777e-05,3.4865e-05,4.56557e-05,6.39369e-05,8.96671e-05,3.87526e-05,6.233e-05,3.12208e-05,3.78783e-05,3.34254e-05,3.41745e-05,4.02579e-05,4.57467e-05,4.25815e-05,3.18848e-05,2.40726e-05,2.17854e-05,1.64296e-05,1.80566e-05,1.77433e-05,1.89043e-05,1.6118e-05,2.00747e-05,2.71539e-05,6.52714e-06,3.17238e-05,2.42589e-05,1.41937e-05,1.43616e-05,1.31474e-05,1.68003e-05,1.24009e-05,1.94654e-05,1.88066e-05,7.98721e-06,1.70703e-05,3.26125e-06,3.24111e-06,6.96784e-06,0,1.14929e-05,3.52302e-06,1.0392e-05,3.68992e-06,7.28629e-06,7.24069e-06,0,3.56521e-06,7.52763e-06,0,0,0,0,0,0,0,0,0,3.07525e-06,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{0,0.00499385,0.00351855,0.00289774,0.00332006,0.00328824,0.00221934,0.00239638,0.00252919,0.00251999,0.00301711,0.00252578,0.0021833,0.00235975,0.00209622,0.00231704,0.0021818,0.00202819,0.00154212,0.00194561,0.00177098,0.00173812,0.0016482,0.00143773,0.0014341,0.00146578,0.00150746,0.00133619,0.00127013,0.00116554,0.00115352,0.00104626,0.00100327,0.00105308,0.00120712,0.0012533,0.000992634,0.00103589,0.000972166,0.000972652,0.00111757,0.00088289,0.000731497,0.000684856,0.000747258,0.000896305,0.000681229,0.00119248,0.000745512,0.000772463,0.000730882,0.000688841,0.000693236,0.000717333,0.000646536,0.000452225,0.000700039,0.000736584,0.000945012,0.000542876,0.00067378,0.000575655,0.000632956,0.000558551,0.000819598,0.000629469,0.000553955,0.000530597,0.000562864,0.000319054,0.00061111,0.000391678,0.000411569,0.000412945,0.000280138,0.000419522,0.000204191,0.000310362,0.00022679,0.000187095,0.000184833,0.000169585,0.000144665,0.000167298,0.000105182,8.78842e-05,0.000185563,8.30886e-05,6.62672e-05,6.51182e-05,7.38777e-05,3.4865e-05,4.56557e-05,6.39369e-05,8.96671e-05,3.87526e-05,6.233e-05,3.12208e-05,3.78783e-05,3.34254e-05,3.41745e-05,4.02579e-05,4.57467e-05,4.25815e-05,3.18848e-05,2.40726e-05,2.17854e-05,1.64296e-05,1.80566e-05,1.77433e-05,1.89043e-05,1.6118e-05,2.00747e-05,2.71539e-05,6.52714e-06,3.17238e-05,2.42589e-05,1.41937e-05,1.43616e-05,1.31474e-05,1.68003e-05,1.24009e-05,1.94654e-05,1.88066e-05,7.98721e-06,1.70703e-05,3.26125e-06,3.24111e-06,6.96784e-06,0,1.14929e-05,3.52302e-06,1.0392e-05,3.68992e-06,7.28629e-06,7.24069e-06,0,3.56521e-06,7.52763e-06,0,0,0,0,0,0,0,0,0,3.07525e-06,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{-0.5,0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99}',
'{0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99,99.5}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,BIN_MIN,BIN_MAX) VALUES (239,1,'nuebar','{200}','{"neutrino energy (GeV)"," #nu/m^{2}/10^{6}POT/0.5 GeV"}','{0,0.0628474,0.176302,0.272437,0.32247,0.338967,0.333719,0.307005,0.250205,0.210703,0.162889,0.147659,0.106922,0.0775145,0.0681949,0.0458304,0.0367077,0.0329312,0.0252796,0.0207715,0.0194316,0.0225035,0.0173973,0.0151289,0.0122405,0.0113601,0.0105034,0.0111093,0.0116803,0.00822219,0.00914408,0.00859074,0.00683653,0.00583322,0.00487369,0.0064405,0.00503799,0.00413343,0.00512562,0.00448176,0.00361843,0.00371439,0.00302919,0.00349472,0.00294786,0.00280566,0.0032454,0.00286574,0.00318945,0.00237091,0.00341628,0.00192616,0.0018582,0.0021897,0.00257844,0.00157028,0.00160152,0.00144598,0.0012734,0.00101355,0.00157123,0.00122935,0.001295,0.000865062,0.00123521,0.00101561,0.00106159,0.00139673,0.00112809,0.00114985,0.00074204,0.000905812,0.000917976,0.000674945,0.000255761,0.000662679,0.000415741,0.000417108,0.000495423,0.000574075,0.000275445,0.000106666,0.000361835,0.00033993,0.000282544,0.000192195,0.000113216,0.000294321,0.000311365,0.000192227,0.000162487,0.000240622,4.80729e-05,0.000451764,0.000151972,0.000226519,7.26386e-05,0.000100696,9.08012e-05,2.44498e-05,2.2667e-05,0.000100459,2.34529e-05,4.80636e-05,4.7075e-05,0,0,0,0,7.1591e-05,0,2.22547e-05,4.43684e-05,2.16438e-05,0,0,6.76861e-05,2.14227e-05,0,0,0,0,0,0,0,2.00536e-05,0,2.11068e-05,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{0,0.00707136,0.0152536,0.0194122,0.0222132,0.0201955,0.022123,0.0202071,0.0189614,0.0209851,0.0177101,0.0126666,0.00774874,0.00441892,0.00364237,0.00322031,0.00283695,0.00241601,0.00181301,0.00145725,0.00173168,0.00183336,0.00163696,0.00108852,0.00113531,0.000983527,0.00092952,0.00112748,0.00109319,0.000846217,0.000761678,0.000808745,0.000612036,0.000761202,0.000468513,0.00066294,0.000464461,0.000500921,0.000538789,0.000367067,0.00042897,0.000451516,0.000354315,0.000438086,0.000395431,0.000301356,0.000405372,0.000307199,0.000323941,0.000278866,0.000447704,0.000212005,0.000193005,0.000275827,0.000386061,0.000189742,0.000266141,0.000241146,0.000161891,0.000134503,0.00018677,0.000205398,0.000217983,0.000130873,0.000193546,0.000160774,0.000165264,0.000211459,0.000176818,0.000207676,0.000130629,0.00019217,0.000143587,0.000109789,5.18524e-05,0.000125814,7.39302e-05,6.4944e-05,7.6371e-05,0.000103729,7.16434e-05,2.4714e-05,7.51708e-05,6.58076e-05,4.86149e-05,4.78889e-05,2.84706e-05,5.83058e-05,7.74999e-05,4.86219e-05,3.63264e-05,7.06362e-05,1.68009e-05,9.24551e-05,4.66636e-05,6.68261e-05,2.5644e-05,4.31178e-05,2.61188e-05,1.0538e-05,9.92083e-06,2.54378e-05,8.18946e-06,1.24792e-05,2.01714e-05,0,0,0,0,2.00786e-05,0,9.51564e-06,1.89531e-05,9.23849e-06,0,0,1.78927e-05,9.16918e-06,0,0,0,0,0,0,0,8.58318e-06,0,7.43185e-06,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{0,0.00707136,0.0152536,0.0194122,0.0222132,0.0201955,0.022123,0.0202071,0.0189614,0.0209851,0.0177101,0.0126666,0.00774874,0.00441892,0.00364237,0.00322031,0.00283695,0.00241601,0.00181301,0.00145725,0.00173168,0.00183336,0.00163696,0.00108852,0.00113531,0.000983527,0.00092952,0.00112748,0.00109319,0.000846217,0.000761678,0.000808745,0.000612036,0.000761202,0.000468513,0.00066294,0.000464461,0.000500921,0.000538789,0.000367067,0.00042897,0.000451516,0.000354315,0.000438086,0.000395431,0.000301356,0.000405372,0.000307199,0.000323941,0.000278866,0.000447704,0.000212005,0.000193005,0.000275827,0.000386061,0.000189742,0.000266141,0.000241146,0.000161891,0.000134503,0.00018677,0.000205398,0.000217983,0.000130873,0.000193546,0.000160774,0.000165264,0.000211459,0.000176818,0.000207676,0.000130629,0.00019217,0.000143587,0.000109789,5.18524e-05,0.000125814,7.39302e-05,6.4944e-05,7.6371e-05,0.000103729,7.16434e-05,2.4714e-05,7.51708e-05,6.58076e-05,4.86149e-05,4.78889e-05,2.84706e-05,5.83058e-05,7.74999e-05,4.86219e-05,3.63264e-05,7.06362e-05,1.68009e-05,9.24551e-05,4.66636e-05,6.68261e-05,2.5644e-05,4.31178e-05,2.61188e-05,1.0538e-05,9.92083e-06,2.54378e-05,8.18946e-06,1.24792e-05,2.01714e-05,0,0,0,0,2.00786e-05,0,9.51564e-06,1.89531e-05,9.23849e-06,0,0,1.78927e-05,9.16918e-06,0,0,0,0,0,0,0,8.58318e-06,0,7.43185e-06,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}',
'{-0.5,0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99}',
'{0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,27,27.5,28,28.5,29,29.5,30,30.5,31,31.5,32,32.5,33,33.5,34,34.5,35,35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,44.5,45,45.5,46,46.5,47,47.5,48,48.5,49,49.5,50,50.5,51,51.5,52,52.5,53,53.5,54,54.5,55,55.5,56,56.5,57,57.5,58,58.5,59,59.5,60,60.5,61,61.5,62,62.5,63,63.5,64,64.5,65,65.5,66,66.5,67,67.5,68,68.5,69,69.5,70,70.5,71,71.5,72,72.5,73,73.5,74,74.5,75,75.5,76,76.5,77,77.5,78,78.5,79,79.5,80,80.5,81,81.5,82,82.5,83,83.5,84,84.5,85,85.5,86,86.5,87,87.5,88,88.5,89,89.5,90,90.5,91,91.5,92,92.5,93,93.5,94,94.5,95,95.5,96,96.5,97,97.5,98,98.5,99,99.5}');

INSERT INTO PUBLIC.BEAM(BID,BNAME,REFID,PARTICLEIDS,MEANENERGY,DATATABLEIDS)  VALUES (68,'Fermilab NuMI BEAM reverse horn current',54,'{14,-14,12,-12}','{9262,3569,11988,4461}','{236,237,238,239}');

--
-- table representing dictionary of working group
--
CREATE TABLE WGROUPS (
    WGID SERIAL  PRIMARY KEY,                    -- working group id
    WGNAME      CHARACTER VARYING(50) UNIQUE     -- name of the working group             
);
ALTER TABLE WGROUPS OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON WGROUPS  TO G4VALWRITER;
GRANT SELECT ON WGROUPS TO G4VALREADER;

INSERT INTO PUBLIC.WGROUPS (WGNAME) VALUES ('Geant4 electromagnetic working group');
INSERT INTO PUBLIC.WGROUPS (WGNAME) VALUES ('Geant4 hadronic working group');
INSERT INTO PUBLIC.WGROUPS (WGNAME) VALUES ('Geant4 medical working group');
--
-- table representing a TEST and providing a description. 
--
CREATE TABLE TEST (
    TESTID      INTEGER NOT NULL PRIMARY KEY,            -- number matches geant 4 test number if applicable
    TESTNAME    CHARACTER VARYING(50) UNIQUE,            -- name of the test
    MCID        INTEGER REFERENCES MCTOOL(MCID),         -- what montecarlo (MCTOOL)  (e.g. Genie or Geant4)
    DESCRIPTION TEXT,                                    -- text describing the purpose of the test
    RESPONSIBLE CHARACTER VARYING(100)[],                -- names of persons responsible for the test
    WG          INTEGER REFERENCES WGROUPS(WGID),        -- working group that the test is associated with
    KEYWORDS    CHARACTER VARYING(50)[],                 -- keywords associated with the test
    REFERENCEID INTEGER[],                               -- array elements reference RID in the  REFERENCE table
    DEFAULTS    INTEGER[]                                -- references to the default results to be displayed
);
ALTER TABLE TEST OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON TEST  TO G4VALWRITER;
GRANT SELECT ON TEST TO G4VALREADER;
INSERT INTO PUBLIC.TEST ( TESTID, TESTNAME,MCID,DESCRIPTION,RESPONSIBLE,WG,KEYWORDS,REFERENCEID,DEFAULTS) VALUES 
(19,'test19',2,
'High energy Experiment, provides comparison with NA61 (31 GeV/c proton beam) and NA49 (158 GeV/c proton beam) data sets.', 
'{"Julia Yarba (Fermilab)"}',
2,
'{"Inclusive production","high energy","thick Target"}',
'{49,41}',NULL
);
INSERT INTO PUBLIC.TEST ( TESTID, TESTNAME,MCID,DESCRIPTION,RESPONSIBLE,WG,KEYWORDS,REFERENCEID,DEFAULTS) VALUES 
(10000,'Franz',2,
'Neutron-induced production of protons, deuterons and tritons by neutrons between 300-580 MeV',
'{"Hans Wenzel (Fermilab)"}',
2,
'{"particle production","differential cross section","Thin Target","neutron induced"}',
'{20}',NULL
);
INSERT INTO PUBLIC.TEST ( TESTID, TESTNAME,MCID,DESCRIPTION,RESPONSIBLE,WG,KEYWORDS,REFERENCEID,DEFAULTS) VALUES 
(10001,'NA49',2,
'Detailed analysis of soft hadronic interactions',
'{"Julia Yarba (Fermilab)"}',
2,
'{"hadronic interactions","particle production","differential cross section","Thin Target","proton induced"}',
'{52}',NULL
);
INSERT INTO PUBLIC.TEST ( TESTID, TESTNAME,MCID,DESCRIPTION,RESPONSIBLE,WG,KEYWORDS,REFERENCEID,DEFAULTS) VALUES 
(10002,'Pion Cross sections',2,
'Compare total,elastic,inelastic pion cross section with data',
'{"Hans Wenzel (Fermilab)"}',
2,
'{"hadronic interactions","total cross section","inelastic cross section","elastic cross section","Thin Target"}',
'{6,4,5,8}',NULL
);
---
---  An imageblob is used to store images (jpeg, gif etc.) directly in form of a blob (Binary Large OBject (BLOB))
---  to allow the use of blobs in the postgres database you must:  
---  modify the following /postgresql.conf
---  # For Postgresql versions later than 9
---  lo_compat_privileges = on 
---
CREATE TABLE IMAGEBLOB (
    IBID SERIAL PRIMARY KEY,
    IMAGE OID NOT NULL,
    MTYPE CHARACTER VARYING(50) DEFAULT 'IMAGE/GIF'::CHARACTER VARYING NOT NULL,
    FNAME CHARACTER VARYING(50) DEFAULT 'IMAGE.GIF'::CHARACTER VARYING NOT NULL
);
ALTER TABLE IMAGEBLOB OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON IMAGEBLOB TO G4VALWRITER;
GRANT SELECT ON IMAGEBLOB TO G4VALREADER;
--
-- table representing RESULT (experimental or simulation)
-- it defines the meta data and links to a datatable or imageblob
--
CREATE TABLE RESULT (
   TRID         SERIAL PRIMARY KEY ,
   TID          INTEGER REFERENCES TEST(TESTID),                         -- REFERENCES THE TEST THAT TEST RESULT IS ASSOCIATED WITH
   REFID        INTEGER REFERENCES REFERENCE(REFID),                     -- reference the Reference that the experimental data is associated with
   MCDTID       INTEGER REFERENCES MCDETAIL(MCDTID),                     -- REFERENCES the MC details (tool, version,model)
   BEAM         INTEGER REFERENCES BEAM(BID),                            -- REFERENCES PARTICLE TABLE (PDGID,NAME, ...)
   TARGET       INTEGER REFERENCES MATERIAL(MID),                        -- REFERENCES TABLE defining materials
   OBSERVABLE   INTEGER REFERENCES OBSERVABLE(OID),                      -- REFERENCES TABLE defininG OBSERVABLES (CROSS SECTION, MOMENTUM OF OUTGOING PARTICLES..)
   SECONDARY    INTEGER REFERENCES PARTICLE(PDGID),                      -- REFERENCES PARTICLE TABLE (PDGID,NAME, ...)
   REACTION     INTEGER REFERENCES REACTION(RID),                        -- REFERENCES REACTION TABLE (SCATTERING, PARTICLE PRODUCTION, CAPTURE, DECAY....)
   DATATABLEID  INTEGER REFERENCES DATATABLE(DTID) ON DELETE CASCADE,    -- REFERENCES DATATABLE can be null
   IMAGEBLOBID  INTEGER REFERENCES IMAGEBLOB(IBID) ON DELETE CASCADE,    -- REFERENCES IMAGEBLOB can be null
   SCORE        INTEGER REFERENCES SCORES(SCOREID),                      -- TEST SCORE AND TYPE
   ACCESS       INTEGER REFERENCES ACCESS(ACCESSID),                     -- controls who has access to the test result
   PARNAMES     CHARACTER VARYING(50)[],                                 -- additional parameters e.g. phi angle of outgoing particle 
   PARVALUES    CHARACTER VARYING(50)[],                                 -- value of additional parameter e.g 50 deg
   MODTIME      TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP       -- make the default value of modtime be the time at which the row is inserted
);
ALTER TABLE RESULT OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON RESULT TO G4VALWRITER;
GRANT SELECT ON RESULT TO G4VALREADER;
---
--- table representing the default curves that should be displayed for a given Experiment
---
CREATE TABLE EXPDEFAULTS(
REFID        INTEGER REFERENCES REFERENCE(REFID) PRIMARY KEY,                     -- reference the Reference that the experimental data is associated with
LINKS        INTEGER[]
);
ALTER TABLE EXPDEFAULTS OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON EXPDEFAULTS TO G4VALWRITER;
GRANT SELECT ON EXPDEFAULTS TO G4VALREADER;
INSERT INTO PUBLIC.EXPDEFAULTS(REFID,LINKS) VALUES (20,'{1,2,3,4,5,6}');
INSERT INTO PUBLIC.EXPDEFAULTS(REFID,LINKS) VALUES (12,'{181, 182, 183, 184, 185, 186, 187}');
INSERT INTO PUBLIC.EXPDEFAULTS(REFID,LINKS) VALUES (4,'{198,200,202,204,206,208}');
INSERT INTO PUBLIC.EXPDEFAULTS(REFID,LINKS) VALUES (5,'{210,212,214,216,218,220,222,224,226}');
INSERT INTO PUBLIC.EXPDEFAULTS(REFID,LINKS) VALUES (6,'{188,191,193,195,196}');
--- INSERT INTO PUBLIC.EXPDEFAULTS(REFID,LINKS) VALUES (43,'{262,263,264,265,266,267,268,269,270}');
---INSERT INTO PUBLIC.EXPDEFAULTS(REFID,LINKS) VALUES (44,'{388}');
---INSERT INTO PUBLIC.EXPDEFAULTS(REFID,LINKS) VALUES (45,'{392}');
---INSERT INTO PUBLIC.EXPDEFAULTS(REFID,LINKS) VALUES (46,'{258}');
---INSERT INTO PUBLIC.EXPDEFAULTS(REFID,LINKS) VALUES (52,'{181}');
--
-- Franz data set:
--

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
1,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{32}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{852.8, 771.7, 717.6, 691.7, 636.8, 515.4, 402.9, 356.7, 343.7, 342.5, 299.8, 269.1, 250, 236.6, 218.8, 196.5, 184.6, 158.8, 148.3, 120.5, 108.5, 99.43, 88.62, 70.12, 52.72, 39.42, 30.51, 25, 17.4, 11.6, 7.703, 4.906}',
'{10.885, 10.045, 9.47, 7.19, 6.65, 5.47, 4.3535, 3.902, 3.7915, 3.799, 3.3915, 3.1035, 2.9295, 2.812, 2.649, 2.438, 2.3365, 2.0845, 1.987, 1.7065, 1.5875, 1.4975, 1.3865, 0.9645, 0.79, 0.648, 0.548, 0.483, 0.388, 0.2585, 0.2045, 0.159}',
'{10.885, 10.045, 9.47, 7.19, 6.65, 5.47, 4.3535, 3.902, 3.7915, 3.799, 3.3915, 3.1035, 2.9295, 2.812, 2.649, 2.438, 2.3365, 2.0845, 1.987, 1.7065, 1.5875, 1.4975, 1.3865, 0.9645, 0.79, 0.648, 0.548, 0.483, 0.388, 0.2585, 0.2045, 0.159}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420, 450}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
1,NULL,20,1,
1,29,1,2212,1,1,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
2,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{31}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{890.2, 877.1, 806.3, 725.5, 642.3, 464.2, 351.4, 313.2, 334.3, 335, 274, 245.4, 244.6, 223.7, 206.3, 204.5, 180.3, 152.1, 129.5, 113.8, 102.1, 87.25, 80.85, 64.4, 45.92, 38.73, 24.51, 20.46, 13.86, 8.83, 5.381}',
'{13.68, 13.515, 12.665, 7.99, 7.17, 5.35, 4.163, 3.8105, 4.0955, 4.1625, 3.5615, 3.307, 3.357, 3.1805, 3.0365, 3.0835, 2.8515, 2.5605, 2.3235, 2.161, 2.0385, 1.8665, 1.7965, 1.217, 0.991, 0.896, 0.6915, 0.626, 0.507, 0.33, 0.255}',
'{13.68, 13.515, 12.665, 7.99, 7.17, 5.35, 4.163, 3.8105, 4.0955, 4.1625, 3.5615, 3.307, 3.357, 3.1805, 3.0365, 3.0835, 2.8515, 2.5605, 2.3235, 2.161, 2.0385, 1.8665, 1.7965, 1.217, 0.991, 0.896, 0.6915, 0.626, 0.507, 0.33, 0.255}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
2,NULL,20,1,
2,29,1,2212,1,2,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
3,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{31}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{914.7, 754.7, 687.4, 680.2, 570.8, 387.3, 306.9, 276.5, 273.6, 297, 242.7, 208.5, 213.8, 190.1, 189.1, 175.2, 148.1, 121.8, 113.9, 93.89, 76.08, 67.83, 57.85, 45.22, 29.12, 21.81, 12.39, 10.73, 6.684, 3.707, 2.734}',
'{15.315, 13.215, 12.285, 7.995, 6.845, 4.8225, 3.9555, 3.6785, 3.7245, 4.0815, 3.5215, 3.186, 3.334, 3.1155, 3.206, 3.0955, 2.8025, 2.5025, 2.4395, 2.192, 1.954, 1.842, 1.752, 1.1255, 0.879, 0.751, 0.5565, 0.5165, 0.404, 0.2455, 0.2105}',
'{15.315, 13.215, 12.285, 7.995, 6.845, 4.8225, 3.9555, 3.6785, 3.7245, 4.0815, 3.5215, 3.186, 3.334, 3.1155, 3.206, 3.0955, 2.8025, 2.5025, 2.4395, 2.192, 1.954, 1.842, 1.752, 1.1255, 0.879, 0.751, 0.5565, 0.5165, 0.404, 0.2455, 0.2105}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
3,NULL,20,1,
3,29,1,2212,1,3,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
4,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{29}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{881.9, 784.5, 695.1, 684.5, 604.7, 409.3, 333.5, 293.8, 305.2, 309.4, 270.8, 221.5, 208.5, 206.7, 185.7, 158.8, 147.5, 117.5, 88.93, 84.73, 72.92, 58.94, 45.94, 35.01, 19.48, 12.67, 8.083, 5.298, 2.836}',
'{15.01, 13.74, 12.525, 7.84, 7.035, 4.9885, 4.253, 3.897, 4.1185, 4.2565, 3.908, 3.419, 3.3475, 3.437, 3.249, 2.975, 2.887, 2.534, 2.166, 2.134, 2.0455, 1.82, 1.592, 1.0005, 0.729, 0.5815, 0.4615, 0.372, 0.271}',
'{15.01, 13.74, 12.525, 7.84, 7.035, 4.9885, 4.253, 3.897, 4.1185, 4.2565, 3.908, 3.419, 3.3475, 3.437, 3.249, 2.975, 2.887, 2.534, 2.166, 2.134, 2.0455, 1.82, 1.592, 1.0005, 0.729, 0.5815, 0.4615, 0.372, 0.271}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
4,NULL,20,1,
4,29,1,2212,1,4,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
5,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{28}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{893.3, 807.6, 721.7, 694, 574.8, 485.2, 378.5, 340.4, 339.6, 311.5, 295.3, 242, 227.2, 198.3, 175.5, 167, 133.4, 103.6, 86.12, 80.92, 58.16, 49.76, 39.24, 24.2, 13.35, 8.054, 4.745, 2.115}',
'{15.62, 14.475, 13.3, 8.33, 7.05, 6.12, 5.015, 4.6675, 4.7435, 4.5045, 4.399, 3.844, 3.7665, 3.4735, 3.247, 3.189, 2.7915, 2.4115, 2.1735, 2.186, 1.8195, 1.6715, 1.4715, 0.823, 0.6005, 0.462, 0.353, 0.2345}',
'{15.62, 14.475, 13.3, 8.33, 7.05, 6.12, 5.015, 4.6675, 4.7435, 4.5045, 4.399, 3.844, 3.7665, 3.4735, 3.247, 3.189, 2.7915, 2.4115, 2.1735, 2.186, 1.8195, 1.6715, 1.4715, 0.823, 0.6005, 0.462, 0.353, 0.2345}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
5,NULL,20,1,
5,29,1,2212,1,5,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
6,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{26}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{837.5, 807.4, 706.9, 628.4, 557.1, 482, 380.7, 338.5, 353.7, 326.9, 294.1, 223.1, 222.9, 193.4, 157.2, 144.7, 115.2, 97, 74.22, 55.64, 43.07, 34.59, 23.25, 13.96, 8.402, 3.993}',
'{15.55, 15.125, 13.695, 8.16, 7.32, 6.535, 5.41, 4.9815, 5.255, 5.01, 4.683, 3.8365, 3.951, 3.6245, 3.1895, 3.062, 2.6735, 2.5135, 2.155, 1.8355, 1.596, 1.4185, 1.1505, 0.6335, 0.486, 0.332}',
'{15.55, 15.125, 13.695, 8.16, 7.32, 6.535, 5.41, 4.9815, 5.255, 5.01, 4.683, 3.8365, 3.951, 3.6245, 3.1895, 3.062, 2.6735, 2.5135, 2.155, 1.8355, 1.596, 1.4185, 1.1505, 0.6335, 0.486, 0.332}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
6,NULL,20,1,
6,29,1,2212,1,6,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
7,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{32}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{633.3, 622, 610.4, 529, 472.1, 390, 351.8, 307.6, 280.6, 250.2, 203.3, 176.3, 143.5, 128.3, 107.6, 86.38, 71.18, 58.7, 48.69, 39.15, 33.04, 26.62, 20.26, 15.06, 11.21, 7.279, 5.077, 2.946, 2.197, 1.175, 0.7022, 0.4693}',
'{8.29, 8.185, 8.08, 5.555, 5.02, 4.2355, 3.884, 3.47, 3.2235, 2.9405, 2.4875, 2.23, 1.909, 1.764, 1.556, 1.337, 1.176, 1.0395, 0.926, 0.811, 0.7345, 0.6485, 0.556, 0.35, 0.296, 0.2325, 0.192, 0.1445, 0.124, 0.074, 0.0565, 0.0465}',
'{8.29, 8.185, 8.08, 5.555, 5.02, 4.2355, 3.884, 3.47, 3.2235, 2.9405, 2.4875, 2.23, 1.909, 1.764, 1.556, 1.337, 1.176, 1.0395, 0.926, 0.811, 0.7345, 0.6485, 0.556, 0.35, 0.296, 0.2325, 0.192, 0.1445, 0.124, 0.074, 0.0565, 0.0465}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420, 450}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
7,NULL,20,1,
1,29,1,2212,1,7,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
8,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{31}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{652.3, 599.2, 597.6, 527.5, 452.4, 349.5, 330, 288, 281.4, 245.6, 182.7, 165.6, 141.3, 114.5, 103.2, 75.6, 61.84, 51.05, 45.32, 36.03, 28.04, 21.03, 18.28, 11.48, 7.307, 5.179, 3.667, 1.853, 1.493, 1.1, 0.2663}',
'{10.155, 9.53, 9.56, 5.915, 5.225, 4.2335, 4.1085, 3.7375, 3.7405, 3.419, 2.779, 2.638, 2.3995, 2.114, 2.006, 1.6705, 1.4975, 1.3525, 1.275, 1.129, 0.99, 0.852, 0.7945, 0.45, 0.3555, 0.2975, 0.2495, 0.1765, 0.158, 0.111, 0.0545}',
'{10.155, 9.53, 9.56, 5.915, 5.225, 4.2335, 4.1085, 3.7375, 3.7405, 3.419, 2.779, 2.638, 2.3995, 2.114, 2.006, 1.6705, 1.4975, 1.3525, 1.275, 1.129, 0.99, 0.852, 0.7945, 0.45, 0.3555, 0.2975, 0.2495, 0.1765, 0.158, 0.111, 0.0545}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
8,NULL,20,1,
2,29,1,2212,1,8,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
9,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{28}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{564.3, 510.5, 526, 462, 413.4, 320.5, 296.9, 269.8, 223.8, 194.7, 154.5, 136.7, 104.3, 88.73, 72.88, 56.63, 42.76, 33.98, 27.56, 21.54, 15.72, 11.06, 9.549, 7.364, 5.017, 2.967, 1.825, 0.7912}',
'{9.91, 9.205, 9.5, 5.59, 5.17, 4.2435, 4.087, 3.882, 3.4375, 3.178, 2.754, 2.596, 2.215, 2.0435, 1.851, 1.62, 1.3985, 1.2475, 1.126, 0.996, 0.85, 0.7105, 0.667, 0.4165, 0.342, 0.262, 0.2055, 0.1345}',
'{9.91, 9.205, 9.5, 5.59, 5.17, 4.2435, 4.087, 3.882, 3.4375, 3.178, 2.754, 2.596, 2.215, 2.0435, 1.851, 1.62, 1.3985, 1.2475, 1.126, 0.996, 0.85, 0.7105, 0.667, 0.4165, 0.342, 0.262, 0.2055, 0.1345}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
9,NULL,20,1,
3,29,1,2212,1,9,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
10,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{28}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{604.2, 566, 496.8, 454.7, 414.7, 352.9, 303.1, 254.2, 249.2, 199.2, 143.6, 129.3, 92.7, 82.62, 62.99, 48.42, 38.07, 28.01, 19.74, 16.56, 11.77, 7.987, 6.921, 4.381, 1.483, 1.464, 1.212, 0.5137}',
'{10.73, 10.27, 9.345, 5.44, 5.15, 4.614, 4.189, 3.7515, 3.801, 3.3095, 2.699, 2.584, 2.134, 2.0405, 1.7675, 1.545, 1.3705, 1.1735, 0.9835, 0.904, 0.769, 0.6315, 0.5875, 0.331, 0.1915, 0.1905, 0.173, 0.1125}',
'{10.73, 10.27, 9.345, 5.44, 5.15, 4.614, 4.189, 3.7515, 3.801, 3.3095, 2.699, 2.584, 2.134, 2.0405, 1.7675, 1.545, 1.3705, 1.1735, 0.9835, 0.904, 0.769, 0.6315, 0.5875, 0.331, 0.1915, 0.1905, 0.173, 0.1125}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
10,NULL,20,1,
4,29,1,2212,1,10,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
11,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{28}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{618.7, 579.3, 618.3, 502.8, 443.2, 361, 331.6, 280, 227.2, 188.8, 145.7, 117.2, 90.35, 69.48, 56.48, 43.86, 29.55, 21.96, 14.03, 11.55, 6.822, 5.073, 4.704, 2.478, 0.9038, 0.7288, 0.6608, 0.2527}',
'{11.435, 10.95, 11.59, 6.275, 5.75, 4.953, 4.732, 4.239, 3.707, 3.318, 2.8365, 2.509, 2.1755, 1.8885, 1.7, 1.493, 1.2145, 1.0455, 0.829, 0.7635, 0.5845, 0.503, 0.4845, 0.2485, 0.15, 0.134, 0.128, 0.079}',
'{11.435, 10.95, 11.59, 6.275, 5.75, 4.953, 4.732, 4.239, 3.707, 3.318, 2.8365, 2.509, 2.1755, 1.8885, 1.7, 1.493, 1.2145, 1.0455, 0.829, 0.7635, 0.5845, 0.503, 0.4845, 0.2485, 0.15, 0.134, 0.128, 0.079}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
11,NULL,20,1,
5,29,1,2212,1,11,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
12,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{26}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{641.5, 590.7, 594.8, 505.9, 472.1, 373.4, 330.8, 264.7, 221.4, 185.9, 143.4, 108.2, 78, 64.29, 42.2, 29.14, 18.6, 18.91, 9.372, 6.521, 5.393, 2.748, 1.887, 1.405, 0.5127, 0.2358}',
'{12.435, 11.755, 11.905, 6.78, 6.51, 5.455, 5.05, 4.3325, 3.8705, 3.484, 2.9705, 2.5205, 2.091, 1.892, 1.5045, 1.2395, 0.9825, 1.0125, 0.706, 0.587, 0.5335, 0.3795, 0.3145, 0.192, 0.116, 0.0785}',
'{12.435, 11.755, 11.905, 6.78, 6.51, 5.455, 5.05, 4.3325, 3.8705, 3.484, 2.9705, 2.5205, 2.091, 1.892, 1.5045, 1.2395, 0.9825, 1.0125, 0.706, 0.587, 0.5335, 0.3795, 0.3145, 0.192, 0.116, 0.0785}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
12,NULL,20,1,
6,29,1,2212,1,12,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
13,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{31}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{495.2, 463.3, 433.3, 426.9, 354.8, 284, 244.9, 182.6, 151.2, 118, 88.73, 74.85, 62.29, 47.22, 38.62, 28.97, 22, 17.02, 14.38, 11.99, 9.503, 6.962, 5.557, 4.332, 3.046, 1.884, 1.137, 0.5239, 0.3568, 0.222, 0.114}',
'{5.155, 4.8515, 4.5635, 4.505, 3.4735, 2.8, 2.429, 1.836, 1.5385, 1.222, 0.942, 0.811, 0.692, 0.5465, 0.464, 0.3705, 0.3005, 0.2495, 0.2225, 0.197, 0.17, 0.14, 0.1225, 0.106, 0.065, 0.049, 0.0365, 0.024, 0.0205, 0.0155, 0.0095}',
'{5.155, 4.8515, 4.5635, 4.505, 3.4735, 2.8, 2.429, 1.836, 1.5385, 1.222, 0.942, 0.811, 0.692, 0.5465, 0.464, 0.3705, 0.3005, 0.2495, 0.2225, 0.197, 0.17, 0.14, 0.1225, 0.106, 0.065, 0.049, 0.0365, 0.024, 0.0205, 0.0155, 0.0095}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
13,NULL,20,1,
1,29,1,2212,1,13,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
14,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{31}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{508.2, 489.5, 458.1, 435.3, 344.5, 273.9, 232.1, 174.7, 135.1, 107.3, 79.2, 66.36, 54.66, 43.15, 33.49, 25.33, 17.17, 13.78, 11.57, 9.537, 8.371, 5.993, 4.048, 3.126, 2.135, 1.271, 0.6105, 0.4873, 0.256, 0.2335, 0.1314}',
'{5.62, 5.45, 5.15, 4.9375, 3.405, 2.7495, 2.3685, 1.8315, 1.464, 1.207, 0.9425, 0.826, 0.717, 0.6065, 0.5105, 0.426, 0.3335, 0.294, 0.2675, 0.241, 0.2245, 0.1875, 0.152, 0.1325, 0.0785, 0.06, 0.041, 0.0365, 0.0265, 0.025, 0.0155}',
'{5.62, 5.45, 5.15, 4.9375, 3.405, 2.7495, 2.3685, 1.8315, 1.464, 1.207, 0.9425, 0.826, 0.717, 0.6065, 0.5105, 0.426, 0.3335, 0.294, 0.2675, 0.241, 0.2245, 0.1875, 0.152, 0.1325, 0.0785, 0.06, 0.041, 0.0365, 0.0265, 0.025, 0.0155}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
14,NULL,20,1,
2,29,1,2212,1,14,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
15,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{29}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{418.1, 382, 377.5, 345.9, 284.6, 214.3, 187.7, 137.7, 107.8, 79.19, 55.75, 47.95, 38.46, 30.84, 22.4, 17.4, 11.42, 9.219, 7.011, 5.627, 3.943, 2.993, 2.345, 1.571, 1.083, 0.4678, 0.1868, 0.1853, 0.1147}',
'{5.015, 4.647, 4.6225, 4.3005, 2.958, 2.283, 2.0435, 1.5605, 1.2785, 1.0005, 0.767, 0.7005, 0.608, 0.531, 0.4375, 0.379, 0.298, 0.2675, 0.232, 0.208, 0.1735, 0.1505, 0.134, 0.109, 0.064, 0.042, 0.026, 0.026, 0.0205}',
'{5.015, 4.647, 4.6225, 4.3005, 2.958, 2.283, 2.0435, 1.5605, 1.2785, 1.0005, 0.767, 0.7005, 0.608, 0.531, 0.4375, 0.379, 0.298, 0.2675, 0.232, 0.208, 0.1735, 0.1505, 0.134, 0.109, 0.064, 0.042, 0.026, 0.026, 0.0205}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
15,NULL,20,1,
3,29,1,2212,1,15,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
16,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{29}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{417.4, 389.1, 365.6, 341.7, 278.7, 217.2, 177.2, 125.4, 101.5, 75.76, 54.79, 44.02, 35.17, 26.08, 19.19, 13.21, 9.427, 6.58, 5.116, 3.853, 2.71, 2.147, 1.427, 0.7865, 0.528, 0.3584, 0.2505, 0.1062, 0.0523}',
'{4.9215, 4.6545, 4.4355, 4.2105, 2.7755, 2.226, 1.876, 1.403, 1.194, 0.959, 0.762, 0.6645, 0.581, 0.4855, 0.408, 0.3315, 0.278, 0.2305, 0.204, 0.177, 0.1475, 0.1325, 0.1075, 0.0795, 0.046, 0.038, 0.0315, 0.0205, 0.0145}',
'{4.9215, 4.6545, 4.4355, 4.2105, 2.7755, 2.226, 1.876, 1.403, 1.194, 0.959, 0.762, 0.6645, 0.581, 0.4855, 0.408, 0.3315, 0.278, 0.2305, 0.204, 0.177, 0.1475, 0.1325, 0.1075, 0.0795, 0.046, 0.038, 0.0315, 0.0205, 0.0145}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
16,NULL,20,1,
4,29,1,2212,1,16,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
17,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{28}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{415.8, 405.4, 372.1, 374.3, 299.2, 224.9, 183.9, 130.9, 100.5, 70.29, 47.11, 42.9, 31.64, 22.39, 16.34, 10.35, 7.254, 5.231, 3.534, 3.218, 2.029, 1.316, 0.8267, 0.6709, 0.4865, 0.1367, 0.1304, 0.1018}',
'{5.17, 5.085, 4.744, 4.7945, 3.15, 2.437, 2.052, 1.539, 1.248, 0.949, 0.7105, 0.6805, 0.5605, 0.4555, 0.381, 0.2945, 0.2445, 0.2075, 0.1695, 0.1635, 0.1285, 0.1035, 0.0815, 0.0735, 0.0445, 0.0235, 0.023, 0.02}',
'{5.17, 5.085, 4.744, 4.7945, 3.15, 2.437, 2.052, 1.539, 1.248, 0.949, 0.7105, 0.6805, 0.5605, 0.4555, 0.381, 0.2945, 0.2445, 0.2075, 0.1695, 0.1635, 0.1285, 0.1035, 0.0815, 0.0735, 0.0445, 0.0235, 0.023, 0.02}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
17,NULL,20,1,
5,29,1,2212,1,17,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
18,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{27}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{427.7, 399.9, 378.2, 364.3, 292, 220.6, 167.6, 124.3, 94.16, 66.36, 44.07, 36.31, 26.65, 17.53, 12.43, 8.547, 5.673, 3.723, 2.62, 2.03, 1.164, 0.8521, 0.5468, 0.2919, 0.1761, 0.151, 0.0503}',
'{5.685, 5.385, 5.16, 5.02, 3.3385, 2.593, 2.04, 1.5855, 1.2705, 0.971, 0.722, 0.6415, 0.529, 0.41, 0.339, 0.2765, 0.223, 0.179, 0.1515, 0.1325, 0.1, 0.0855, 0.068, 0.05, 0.0275, 0.0255, 0.0145}',
'{5.685, 5.385, 5.16, 5.02, 3.3385, 2.593, 2.04, 1.5855, 1.2705, 0.971, 0.722, 0.6415, 0.529, 0.41, 0.339, 0.2765, 0.223, 0.179, 0.1515, 0.1325, 0.1, 0.0855, 0.068, 0.05, 0.0275, 0.0255, 0.0145}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
18,NULL,20,1,
6,29,1,2212,1,18,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
19,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{25}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{325.6, 287.2, 191.1, 138.3, 102.9, 72.62, 54.32, 35.84, 28.04, 19.98, 14.15, 10.06, 7.821, 5.634, 3.691, 2.299, 1.557, 0.9826, 1.025, 0.5764, 0.3413, 0.2719, 0.1133, 0.0763, 0.0431}',
'{4.2045, 2.9935, 2.0765, 1.574, 1.2355, 0.9395, 0.7595, 0.567, 0.4845, 0.392, 0.3195, 0.263, 0.23, 0.1925, 0.154, 0.1205, 0.099, 0.078, 0.08, 0.0595, 0.046, 0.0295, 0.0185, 0.0155, 0.0115}',
'{4.2045, 2.9935, 2.0765, 1.574, 1.2355, 0.9395, 0.7595, 0.567, 0.4845, 0.392, 0.3195, 0.263, 0.23, 0.1925, 0.154, 0.1205, 0.099, 0.078, 0.08, 0.0595, 0.046, 0.0295, 0.0185, 0.0155, 0.0115}',
null,null,
'{28, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 250, 270, 290}',
'{30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 250, 270, 290, 310}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
19,NULL,20,1,
1,29,1,2212,1,19,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
20,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{24}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{322.4, 281.7, 171.3, 115.4, 86.67, 56.11, 42.77, 31.39, 21.64, 14.64, 9.823, 8.08, 5.333, 4.07, 2.164, 1.346, 1.257, 0.6051, 0.4908, 0.3851, 0.1989, 0.2147, 0.0877, 0.0363}',
'{5.02, 3.149, 2.095, 1.561, 1.292, 0.971, 0.833, 0.7015, 0.573, 0.465, 0.379, 0.347, 0.2805, 0.2465, 0.179, 0.141, 0.1375, 0.095, 0.086, 0.0765, 0.055, 0.0405, 0.026, 0.0165}',
'{5.02, 3.149, 2.095, 1.561, 1.292, 0.971, 0.833, 0.7015, 0.573, 0.465, 0.379, 0.347, 0.2805, 0.2465, 0.179, 0.141, 0.1375, 0.095, 0.086, 0.0765, 0.055, 0.0405, 0.026, 0.0165}',
null,null,
'{28, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 250, 270}',
'{30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 250, 270, 290}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
20,NULL,20,1,
2,29,1,2212,1,20,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
21,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{22}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{273.4, 211.7, 135.9, 94.88, 65.72, 42.92, 33.46, 21.71, 17.08, 9.563, 7.174, 4.466, 4.155, 2.137, 2.231, 0.7187, 1.055, 0.4669, 0.2885, 0.2152, 0.1412, 0.0623}',
'{4.8355, 2.6055, 1.861, 1.4615, 1.1625, 0.901, 0.8015, 0.6345, 0.5715, 0.4195, 0.369, 0.2915, 0.2875, 0.205, 0.213, 0.1195, 0.1475, 0.0985, 0.0775, 0.067, 0.0385, 0.0255}',
'{4.8355, 2.6055, 1.861, 1.4615, 1.1625, 0.901, 0.8015, 0.6345, 0.5715, 0.4195, 0.369, 0.2915, 0.2875, 0.205, 0.213, 0.1195, 0.1475, 0.0985, 0.0775, 0.067, 0.0385, 0.0255}',
null,null,
'{28, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 230, 250}',
'{30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 250, 270}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
21,NULL,20,1,
3,29,1,2212,1,21,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
22,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{17}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{230.5, 199.4, 128.7, 91, 62.6, 41.74, 26.97, 17.61, 12.25, 6.76, 5.038, 4.644, 2.76, 1.915, 1.638, 0.6168, 0.6019}',
'{4.3245, 2.44, 1.7825, 1.4355, 1.1505, 0.916, 0.724, 0.582, 0.49, 0.36, 0.3175, 0.3135, 0.242, 0.2035, 0.19, 0.1165, 0.116}',
'{4.3245, 2.44, 1.7825, 1.4355, 1.1505, 0.916, 0.724, 0.582, 0.49, 0.36, 0.3175, 0.3135, 0.242, 0.2035, 0.19, 0.1165, 0.116}',
null,null,
'{28, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180}',
'{30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
22,NULL,20,1,
4,29,1,2212,1,22,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
23,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{16}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{259.3, 214.6, 125.1, 84.32, 56.73, 33.94, 22.56, 16.15, 9.795, 6.775, 4.351, 1.869, 1.537, 1.22, 0.4902, 0.3967}',
'{4.9505, 2.735, 1.8275, 1.4135, 1.1145, 0.825, 0.6665, 0.5695, 0.4405, 0.371, 0.299, 0.1935, 0.18, 0.1625, 0.103, 0.094}',
'{4.9505, 2.735, 1.8275, 1.4135, 1.1145, 0.825, 0.6665, 0.5695, 0.4405, 0.371, 0.299, 0.1935, 0.18, 0.1625, 0.103, 0.094}',
null,null,
'{28, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170}',
'{30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
23,NULL,20,1,
5,29,1,2212,1,23,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
24,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{16}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{237.9, 206.2, 116.2, 80.89, 51.67, 33.87, 20.19, 13.69, 6.984, 4.449, 3.429, 2.398, 1.273, 1.289, 1.061, 0.3474}',
'{4.8875, 2.8295, 1.834, 1.4545, 1.1035, 0.8695, 0.654, 0.5405, 0.3785, 0.305, 0.274, 0.2325, 0.17, 0.174, 0.159, 0.091}',
'{4.8875, 2.8295, 1.834, 1.4545, 1.1035, 0.8695, 0.654, 0.5405, 0.3785, 0.305, 0.274, 0.2325, 0.17, 0.174, 0.159, 0.091}',
null,null,
'{28, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170}',
'{30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
24,NULL,20,1,
6,29,1,2212,1,24,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
25,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{24}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{167.9, 149.9, 139.6, 126.1, 92.58, 60.27, 41.84, 27.12, 20.97, 13.75, 9.603, 6.505, 4.106, 3.03, 1.925, 1.107, 0.6099, 0.3518, 0.2359, 0.1309, 0.0936, 0.0848, 0.0227, 0.0303}',
'{2.324, 2.1405, 2.039, 1.901, 1.0485, 0.7375, 0.558, 0.4085, 0.3445, 0.263, 0.2125, 0.17, 0.1325, 0.1125, 0.089, 0.0665, 0.0495, 0.0375, 0.031, 0.023, 0.019, 0.0185, 0.0095, 0.011}',
'{2.324, 2.1405, 2.039, 1.901, 1.0485, 0.7375, 0.558, 0.4085, 0.3445, 0.263, 0.2125, 0.17, 0.1325, 0.1125, 0.089, 0.0665, 0.0495, 0.0375, 0.031, 0.023, 0.019, 0.0185, 0.0095, 0.011}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
25,NULL,20,1,
1,29,1,2212,1,25,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
26,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{23}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{169.1, 152, 131.6, 108.5, 80.02, 48.72, 33.13, 22.56, 17.14, 10.86, 7.805, 5.113, 3.856, 2.158, 1.224, 1.339, 0.5143, 0.5178, 0.1825, 0.1378, 0.0423, 0.0464, 0.0285}',
'{2.8835, 2.7015, 2.4635, 2.175, 1.0575, 0.748, 0.5895, 0.4725, 0.4085, 0.319, 0.2695, 0.2165, 0.189, 0.1405, 0.106, 0.112, 0.0685, 0.069, 0.041, 0.036, 0.0195, 0.021, 0.0115}',
'{2.8835, 2.7015, 2.4635, 2.175, 1.0575, 0.748, 0.5895, 0.4725, 0.4085, 0.319, 0.2695, 0.2165, 0.189, 0.1405, 0.106, 0.112, 0.0685, 0.069, 0.041, 0.036, 0.0195, 0.021, 0.0115}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 230, 240}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 240, 260}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
26,NULL,20,1,
2,29,1,2212,1,26,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
27,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{19}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{133.2, 120, 106.2, 79.45, 57.38, 34.89, 22.59, 16.48, 13.16, 7.972, 3.961, 2.735, 1.939, 1.276, 0.6005, 0.6652, 0.2068, 0.3648, 0.1804}',
'{2.7175, 2.5625, 2.3885, 1.9715, 0.893, 0.6545, 0.5145, 0.4435, 0.4045, 0.312, 0.2155, 0.181, 0.1545, 0.1265, 0.0865, 0.093, 0.0515, 0.0695, 0.0485}',
'{2.7175, 2.5625, 2.3885, 1.9715, 0.893, 0.6545, 0.5145, 0.4435, 0.4045, 0.312, 0.2155, 0.181, 0.1545, 0.1265, 0.0865, 0.093, 0.0515, 0.0695, 0.0485}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
27,NULL,20,1,
3,29,1,2212,1,27,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
28,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{18}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{104, 114.1, 90.18, 74.48, 56.01, 30.59, 19.49, 13.32, 8.222, 5.565, 3.847, 2.007, 0.9789, 0.925, 0.4421, 0.2417, 0.0975, 0.1123}',
'{2.3265, 2.547, 2.1945, 1.955, 0.8875, 0.6115, 0.4825, 0.403, 0.316, 0.264, 0.2225, 0.16, 0.1115, 0.1115, 0.077, 0.0575, 0.0365, 0.04}',
'{2.3265, 2.547, 2.1945, 1.955, 0.8875, 0.6115, 0.4825, 0.403, 0.316, 0.264, 0.2225, 0.16, 0.1115, 0.1115, 0.077, 0.0575, 0.0365, 0.04}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
28,NULL,20,1,
4,29,1,2212,1,28,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
29,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{17}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{108.9, 99.33, 92.56, 82.95, 51.91, 29.51, 18.31, 10.71, 6.747, 4.063, 1.998, 1.82, 0.6851, 0.2781, 0.489, 0.1507, 0.1778}',
'{2.4995, 2.388, 2.3165, 2.1865, 0.873, 0.62, 0.479, 0.361, 0.2875, 0.2245, 0.156, 0.1545, 0.0935, 0.0585, 0.0825, 0.0455, 0.0505}',
'{2.4995, 2.388, 2.3165, 2.1865, 0.873, 0.62, 0.479, 0.361, 0.2875, 0.2245, 0.156, 0.1545, 0.0935, 0.0585, 0.0825, 0.0455, 0.0505}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
29,NULL,20,1,
5,29,1,2212,1,29,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
30,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{17}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{106.9, 95.65, 78.93, 65.43, 45.78, 22.86, 15.73, 9.179, 4.401, 2.989, 2.556, 1.953, 1.006, 0.3286, 0.179, 0.279, 0.0625}',
'{2.587, 2.436, 2.167, 1.937, 0.8395, 0.543, 0.454, 0.343, 0.2315, 0.1955, 0.188, 0.167, 0.12, 0.068, 0.051, 0.065, 0.0305}',
'{2.587, 2.436, 2.167, 1.937, 0.8395, 0.543, 0.454, 0.343, 0.2315, 0.1955, 0.188, 0.167, 0.12, 0.068, 0.051, 0.065, 0.0305}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
30,NULL,20,1,
6,29,1,2212,1,30,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
31,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{26}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{205.6, 209.1, 182.8, 140.9, 107.4, 94.31, 82.38, 70.01, 61.1, 52.44, 43.16, 37.08, 32.43, 26.36, 24.24, 18.65, 16.68, 14.93, 13.02, 9.607, 7.084, 6.43, 3.982, 1.752, 0.9716, 0.1027}',
'{4.1175, 4.1625, 2.308, 1.866, 1.491, 1.3655, 1.2505, 1.126, 1.0375, 0.9475, 0.8435, 0.7755, 0.721, 0.6425, 0.6165, 0.534, 0.5045, 0.4765, 0.4435, 0.378, 0.322, 0.307, 0.1715, 0.1125, 0.0835, 0.027}',
'{4.1175, 4.1625, 2.308, 1.866, 1.491, 1.3655, 1.2505, 1.126, 1.0375, 0.9475, 0.8435, 0.7755, 0.721, 0.6425, 0.6165, 0.534, 0.5045, 0.4765, 0.4435, 0.378, 0.322, 0.307, 0.1715, 0.1125, 0.0835, 0.027}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290, 310}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290, 310, 330}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
31,NULL,20,1,
1,29,1,1000010020,1,31,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
32,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{25}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{252.4, 239.3, 207.3, 140.9, 103, 91.6, 75.76, 71.68, 58.95, 49.78, 36.14, 32.5, 26.56, 25.53, 23.98, 17.73, 14.96, 9.338, 8.862, 7.126, 5.584, 3.204, 3.129, 1.288, 0.7158}',
'{6.53, 6.305, 3.0895, 2.27, 1.796, 1.6965, 1.527, 1.5195, 1.3735, 1.2675, 1.0645, 1.0265, 0.933, 0.931, 0.912, 0.782, 0.721, 0.566, 0.5555, 0.4985, 0.442, 0.3345, 0.2345, 0.15, 0.1115}',
'{6.53, 6.305, 3.0895, 2.27, 1.796, 1.6965, 1.527, 1.5195, 1.3735, 1.2675, 1.0645, 1.0265, 0.933, 0.931, 0.912, 0.782, 0.721, 0.566, 0.5555, 0.4985, 0.442, 0.3345, 0.2345, 0.15, 0.1115}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290, 310}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
32,NULL,20,1,
2,29,1,1000010020,1,32,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
33,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{24}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{232.9, 226.2, 187.8, 129.6, 105.4, 91.64, 77.13, 65.8, 57.2, 44.93, 34.55, 26.69, 22.1, 21.4, 15.31, 12.89, 9.908, 8.812, 7.324, 5.486, 3.707, 2.191, 1.591, 0.4774}',
'{7.045, 6.895, 3.1495, 2.335, 2.062, 1.9285, 1.772, 1.651, 1.56, 1.383, 1.2135, 1.0725, 0.987, 0.991, 0.838, 0.775, 0.6825, 0.6475, 0.592, 0.5145, 0.422, 0.324, 0.195, 0.1065}',
'{7.045, 6.895, 3.1495, 2.335, 2.062, 1.9285, 1.772, 1.651, 1.56, 1.383, 1.2135, 1.0725, 0.987, 0.991, 0.838, 0.775, 0.6825, 0.6475, 0.592, 0.5145, 0.422, 0.324, 0.195, 0.1065}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
33,NULL,20,1,
3,29,1,1000010020,1,33,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
34,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{24}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{231.9, 216.3, 193.5, 136, 109, 92.94, 83.12, 66.84, 54.57, 45.46, 32.53, 28.16, 21.54, 21.37, 16.86, 10.12, 8.902, 8.155, 6.58, 3.815, 3.149, 1.816, 0.6358, 0.1917}',
'{7.24, 6.9, 3.2355, 2.5105, 2.2065, 2.0445, 1.964, 1.761, 1.6, 1.474, 1.243, 1.1715, 1.029, 1.0405, 0.927, 0.7165, 0.676, 0.65, 0.5845, 0.4445, 0.4035, 0.306, 0.128, 0.07}',
'{7.24, 6.9, 3.2355, 2.5105, 2.2065, 2.0445, 1.964, 1.761, 1.6, 1.474, 1.243, 1.1715, 1.029, 1.0405, 0.927, 0.7165, 0.676, 0.65, 0.5845, 0.4445, 0.4035, 0.306, 0.128, 0.07}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
34,NULL,20,1,
4,29,1,1000010020,1,34,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
35,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{21}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{232.5, 213.2, 223.7, 158.5, 144.6, 122.3, 100.8, 74.99, 68.9, 48.99, 46.08, 32.43, 27.78, 22.3, 17.16, 12.3, 9.442, 7.791, 5.484, 4.249, 1.842}',
'{7.39, 6.97, 7.15, 2.869, 2.748, 2.5065, 2.2585, 1.9135, 1.865, 1.552, 1.532, 1.2765, 1.1905, 1.0705, 0.94, 0.796, 0.6995, 0.638, 0.534, 0.4695, 0.3085}',
'{7.39, 6.97, 7.15, 2.869, 2.748, 2.5065, 2.2585, 1.9135, 1.865, 1.552, 1.532, 1.2765, 1.1905, 1.0705, 0.94, 0.796, 0.6995, 0.638, 0.534, 0.4695, 0.3085}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
35,NULL,20,1,
5,29,1,1000010020,1,35,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
36,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{21}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{234.8, 217.7, 187.5, 143.6, 117.1, 103.5, 86.33, 63.79, 50.21, 40.01, 33.92, 24.81, 20.95, 10.14, 10.9, 6.982, 4.996, 3.438, 2.263, 1.516, 0.9181}',
'{7.65, 7.255, 3.453, 2.907, 2.5835, 2.4365, 2.217, 1.8755, 1.659, 1.4815, 1.3695, 1.167, 1.076, 0.741, 0.775, 0.62, 0.5235, 0.4335, 0.3515, 0.2875, 0.2235}',
'{7.65, 7.255, 3.453, 2.907, 2.5835, 2.4365, 2.217, 1.8755, 1.659, 1.4815, 1.3695, 1.167, 1.076, 0.741, 0.775, 0.62, 0.5235, 0.4335, 0.3515, 0.2875, 0.2235}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
36,NULL,20,1,
6,29,1,1000010020,1,36,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
37,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{20}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{144.6, 124.9, 103.9, 83.05, 61.59, 51.93, 39.45, 32.37, 23.03, 20.83, 15.91, 12.19, 9.087, 7.46, 5.755, 4.146, 3.204, 2.492, 1.934, 0.8499}',
'{3.113, 1.665, 1.4625, 1.255, 1.028, 0.9305, 0.788, 0.7055, 0.581, 0.554, 0.4795, 0.416, 0.357, 0.3225, 0.283, 0.239, 0.21, 0.185, 0.1625, 0.1075}',
'{3.113, 1.665, 1.4625, 1.255, 1.028, 0.9305, 0.788, 0.7055, 0.581, 0.554, 0.4795, 0.416, 0.357, 0.3225, 0.283, 0.239, 0.21, 0.185, 0.1625, 0.1075}',
null,null,
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230}',
'{50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
37,NULL,20,1,
1,29,1,1000010020,1,37,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
38,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{20}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{162.5, 132.8, 104, 79.31, 63.45, 53.39, 34.94, 24.59, 20.69, 19.02, 12.34, 10.21, 7.214, 4.986, 3.964, 3.308, 2.303, 1.1, 0.9901, 0.3261}',
'{4.5945, 2.147, 1.853, 1.5825, 1.41, 1.3045, 1.0305, 0.8595, 0.8005, 0.7815, 0.626, 0.5745, 0.4835, 0.402, 0.3605, 0.3305, 0.276, 0.1905, 0.1815, 0.104}',
'{4.5945, 2.147, 1.853, 1.5825, 1.41, 1.3045, 1.0305, 0.8595, 0.8005, 0.7815, 0.626, 0.5745, 0.4835, 0.402, 0.3605, 0.3305, 0.276, 0.1905, 0.1815, 0.104}',
null,null,
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230}',
'{50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
38,NULL,20,1,
2,29,1,1000010020,1,38,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
39,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{19}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{151.1, 112.7, 95.41, 73.62, 55.88, 43.14, 31.91, 23.15, 16.3, 15.41, 12.51, 9.303, 5.492, 1.643, 3.24, 2.629, 0.9939, 1.686, 0.2877}',
'{4.998, 2.119, 1.9645, 1.7135, 1.489, 1.316, 1.1345, 0.9705, 0.8175, 0.815, 0.742, 0.6425, 0.4925, 0.2635, 0.382, 0.3455, 0.212, 0.2775, 0.1145}',
'{4.998, 2.119, 1.9645, 1.7135, 1.489, 1.316, 1.1345, 0.9705, 0.8175, 0.815, 0.742, 0.6425, 0.4925, 0.2635, 0.382, 0.3455, 0.212, 0.2775, 0.1145}',
null,null,
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220}',
'{50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
39,NULL,20,1,
3,29,1,1000010020,1,39,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
40,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{17}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{140.2, 118.4, 97.39, 67.14, 49.38, 36.49, 30.8, 21.3, 13.85, 10.37, 8.732, 5.221, 4.489, 3.381, 1.211, 1.385, 0.2908}',
'{5.02, 2.2825, 2.0785, 1.689, 1.4475, 1.251, 1.174, 0.978, 0.789, 0.69, 0.642, 0.496, 0.465, 0.4055, 0.2415, 0.2605, 0.1195}',
'{5.02, 2.2825, 2.0785, 1.689, 1.4475, 1.251, 1.174, 0.978, 0.789, 0.69, 0.642, 0.496, 0.465, 0.4055, 0.2415, 0.2605, 0.1195}',
null,null,
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200}',
'{50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
40,NULL,20,1,
4,29,1,1000010020,1,40,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
41,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{18}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{145.3, 135.2, 128.1, 98.38, 75.69, 49.57, 38.59, 29.38, 20.2, 16.14, 6.884, 6.695, 4.884, 3.993, 2.557, 1.255, 0.6608, 0.2138}',
'{5.3, 5.105, 2.5225, 2.18, 1.9015, 1.5065, 1.3385, 1.1745, 0.974, 0.879, 0.564, 0.5665, 0.4865, 0.442, 0.3545, 0.2485, 0.181, 0.1025}',
'{5.3, 5.105, 2.5225, 2.18, 1.9015, 1.5065, 1.3385, 1.1745, 0.974, 0.879, 0.564, 0.5665, 0.4865, 0.442, 0.3545, 0.2485, 0.181, 0.1025}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
41,NULL,20,1,
5,29,1,1000010020,1,41,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
42,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{17}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{165.6, 137.1, 131.9, 106.7, 75.06, 52.31, 32.71, 25.24, 18.66, 13.07, 8.033, 4.34, 3.779, 1.96, 2.372, 1.046, 0.5947}',
'{6.08, 5.43, 2.7305, 2.436, 1.9955, 1.641, 1.2755, 1.1275, 0.974, 0.8165, 0.639, 0.469, 0.441, 0.318, 0.352, 0.234, 0.176}',
'{6.08, 5.43, 2.7305, 2.436, 1.9955, 1.641, 1.2755, 1.1275, 0.974, 0.8165, 0.639, 0.469, 0.441, 0.318, 0.352, 0.234, 0.176}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
42,NULL,20,1,
6,29,1,1000010020,1,42,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
43,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{26}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{93.33, 88.05, 67.79, 50.28, 39.02, 27.83, 20.86, 13.94, 10.07, 8.011, 6.175, 4.119, 3.048, 2.203, 1.609, 1.24, 0.9659, 0.6781, 0.3748, 0.2651, 0.202, 0.1316, 0.0907, 0.0523, 0.0384, 0.024}',
'{1.2775, 1.225, 0.735, 0.5695, 0.4625, 0.3535, 0.286, 0.214, 0.173, 0.1505, 0.129, 0.1015, 0.086, 0.072, 0.061, 0.053, 0.0465, 0.039, 0.0285, 0.024, 0.021, 0.017, 0.01, 0.008, 0.0065, 0.005}',
'{1.2775, 1.225, 0.735, 0.5695, 0.4625, 0.3535, 0.286, 0.214, 0.173, 0.1505, 0.129, 0.1015, 0.086, 0.072, 0.061, 0.053, 0.0465, 0.039, 0.0285, 0.024, 0.021, 0.017, 0.01, 0.008, 0.0065, 0.005}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290, 310}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290, 310, 330}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
43,NULL,20,1,
1,29,1,1000010020,1,43,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
44,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{22}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{99.49, 82.16, 69.24, 48.42, 36.15, 26.6, 20.23, 12.53, 8.379, 6.709, 5.168, 3.197, 2.659, 1.802, 1.28, 0.8854, 0.569, 0.5354, 0.3044, 0.1729, 0.1478, 0.06}',
'{1.6675, 1.459, 0.8295, 0.633, 0.517, 0.4235, 0.3595, 0.2685, 0.2135, 0.192, 0.1685, 0.1305, 0.1195, 0.098, 0.0825, 0.069, 0.055, 0.054, 0.0405, 0.0305, 0.028, 0.018}',
'{1.6675, 1.459, 0.8295, 0.633, 0.517, 0.4235, 0.3595, 0.2685, 0.2135, 0.192, 0.1685, 0.1305, 0.1195, 0.098, 0.0825, 0.069, 0.055, 0.054, 0.0405, 0.0305, 0.028, 0.018}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
44,NULL,20,1,
2,29,1,1000010020,1,44,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
45,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{19}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{77.18, 74.32, 57.49, 34.36, 28.09, 20.01, 14.41, 9.965, 6.33, 4.877, 3.488, 2.477, 1.86, 0.8951, 0.6238, 0.5081, 0.3887, 0.2509, 0.082}',
'{1.5405, 1.519, 0.7675, 0.528, 0.475, 0.3895, 0.3245, 0.2665, 0.209, 0.1855, 0.157, 0.133, 0.116, 0.0795, 0.067, 0.061, 0.0535, 0.0425, 0.0245}',
'{1.5405, 1.519, 0.7675, 0.528, 0.475, 0.3895, 0.3245, 0.2665, 0.209, 0.1855, 0.157, 0.133, 0.116, 0.0795, 0.067, 0.061, 0.0535, 0.0425, 0.0245}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
45,NULL,20,1,
3,29,1,1000010020,1,45,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
46,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{19}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{70.26, 76.5, 51.89, 37.66, 25.92, 18.68, 14.1, 7.856, 5.937, 3.685, 3.292, 1.326, 1.098, 0.759, 0.7535, 0.3273, 0.2837, 0.1427, 0.111}',
'{1.493, 1.6105, 0.708, 0.5775, 0.4605, 0.384, 0.3335, 0.24, 0.211, 0.1655, 0.1595, 0.0995, 0.0915, 0.0765, 0.0775, 0.0505, 0.0475, 0.0335, 0.0295}',
'{1.493, 1.6105, 0.708, 0.5775, 0.4605, 0.384, 0.3335, 0.24, 0.211, 0.1655, 0.1595, 0.0995, 0.0915, 0.0765, 0.0775, 0.0505, 0.0475, 0.0335, 0.0295}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
46,NULL,20,1,
4,29,1,1000010020,1,46,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
47,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{17}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{83.09, 77.65, 69.77, 56.26, 35.9, 27.09, 17.45, 11.9, 7.191, 4.538, 3.751, 2.039, 1.481, 0.8425, 0.6524, 0.3377, 0.18}',
'{1.7385, 1.6765, 1.573, 0.794, 0.5835, 0.4945, 0.3805, 0.309, 0.235, 0.185, 0.171, 0.125, 0.107, 0.081, 0.072, 0.052, 0.0375}',
'{1.7385, 1.6765, 1.573, 0.794, 0.5835, 0.4945, 0.3805, 0.309, 0.235, 0.185, 0.171, 0.125, 0.107, 0.081, 0.072, 0.052, 0.0375}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
47,NULL,20,1,
5,29,1,1000010020,1,47,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
48,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{15}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{75.07, 74.03, 69.63, 55.11, 34.81, 23.81, 16.76, 9.602, 6.637, 3.807, 2.488, 1.595, 0.7609, 0.5953, 0.235}',
'{1.713, 1.7155, 1.662, 0.8355, 0.6065, 0.479, 0.3915, 0.284, 0.235, 0.175, 0.142, 0.114, 0.0785, 0.07, 0.044}',
'{1.713, 1.7155, 1.662, 0.8355, 0.6065, 0.479, 0.3915, 0.284, 0.235, 0.175, 0.142, 0.114, 0.0785, 0.07, 0.044}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
48,NULL,20,1,
6,29,1,1000010020,1,48,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
49,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{13}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{47.19, 27.95, 16.97, 11.22, 7.08, 4.747, 3.414, 2.155, 0.8245, 0.6453, 0.5383, 0.3045, 0.1728}',
'{0.673, 0.4705, 0.345, 0.273, 0.2115, 0.172, 0.1455, 0.115, 0.0695, 0.062, 0.057, 0.043, 0.032}',
'{0.673, 0.4705, 0.345, 0.273, 0.2115, 0.172, 0.1455, 0.115, 0.0695, 0.062, 0.057, 0.043, 0.032}',
null,null,
'{40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160}',
'{50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
49,NULL,20,1,
1,29,1,1000010020,1,49,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
50,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{11}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{44.37, 22.88, 15.35, 9.076, 5.663, 3.474, 1.332, 1.393, 0.8121, 0.4762, 0.2344}',
'{0.82, 0.5505, 0.4515, 0.3435, 0.272, 0.214, 0.128, 0.1375, 0.1055, 0.081, 0.057}',
'{0.82, 0.5505, 0.4515, 0.3435, 0.272, 0.214, 0.128, 0.1375, 0.1055, 0.081, 0.057}',
null,null,
'{40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140}',
'{50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
50,NULL,20,1,
2,29,1,1000010020,1,50,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
51,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{11}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{34.28, 18.97, 11.61, 7.002, 3.917, 2.536, 1.806, 0.8591, 0.9044, 0.3519, 0.2088}',
'{0.763, 0.5495, 0.431, 0.3365, 0.2515, 0.2065, 0.179, 0.122, 0.1315, 0.0805, 0.0625}',
'{0.763, 0.5495, 0.431, 0.3365, 0.2515, 0.2065, 0.179, 0.122, 0.1315, 0.0805, 0.0625}',
null,null,
'{40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140}',
'{50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
51,NULL,20,1,
3,29,1,1000010020,1,51,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
52,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{9}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{30.51, 15.24, 7.916, 5.546, 3.835, 2.562, 0.4164, 0.6498, 0.3699}',
'{0.7295, 0.496, 0.352, 0.3065, 0.263, 0.2195, 0.075, 0.11, 0.084}',
'{0.7295, 0.496, 0.352, 0.3065, 0.263, 0.2195, 0.075, 0.11, 0.084}',
null,null,
'{40, 50, 60, 70, 80, 90, 100, 110, 120}',
'{50, 60, 70, 80, 90, 100, 110, 120, 130}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
52,NULL,20,1,
4,29,1,1000010020,1,52,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
53,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{8}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{29.4, 14.37, 10.85, 4.715, 2.504, 1.255, 0.951, 0.3336}',
'{0.7395, 0.496, 0.4495, 0.2865, 0.2085, 0.147, 0.134, 0.0765}',
'{0.7395, 0.496, 0.4495, 0.2865, 0.2085, 0.147, 0.134, 0.0765}',
null,null,
'{40, 50, 60, 70, 80, 90, 100, 110}',
'{50, 60, 70, 80, 90, 100, 110, 120}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
53,NULL,20,1,
5,29,1,1000010020,1,53,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
54,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{8}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{26.69, 17.4, 7.151, 4.524, 2.441, 1.544, 1.26, 0.3466}',
'{0.729, 0.593, 0.361, 0.2935, 0.2165, 0.176, 0.1645, 0.083}',
'{0.729, 0.593, 0.361, 0.2935, 0.2165, 0.176, 0.1645, 0.083}',
null,null,
'{40, 50, 60, 70, 80, 90, 100, 110}',
'{50, 60, 70, 80, 90, 100, 110, 120}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
54,NULL,20,1,
6,29,1,1000010020,1,54,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
55,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{11}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{18.74, 18.26, 15.27, 10.44, 6.301, 3.474, 1.79, 1.083, 0.5128, 0.3437, 0.2114}',
'{0.609, 0.6035, 0.5475, 0.2195, 0.1655, 0.1195, 0.084, 0.065, 0.0445, 0.037, 0.0285}',
'{0.609, 0.6035, 0.5475, 0.2195, 0.1655, 0.1195, 0.084, 0.065, 0.0445, 0.037, 0.0285}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
55,NULL,20,1,
1,29,1,1000010020,1,55,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
56,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{11}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{18.82, 17.36, 15.57, 9.215, 5.273, 2.917, 1.484, 0.8148, 0.4853, 0.3074, 0.1504}',
'{0.8735, 0.843, 0.8, 0.2835, 0.213, 0.158, 0.1125, 0.0835, 0.065, 0.052, 0.036}',
'{0.8735, 0.843, 0.8, 0.2835, 0.213, 0.158, 0.1125, 0.0835, 0.065, 0.052, 0.036}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
56,NULL,20,1,
2,29,1,1000010020,1,56,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
57,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{8}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{13.21, 14.38, 9.925, 7.063, 3.866, 1.588, 1.037, 0.4221}',
'{0.8205, 0.8815, 0.7125, 0.279, 0.2075, 0.13, 0.108, 0.068}',
'{0.8205, 0.8815, 0.7125, 0.279, 0.2075, 0.13, 0.108, 0.068}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90}',
'{46, 48, 50, 60, 70, 80, 90, 100}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
57,NULL,20,1,
3,29,1,1000010020,1,57,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
58,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{7}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{10.74, 8.609, 6.756, 4.866, 2.87, 2.033, 0.4267}',
'{0.7555, 0.6705, 0.5865, 0.231, 0.1815, 0.1585, 0.0665}',
'{0.7555, 0.6705, 0.5865, 0.231, 0.1815, 0.1585, 0.0665}',
null,null,
'{44, 46, 48, 50, 60, 70, 80}',
'{46, 48, 50, 60, 70, 80, 90}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
58,NULL,20,1,
4,29,1,1000010020,1,58,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
59,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{9}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{10.41, 8.183, 6.286, 4.3, 2.308, 1.083, 0.7179, 0.1101, 0.137}',
'{0.7615, 0.6685, 0.578, 0.22, 0.163, 0.1115, 0.0935, 0.032, 0.0405}',
'{0.7615, 0.6685, 0.578, 0.22, 0.163, 0.1115, 0.0935, 0.032, 0.0405}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
59,NULL,20,1,
5,29,1,1000010020,1,59,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
60,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{9}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{10.99, 4.581, 5.454, 4.127, 2.207, 0.9488, 0.1813, 0.1657, 0.1463}',
'{0.8215, 0.479, 0.5515, 0.224, 0.166, 0.108, 0.0415, 0.044, 0.0445}',
'{0.8215, 0.479, 0.5515, 0.224, 0.166, 0.108, 0.0415, 0.044, 0.0445}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
60,NULL,20,1,
6,29,1,1000010020,1,60,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
61,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{18}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{45.87, 45.72, 38.18, 36.88, 25.55, 20.43, 13.18, 11.26, 9.014, 7.846, 7.257, 3.594, 3.493, 2.655, 2.188, 1.308, 0.925, 0.6147}',
'{1.784, 1.778, 1.6015, 1.566, 0.599, 0.532, 0.416, 0.3885, 0.3495, 0.329, 0.3195, 0.2215, 0.221, 0.1925, 0.1755, 0.135, 0.114, 0.093}',
'{1.784, 1.778, 1.6015, 1.566, 0.599, 0.532, 0.416, 0.3885, 0.3495, 0.329, 0.3195, 0.2215, 0.221, 0.1925, 0.1755, 0.135, 0.114, 0.093}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
61,NULL,20,1,
1,29,1,1000010030,1,61,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
62,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{14}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{47.19, 48.62, 33.8, 22.97, 27.79, 19.17, 13.42, 11.43, 7.537, 4.438, 4.705, 3.881, 2.609, 0.8866}',
'{2.6585, 2.693, 2.145, 1.656, 0.8905, 0.7325, 0.6125, 0.5805, 0.472, 0.36, 0.3855, 0.355, 0.2925, 0.167}',
'{2.6585, 2.693, 2.145, 1.656, 0.8905, 0.7325, 0.6125, 0.5805, 0.472, 0.36, 0.3855, 0.355, 0.2925, 0.167}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
62,NULL,20,1,
2,29,1,1000010030,1,62,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
63,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{15}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{36.39, 45.08, 31.31, 28.47, 24.33, 19.9, 14.13, 12.74, 11.03, 4.764, 3.309, 3.234, 2.703, 1.882, 0.9588}',
'{2.5625, 2.914, 2.3345, 2.2175, 0.95, 0.8785, 0.745, 0.7305, 0.6945, 0.446, 0.3755, 0.381, 0.352, 0.2945, 0.2105}',
'{2.5625, 2.914, 2.3345, 2.2175, 0.95, 0.8785, 0.745, 0.7305, 0.6945, 0.446, 0.3755, 0.381, 0.352, 0.2945, 0.2105}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
63,NULL,20,1,
3,29,1,1000010030,1,63,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
64,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{15}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{35.59, 45.83, 25.85, 35.19, 23.73, 17.17, 17.55, 10.31, 5.364, 5.421, 5.396, 2.49, 2.555, 1.5, 0.912}',
'{2.653, 3.123, 2.2105, 2.705, 0.996, 0.8545, 0.9005, 0.6865, 0.49, 0.5085, 0.517, 0.349, 0.3585, 0.275, 0.2145}',
'{2.653, 3.123, 2.2105, 2.705, 0.996, 0.8545, 0.9005, 0.6865, 0.49, 0.5085, 0.517, 0.349, 0.3585, 0.275, 0.2145}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
64,NULL,20,1,
4,29,1,1000010030,1,64,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
65,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{14}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{41.18, 53.79, 32.57, 36.1, 32.79, 18.43, 13.58, 8.78, 6.176, 4.72, 4.207, 1.85, 1.523, 0.5909}',
'{2.9975, 3.522, 2.6465, 2.832, 1.254, 0.923, 0.7995, 0.6445, 0.545, 0.4815, 0.4595, 0.303, 0.2775, 0.172}',
'{2.9975, 3.522, 2.6465, 2.832, 1.254, 0.923, 0.7995, 0.6445, 0.545, 0.4815, 0.4595, 0.303, 0.2775, 0.172}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
65,NULL,20,1,
5,29,1,1000010030,1,65,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
66,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{14}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{44.78, 34.56, 41.6, 38.25, 22.36, 18.94, 10.04, 9.036, 6.84, 5.723, 2.307, 1.903, 1.498, 0.4197}',
'{3.2945, 2.856, 3.195, 3.0635, 1.05, 0.983, 0.7065, 0.684, 0.6, 0.553, 0.349, 0.319, 0.2845, 0.1495}',
'{3.2945, 2.856, 3.195, 3.0635, 1.05, 0.983, 0.7065, 0.684, 0.6, 0.553, 0.349, 0.319, 0.2845, 0.1495}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
66,NULL,20,1,
6,29,1,1000010030,1,66,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
67,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{15}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{27.64, 24.29, 20.05, 12.86, 9.931, 5.999, 4.451, 3.054, 2.75, 1.508, 1.214, 0.7664, 0.3617, 0.1728, 0.0944}',
'{1.31, 1.2225, 0.5265, 0.414, 0.3645, 0.28, 0.2425, 0.201, 0.192, 0.142, 0.1275, 0.101, 0.0695, 0.048, 0.0355}',
'{1.31, 1.2225, 0.5265, 0.414, 0.3645, 0.28, 0.2425, 0.201, 0.192, 0.142, 0.1275, 0.101, 0.0695, 0.048, 0.0355}',
null,null,
'{56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180}',
'{58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
67,NULL,20,1,
1,29,1,1000010030,1,67,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
68,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{12}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{27.07, 32.65, 20.39, 14.09, 8.422, 6.833, 4.348, 3.392, 1.572, 1.21, 0.6896, 0.5085}',
'{1.915, 2.162, 0.771, 0.6445, 0.496, 0.4565, 0.3655, 0.327, 0.221, 0.1965, 0.1485, 0.128}',
'{1.915, 2.162, 0.771, 0.6445, 0.496, 0.4565, 0.3655, 0.327, 0.221, 0.1965, 0.1485, 0.128}',
null,null,
'{56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150}',
'{58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
68,NULL,20,1,
2,29,1,1000010030,1,68,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
69,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{11}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{39.49, 20.42, 20.07, 7.495, 8.289, 4.887, 3.67, 2.118, 1.526, 0.8712, 0.3374}',
'{2.8005, 1.9065, 0.89, 0.515, 0.578, 0.445, 0.3925, 0.299, 0.257, 0.1945, 0.1205}',
'{2.8005, 1.9065, 0.89, 0.515, 0.578, 0.445, 0.3925, 0.299, 0.257, 0.1945, 0.1205}',
null,null,
'{56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140}',
'{58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
69,NULL,20,1,
3,29,1,1000010030,1,69,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
70,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{10}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{23.25, 24.81, 15.51, 9.431, 8.125, 3.129, 1.137, 1.2, 1.668, 0.3994}',
'{2.169, 2.277, 0.806, 0.631, 0.604, 0.367, 0.2155, 0.232, 0.2825, 0.136}',
'{2.169, 2.277, 0.806, 0.631, 0.604, 0.367, 0.2155, 0.232, 0.2825, 0.136}',
null,null,
'{56, 58, 60, 70, 80, 90, 100, 110, 120, 130}',
'{58, 60, 70, 80, 90, 100, 110, 120, 130, 140}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
70,NULL,20,1,
4,29,1,1000010030,1,70,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
71,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{9}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{24.91, 28.27, 13.51, 10.57, 6.402, 1.988, 2.068, 1.446, 0.3563}',
'{2.3215, 2.512, 0.7655, 0.692, 0.541, 0.2925, 0.3095, 0.2615, 0.1275}',
'{2.3215, 2.512, 0.7655, 0.692, 0.541, 0.2925, 0.3095, 0.2615, 0.1275}',
null,null,
'{56, 58, 60, 70, 80, 90, 100, 110, 120}',
'{58, 60, 70, 80, 90, 100, 110, 120, 130}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
71,NULL,20,1,
5,29,1,1000010030,1,71,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
72,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{9}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{21.24, 18.53, 15.14, 5.031, 7.59, 4.122, 2.181, 1.249, 0.4937}',
'{2.216, 2.068, 0.8565, 0.4755, 0.6175, 0.4545, 0.331, 0.251, 0.1575}',
'{2.216, 2.068, 0.8565, 0.4755, 0.6175, 0.4545, 0.331, 0.251, 0.1575}',
null,null,
'{56, 58, 60, 70, 80, 90, 100, 110, 120}',
'{58, 60, 70, 80, 90, 100, 110, 120, 130}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
72,NULL,20,1,
6,29,1,1000010030,1,72,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
73,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{19}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{13.83, 12.61, 12.16, 8.174, 5.33, 3.138, 2.302, 1.454, 0.8817, 0.6008, 0.4365, 0.3018, 0.1474, 0.2398, 0.1265, 0.058, 0.0356, 0.0139, 0.0122}',
'{0.3825, 0.365, 0.3595, 0.1465, 0.114, 0.0845, 0.0715, 0.0565, 0.0435, 0.036, 0.0305, 0.026, 0.018, 0.023, 0.0165, 0.0115, 0.009, 0.0055, 0.005}',
'{0.3825, 0.365, 0.3595, 0.1465, 0.114, 0.0845, 0.0715, 0.0565, 0.0435, 0.036, 0.0305, 0.026, 0.018, 0.023, 0.0165, 0.0115, 0.009, 0.0055, 0.005}',
null,null,
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210}',
'{56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
73,NULL,20,1,
1,29,1,1000010030,1,73,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
74,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{17}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{15.32, 11.35, 10.63, 8.105, 5.238, 3.479, 1.996, 1.167, 0.7597, 0.4597, 0.3198, 0.144, 0.1106, 0.1453, 0.0738, 0.0402, 0.0319}',
'{0.5835, 0.4885, 0.476, 0.1995, 0.159, 0.1305, 0.098, 0.075, 0.0615, 0.048, 0.0405, 0.027, 0.024, 0.0275, 0.02, 0.015, 0.013}',
'{0.5835, 0.4885, 0.476, 0.1995, 0.159, 0.1305, 0.098, 0.075, 0.0615, 0.048, 0.0405, 0.027, 0.024, 0.0275, 0.02, 0.015, 0.013}',
null,null,
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190}',
'{56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
74,NULL,20,1,
2,29,1,1000010030,1,74,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
75,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{13}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{10.53, 5.378, 3.463, 2.22, 1.681, 1.206, 0.52, 0.5256, 0.3033, 0.1652, 0.0667, 0.0541, 0.0316}',
'{0.549, 0.174, 0.142, 0.116, 0.104, 0.0895, 0.0585, 0.0605, 0.046, 0.034, 0.0215, 0.0195, 0.015}',
'{0.549, 0.174, 0.142, 0.116, 0.104, 0.0895, 0.0585, 0.0605, 0.046, 0.034, 0.0215, 0.0195, 0.015}',
null,null,
'{58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170}',
'{60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
75,NULL,20,1,
3,29,1,1000010030,1,75,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
76,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{12}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{12.43, 9.672, 10.78, 5.709, 3.132, 1.9, 0.986, 0.9027, 0.4712, 0.2102, 0.1028, 0.0931}',
'{0.6255, 0.5425, 0.59, 0.192, 0.1415, 0.112, 0.0805, 0.0805, 0.0585, 0.0385, 0.027, 0.027}',
'{0.6255, 0.5425, 0.59, 0.192, 0.1415, 0.112, 0.0805, 0.0805, 0.0585, 0.0385, 0.027, 0.027}',
null,null,
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140}',
'{56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
76,NULL,20,1,
4,29,1,1000010030,1,76,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
77,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{13}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{9.929, 7.984, 7.593, 5.963, 3.208, 2.087, 1.063, 0.6936, 0.4084, 0.2011, 0.149, 0.1434, 0.0752}',
'{0.5625, 0.499, 0.4915, 0.2045, 0.149, 0.1215, 0.087, 0.071, 0.055, 0.039, 0.034, 0.0335, 0.0245}',
'{0.5625, 0.499, 0.4915, 0.2045, 0.149, 0.1215, 0.087, 0.071, 0.055, 0.039, 0.034, 0.0335, 0.0245}',
null,null,
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150}',
'{56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
77,NULL,20,1,
5,29,1,1000010030,1,77,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
78,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{12}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{13.4, 11.45, 7.634, 7.883, 6.154, 2.581, 2.263, 1.133, 0.6606, 0.2831, 0.2855, 0.0933}',
'{0.7, 0.645, 0.511, 0.5285, 0.2195, 0.1375, 0.1335, 0.094, 0.072, 0.047, 0.048, 0.0275}',
'{0.7, 0.645, 0.511, 0.5285, 0.2195, 0.1375, 0.1335, 0.094, 0.072, 0.047, 0.048, 0.0275}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
78,NULL,20,1,
6,29,1,1000010030,1,78,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
79,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{8}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{8.636, 5.237, 2.873, 1.34, 0.8335, 0.6062, 0.32, 0.1198}',
'{0.501, 0.1795, 0.1315, 0.089, 0.0705, 0.061, 0.0445, 0.027}',
'{0.501, 0.1795, 0.1315, 0.089, 0.0705, 0.061, 0.0445, 0.027}',
null,null,
'{48, 50, 60, 70, 80, 90, 100, 110}',
'{50, 60, 70, 80, 90, 100, 110, 120}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
79,NULL,20,1,
1,29,1,1000010030,1,79,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
80,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{8}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{7.32, 5.414, 2.639, 2.075, 0.8498, 0.2663, 0.1684, 0.1152}',
'{0.6795, 0.27, 0.189, 0.172, 0.1095, 0.0605, 0.049, 0.041}',
'{0.6795, 0.27, 0.189, 0.172, 0.1095, 0.0605, 0.049, 0.041}',
null,null,
'{48, 50, 60, 70, 80, 90, 100, 110}',
'{50, 60, 70, 80, 90, 100, 110, 120}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
80,NULL,20,1,
2,29,1,1000010030,1,80,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
81,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{7}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{5.423, 3.115, 1.001, 0.4114, 0.6762, 0.219, 0.1223}',
'{0.6565, 0.224, 0.1225, 0.078, 0.1135, 0.0635, 0.05}',
'{0.6565, 0.224, 0.1225, 0.078, 0.1135, 0.0635, 0.05}',
null,null,
'{48, 50, 60, 70, 80, 90, 120}',
'{50, 60, 70, 80, 90, 100, 130}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
81,NULL,20,1,
3,29,1,1000010030,1,81,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
82,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{7}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{4.136, 3.11, 1.297, 1.025, 0.7221, 0.2451, 0.1694}',
'{0.5855, 0.2365, 0.1525, 0.143, 0.123, 0.071, 0.0605}',
'{0.5855, 0.2365, 0.1525, 0.143, 0.123, 0.071, 0.0605}',
null,null,
'{48, 50, 60, 70, 80, 90, 100}',
'{50, 60, 70, 80, 90, 100, 110}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
82,NULL,20,1,
4,29,1,1000010030,1,82,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
83,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{5}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{4.029, 3.349, 1.266, 0.5786, 0.1628}',
'{0.595, 0.253, 0.1545, 0.1055, 0.058}',
'{0.595, 0.253, 0.1545, 0.1055, 0.058}',
null,null,
'{48, 50, 60, 70, 90}',
'{50, 60, 70, 80, 100}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
83,NULL,20,1,
5,29,1,1000010030,1,83,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
84,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{5}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{2.797, 2.289, 0.9529, 0.5521, 0.1769}',
'{0.497, 0.2115, 0.137, 0.1075, 0.0625}',
'{0.497, 0.2115, 0.137, 0.1075, 0.0625}',
null,null,
'{48, 50, 60, 70, 90}',
'{50, 60, 70, 80, 100}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
84,NULL,20,1,
6,29,1,1000010030,1,84,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
85,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{6}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{1.618, 1.511, 0.8816, 0.3047, 0.1531, 0.0273}',
'{0.1745, 0.169, 0.058, 0.034, 0.0245, 0.01}',
'{0.1745, 0.169, 0.058, 0.034, 0.0245, 0.01}',
null,null,
'{56, 58, 60, 70, 80, 90}',
'{58, 60, 70, 80, 90, 100}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
85,NULL,20,1,
1,29,1,1000010030,1,85,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
86,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{4}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{2.646, 0.8923, 0.2864, 0.0568}',
'{0.3415, 0.087, 0.0485, 0.0205}',
'{0.3415, 0.087, 0.0485, 0.0205}',
null,null,
'{58, 60, 70, 80}',
'{60, 70, 80, 90}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
86,NULL,20,1,
2,29,1,1000010030,1,86,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
87,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{4}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{1.079, 0.5895, 0.2114, 0.0953}',
'{0.2455, 0.081, 0.0485, 0.0345}',
'{0.2455, 0.081, 0.0485, 0.0345}',
null,null,
'{58, 60, 70, 90}',
'{60, 70, 80, 100}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
87,NULL,20,1,
3,29,1,1000010030,1,87,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
88,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{3}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{0.4422, 0.1885, 0.0962}',
'{0.0725, 0.048, 0.035}',
'{0.0725, 0.048, 0.035}',
null,null,
'{60, 70, 80}',
'{70, 80, 90}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
88,NULL,20,1,
4,29,1,1000010030,1,88,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
89,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{3}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{1.222, 0.1966, 0.0822}',
'{0.2805, 0.045, 0.03}',
'{0.2805, 0.045, 0.03}',
null,null,
'{58, 60, 70}',
'{60, 70, 80}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
89,NULL,20,1,
5,29,1,1000010030,1,89,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
90,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{1}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{0.2815}',
'{0.059}',
'{0.059}',
null,null,
'{60}',
'{70}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
90,NULL,20,1,
6,29,1,1000010030,1,90,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
91,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{34}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{2000, 1664, 1620, 1494, 1364, 1143, 922.8, 737.3, 675.4, 659.8, 604, 482.8, 470, 399.3, 387.8, 361.1, 330.2, 304.5, 276, 234.5, 225, 188.6, 174.7, 136.9, 116, 87.21, 75.51, 59.74, 47.45, 36.91, 27.86, 18.21, 12.63, 8.558}',
'{35.47, 31.305, 30.735, 18.2, 16.88, 14.6, 12.255, 10.3, 9.68, 9.58, 9.025, 7.69, 7.62, 6.84, 6.765, 6.485, 6.235, 5.955, 5.635, 5.12, 5.03, 4.546, 4.3735, 2.8575, 2.6495, 2.247, 2.071, 1.8195, 1.605, 1.163, 0.9995, 0.7985, 0.66, 0.5405}',
'{35.47, 31.305, 30.735, 18.2, 16.88, 14.6, 12.255, 10.3, 9.68, 9.58, 9.025, 7.69, 7.62, 6.84, 6.765, 6.485, 6.235, 5.955, 5.635, 5.12, 5.03, 4.546, 4.3735, 2.8575, 2.6495, 2.247, 2.071, 1.8195, 1.605, 1.163, 0.9995, 0.7985, 0.66, 0.5405}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420, 450, 480}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420, 450, 480, 510}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
91,NULL,20,1,
1,83,1,2212,1,91,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
92,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{31}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{2041, 1790, 1599, 1568, 1253, 990.7, 717.1, 653.8, 546.9, 583.5, 482.9, 404.3, 411.4, 326.7, 322.5, 328.8, 323.3, 318.8, 264.5, 218.7, 197.2, 172.2, 156.7, 140.1, 109.9, 87.93, 60.54, 50.31, 42.76, 32.87, 21.81}',
'{48.7, 44.61, 41.335, 22.45, 18.87, 15.68, 12.215, 11.555, 10.245, 10.985, 9.71, 8.695, 9.025, 7.815, 7.89, 8.41, 8.485, 8.56, 7.745, 7.005, 6.695, 6.275, 6.01, 4.3475, 3.814, 3.386, 2.784, 2.5285, 2.3255, 1.6685, 1.3515}',
'{48.7, 44.61, 41.335, 22.45, 18.87, 15.68, 12.215, 11.555, 10.245, 10.985, 9.71, 8.695, 9.025, 7.815, 7.89, 8.41, 8.485, 8.56, 7.745, 7.005, 6.695, 6.275, 6.01, 4.3475, 3.814, 3.386, 2.784, 2.5285, 2.3255, 1.6685, 1.3515}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
92,NULL,20,1,
2,83,1,2212,1,92,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
93,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{32}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{1872, 1618, 1526, 1759, 1372, 1178, 964, 674.7, 519.4, 502.8, 545.1, 459.8, 431.3, 360.9, 278.5, 295.8, 285, 298.9, 217.2, 159.7, 150, 148.7, 127.8, 126.6, 103.9, 76.74, 56.35, 42.89, 30.62, 28.31, 16.89, 12.14}',
'{51.7, 46.77, 44.91, 49.44, 22.12, 19.665, 16.755, 12.87, 10.69, 10.69, 11.67, 10.52, 10.3, 9.28, 7.865, 8.765, 8.785, 9.29, 7.745, 6.53, 6.45, 6.55, 5.985, 6.645, 4.303, 3.6715, 3.1285, 2.7195, 2.29, 2.201, 1.3875, 1.174}',
'{51.7, 46.77, 44.91, 49.44, 22.12, 19.665, 16.755, 12.87, 10.69, 10.69, 11.67, 10.52, 10.3, 9.28, 7.865, 8.765, 8.785, 9.29, 7.745, 6.53, 6.45, 6.55, 5.985, 6.645, 4.303, 3.6715, 3.1285, 2.7195, 2.29, 2.201, 1.3875, 1.174}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
93,NULL,20,1,
3,83,1,2212,1,93,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
94,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{29}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{1929, 1490, 1439, 1455, 1096, 1004, 590.1, 561.1, 489.7, 523.2, 420.7, 379.6, 312.7, 330.4, 312.8, 284.3, 234.7, 241.1, 194.5, 149.9, 185.4, 122.3, 141.1, 88.42, 62.1, 43.46, 26.53, 23.4, 9.674}',
'{54.15, 45.49, 44.465, 23.31, 18.78, 17.695, 11.975, 11.81, 10.92, 11.79, 10.31, 9.84, 8.785, 9.695, 9.59, 9.245, 8.38, 8.725, 7.805, 6.74, 8.375, 6.76, 7.275, 4.0865, 3.405, 2.837, 2.2085, 2.073, 1.3285}',
'{54.15, 45.49, 44.465, 23.31, 18.78, 17.695, 11.975, 11.81, 10.92, 11.79, 10.31, 9.84, 8.785, 9.695, 9.59, 9.245, 8.38, 8.725, 7.805, 6.74, 8.375, 6.76, 7.275, 4.0865, 3.405, 2.837, 2.2085, 2.073, 1.3285}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
94,NULL,20,1,
4,83,1,2212,1,94,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
95,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{28}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{1626, 1539, 1551, 1416, 1330, 1128, 988.7, 622.9, 626.6, 533.5, 562.1, 473.9, 417, 407.6, 353.6, 264.1, 267.2, 222.3, 187.7, 183.3, 155.5, 132.5, 119.8, 91.29, 65.04, 50.99, 33.52, 18.69}',
'{49.37, 47.57, 47.885, 45.085, 22.515, 19.845, 18.245, 13.11, 13.46, 12.21, 12.985, 11.755, 10.995, 11.305, 10.52, 8.9, 9.185, 8.365, 7.675, 7.57, 7.675, 7.065, 6.705, 5.835, 3.4985, 3.087, 2.4915, 1.8535}',
'{49.37, 47.57, 47.885, 45.085, 22.515, 19.845, 18.245, 13.11, 13.46, 12.21, 12.985, 11.755, 10.995, 11.305, 10.52, 8.9, 9.185, 8.365, 7.675, 7.57, 7.675, 7.065, 6.705, 5.835, 3.4985, 3.087, 2.4915, 1.8535}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
95,NULL,20,1,
5,83,1,2212,1,95,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
96,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{27}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{1756, 1434, 1548, 1474, 1347, 1171, 918.3, 754.8, 593.6, 544.9, 511.9, 468, 420.6, 373.1, 320.8, 335.4, 247.1, 259.4, 232.2, 173.2, 191.6, 117.5, 74.5, 73.74, 48.91, 40.71, 20.69}',
'{53.65, 46.885, 49.45, 47.915, 23.855, 21.545, 18.22, 16.015, 13.665, 13.115, 12.82, 12.295, 11.92, 11.24, 10.405, 10.92, 9.19, 9.6, 9.76, 8.36, 8.815, 6.835, 5.41, 5.38, 3.109, 2.8295, 2.006}',
'{53.65, 46.885, 49.45, 47.915, 23.855, 21.545, 18.22, 16.015, 13.665, 13.115, 12.82, 12.295, 11.92, 11.24, 10.405, 10.92, 9.19, 9.6, 9.76, 8.36, 8.815, 6.835, 5.41, 5.38, 3.109, 2.8295, 2.006}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
96,NULL,20,1,
6,83,1,2212,1,96,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
97,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{32}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{1399, 1455, 1384, 1176, 1000, 782.5, 687.7, 600.5, 530, 471.5, 371.4, 325.8, 272.8, 240.2, 200.9, 164.1, 138.4, 111.6, 96.21, 75.8, 65.29, 58.16, 45.12, 36.46, 25.7, 15.55, 13.45, 7.985, 5.385, 3.243, 1.352, 1.042}',
'{18.38, 19.01, 18.29, 12.35, 10.68, 8.58, 7.685, 6.86, 6.2, 5.655, 4.674, 4.24, 3.7195, 3.4035, 3.0055, 2.6195, 2.3495, 2.0505, 1.8755, 1.625, 1.4925, 1.3995, 1.2125, 0.802, 0.6565, 0.4965, 0.459, 0.3485, 0.284, 0.1795, 0.115, 0.1005}',
'{18.38, 19.01, 18.29, 12.35, 10.68, 8.58, 7.685, 6.86, 6.2, 5.655, 4.674, 4.24, 3.7195, 3.4035, 3.0055, 2.6195, 2.3495, 2.0505, 1.8755, 1.625, 1.4925, 1.3995, 1.2125, 0.802, 0.6565, 0.4965, 0.459, 0.3485, 0.284, 0.1795, 0.115, 0.1005}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420, 450}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
97,NULL,20,1,
1,83,1,2212,1,97,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
98,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{32}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{1360, 1367, 1329, 1084, 924, 757.8, 633.1, 564.6, 472, 438, 351.5, 309.9, 258.1, 211, 178, 143.2, 111.1, 98.02, 82.66, 63.62, 49.48, 47.8, 38.43, 31.79, 20.2, 14.8, 7.912, 4.547, 4.199, 2.234, 1.502, 0.5562}',
'{21.765, 21.95, 21.555, 12.32, 10.835, 9.25, 8.075, 7.485, 6.605, 6.36, 5.49, 5.11, 4.5775, 4.066, 3.6975, 3.288, 2.859, 2.693, 2.47, 2.153, 1.892, 1.872, 1.674, 1.1025, 0.8675, 0.7375, 0.535, 0.404, 0.388, 0.231, 0.189, 0.1145}',
'{21.765, 21.95, 21.555, 12.32, 10.835, 9.25, 8.075, 7.485, 6.605, 6.36, 5.49, 5.11, 4.5775, 4.066, 3.6975, 3.288, 2.859, 2.693, 2.47, 2.153, 1.892, 1.872, 1.674, 1.1025, 0.8675, 0.7375, 0.535, 0.404, 0.388, 0.231, 0.189, 0.1145}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420, 450}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
98,NULL,20,1,
2,83,1,2212,1,98,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
99,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{30}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{1365, 1178, 1174, 991.8, 827, 625.9, 590.6, 475.2, 408.9, 370.6, 276, 215.3, 203.2, 174.8, 132.3, 109.7, 94.44, 67.31, 57.25, 46.29, 37.52, 31.33, 23.95, 19.31, 10.71, 7.868, 5.977, 1.871, 1.604, 1.553}',
'{24.05, 21.55, 21.62, 12.165, 10.57, 8.505, 8.31, 7.145, 6.53, 6.23, 5.155, 4.4575, 4.419, 4.106, 3.541, 3.231, 3.0165, 2.526, 2.3455, 2.116, 1.9095, 1.744, 1.545, 0.9875, 0.73, 0.624, 0.5425, 0.3025, 0.28, 0.225}',
'{24.05, 21.55, 21.62, 12.165, 10.57, 8.505, 8.31, 7.145, 6.53, 6.23, 5.155, 4.4575, 4.419, 4.106, 3.541, 3.231, 3.0165, 2.526, 2.3455, 2.116, 1.9095, 1.744, 1.545, 0.9875, 0.73, 0.624, 0.5425, 0.3025, 0.28, 0.225}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
99,NULL,20,1,
3,83,1,2212,1,99,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
100,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{29}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{1250, 1223, 1183, 1020, 838.4, 643.9, 533.6, 472.8, 419.6, 346.7, 256.6, 197.7, 205.2, 144.4, 112, 89.8, 72.37, 46.8, 42.19, 37.42, 26.43, 20.63, 16.3, 11.57, 5.208, 2.244, 1.544, 1.647, 0.5352}',
'{23, 22.765, 22.345, 12.315, 10.64, 8.755, 7.735, 7.245, 6.815, 6.1, 5.08, 4.388, 4.631, 3.821, 3.353, 3.0105, 2.7145, 2.165, 2.08, 1.9705, 1.683, 1.4835, 1.317, 0.7865, 0.525, 0.3435, 0.285, 0.2945, 0.168}',
'{23, 22.765, 22.345, 12.315, 10.64, 8.755, 7.735, 7.245, 6.815, 6.1, 5.08, 4.388, 4.631, 3.821, 3.353, 3.0105, 2.7145, 2.165, 2.08, 1.9705, 1.683, 1.4835, 1.317, 0.7865, 0.525, 0.3435, 0.285, 0.2945, 0.168}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
100,NULL,20,1,
4,83,1,2212,1,100,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
101,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{27}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{1319, 1217, 1160, 1018, 904.4, 704.8, 590.8, 486.6, 401.1, 343.3, 264.7, 213.4, 164.5, 126.6, 114.4, 84.56, 66.37, 52.38, 37.13, 27.33, 17.58, 12.47, 10.48, 6.563, 4.066, 1.403, 1.073}',
'{24.97, 23.655, 22.965, 12.95, 11.935, 9.92, 8.81, 7.765, 6.895, 6.32, 5.41, 4.803, 4.1795, 3.6375, 3.4945, 2.984, 2.6435, 2.3495, 1.962, 1.7165, 1.3715, 1.152, 1.056, 0.591, 0.4645, 0.272, 0.238}',
'{24.97, 23.655, 22.965, 12.95, 11.935, 9.92, 8.81, 7.765, 6.895, 6.32, 5.41, 4.803, 4.1795, 3.6375, 3.4945, 2.984, 2.6435, 2.3495, 1.962, 1.7165, 1.3715, 1.152, 1.056, 0.591, 0.4645, 0.272, 0.238}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
101,NULL,20,1,
5,83,1,2212,1,101,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
102,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{25}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{1230, 1150, 1276, 1045, 899.1, 695.6, 555, 480.5, 398.8, 356.2, 229, 209.5, 154.2, 120.8, 93.81, 70.61, 49.44, 41.72, 26.79, 23.82, 13.59, 8.58, 7.84, 3.375, 2.112}',
'{24.995, 23.915, 26.02, 14.2, 12.725, 10.495, 8.96, 8.2, 7.305, 6.885, 5.195, 5.035, 4.2325, 3.7195, 3.262, 2.8165, 2.339, 2.1975, 1.748, 1.6465, 1.2375, 0.9805, 0.937, 0.4345, 0.3435}',
'{24.995, 23.915, 26.02, 14.2, 12.725, 10.495, 8.96, 8.2, 7.305, 6.885, 5.195, 5.035, 4.2325, 3.7195, 3.262, 2.8165, 2.339, 2.1975, 1.748, 1.6465, 1.2375, 0.9805, 0.937, 0.4345, 0.3435}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
102,NULL,20,1,
6,83,1,2212,1,102,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
103,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{32}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{1150, 1098, 1058, 1029, 843.7, 670.5, 556.3, 420.6, 356.6, 272.4, 199, 169.8, 146.2, 117.6, 92.96, 69.07, 52.65, 42.09, 34.25, 29.37, 22.97, 17.68, 14.75, 11.82, 7.748, 5.237, 3.118, 1.822, 0.9612, 0.5069, 0.2748, 0.1666}',
'{12.145, 11.655, 11.265, 11, 8.29, 6.64, 5.555, 4.262, 3.657, 2.8545, 2.151, 1.8765, 1.6535, 1.378, 1.14, 0.904, 0.7405, 0.6315, 0.5495, 0.4975, 0.4255, 0.3615, 0.3255, 0.286, 0.1675, 0.134, 0.1, 0.075, 0.054, 0.039, 0.0235, 0.0185}',
'{12.145, 11.655, 11.265, 11, 8.29, 6.64, 5.555, 4.262, 3.657, 2.8545, 2.151, 1.8765, 1.6535, 1.378, 1.14, 0.904, 0.7405, 0.6315, 0.5495, 0.4975, 0.4255, 0.3615, 0.3255, 0.286, 0.1675, 0.134, 0.1, 0.075, 0.054, 0.039, 0.0235, 0.0185}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
103,NULL,20,1,
1,83,1,2212,1,103,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
104,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{32}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{1154, 1139, 1077, 994.9, 810.4, 618.3, 514.9, 376.1, 320.2, 241.3, 172.1, 150.5, 129.5, 103.6, 75.99, 59.49, 42.41, 31.42, 29, 23.43, 18.92, 14.57, 10.82, 8.467, 5.833, 3.648, 1.999, 1.061, 0.737, 0.2983, 0.1778, 0.1281}',
'{13.15, 13.035, 12.445, 11.645, 8.085, 6.29, 5.34, 4.0355, 3.531, 2.789, 2.1275, 1.9385, 1.75, 1.4985, 1.2155, 1.043, 0.847, 0.712, 0.687, 0.6115, 0.5455, 0.4745, 0.4055, 0.357, 0.212, 0.1655, 0.1215, 0.088, 0.0735, 0.0465, 0.0295, 0.0245}',
'{13.15, 13.035, 12.445, 11.645, 8.085, 6.29, 5.34, 4.0355, 3.531, 2.789, 2.1275, 1.9385, 1.75, 1.4985, 1.2155, 1.043, 0.847, 0.712, 0.687, 0.6115, 0.5455, 0.4745, 0.4055, 0.357, 0.212, 0.1655, 0.1215, 0.088, 0.0735, 0.0465, 0.0295, 0.0245}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
104,NULL,20,1,
2,83,1,2212,1,104,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
105,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{31}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{918, 859.3, 848.8, 639.6, 495.8, 405.9, 292.8, 242.7, 176.6, 129.4, 103.9, 94.55, 70.73, 54.7, 39.56, 26.62, 19.37, 17.18, 13.75, 9.666, 8.377, 5.801, 4.127, 2.603, 1.634, 1.122, 0.6617, 0.3663, 0.1418, 0.0761, 0.0656}',
'{11.54, 10.945, 10.885, 6.745, 5.37, 4.526, 3.425, 2.9645, 2.314, 1.8445, 1.5995, 1.533, 1.278, 1.098, 0.913, 0.7305, 0.619, 0.589, 0.528, 0.441, 0.412, 0.341, 0.289, 0.1625, 0.1285, 0.106, 0.0815, 0.06, 0.0375, 0.0225, 0.021}',
'{11.54, 10.945, 10.885, 6.745, 5.37, 4.526, 3.425, 2.9645, 2.314, 1.8445, 1.5995, 1.533, 1.278, 1.098, 0.913, 0.7305, 0.619, 0.589, 0.528, 0.441, 0.412, 0.341, 0.289, 0.1625, 0.1285, 0.106, 0.0815, 0.06, 0.0375, 0.0225, 0.021}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360, 390, 420}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
105,NULL,20,1,
3,83,1,2212,1,105,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
106,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{29}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{894, 830, 775.9, 638.6, 474.1, 389.8, 280.5, 227.9, 168.4, 120.5, 97.74, 81.22, 63.95, 46.65, 32.35, 20.79, 14.74, 13.63, 10.04, 7.841, 5.851, 3.77, 2.794, 1.8, 0.9171, 0.3184, 0.1613, 0.293, 0.1826}',
'{11.16, 10.54, 10.02, 6.47, 4.981, 4.244, 3.241, 2.781, 2.2275, 1.7655, 1.5585, 1.4065, 1.227, 1.028, 0.84, 0.662, 0.5565, 0.542, 0.4655, 0.4115, 0.3575, 0.286, 0.2455, 0.1395, 0.099, 0.0585, 0.0415, 0.056, 0.044}',
'{11.16, 10.54, 10.02, 6.47, 4.981, 4.244, 3.241, 2.781, 2.2275, 1.7655, 1.5585, 1.4065, 1.227, 1.028, 0.84, 0.662, 0.5565, 0.542, 0.4655, 0.4115, 0.3575, 0.286, 0.2455, 0.1395, 0.099, 0.0585, 0.0415, 0.056, 0.044}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340, 360}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
106,NULL,20,1,
4,83,1,2212,1,106,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
107,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{29}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{957.3, 860, 811.1, 795.5, 641.6, 490.6, 386.6, 271.5, 220.7, 167.1, 114, 96.17, 75.71, 54.78, 39.54, 29.86, 19.52, 12.32, 9.243, 7.494, 5.73, 4.087, 3.158, 1.728, 1.124, 0.4555, 0.1916, 0.183, 0.0638}',
'{12.375, 11.355, 10.87, 10.755, 6.89, 5.44, 4.453, 3.3255, 2.8495, 2.3245, 1.773, 1.607, 1.3915, 1.15, 0.956, 0.8235, 0.655, 0.5155, 0.447, 0.401, 0.3545, 0.2985, 0.2615, 0.193, 0.1105, 0.07, 0.0455, 0.0445, 0.026}',
'{12.375, 11.355, 10.87, 10.755, 6.89, 5.44, 4.453, 3.3255, 2.8495, 2.3245, 1.773, 1.607, 1.3915, 1.15, 0.956, 0.8235, 0.655, 0.5155, 0.447, 0.401, 0.3545, 0.2985, 0.2615, 0.193, 0.1105, 0.07, 0.0455, 0.0445, 0.026}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320, 340}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
107,NULL,20,1,
5,83,1,2212,1,107,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
108,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{27}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{908.2, 846.1, 812.1, 816.3, 620.1, 481.1, 376.2, 276.3, 209.3, 149.7, 99.88, 75.62, 68.09, 46.02, 33.12, 20.84, 14.92, 9.886, 6.926, 6.198, 3.629, 2.82, 1.734, 0.9611, 0.6423, 0.2291, 0.1662}',
'{12.665, 11.985, 11.635, 11.75, 7.23, 5.78, 4.69, 3.6345, 2.928, 2.281, 1.715, 1.44, 1.3745, 1.086, 0.9035, 0.7005, 0.5895, 0.4765, 0.402, 0.3795, 0.289, 0.254, 0.1985, 0.148, 0.0855, 0.051, 0.0435}',
'{12.665, 11.985, 11.635, 11.75, 7.23, 5.78, 4.69, 3.6345, 2.928, 2.281, 1.715, 1.44, 1.3745, 1.086, 0.9035, 0.7005, 0.5895, 0.4765, 0.402, 0.3795, 0.289, 0.254, 0.1985, 0.148, 0.0855, 0.051, 0.0435}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
108,NULL,20,1,
6,83,1,2212,1,108,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
109,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{21}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{634.9, 555.9, 383.6, 276.8, 192.6, 136.1, 100.8, 70.17, 49.8, 38.92, 26.08, 18.77, 12.2, 7.063, 7.05, 5.027, 2.474, 2.989, 1.931, 1.482, 0.9259}',
'{39.135, 30.725, 21.545, 15.845, 11.35, 8.315, 6.415, 4.75, 3.6245, 3.0105, 2.2645, 1.817, 1.3845, 1.004, 1.003, 0.8295, 0.5665, 0.626, 0.4975, 0.4335, 0.3405}',
'{39.135, 30.725, 21.545, 15.845, 11.35, 8.315, 6.415, 4.75, 3.6245, 3.0105, 2.2645, 1.817, 1.3845, 1.004, 1.003, 0.8295, 0.5665, 0.626, 0.4975, 0.4335, 0.3405}',
null,null,
'{28, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220}',
'{30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
109,NULL,20,1,
1,83,1,2212,1,109,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
110,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{20}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{759.5, 643.3, 415.3, 286.8, 205.7, 151.1, 102.9, 62.91, 35.53, 29.59, 20.17, 15.53, 8.315, 4.969, 3.995, 2.241, 2.371, 1.527, 0.6821, 0.6496}',
'{53.55, 37.82, 25.305, 18.225, 13.725, 10.665, 7.91, 5.55, 3.8045, 3.395, 2.7, 2.323, 1.6465, 1.253, 1.1185, 0.8305, 0.855, 0.683, 0.4545, 0.4435}',
'{53.55, 37.82, 25.305, 18.225, 13.725, 10.665, 7.91, 5.55, 3.8045, 3.395, 2.7, 2.323, 1.6465, 1.253, 1.1185, 0.8305, 0.855, 0.683, 0.4545, 0.4435}',
null,null,
'{28, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210}',
'{30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
110,NULL,20,1,
2,83,1,2212,1,110,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
111,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{17}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{678.9, 599.4, 360.8, 255.8, 176.5, 111, 64.14, 46.66, 33.85, 26.49, 16.46, 6.289, 6.2, 2.989, 2.676, 1.204, 0.8921}',
'{55.5, 39.415, 24.95, 18.535, 13.645, 9.525, 6.44, 5.215, 4.262, 3.6755, 2.791, 1.6565, 1.6445, 1.126, 1.0645, 0.7095, 0.61}',
'{55.5, 39.415, 24.95, 18.535, 13.645, 9.525, 6.44, 5.215, 4.262, 3.6755, 2.791, 1.6565, 1.6445, 1.126, 1.0645, 0.7095, 0.61}',
null,null,
'{28, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180}',
'{30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
111,NULL,20,1,
3,83,1,2212,1,111,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
112,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{21}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{652.3, 541, 346.8, 221, 128.7, 91.79, 57.75, 34.14, 29.57, 18.9, 9.905, 8.126, 4.76, 2.308, 1.202, 1.106, 1.01, 0.9136, 0.8174, 0.6251, 0.3366}',
'{55.35, 36.725, 24.69, 16.835, 10.955, 8.535, 6.195, 4.4305, 4.061, 3.1265, 2.188, 1.968, 1.486, 1.0245, 0.736, 0.7055, 0.674, 0.6405, 0.606, 0.529, 0.2745}',
'{55.35, 36.725, 24.69, 16.835, 10.955, 8.535, 6.195, 4.4305, 4.061, 3.1265, 2.188, 1.968, 1.486, 1.0245, 0.736, 0.7055, 0.674, 0.6405, 0.606, 0.529, 0.2745}',
null,null,
'{28, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 230}',
'{30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 250}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
112,NULL,20,1,
4,83,1,2212,1,112,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
113,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{16}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{633.7, 480.1, 294.6, 205.1, 122.6, 83.84, 56.45, 32.01, 19.19, 13.45, 8.63, 4.628, 4.869, 2.314, 1.205, 0.5785}',
'{53.3, 32.105, 20.95, 15.51, 10.395, 7.895, 6.045, 4.235, 3.143, 2.5795, 2.03, 1.4645, 1.5035, 1.0265, 0.7375, 0.51}',
'{53.3, 32.105, 20.95, 15.51, 10.395, 7.895, 6.045, 4.235, 3.143, 2.5795, 2.03, 1.4645, 1.5035, 1.0265, 0.7375, 0.51}',
null,null,
'{28, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170}',
'{30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
113,NULL,20,1,
5,83,1,2212,1,113,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
114,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{15}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{678.3, 454.6, 300.8, 182.9, 108.2, 79.15, 42.27, 26.81, 16.53, 11.29, 8.088, 3.917, 2.086, 1.424, 0.8647}',
'{58.6, 32.225, 22.425, 14.82, 9.89, 7.895, 5.2, 3.938, 2.981, 2.4155, 2.0185, 1.3815, 1.0005, 0.8245, 0.641}',
'{58.6, 32.225, 22.425, 14.82, 9.89, 7.895, 5.2, 3.938, 2.981, 2.4155, 2.0185, 1.3815, 1.0005, 0.8245, 0.641}',
null,null,
'{28, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160}',
'{30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
114,NULL,20,1,
6,83,1,2212,1,114,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
115,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{28}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{367.3, 353.6, 321.7, 297.2, 214.4, 141, 98.94, 67.5, 44.73, 30.97, 20.66, 15.36, 11.55, 7.497, 5.02, 3.352, 1.544, 1.325, 0.7836, 0.3746, 0.2241, 0.2848, 0.2979, 0.1339, 0.0907, 0.0561, 0.0432, 0.0389}',
'{5.42, 5.305, 4.9835, 4.7355, 2.5085, 1.802, 1.3885, 1.0665, 0.817, 0.6545, 0.5175, 0.4395, 0.3765, 0.2995, 0.243, 0.1975, 0.133, 0.1235, 0.0945, 0.0655, 0.0505, 0.0575, 0.0585, 0.0395, 0.023, 0.018, 0.016, 0.015}',
'{5.42, 5.305, 4.9835, 4.7355, 2.5085, 1.802, 1.3885, 1.0665, 0.817, 0.6545, 0.5175, 0.4395, 0.3765, 0.2995, 0.243, 0.1975, 0.133, 0.1235, 0.0945, 0.0655, 0.0505, 0.0575, 0.0585, 0.0395, 0.023, 0.018, 0.016, 0.015}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 260, 280, 300, 320}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
115,NULL,20,1,
1,83,1,2212,1,115,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
116,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{23}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{323.9, 312.6, 292.2, 252.2, 192.1, 122.8, 83.75, 54.86, 41.03, 28.43, 18.25, 13.97, 8.887, 5.592, 3.823, 1.684, 2.148, 0.917, 0.5452, 0.2932, 0.2939, 0.1623, 0.1522}',
'{6.15, 6.11, 5.925, 5.425, 2.655, 1.981, 1.58, 1.2425, 1.071, 0.884, 0.7015, 0.6145, 0.4885, 0.3865, 0.3195, 0.2115, 0.24, 0.1565, 0.1205, 0.0885, 0.089, 0.066, 0.064}',
'{6.15, 6.11, 5.925, 5.425, 2.655, 1.981, 1.58, 1.2425, 1.071, 0.884, 0.7015, 0.6145, 0.4885, 0.3865, 0.3195, 0.2115, 0.24, 0.1565, 0.1205, 0.0885, 0.089, 0.066, 0.064}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
116,NULL,20,1,
2,83,1,2212,1,116,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
117,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{19}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{250.2, 234.6, 197.5, 144.5, 101.3, 59.69, 42.5, 29.45, 16.21, 10.31, 7.76, 6.085, 2.961, 2.075, 1.645, 0.3916, 0.6721, 0.387, 0.2447}',
'{5.86, 5.735, 5.18, 2.352, 1.946, 1.443, 1.229, 1.0285, 0.754, 0.6045, 0.5315, 0.475, 0.3305, 0.278, 0.249, 0.1205, 0.1595, 0.121, 0.0965}',
'{5.86, 5.735, 5.18, 2.352, 1.946, 1.443, 1.229, 1.0285, 0.754, 0.6045, 0.5315, 0.475, 0.3305, 0.278, 0.249, 0.1205, 0.1595, 0.121, 0.0965}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 200}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 210}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
117,NULL,20,1,
3,83,1,2212,1,117,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
118,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{18}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{212.9, 209.2, 155.5, 126.6, 72.02, 44.64, 29.15, 19.68, 14.79, 8.575, 7.662, 3.276, 2.631, 1.623, 0.7287, 0.397, 0.3547, 0.1856}',
'{5.39, 5.48, 4.5395, 2.163, 1.5715, 1.2355, 1.012, 0.8455, 0.749, 0.5715, 0.5515, 0.358, 0.3245, 0.2555, 0.171, 0.1265, 0.1205, 0.087}',
'{5.39, 5.48, 4.5395, 2.163, 1.5715, 1.2355, 1.012, 0.8455, 0.749, 0.5715, 0.5515, 0.358, 0.3245, 0.2555, 0.171, 0.1265, 0.1205, 0.087}',
null,null,
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180}',
'{36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
118,NULL,20,1,
4,83,1,2212,1,118,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
119,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{16}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{216.1, 223.2, 182.8, 142.4, 123.6, 73.99, 39.91, 30.38, 19, 10.49, 6.899, 6.073, 2.302, 1.829, 1.145, 0.2868}',
'{5.515, 5.81, 5.165, 4.4325, 2.214, 1.662, 1.186, 1.066, 0.8465, 0.628, 0.5155, 0.492, 0.3005, 0.271, 0.215, 0.107}',
'{5.515, 5.81, 5.165, 4.4325, 2.214, 1.662, 1.186, 1.066, 0.8465, 0.628, 0.5155, 0.492, 0.3005, 0.271, 0.215, 0.107}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
119,NULL,20,1,
5,83,1,2212,1,119,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
120,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{15}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{234.7, 195, 170.6, 175, 111.3, 67.1, 39.93, 20.77, 13.74, 8.732, 5.451, 3.491, 2.048, 1.295, 0.8005}',
'{6.16, 5.525, 5.155, 5.385, 2.1635, 1.6335, 1.2445, 0.8825, 0.731, 0.5895, 0.4705, 0.38, 0.2925, 0.234, 0.186}',
'{6.16, 5.525, 5.155, 5.385, 2.1635, 1.6335, 1.2445, 0.8825, 0.731, 0.5895, 0.4705, 0.38, 0.2925, 0.234, 0.186}',
null,null,
'{32, 34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 150}',
'{34, 36, 38, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 160}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
120,NULL,20,1,
6,83,1,2212,1,120,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
121,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{25}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{562.1, 497, 432.4, 310.9, 274.1, 205.8, 167.4, 141.5, 125.7, 111.2, 79.17, 75.26, 56.54, 50.81, 43.46, 34.11, 30.48, 23.13, 15.9, 16.03, 11.37, 8.84, 6.36, 3.777, 1.057}',
'{16.975, 15.785, 7.465, 5.91, 5.425, 4.5475, 4.047, 3.709, 3.512, 3.32, 2.7605, 2.724, 2.3495, 2.242, 2.079, 1.841, 1.746, 1.5195, 1.258, 1.2685, 1.0675, 0.941, 0.5665, 0.436, 0.2305}',
'{16.975, 15.785, 7.465, 5.91, 5.425, 4.5475, 4.047, 3.709, 3.512, 3.32, 2.7605, 2.724, 2.3495, 2.242, 2.079, 1.841, 1.746, 1.5195, 1.258, 1.2685, 1.0675, 0.941, 0.5665, 0.436, 0.2305}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290, 310}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
121,NULL,20,1,
1,83,1,1000010020,1,121,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
122,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{21}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{631.3, 541.6, 439.3, 308, 218.1, 195.5, 149.3, 129.2, 110.8, 78.16, 58.17, 80.31, 49.72, 49.51, 29.92, 24.99, 26.13, 17.04, 12.88, 13.99, 11.97}',
'{26.54, 24.19, 10.135, 7.815, 6.34, 6.105, 5.295, 5.01, 4.721, 3.941, 3.416, 4.255, 3.3175, 3.378, 2.612, 2.4105, 2.4985, 2.019, 1.762, 1.849, 1.714}',
'{26.54, 24.19, 10.135, 7.815, 6.34, 6.105, 5.295, 5.01, 4.721, 3.941, 3.416, 4.255, 3.3175, 3.378, 2.612, 2.4105, 2.4985, 2.019, 1.762, 1.849, 1.714}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
122,NULL,20,1,
2,83,1,1000010020,1,122,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
123,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{23}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{495.8, 469.8, 524.5, 342.2, 256.3, 225.5, 165.7, 168.1, 109.3, 101.1, 79.04, 75.74, 47.57, 57.4, 22.69, 21.47, 20.96, 16.05, 9.754, 9.003, 8.446, 8.05, 4.091}',
'{26.335, 25.39, 27.075, 9.56, 7.865, 7.485, 6.325, 6.675, 5.27, 5.245, 4.6895, 4.7325, 3.729, 4.2495, 2.599, 2.587, 2.6005, 2.291, 1.7905, 1.734, 1.689, 1.649, 1.175}',
'{26.335, 25.39, 27.075, 9.56, 7.865, 7.485, 6.325, 6.675, 5.27, 5.245, 4.6895, 4.7325, 3.729, 4.2495, 2.599, 2.587, 2.6005, 2.291, 1.7905, 1.734, 1.689, 1.649, 1.175}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
123,NULL,20,1,
3,83,1,1000010020,1,123,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
124,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{23}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{461.9, 435.2, 526.7, 366.5, 292, 248.3, 211.1, 160.4, 99.98, 112.5, 73.29, 51.59, 53.87, 34.02, 19.68, 21.49, 16.58, 16.15, 8.821, 7.113, 3.272, 2.845, 1.707}',
'{26.13, 25.07, 28.065, 10.31, 9.115, 8.48, 7.92, 6.905, 5.355, 5.97, 4.796, 4.045, 4.2535, 3.379, 2.5655, 2.7395, 2.4235, 2.4115, 1.792, 1.6085, 1.0905, 1.017, 0.7875}',
'{26.13, 25.07, 28.065, 10.31, 9.115, 8.48, 7.92, 6.905, 5.355, 5.97, 4.796, 4.045, 4.2535, 3.379, 2.5655, 2.7395, 2.4235, 2.4115, 1.792, 1.6085, 1.0905, 1.017, 0.7875}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
124,NULL,20,1,
4,83,1,1000010020,1,124,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
125,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{21}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{483.1, 456.2, 501.3, 383.5, 268, 220.6, 192, 189.6, 127.8, 103.4, 85.64, 62.14, 39.53, 20.15, 22.37, 15.7, 13.26, 16.69, 10.98, 5.135, 3.994}',
'{27.15, 26.085, 27.48, 11.035, 8.97, 8.18, 7.75, 7.92, 6.455, 5.865, 5.4, 4.615, 3.679, 2.61, 2.808, 2.3655, 2.189, 2.471, 2.003, 1.3685, 1.2065}',
'{27.15, 26.085, 27.48, 11.035, 8.97, 8.18, 7.75, 7.92, 6.455, 5.865, 5.4, 4.615, 3.679, 2.61, 2.808, 2.3655, 2.189, 2.471, 2.003, 1.3685, 1.2065}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
125,NULL,20,1,
5,83,1,1000010020,1,125,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
126,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{20}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{482.3, 464.6, 349.7, 374.4, 302.5, 234.3, 205.9, 152.3, 110.9, 94.75, 68.54, 56.03, 46.89, 32.53, 16.91, 27.73, 26.94, 13.09, 8.578, 3.612}',
'{27.89, 27.27, 22.98, 11.445, 10.235, 8.97, 8.52, 7.32, 6.25, 5.855, 4.995, 4.5545, 4.198, 3.5055, 2.5265, 3.271, 3.23, 2.247, 1.8175, 1.1785}',
'{27.89, 27.27, 22.98, 11.445, 10.235, 8.97, 8.52, 7.32, 6.25, 5.855, 4.995, 4.5545, 4.198, 3.5055, 2.5265, 3.271, 3.23, 2.247, 1.8175, 1.1785}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
126,NULL,20,1,
6,83,1,1000010020,1,126,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
127,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{28}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{365.8, 368.5, 306.3, 244.4, 180.4, 136.9, 102.3, 86.52, 69.06, 51.38, 39.04, 34.35, 22.59, 23.1, 16.29, 9.729, 6.506, 7.318, 6.213, 3.498, 3.351, 2.515, 1.217, 0.3115, 0.1812, 0.1076, 0.0793, 0.068}',
'{7.495, 7.555, 3.9905, 3.375, 2.7125, 2.2515, 1.8685, 1.6975, 1.49, 1.258, 1.082, 1.017, 0.8105, 0.8295, 0.691, 0.528, 0.43, 0.4605, 0.425, 0.3175, 0.312, 0.27, 0.133, 0.0675, 0.051, 0.0395, 0.034, 0.0315}',
'{7.495, 7.555, 3.9905, 3.375, 2.7125, 2.2515, 1.8685, 1.6975, 1.49, 1.258, 1.082, 1.017, 0.8105, 0.8295, 0.691, 0.528, 0.43, 0.4605, 0.425, 0.3175, 0.312, 0.27, 0.133, 0.0675, 0.051, 0.0395, 0.034, 0.0315}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290, 310, 330, 350}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290, 310, 330, 350, 370}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
127,NULL,20,1,
1,83,1,1000010020,1,127,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
128,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{25}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{352.8, 354.5, 305, 220.8, 161.3, 123.5, 103.1, 78.06, 51.66, 43.93, 26.94, 23.56, 17.85, 17.63, 14.93, 4.823, 5.849, 5.233, 4.116, 2.466, 3.548, 2.114, 1.154, 0.4589, 0.2225}',
'{10.015, 10.11, 4.8875, 3.9495, 3.254, 2.801, 2.5735, 2.223, 1.7735, 1.6625, 1.281, 1.2235, 1.0725, 1.0885, 1.01, 0.5575, 0.631, 0.602, 0.5365, 0.416, 0.502, 0.388, 0.203, 0.1275, 0.0885}',
'{10.015, 10.11, 4.8875, 3.9495, 3.254, 2.801, 2.5735, 2.223, 1.7735, 1.6625, 1.281, 1.2235, 1.0725, 1.0885, 1.01, 0.5575, 0.631, 0.602, 0.5365, 0.416, 0.502, 0.388, 0.203, 0.1275, 0.0885}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290, 310}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
128,NULL,20,1,
2,83,1,1000010020,1,128,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
129,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{23}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{302.5, 264.1, 265.5, 209.1, 160, 110.3, 88.61, 58.27, 41.24, 31.36, 30.21, 19.24, 18.52, 11.98, 6.92, 7.514, 4.057, 4.613, 4.363, 2.139, 0.9548, 0.8784, 0.401}',
'{10.29, 9.44, 4.9225, 4.2825, 3.685, 2.976, 2.689, 2.1415, 1.801, 1.59, 1.6155, 1.285, 1.292, 1.0405, 0.788, 0.8395, 0.6165, 0.6655, 0.6515, 0.457, 0.305, 0.293, 0.14}',
'{10.29, 9.44, 4.9225, 4.2825, 3.685, 2.976, 2.689, 2.1415, 1.801, 1.59, 1.6155, 1.285, 1.292, 1.0405, 0.788, 0.8395, 0.6165, 0.6655, 0.6515, 0.457, 0.305, 0.293, 0.14}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
129,NULL,20,1,
3,83,1,1000010020,1,129,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
130,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{23}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{298.2, 289.6, 264.1, 194.9, 162.5, 108.3, 85.43, 57, 56.11, 30.05, 23.27, 16.02, 11.51, 11.57, 5.212, 4.814, 5.655, 4.447, 1.77, 1.77, 1.029, 0.5764, 0.4323}',
'{10.755, 10.635, 5.065, 4.2415, 3.8965, 3.103, 2.778, 2.247, 2.309, 1.6535, 1.476, 1.231, 1.0525, 1.078, 0.718, 0.7005, 0.768, 0.6845, 0.4315, 0.4315, 0.329, 0.246, 0.1505}',
'{10.755, 10.635, 5.065, 4.2415, 3.8965, 3.103, 2.778, 2.247, 2.309, 1.6535, 1.476, 1.231, 1.0525, 1.078, 0.718, 0.7005, 0.768, 0.6845, 0.4315, 0.4315, 0.329, 0.246, 0.1505}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
130,NULL,20,1,
4,83,1,1000010020,1,130,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
131,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{20}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{325.1, 314, 271.7, 192.8, 139.2, 94.65, 73.51, 57.32, 32.28, 26.28, 17.01, 17.42, 10.62, 6.397, 7.156, 5.646, 3.963, 2.518, 1.362, 0.5366}',
'{11.815, 11.63, 5.385, 4.385, 3.6545, 2.9585, 2.6235, 2.336, 1.7205, 1.581, 1.273, 1.321, 1.0315, 0.8025, 0.862, 0.7695, 0.647, 0.5155, 0.379, 0.238}',
'{11.815, 11.63, 5.385, 4.385, 3.6545, 2.9585, 2.6235, 2.336, 1.7205, 1.581, 1.273, 1.321, 1.0315, 0.8025, 0.862, 0.7695, 0.647, 0.5155, 0.379, 0.238}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
131,NULL,20,1,
5,83,1,1000010020,1,131,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
132,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{21}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{299, 301.7, 282.3, 213.6, 149.9, 103.1, 71.83, 60, 44.4, 23.26, 21.18, 15.4, 14.54, 9.388, 7.454, 6.62, 2.57, 3.18, 2.962, 1.916, 0.8275}',
'{11.765, 11.915, 5.855, 4.9695, 4.052, 3.2965, 2.7275, 2.5225, 2.1745, 1.5505, 1.504, 1.289, 1.2655, 1.018, 0.911, 0.8605, 0.535, 0.5955, 0.5745, 0.4615, 0.3035}',
'{11.765, 11.915, 5.855, 4.9695, 4.052, 3.2965, 2.7275, 2.5225, 2.1745, 1.5505, 1.504, 1.289, 1.2655, 1.018, 0.911, 0.8605, 0.535, 0.5955, 0.5745, 0.4615, 0.3035}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
132,NULL,20,1,
6,83,1,1000010020,1,132,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
133,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{28}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{250.5, 240.4, 214.7, 181.1, 129.4, 94.87, 70.72, 51.27, 35.17, 25.33, 19.72, 14.38, 10.67, 7.133, 5.737, 4.596, 3.199, 2.484, 2.001, 1.227, 1.179, 0.6631, 0.5385, 0.2137, 0.153, 0.0876, 0.0794, 0.021}',
'{3.4355, 3.34, 3.072, 1.9695, 1.4735, 1.143, 0.9085, 0.7155, 0.549, 0.4425, 0.3795, 0.314, 0.265, 0.212, 0.1885, 0.1685, 0.139, 0.1225, 0.1095, 0.0855, 0.0835, 0.0625, 0.0565, 0.025, 0.0215, 0.0165, 0.0155, 0.008}',
'{3.4355, 3.34, 3.072, 1.9695, 1.4735, 1.143, 0.9085, 0.7155, 0.549, 0.4425, 0.3795, 0.314, 0.265, 0.212, 0.1885, 0.1685, 0.139, 0.1225, 0.1095, 0.0855, 0.0835, 0.0625, 0.0565, 0.025, 0.0215, 0.0165, 0.0155, 0.008}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290, 310, 330}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290, 310, 330, 350}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
133,NULL,20,1,
1,83,1,1000010020,1,133,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
134,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{26}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{247.6, 234.9, 213.3, 171.1, 125.7, 95.32, 64.51, 46.15, 35.59, 23.09, 19.18, 12.33, 8.863, 5.92, 3.931, 3.828, 2.161, 1.918, 1.664, 0.7235, 0.7183, 0.4292, 0.3613, 0.3843, 0.0975, 0.066}',
'{4.2525, 4.125, 3.8765, 2.0875, 1.6595, 1.3715, 1.055, 0.8585, 0.742, 0.579, 0.53, 0.4175, 0.353, 0.2875, 0.2335, 0.2335, 0.1745, 0.1655, 0.1545, 0.1015, 0.102, 0.0785, 0.072, 0.0525, 0.0265, 0.022}',
'{4.2525, 4.125, 3.8765, 2.0875, 1.6595, 1.3715, 1.055, 0.8585, 0.742, 0.579, 0.53, 0.4175, 0.353, 0.2875, 0.2335, 0.2335, 0.1745, 0.1655, 0.1545, 0.1015, 0.102, 0.0785, 0.072, 0.0525, 0.0265, 0.022}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 270, 290, 310}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
134,NULL,20,1,
2,83,1,1000010020,1,134,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
135,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{20}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{203.9, 195, 176.5, 137.9, 99.38, 69.6, 47.77, 35.03, 22.23, 16.57, 11.68, 10.33, 6.024, 4.105, 2.138, 2.25, 1.73, 1.13, 0.3742, 0.3038}',
'{4.1155, 4.031, 3.7905, 1.8925, 1.513, 1.2035, 0.959, 0.8105, 0.6285, 0.5455, 0.458, 0.4405, 0.333, 0.276, 0.1975, 0.2075, 0.183, 0.148, 0.085, 0.0775}',
'{4.1155, 4.031, 3.7905, 1.8925, 1.513, 1.2035, 0.959, 0.8105, 0.6285, 0.5455, 0.458, 0.4405, 0.333, 0.276, 0.1975, 0.2075, 0.183, 0.148, 0.085, 0.0775}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
135,NULL,20,1,
3,83,1,1000010020,1,135,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
136,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{20}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{204.3, 164.7, 129.1, 91.61, 63.55, 45.21, 33.03, 20.75, 12.98, 11.15, 6.396, 3.386, 2.795, 2.445, 1.653, 1.22, 0.3211, 0.535, 0.2547, 0.1698}',
'{4.327, 3.7545, 1.805, 1.4465, 1.159, 0.9575, 0.8135, 0.6325, 0.495, 0.47, 0.353, 0.2545, 0.2355, 0.224, 0.185, 0.16, 0.0815, 0.107, 0.074, 0.06}',
'{4.327, 3.7545, 1.805, 1.4465, 1.159, 0.9575, 0.8135, 0.6325, 0.495, 0.47, 0.353, 0.2545, 0.2355, 0.224, 0.185, 0.16, 0.0815, 0.107, 0.074, 0.06}',
null,null,
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220}',
'{48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
136,NULL,20,1,
4,83,1,1000010020,1,136,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
137,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{19}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{213.7, 181.9, 172.8, 139.4, 88.04, 60.16, 45.25, 31.16, 18.93, 12.15, 9.323, 6.311, 4.217, 2.686, 1.39, 0.9601, 0.4604, 0.5534, 0.3235}',
'{4.5985, 4.15, 4.049, 2.014, 1.469, 1.162, 0.995, 0.811, 0.6175, 0.4905, 0.4345, 0.3585, 0.294, 0.235, 0.169, 0.1415, 0.0985, 0.109, 0.0835}',
'{4.5985, 4.15, 4.049, 2.014, 1.469, 1.162, 0.995, 0.811, 0.6175, 0.4905, 0.4345, 0.3585, 0.294, 0.235, 0.169, 0.1415, 0.0985, 0.109, 0.0835}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
137,NULL,20,1,
5,83,1,1000010020,1,137,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
138,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{18}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{200.1, 180.8, 161.2, 129.1, 78.51, 60.03, 40.19, 26.66, 16.64, 10.1, 8.588, 3.545, 3.477, 2.204, 1.694, 0.4312, 0.8174, 0.4312}',
'{4.641, 4.3685, 4.076, 2.0245, 1.4355, 1.2305, 0.972, 0.774, 0.6005, 0.4635, 0.4335, 0.2725, 0.276, 0.2205, 0.195, 0.099, 0.1365, 0.099}',
'{4.641, 4.3685, 4.076, 2.0245, 1.4355, 1.2305, 0.972, 0.774, 0.6005, 0.4635, 0.4335, 0.2725, 0.276, 0.2205, 0.195, 0.099, 0.1365, 0.099}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
138,NULL,20,1,
6,83,1,1000010020,1,138,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
139,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{15}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{87.84, 54.61, 36.64, 20.92, 14.41, 9.015, 5.207, 3.07, 2.539, 0.9473, 1.371, 0.6842, 0.3481, 0.2332, 0.1562}',
'{5.57, 3.779, 2.798, 1.885, 1.489, 1.1225, 0.821, 0.617, 0.563, 0.3345, 0.412, 0.2885, 0.205, 0.168, 0.1375}',
'{5.57, 3.779, 2.798, 1.885, 1.489, 1.1225, 0.821, 0.617, 0.563, 0.3345, 0.412, 0.2885, 0.205, 0.168, 0.1375}',
null,null,
'{40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180}',
'{50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
139,NULL,20,1,
1,83,1,1000010020,1,139,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
140,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{10}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{85.9, 45.37, 26.63, 18.24, 10.92, 7.967, 3.861, 3.159, 2.543, 1.266}',
'{6.415, 4.0365, 2.8715, 2.331, 1.753, 1.5065, 1.0195, 0.9415, 0.856, 0.599}',
'{6.415, 4.0365, 2.8715, 2.331, 1.753, 1.5065, 1.0195, 0.9415, 0.856, 0.599}',
null,null,
'{40, 50, 60, 70, 80, 90, 100, 110, 120, 130}',
'{50, 60, 70, 80, 90, 100, 110, 120, 130, 140}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
140,NULL,20,1,
2,83,1,1000010020,1,140,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
141,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{8}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{68.87, 32.22, 24.22, 15.67, 4.522, 6.398, 4.017, 1.259}',
'{6.02, 3.522, 3.0715, 2.4255, 1.1355, 1.535, 1.218, 0.643}',
'{6.02, 3.522, 3.0715, 2.4255, 1.1355, 1.535, 1.218, 0.643}',
null,null,
'{40, 50, 60, 70, 80, 90, 100, 110}',
'{50, 60, 70, 80, 90, 100, 110, 120}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
141,NULL,20,1,
3,83,1,1000010020,1,141,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
142,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{9}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{48.83, 22.26, 20.91, 8.787, 4.152, 3.781, 3.081, 3.195, 1.501}',
'{4.7415, 2.791, 2.912, 1.7335, 1.1465, 1.1745, 1.1, 1.1635, 0.789}',
'{4.7415, 2.791, 2.912, 1.7335, 1.1465, 1.1745, 1.1, 1.1635, 0.789}',
null,null,
'{40, 50, 60, 70, 80, 90, 100, 110, 120}',
'{50, 60, 70, 80, 90, 100, 110, 120, 130}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
142,NULL,20,1,
4,83,1,1000010020,1,142,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
143,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{6}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{51.72, 32.11, 19.98, 14.12, 1.673, 2.182}',
'{5.025, 3.7575, 2.8805, 2.432, 0.635, 0.8605}',
'{5.025, 3.7575, 2.8805, 2.432, 0.635, 0.8605}',
null,null,
'{40, 50, 60, 70, 80, 90}',
'{50, 60, 70, 80, 90, 100}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
143,NULL,20,1,
5,83,1,1000010020,1,143,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
144,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{7}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{50.31, 32.1, 16.34, 9.705, 8.202, 1.951, 2.256}',
'{5.185, 3.9515, 2.641, 2.0075, 1.908, 0.849, 0.982}',
'{5.185, 3.9515, 2.641, 2.0075, 1.908, 0.849, 0.982}',
null,null,
'{40, 50, 60, 70, 80, 90, 100}',
'{50, 60, 70, 80, 90, 100, 110}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
144,NULL,20,1,
6,83,1,1000010020,1,144,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
145,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{16}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{49.45, 47.3, 34.11, 28.39, 16.51, 9.508, 6.244, 3.102, 1.972, 1.029, 0.4489, 0.2832, 0.3168, 0.0807, 0.0858, 0.0472}',
'{1.613, 1.5895, 1.321, 0.6, 0.4435, 0.3305, 0.267, 0.1865, 0.1485, 0.1075, 0.0705, 0.056, 0.06, 0.03, 0.031, 0.0235}',
'{1.613, 1.5895, 1.321, 0.6, 0.4435, 0.3305, 0.267, 0.1865, 0.1485, 0.1075, 0.0705, 0.056, 0.06, 0.03, 0.031, 0.0235}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
145,NULL,20,1,
1,83,1,1000010020,1,145,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
146,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{12}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{41.05, 31.86, 39.58, 23.12, 11.38, 8.543, 4.693, 1.663, 0.992, 1.014, 0.4639, 0.3042}',
'{1.99, 1.7275, 2.0405, 0.719, 0.502, 0.453, 0.338, 0.1975, 0.1545, 0.161, 0.109, 0.089}',
'{1.99, 1.7275, 2.0405, 0.719, 0.502, 0.453, 0.338, 0.1975, 0.1545, 0.161, 0.109, 0.089}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
146,NULL,20,1,
2,83,1,1000010020,1,146,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
147,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{11}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{29.81, 22.43, 26.8, 16.9, 7.607, 5.602, 3.917, 1.429, 1.019, 0.5959, 0.2507}',
'{1.85, 1.568, 1.841, 0.677, 0.453, 0.415, 0.3605, 0.2145, 0.1865, 0.1445, 0.0935}',
'{1.85, 1.568, 1.841, 0.677, 0.453, 0.415, 0.3605, 0.2145, 0.1865, 0.1445, 0.0935}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
147,NULL,20,1,
3,83,1,1000010020,1,147,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
148,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{10}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{33.99, 11.71, 19.3, 14.74, 6.592, 4.489, 2.478, 0.8963, 0.7744, 0.4872}',
'{2.149, 1.028, 1.557, 0.6505, 0.4365, 0.383, 0.291, 0.1715, 0.168, 0.136}',
'{2.149, 1.028, 1.557, 0.6505, 0.4365, 0.383, 0.291, 0.1715, 0.168, 0.136}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
148,NULL,20,1,
4,83,1,1000010020,1,148,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
149,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{11}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{26.31, 14.51, 25.44, 13.58, 6.431, 2.536, 1.628, 0.7369, 0.9113, 0.4381, 0.2995}',
'{1.877, 1.28, 1.95, 0.6395, 0.4455, 0.2765, 0.2325, 0.1575, 0.187, 0.13, 0.1085}',
'{1.877, 1.28, 1.95, 0.6395, 0.4455, 0.2765, 0.2325, 0.1575, 0.187, 0.13, 0.1085}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90, 100, 110, 120}',
'{46, 48, 50, 60, 70, 80, 90, 100, 110, 120, 130}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
149,NULL,20,1,
5,83,1,1000010020,1,149,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
150,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{8}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{15.17, 29.06, 22.32, 10.45, 7.006, 2.643, 2.011, 1.742}',
'{1.341, 2.169, 1.878, 0.565, 0.493, 0.2985, 0.2755, 0.266}',
'{1.341, 2.169, 1.878, 0.565, 0.493, 0.2985, 0.2755, 0.266}',
null,null,
'{44, 46, 48, 50, 60, 70, 80, 90}',
'{46, 48, 50, 60, 70, 80, 90, 100}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
150,NULL,20,1,
6,83,1,1000010020,1,150,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
151,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{18}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{126.2, 97.42, 117.3, 75.33, 83.85, 62.8, 43.07, 26.96, 18.25, 15.92, 11.58, 8.584, 8.017, 6.869, 4.845, 2.815, 2.895, 0.7823}',
'{7.64, 6.565, 7.255, 5.55, 2.7705, 2.4205, 2.0135, 1.596, 1.3225, 1.25, 1.0705, 0.9245, 0.896, 0.83, 0.697, 0.5305, 0.5385, 0.28}',
'{7.64, 6.565, 7.255, 5.55, 2.7705, 2.4205, 2.0135, 1.596, 1.3225, 1.25, 1.0705, 0.9245, 0.896, 0.83, 0.697, 0.5305, 0.5385, 0.28}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
151,NULL,20,1,
1,83,1,1000010030,1,151,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
152,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{16}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{164.4, 135.3, 95.29, 111, 79.47, 71.19, 51.62, 28.43, 14.59, 10.16, 10.31, 9.407, 10.24, 4.788, 4.414, 1.631}',
'{12.91, 11.33, 9.145, 10.23, 3.9315, 3.9055, 3.394, 2.5265, 1.815, 1.5375, 1.5745, 1.5135, 1.5845, 1.083, 1.0405, 0.6325}',
'{12.91, 11.33, 9.145, 10.23, 3.9315, 3.9055, 3.394, 2.5265, 1.815, 1.5375, 1.5745, 1.5135, 1.5845, 1.083, 1.0405, 0.6325}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
152,NULL,20,1,
2,83,1,1000010030,1,152,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
153,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{14}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{151.9, 110, 96.71, 116.7, 60.14, 64.86, 24.06, 33.73, 17.94, 14.13, 11.72, 2.579, 7.91, 1.713}',
'{14.135, 11.715, 10.965, 12.51, 3.9185, 4.386, 2.6085, 3.2845, 2.404, 2.157, 1.9775, 0.922, 1.6335, 0.7595}',
'{14.135, 11.715, 10.965, 12.51, 3.9185, 4.386, 2.6085, 3.2845, 2.404, 2.157, 1.9775, 0.922, 1.6335, 0.7595}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 150, 160}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 160, 170}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
153,NULL,20,1,
3,83,1,1000010030,1,153,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
154,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{13}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{192.7, 134, 125.4, 107.9, 62.18, 70.26, 23.71, 25.81, 19.42, 9.054, 11.17, 9.363, 5.681}',
'{17.41, 14.235, 13.84, 12.82, 4.329, 4.8735, 2.779, 3.0015, 2.6285, 1.7965, 2.0105, 1.8435, 1.4365}',
'{17.41, 14.235, 13.84, 12.82, 4.329, 4.8735, 2.779, 3.0015, 2.6285, 1.7965, 2.0105, 1.8435, 1.4365}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
154,NULL,20,1,
4,83,1,1000010030,1,154,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
155,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{12}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{105.8, 74.3, 141, 96.06, 72.89, 65.63, 38.91, 18.33, 26.43, 17.45, 4.818, 7.977}',
'{12.71, 10.44, 15.245, 12.405, 4.9205, 4.794, 3.708, 2.5465, 3.099, 2.521, 1.3215, 1.705}',
'{12.71, 10.44, 15.245, 12.405, 4.9205, 4.794, 3.708, 2.5465, 3.099, 2.521, 1.3215, 1.705}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
155,NULL,20,1,
5,83,1,1000010030,1,155,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
156,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{9}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{176.2, 65.17, 102.9, 65.09, 52.43, 41.21, 26.44, 6.949, 9.439}',
'{17.87, 10.385, 13.51, 4.8465, 4.426, 3.9595, 3.179, 1.6215, 1.903}',
'{17.87, 10.385, 13.51, 4.8465, 4.426, 3.9595, 3.179, 1.6215, 1.903}',
null,null,
'{54, 56, 58, 60, 70, 80, 90, 100, 110}',
'{56, 58, 60, 70, 80, 90, 100, 110, 120}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
156,NULL,20,1,
6,83,1,1000010030,1,156,2,1,'{"theta outgoing particle"}','{"54"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
157,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{16}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{79.81, 80.16, 60.58, 42.69, 27.89, 19.66, 12.69, 8.726, 6.284, 4.307, 2.688, 1.538, 0.7753, 0.7108, 0.5425, 0.1581}',
'{3.208, 3.2445, 1.362, 1.129, 0.901, 0.7545, 0.604, 0.501, 0.4255, 0.3525, 0.2785, 0.2105, 0.1495, 0.1435, 0.125, 0.0675}',
'{3.208, 3.2445, 1.362, 1.129, 0.901, 0.7545, 0.604, 0.501, 0.4255, 0.3525, 0.2785, 0.2105, 0.1495, 0.1435, 0.125, 0.0675}',
null,null,
'{56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190}',
'{58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
157,NULL,20,1,
1,83,1,1000010030,1,157,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
158,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{14}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{114.8, 81.49, 60.93, 43.97, 24, 16.69, 6.918, 7.619, 7.386, 3.73, 1.8, 1.653, 1.416, 0.3327}',
'{5.875, 4.8255, 1.9485, 1.683, 1.2355, 1.046, 0.6645, 0.7205, 0.7185, 0.5105, 0.3545, 0.3415, 0.317, 0.1535}',
'{5.875, 4.8255, 1.9485, 1.683, 1.2355, 1.046, 0.6645, 0.7205, 0.7185, 0.5105, 0.3545, 0.3415, 0.317, 0.1535}',
null,null,
'{56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 170, 180}',
'{58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 180, 190}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
158,NULL,20,1,
2,83,1,1000010030,1,158,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
159,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{11}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{78.85, 59.24, 64.79, 37.89, 21.54, 15.23, 13.7, 5.205, 4.76, 1.587, 0.741}',
'{5.475, 4.6465, 2.364, 1.808, 1.3685, 1.1725, 1.136, 0.6945, 0.6735, 0.387, 0.2645}',
'{5.475, 4.6465, 2.364, 1.808, 1.3685, 1.1725, 1.136, 0.6945, 0.6735, 0.387, 0.2645}',
null,null,
'{56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140}',
'{58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
159,NULL,20,1,
3,83,1,1000010030,1,159,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
160,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{12}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{90.55, 63.57, 62.15, 38.63, 22.95, 14.99, 10.05, 6.661, 4.663, 2.606, 1.306, 1.643}',
'{6.345, 5.205, 2.4365, 1.934, 1.5, 1.225, 1.011, 0.828, 0.696, 0.5205, 0.369, 0.415}',
'{6.345, 5.205, 2.4365, 1.934, 1.5, 1.225, 1.011, 0.828, 0.696, 0.5205, 0.369, 0.415}',
null,null,
'{56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150}',
'{58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
160,NULL,20,1,
4,83,1,1000010030,1,160,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
161,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{12}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{105.4, 87.18, 80.28, 53.82, 35.83, 17.14, 15.33, 6.497, 4.117, 3.926, 2.504, 1.233}',
'{7.055, 6.39, 6.155, 2.3035, 1.8975, 1.305, 1.257, 0.815, 0.652, 0.641, 0.513, 0.36}',
'{7.055, 6.39, 6.155, 2.3035, 1.8975, 1.305, 1.257, 0.815, 0.652, 0.641, 0.513, 0.36}',
null,null,
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140}',
'{56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
161,NULL,20,1,
5,83,1,1000010030,1,161,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
162,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{10}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{85.55, 63.39, 70.62, 56.26, 34.45, 17.18, 10.45, 6.483, 2.909, 2.463}',
'{6.56, 5.59, 5.99, 2.4725, 1.9355, 1.3605, 1.066, 0.843, 0.5645, 0.5215}',
'{6.56, 5.59, 5.99, 2.4725, 1.9355, 1.3605, 1.066, 0.843, 0.5645, 0.5215}',
null,null,
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120}',
'{56, 58, 60, 70, 80, 90, 100, 110, 120, 130}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
162,NULL,20,1,
6,83,1,1000010030,1,162,2,1,'{"theta outgoing particle"}','{"68"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
163,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{21}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{60.53, 56.5, 52.97, 50.05, 33.32, 19.29, 13.03, 7.935, 4.625, 3.41, 2.212, 1.201, 0.9506, 0.6668, 0.3427, 0.1398, 0.2346, 0.1793, 0.1025, 0.07, 0.035}',
'{1.3655, 1.316, 1.273, 1.2365, 0.523, 0.368, 0.2925, 0.2215, 0.1655, 0.1425, 0.114, 0.083, 0.0745, 0.062, 0.0445, 0.0285, 0.037, 0.0325, 0.0245, 0.0205, 0.0145}',
'{1.3655, 1.316, 1.273, 1.2365, 0.523, 0.368, 0.2925, 0.2215, 0.1655, 0.1425, 0.114, 0.083, 0.0745, 0.062, 0.0445, 0.0285, 0.037, 0.0325, 0.0245, 0.0205, 0.0145}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 220, 240}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 230, 250}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
163,NULL,20,1,
1,83,1,1000010030,1,163,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
164,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{17}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{61.32, 58.1, 45.22, 39.95, 34.82, 18.88, 11.02, 7.604, 4.115, 2.304, 1.485, 1.274, 1.1, 0.3637, 0.3313, 0.0827, 0.0673}',
'{1.9385, 1.898, 1.6395, 1.537, 0.7185, 0.506, 0.381, 0.319, 0.233, 0.174, 0.1415, 0.133, 0.1245, 0.071, 0.068, 0.034, 0.0305}',
'{1.9385, 1.898, 1.6395, 1.537, 0.7185, 0.506, 0.381, 0.319, 0.233, 0.174, 0.1415, 0.133, 0.1245, 0.071, 0.068, 0.034, 0.0305}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
164,NULL,20,1,
2,83,1,1000010030,1,164,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
165,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{15}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{48.01, 47.06, 41.83, 23.73, 16.74, 8.592, 5.457, 3.417, 2.31, 0.9467, 0.5787, 0.7082, 0.6661, 0.2312, 0.1251}',
'{1.9415, 1.9475, 1.835, 0.6355, 0.5425, 0.3815, 0.3075, 0.246, 0.2055, 0.13, 0.1025, 0.117, 0.114, 0.067, 0.0495}',
'{1.9415, 1.9475, 1.835, 0.6355, 0.5425, 0.3815, 0.3075, 0.246, 0.2055, 0.13, 0.1025, 0.117, 0.114, 0.067, 0.0495}',
null,null,
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 180}',
'{56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 190}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
165,NULL,20,1,
3,83,1,1000010030,1,165,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
166,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{12}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{40.59, 41.37, 36.83, 37.61, 23.3, 13.62, 8.378, 4.137, 3.463, 1.516, 0.7003, 0.4169}',
'{1.8125, 1.8685, 1.7645, 1.816, 0.66, 0.5025, 0.3975, 0.2775, 0.2615, 0.172, 0.1165, 0.091}',
'{1.8125, 1.8685, 1.7645, 1.816, 0.66, 0.5025, 0.3975, 0.2775, 0.2615, 0.172, 0.1165, 0.091}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
166,NULL,20,1,
4,83,1,1000010030,1,166,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
167,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{12}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{51.65, 39.62, 39.03, 26.75, 23.92, 16.23, 7.251, 4.547, 2.42, 1.535, 0.2453, 0.1169}',
'{2.1855, 1.8825, 1.891, 1.5225, 0.694, 0.575, 0.3755, 0.3, 0.2195, 0.1765, 0.067, 0.046}',
'{2.1855, 1.8825, 1.891, 1.5225, 0.694, 0.575, 0.3755, 0.3, 0.2195, 0.1765, 0.067, 0.046}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
167,NULL,20,1,
5,83,1,1000010030,1,167,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
168,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{13}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{40.55, 33.6, 39.26, 30.3, 22.67, 10.43, 5.445, 4.135, 1.647, 1.148, 0.4663, 0.5456, 0.1394}',
'{1.9795, 1.7895, 1.9895, 1.7255, 0.7065, 0.4645, 0.3335, 0.297, 0.1855, 0.1575, 0.1, 0.11, 0.055}',
'{1.9795, 1.7895, 1.9895, 1.7255, 0.7065, 0.4645, 0.3335, 0.297, 0.1855, 0.1575, 0.1, 0.11, 0.055}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
168,NULL,20,1,
6,83,1,1000010030,1,168,2,1,'{"theta outgoing particle"}','{"90"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
169,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{8}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{26.65, 18.45, 9.413, 4.498, 1.811, 0.3346, 0.9426, 0.2542}',
'{4.141, 1.751, 1.149, 0.751, 0.456, 0.1755, 0.333, 0.1735}',
'{4.141, 1.751, 1.149, 0.751, 0.456, 0.1755, 0.333, 0.1735}',
null,null,
'{48, 50, 60, 70, 80, 90, 100, 130}',
'{50, 60, 70, 80, 90, 100, 110, 140}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
169,NULL,20,1,
1,83,1,1000010030,1,169,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
170,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{7}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{29.85, 8.371, 19.65, 5.393, 3.733, 1.324, 0.7299}',
'{6.44, 2.9405, 2.5185, 1.171, 0.989, 0.5585, 0.4145}',
'{6.44, 2.9405, 2.5185, 1.171, 0.989, 0.5585, 0.4145}',
null,null,
'{46, 48, 50, 60, 70, 80, 90}',
'{48, 50, 60, 70, 80, 90, 100}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
170,NULL,20,1,
2,83,1,1000010030,1,170,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
171,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{6}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{8.529, 14.55, 2.896, 4.455, 1.005, 0.6707}',
'{3.42, 2.396, 0.899, 1.277, 0.5445, 0.4585}',
'{3.42, 2.396, 0.899, 1.277, 0.5445, 0.4585}',
null,null,
'{48, 50, 60, 70, 80, 90}',
'{50, 60, 70, 80, 90, 100}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
171,NULL,20,1,
3,83,1,1000010030,1,171,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
172,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{6}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{16.42, 11.06, 3.959, 3.025, 1.239, 0.7824}',
'{5.515, 2.108, 1.175, 1.066, 0.661, 0.535}',
'{5.515, 2.108, 1.175, 1.066, 0.661, 0.535}',
null,null,
'{48, 50, 60, 70, 80, 90}',
'{50, 60, 70, 80, 90, 100}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
172,NULL,20,1,
4,83,1,1000010030,1,172,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
173,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{6}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{13.19, 6.449, 9.005, 5.232, 1.89, 0.7014}',
'{4.863, 3.1285, 1.8855, 1.4315, 0.822, 0.4795}',
'{4.863, 3.1285, 1.8855, 1.4315, 0.822, 0.4795}',
null,null,
'{46, 48, 50, 60, 70, 80}',
'{48, 50, 60, 70, 80, 90}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
173,NULL,20,1,
5,83,1,1000010030,1,173,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
174,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{6}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{14.63, 4.89, 6.876, 2.695, 2.116, 0.8059}',
'{5.405, 2.736, 1.6635, 0.9955, 0.921, 0.551}',
'{5.405, 2.736, 1.6635, 0.9955, 0.921, 0.551}',
null,null,
'{46, 48, 50, 60, 70, 80}',
'{48, 50, 60, 70, 80, 90}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
174,NULL,20,1,
6,83,1,1000010030,1,174,2,1,'{"theta outgoing particle"}','{"121"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
175,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{10}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{10.01, 7.916, 6.93, 6.452, 3.894, 1.233, 0.743, 0.3165, 0.1554, 0.127}',
'{0.74, 0.655, 0.6125, 0.592, 0.2075, 0.1135, 0.0885, 0.057, 0.04, 0.0375}',
'{0.74, 0.655, 0.6125, 0.592, 0.2075, 0.1135, 0.0885, 0.057, 0.04, 0.0375}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90, 100, 130}',
'{54, 56, 58, 60, 70, 80, 90, 100, 110, 140}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
175,NULL,20,1,
1,83,1,1000010030,1,175,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
176,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{7}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{8.447, 6.409, 7.243, 3.883, 1.549, 0.789, 0.2573}',
'{1.0075, 0.8705, 0.939, 0.306, 0.19, 0.1355, 0.0805}',
'{1.0075, 0.8705, 0.939, 0.306, 0.19, 0.1355, 0.0805}',
null,null,
'{54, 56, 58, 60, 70, 80, 110}',
'{56, 58, 60, 70, 80, 90, 120}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
176,NULL,20,1,
2,83,1,1000010030,1,176,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
177,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{8}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{6.73, 4.304, 2.573, 2.14, 0.8754, 0.871, 0.2192, 0.3254}',
'{1.0455, 0.814, 0.6045, 0.2545, 0.159, 0.1695, 0.08, 0.106}',
'{1.0455, 0.814, 0.6045, 0.2545, 0.159, 0.1695, 0.08, 0.106}',
null,null,
'{54, 56, 58, 60, 70, 80, 90, 100}',
'{56, 58, 60, 70, 80, 90, 100, 110}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
177,NULL,20,1,
3,83,1,1000010030,1,177,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
178,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{8}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{5.06, 3.76, 4.064, 2.173, 1.491, 0.537, 1.125, 0.4081}',
'{0.915, 0.777, 0.8265, 0.5735, 0.2145, 0.1235, 0.205, 0.1215}',
'{0.915, 0.777, 0.8265, 0.5735, 0.2145, 0.1235, 0.205, 0.1215}',
null,null,
'{52, 54, 56, 58, 60, 70, 80, 90}',
'{54, 56, 58, 60, 70, 80, 90, 100}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
178,NULL,20,1,
4,83,1,1000010030,1,178,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
179,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{5}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{4.144, 2.575, 3.735, 0.6307, 0.5894}',
'{0.83, 0.6315, 0.812, 0.1265, 0.1355}',
'{0.83, 0.6315, 0.812, 0.1265, 0.1355}',
null,null,
'{52, 54, 58, 60, 70}',
'{54, 56, 60, 70, 80}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
179,NULL,20,1,
5,83,1,1000010030,1,179,2,1,'{"theta outgoing particle"}','{"164"}');

INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
180,1,'NEUTRON-INDUCED PRODUCTION OF PROTONS, DEUTERONS AND TRITONS ON COPPER AND BISMUTH',null,'{5}',
'{"E(MeV / c) of sec.","dsig / dp[] / dSr"}',
'{2.543, 1.829, 2.743, 0.808, 0.6611}',
'{0.654, 0.5435, 0.7055, 0.157, 0.152}',
'{0.654, 0.5435, 0.7055, 0.157, 0.152}',
null,null,
'{54, 56, 58, 60, 70}',
'{56, 58, 60, 70, 80}');
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
180,NULL,20,1,
6,83,1,1000010030,1,180,2,1,'{"theta outgoing particle"}','{"164"}');
---
--- Madey data set:
---
--- C
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
181,1000,'Neutrons from nuclear capture of negative pions',30,null,
'{"Neutron Energy T [MeV]","Yield Y [Neutrons/MeV Pi]"}',
 '{1.31,1.56,1.85,2.2,2.61,3.1,3.68,4.35,5.15,6.08,7.19,8.55,9.58,11,12.6,14.4,16.5,18.8,21.6,24.7,28.3,32.4,37.3,42.9,49.6,57.7,67.4,79.3,94.4,114,0.171,0.156,0.153,0.128,0.118,0.0935,0.0799,0.0755,0.0682,0.0534,0.0476,0.0399,0.0369,0.0354,0.0335,0.0296,0.0264,0.0259,0.0253,0.0224,0.0211,0.0202,0.0193,0.0206,0.0202,0.0185,0.0164,0.0118,0.0059,0.0023}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.014,0.011,0.009,0.006,0.005,0.0037,0.0031,0.0026,0.0025,0.002,0.0018,0.0018,0.0016,0.0014,0.0013,0.0011,0.001,0.0009,0.0008,0.0007,0.0007,0.0007,0.0006,0.0005,0.0004,0.0004,0.0003,0.0003,0.0002,0.0001}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.014,0.011,0.009,0.006,0.005,0.0037,0.0031,0.0026,0.0025,0.002,0.0018,0.0018,0.0016,0.0014,0.0013,0.0011,0.001,0.0009,0.0008,0.0007,0.0007,0.0007,0.0006,0.0005,0.0004,0.0004,0.0003,0.0003,0.0002,0.0001}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.010773,0.009828,0.009639,0.008064,0.007434,0.0058905,0.0050337,0.0047565,0.0042966,0.0033642,0.0029988,0.0025137,0.0023247,0.0022302,0.0021105,0.0018648,0.0016632,0.0016317,0.0015939,0.0014112,0.0013293,0.0012726,0.0012159,0.0012978,0.0012726,0.0011655,0.0010332,0.0007434,0.0003717,0.0001449}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.010773,0.009828,0.009639,0.008064,0.007434,0.0058905,0.0050337,0.0047565,0.0042966,0.0033642,0.0029988,0.0025137,0.0023247,0.0022302,0.0021105,0.0018648,0.0016632,0.0016317,0.0015939,0.0014112,0.0013293,0.0012726,0.0012159,0.0012978,0.0012726,0.0011655,0.0010332,0.0007434,0.0003717,0.0001449}',null,null);
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
181,NULL,12,1,
8,6,6,2112,2,181,2,1,null,null);
---
--- N
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
182,1000,'Neutrons from nuclear capture of negative pions',30,null,
'{"Neutron Energy T [MeV]","Yield Y [Neutrons/MeV Pi]"}',
'{1.31,1.56,1.85,2.2,2.61,3.1,3.68,4.35,5.15,6.08,7.19,8.55,9.58,11,12.6,14.4,16.5,18.8,21.6,24.7,28.3,32.4,37.3,42.9,49.6,57.7,67.4,79.3,94.4,114,0.245,0.195,0.182,0.163,0.149,0.12,0.105,0.0869,0.0831,0.0671,0.058,0.0487,0.0447,0.0395,0.0369,0.033,0.0279,0.026,0.0257,0.0232,0.0219,0.0207,0.0195,0.0189,0.0179,0.0169,0.0146,0.0108,0.0062,0.0024}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.014,0.011,0.008,0.006,0.005,0.004,0.003,0.0025,0.0024,0.0019,0.0017,0.0017,0.0015,0.0013,0.0011,0.001,0.0008,0.0008,0.0007,0.0006,0.0006,0.0005,0.0004,0.0004,0.0003,0.0003,0.0003,0.0002,0.0002,0.0001}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.014,0.011,0.008,0.006,0.005,0.004,0.003,0.0025,0.0024,0.0019,0.0017,0.0017,0.0015,0.0013,0.0011,0.001,0.0008,0.0008,0.0007,0.0006,0.0006,0.0005,0.0004,0.0004,0.0003,0.0003,0.0003,0.0002,0.0002,0.0001}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.015435,0.012285,0.011466,0.010269,0.009387,0.00756,0.006615,0.0054747,0.0052353,0.0042273,0.003654,0.0030681,0.0028161,0.0024885,0.0023247,0.002079,0.0017577,0.001638,0.0016191,0.0014616,0.0013797,0.0013041,0.0012285,0.0011907,0.0011277,0.0010647,0.0009198,0.0006804,0.0003906,0.0001512}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.015435,0.012285,0.011466,0.010269,0.009387,0.00756,0.006615,0.0054747,0.0052353,0.0042273,0.003654,0.0030681,0.0028161,0.0024885,0.0023247,0.002079,0.0017577,0.001638,0.0016191,0.0014616,0.0013797,0.0013041,0.0012285,0.0011907,0.0011277,0.0010647,0.0009198,0.0006804,0.0003906,0.0001512}',null,null);
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
182,NULL,12,1,
8,7,6,2112,2,182,2,1,null,null);
---
--- O
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
183,1000,'Neutrons from nuclear capture of negative pions',30,null,
'{"Neutron Energy T [MeV]","Yield Y [Neutrons/MeV Pi]"}',
'{1.31,1.56,1.85,2.2,2.61,3.1,3.68,4.35,5.15,6.08,7.19,8.55,9.58,11,12.6,14.4,16.5,18.8,21.6,24.7,28.3,32.4,37.3,42.9,49.6,57.7,67.4,79.3,94.4,114,0.214,0.178,0.182,0.164,0.154,0.124,0.0995,0.086,0.0824,0.0663,0.0539,0.0463,0.045,0.0408,0.0368,0.0311,0.0292,0.0271,0.0256,0.0226,0.0217,0.0211,0.02,0.0194,0.0187,0.0178,0.0158,0.0113,0.0062,0.0025}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.011,0.009,0.007,0.005,0.004,0.003,0.0026,0.0022,0.0021,0.0017,0.0014,0.0014,0.0012,0.0011,0.001,0.0008,0.0007,0.0007,0.0006,0.0006,0.0005,0.0005,0.0004,0.0003,0.0003,0.0003,0.0003,0.0002,0.0002,0.0001}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.011,0.009,0.007,0.005,0.004,0.003,0.0026,0.0022,0.0021,0.0017,0.0014,0.0014,0.0012,0.0011,0.001,0.0008,0.0007,0.0007,0.0006,0.0006,0.0005,0.0005,0.0004,0.0003,0.0003,0.0003,0.0003,0.0002,0.0002,0.0001}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.013482,0.011214,0.011466,0.010332,0.009702,0.007812,0.0062685,0.005418,0.0051912,0.0041769,0.0033957,0.0029169,0.002835,0.0025704,0.0023184,0.0019593,0.0018396,0.0017073,0.0016128,0.0014238,0.0013671,0.0013293,0.00126,0.0012222,0.0011781,0.0011214,0.0009954,0.0007119,0.0003906,0.0001575}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.013482,0.011214,0.011466,0.010332,0.009702,0.007812,0.0062685,0.005418,0.0051912,0.0041769,0.0033957,0.0029169,0.002835,0.0025704,0.0023184,0.0019593,0.0018396,0.0017073,0.0016128,0.0014238,0.0013671,0.0013293,0.00126,0.0012222,0.0011781,0.0011214,0.0009954,0.0007119,0.0003906,0.0001575}',null,null);
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
183,NULL,12,1,
8,8,6,2112,2,183,2,1,null,null);
---
--- Al
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
184,1000,'Neutrons from nuclear capture of negative pions',30,null,
'{"Neutron Energy T [MeV]","Yield Y [Neutrons/MeV Pi]"}',
'{1.31,1.56,1.85,2.2,2.61,3.1,3.68,4.35,5.15,6.08,7.19,8.55,9.58,11,12.6,14.4,16.5,18.8,21.6,24.7,28.3,32.4,37.3,42.9,49.6,57.7,67.4,79.3,94.4,114,0.327,0.357,0.323,0.31,0.267,0.213,0.189,0.153,0.136,0.102,0.0805,0.0634,0.0562,0.046,0.0406,0.0359,0.0335,0.0272,0.0243,0.0217,0.0207,0.0196,0.0181,0.0169,0.0167,0.0152,0.0124,0.0095,0.0058,0.0023}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.014,0.011,0.009,0.007,0.005,0.005,0.004,0.003,0.003,0.002,0.0019,0.0018,0.0017,0.0014,0.0013,0.0011,0.001,0.0008,0.0008,0.0007,0.0006,0.0006,0.0005,0.0004,0.0004,0.0003,0.0003,0.0002,0.0002,0.0001}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.014,0.011,0.009,0.007,0.005,0.005,0.004,0.003,0.003,0.002,0.0019,0.0018,0.0017,0.0014,0.0013,0.0011,0.001,0.0008,0.0008,0.0007,0.0006,0.0006,0.0005,0.0004,0.0004,0.0003,0.0003,0.0002,0.0002,0.0001}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.020601,0.022491,0.020349,0.01953,0.016821,0.013419,0.011907,0.009639,0.008568,0.006426,0.0050715,0.0039942,0.0035406,0.002898,0.0025578,0.0022617,0.0021105,0.0017136,0.0015309,0.0013671,0.0013041,0.0012348,0.0011403,0.0010647,0.0010521,0.0009576,0.0007812,0.0005985,0.0003654,0.0001449}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.020601,0.022491,0.020349,0.01953,0.016821,0.013419,0.011907,0.009639,0.008568,0.006426,0.0050715,0.0039942,0.0035406,0.002898,0.0025578,0.0022617,0.0021105,0.0017136,0.0015309,0.0013671,0.0013041,0.0012348,0.0011403,0.0010647,0.0010521,0.0009576,0.0007812,0.0005985,0.0003654,0.0001449}',null,null);
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
184,NULL,12,1,
8,13,6,2112,2,184,2,1,null,null);
---
--- Cu
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
185,1000,'Neutrons from nuclear capture of negative pions',31,null,
'{"Neutron Energy T [MeV]","Yield Y [Neutrons/MeV Pi]"}',
'{1.31,1.56,1.85,2.2,2.61,3.1,3.68,4.35,5.15,6.08,7.19,8.55,9.58,11,12.6,14.4,16.5,18.8,21.6,24.7,28.3,32.4,37.3,42.9,49.6,57.7,67.4,79.3,94.4,114,140,0.89,0.856,0.745,0.659,0.601,0.468,0.361,0.291,0.243,0.168,0.133,0.0873,0.072,0.0453,0.0365,0.0338,0.0292,0.0251,0.0256,0.0222,0.0197,0.0179,0.0169,0.0159,0.0154,0.0138,0.0113,0.0084,0.0057,0.0024,0.0002}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.022,0.018,0.014,0.012,0.009,0.007,0.006,0.005,0.004,0.003,0.003,0.0025,0.0022,0.0018,0.0015,0.0013,0.0011,0.001,0.0009,0.0008,0.0008,0.0007,0.0006,0.0005,0.0004,0.0004,0.0003,0.0003,0.0002,0.0001,0.0001}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.022,0.018,0.014,0.012,0.009,0.007,0.006,0.005,0.004,0.003,0.003,0.0025,0.0022,0.0018,0.0015,0.0013,0.0011,0.001,0.0009,0.0008,0.0008,0.0007,0.0006,0.0005,0.0004,0.0004,0.0003,0.0003,0.0002,0.0001,0.0001}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.05607,0.053928,0.046935,0.041517,0.037863,0.029484,0.022743,0.018333,0.015309,0.010584,0.008379,0.0054999,0.004536,0.0028539,0.0022995,0.0021294,0.0018396,0.0015813,0.0016128,0.0013986,0.0012411,0.0011277,0.0010647,0.0010017,0.0009702,0.0008694,0.0007119,0.0005292,0.0003591,0.0001512,1.26e-05}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.05607,0.053928,0.046935,0.041517,0.037863,0.029484,0.022743,0.018333,0.015309,0.010584,0.008379,0.0054999,0.004536,0.0028539,0.0022995,0.0021294,0.0018396,0.0015813,0.0016128,0.0013986,0.0012411,0.0011277,0.0010647,0.0010017,0.0009702,0.0008694,0.0007119,0.0005292,0.0003591,0.0001512,1.26e-05}',null,null);
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
185,NULL,12,1,
8,29,6,2112,2,185,2,1,null,null);
---
--- Ta
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
186,1000,'Neutrons from nuclear capture of negative pions',31,null,
'{"Neutron Energy T [MeV]","Yield Y [Neutrons/MeV Pi]"}',
'{1.31,1.56,1.85,2.2,2.61,3.1,3.68,4.35,5.15,6.08,7.19,8.55,9.58,11,12.6,14.4,16.5,18.8,21.6,24.7,28.3,32.4,37.3,42.9,49.6,57.7,67.4,79.3,94.4,114,140,1.99,1.72,1.47,1.25,0.998,0.732,0.517,0.374,0.294,0.185,0.123,0.081,0.0663,0.0452,0.0396,0.0349,0.0315,0.0265,0.0266,0.0212,0.0204,0.0189,0.0175,0.0173,0.015,0.0138,0.0113,0.0083,0.0049,0.0022,0.0002}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.02,0.02,0.02,0.01,0.01,0.004,0.006,0.005,0.004,0.003,0.003,0.0022,0.0019,0.0013,0.0014,0.0012,0.001,0.0008,0.0008,0.0007,0.0007,0.0006,0.0006,0.0005,0.0004,0.0003,0.0003,0.0003,0.0002,0.0001,0.0001}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.02,0.02,0.02,0.01,0.01,0.004,0.006,0.005,0.004,0.003,0.003,0.0022,0.0019,0.0013,0.0014,0.0012,0.001,0.0008,0.0008,0.0007,0.0007,0.0006,0.0006,0.0005,0.0004,0.0003,0.0003,0.0003,0.0002,0.0001,0.0001}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.12537,0.10836,0.09261,0.07875,0.062874,0.046116,0.032571,0.023562,0.018522,0.011655,0.007749,0.005103,0.0041769,0.0028476,0.0024948,0.0021987,0.0019845,0.0016695,0.0016758,0.0013356,0.0012852,0.0011907,0.0011025,0.0010899,0.000945,0.0008694,0.0007119,0.0005229,0.0003087,0.0001386,1.26e-05}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.12537,0.10836,0.09261,0.07875,0.062874,0.046116,0.032571,0.023562,0.018522,0.011655,0.007749,0.005103,0.0041769,0.0028476,0.0024948,0.0021987,0.0019845,0.0016695,0.0016758,0.0013356,0.0012852,0.0011907,0.0011025,0.0010899,0.000945,0.0008694,0.0007119,0.0005229,0.0003087,0.0001386,1.26e-05}',null,null);
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
186,NULL,12,1,
8,73,6,2112,2,186,2,1,null,null);
---
--- Pb
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,NBINS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS,BIN_MIN,BIN_MAX) 
VALUES
(
187,1000,'Neutrons from nuclear capture of negative pions',31,null,
'{"Neutron Energy T [MeV]","Yield Y [Neutrons/MeV Pi]"}',
 '{1.31,1.56,1.85,2.2,2.61,3.1,3.68,4.35,5.15,6.08,7.19,8.55,9.58,11,12.6,14.4,16.5,18.8,21.6,24.7,28.3,32.4,37.3,42.9,49.6,57.7,67.4,79.3,94.4,114,140,2.23,2.05,1.74,1.5,1.18,0.821,0.544,0.389,0.273,0.166,0.107,0.0705,0.057,0.0461,0.0379,0.0332,0.0333,0.0275,0.0258,0.0231,0.022,0.0206,0.0187,0.0167,0.0144,0.0128,0.0103,0.0082,0.005,0.0022,0.001}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.03,0.02,0.02,0.02,0.01,0.01,0.008,0.006,0.005,0.004,0.003,0.0027,0.0024,0.002,0.0018,0.0015,0.0014,0.0012,0.0011,0.001,0.0009,0.0006,0.0005,0.0004,0.0004,0.0003,0.0003,0.0002,0.0002,0.0001,0.0001}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.03,0.02,0.02,0.02,0.01,0.01,0.008,0.006,0.005,0.004,0.003,0.0027,0.0024,0.002,0.0018,0.0015,0.0014,0.0012,0.0011,0.001,0.0009,0.0006,0.0005,0.0004,0.0004,0.0003,0.0003,0.0002,0.0002,0.0001,0.0001}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.14049,0.12915,0.10962,0.0945,0.07434,0.051723,0.034272,0.024507,0.017199,0.010458,0.006741,0.0044415,0.003591,0.0029043,0.0023877,0.0020916,0.0020979,0.0017325,0.0016254,0.0014553,0.001386,0.0012978,0.0011781,0.0010521,0.0009072,0.0008064,0.0006489,0.0005166,0.000315,0.0001386,6.3e-05}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.14049,0.12915,0.10962,0.0945,0.07434,0.051723,0.034272,0.024507,0.017199,0.010458,0.006741,0.0044415,0.003591,0.0029043,0.0023877,0.0020916,0.0020979,0.0017325,0.0016254,0.0014553,0.001386,0.0012978,0.0011781,0.0010521,0.0009072,0.0008064,0.0006489,0.0005166,0.000315,0.0001386,6.3e-05}',null,null);
INSERT INTO PUBLIC.RESULT(TRID,TID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,SECONDARY,REACTION,DATATABLEID,SCORE,ACCESS,PARNAMES,PARVALUES) 
VALUES
(
187,NULL,12,1,
8,82,6,2112,2,187,2,1,null,null);
---
---
--- Clough et al data set
---
---
---
---
--- C Pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(188,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',14,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
' {87,113,126,154,185,227,257,287,335,407,480,565,694,853,501.3,624.6,646.9,682.7,640.9,569.4,522.7,473.7,408.4,352.7,304.4,291.9,297.7,322.2}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,24.9,14.2,11.6,12.0,10.5,13.5,8.0,8.0,4.5,3.5,2.0,4.5,4.0,6.0}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,24.9,14.2,11.6,12.0,10.5,13.5,8.0,8.0,4.5,3.5,2.0,4.5,4.0,6.0}');
INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
188,6,1,
9,6,3,4,188,2,1);
---
--- C Pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(189,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',14,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{87,113,126,154,185,227,257,287,335,407,480,565,694,853,583.1,673.0,687.6,699.4,661.5,584.1,534.9,492.2,421.3,363.9,314.3,301.2,304.5,326.2}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,15.4,18.7,17.6,9.5,13,8.5,8,6.5,4.5,4.5,2.5,2,4,6}', 
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,15.4,18.7,17.6,9.5,13,8.5,8,6.5,4.5,4.5,2.5,2,4,6}'); 
INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
189,6,1,
23,6,3,4,189,2,1);
---
--- Li6 pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(190,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',14,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{91,117,130,158,189,231,261,291,339,411,484,569,698,857,301.7,381.6,438.9,478.9,459,389.3,327.6,288.4,231.4,192.3,162.4,161.6,168.9,192.9}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,7.5,6.7,5.5,9.9,8.9,5.9,8.2,4.5,3,3,3,3,3,3.8}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,7.5,6.7,5.5,9.9,8.9,5.9,8.2,4.5,3,3,3,3,3,3.8}');
INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
190,6,1,
23,285,3,4,190,2,1);
---
--- Li6 Pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(191,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',14,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{91,117,130,158,189,231,261,291,339,411,484,569,698,857,297.3,357.4,397.7,472.5,462.8,372.6,328.1,281,226,183.5,161,156.7,166.5,182.7}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,15.4,17.4,13.1,9.8,7.6,5.8,7.3,4.1,3.8,3,3,3.8,3,3.8}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,15.4,17.4,13.1,9.8,7.6,5.8,7.3,4.1,3.8,3,3,3.8,3,3.8}');
INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
191,6,1,
9,285,3,4,191,2,1);
---
--- Li7 Pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(192,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',14,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
 '{91,117,130,158,189,231,261,291,339,411,484,569,698,857,371.7,510.9,548.9,565.3,528.3,448.4,395.9,344.4,277.5,229.9,190.3,181.3,189.2,210.6}',
 '{0,0,0,0,0,0,0,0,0,0,0,0,0,0,19.8,8.6,4.3,10.5,10.8,8,8.6,5.6,4.3,2.6,2.6,2.3,2.9,4}',
 '{0,0,0,0,0,0,0,0,0,0,0,0,0,0,19.8,8.6,4.3,10.5,10.8,8,8.6,5.6,4.3,2.6,2.6,2.3,2.9,4}');
INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
192,6,1,
23,286,3,4,192,2,1);
---
--- Li7 Pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(193,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',14,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{91,117,130,158,189,231,261,291,339,411,484,569,698,857,312.0,384.4,420.4,487.3,480.7,394.2,356.4,308.6,258.5,212.0,191.8,194.3,199.3,222.7}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,8.2,13.3,4.6,12.0,8.6,7.8,6.6,5.4,4.3,2.5,2.3,3.3,3.9,5.0}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,8.2,13.3,4.6,12.0,8.6,7.8,6.6,5.4,4.3,2.5,2.3,3.3,3.9,5.0}');
  INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
193,6,1,
9,286,3,4,193,2,1);
---
--- Be Pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(194,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',14,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{89,115,128,156,187,229,259,289,337,409,482,567,696,855,466.5,567.1,620.6,645.2,610.8,526.4,469.5,415.5,343.8,282.9,237.1,225.4,232.7,258.8}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,11.1,30.2,9.8,9.8,8.8,7.2,6.8,6.1,4.9,3.4,2.8,3,3,4}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,11.1,30.2,9.8,9.8,8.8,7.2,6.8,6.1,4.9,3.4,2.8,3,3,4}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
194,6,1,
23,4,3,4,194,2,1);
---
--- Be Pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(195,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',14,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{89,115,128,156,187,229,259,289,337,409,482,567,696,855,421.5,491.7,516.6,561.9,540.5,469.1,418.1,383.4,312.3,267.7,236.5,233.2,244.3,268.6}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,11.3,14.9,8,12.3,8.8,11.9,7.3,6.3,5.1,2.9,2.6,3.8,3.8,5.7}',
'{0,0,0,0,0,0,0,0,0,0,0,0,0,0,11.3,14.9,8,12.3,8.8,11.9,7.3,6.3,5.1,2.9,2.6,3.8,3.8,5.7}');
  INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
195,6,1,
9,4,3,4,195,2,1);
---
--- O Pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(196,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',6,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
 '{87,113,126,154,185,227,674.3,749.1,783.1,818.2,772.6,688.9}',
 '{0,0,0,0,0,0,37.1,11.7,17.1,15.2,15.2,11.2}',
 '{0,0,0,0,0,0,37.1,11.7,17.1,15.2,15.2,11.2}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
196,6,1,
9,8,3,4,196,2,1);
---
--- O Pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(197,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',6,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{87,113,126,154,185,227,803.7,868.6,866.3,826.9,801,719.4}',
'{0,0,0,0,0,0,54.3,17.4,6.6,19.7,16,13}',
'{0,0,0,0,0,0,54.3,17.4,6.6,19.7,16,13}');
  INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
197,6,1,
23,8,3,4,197,2,1);
---
--- Wilkin et al data
---
---
--- 6Li pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(198,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',6,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{82,110,145,189,224,262,223,340,461,459,395,322}',       
'{0,0,0,0,0,0,4,4,4,4,2,2}', 
'{0,0,0,0,0,0,4,4,4,4,2,2}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
198,4,1,
9,285,3,4,198,2,1);
---
--- 6Li pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(199,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',6,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{82,110,145,189,224,262,249,379,476,459,400,323}',
'{0,0,0,0,0,0,4,4,4,4,2,2}',
'{0,0,0,0,0,0,4,4,4,4,2,2}');   
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
199,4,1,
23,285,3,4,199,2,1);
---
--- 7Li pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(200,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',5,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{110,145,189,224,262,387,483,487,426,358}',
'{0,0,0,0,0,4,4,4,2,2}',
'{0,0,0,0,0,4,4,4,2,2}'); 
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
200,4,1,
9,286,3,4,200,2,1);
---
--- 7Li pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(201,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',5,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{110,145,189,224,262,484,575,547,467,393}', 
'{0,0,0,0,0,4,4,4,2,2}',
'{0,0,0,0,0,4,4,4,2,2}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
201,4,1,
23,286,3,4,201,2,1);
---
--- Be pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(202,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',5,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{107,142,186,221,260,454,565,554,495,422}',
'{0,0,0,0,0,4,4,4,4,2}',
'{0,0,0,0,0,4,4,4,4,2}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
202,4,1,
9,4,3,4,202,2,1);
---
--- Be pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(203,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',5,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{107,142,186,221,260,556,654,614,542,460}',
'{0,0,0,0,0,4,4,4,4,2}',
'{0,0,0,0,0,4,4,4,4,2}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
203,4,1,
23,4,3,4,203,2,1);
---
--- C pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(204,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',6,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{78,106,141,185,221,258,438,593,679,658,592,524}',
'{0,0,0,0,0,0,7,4,4,4,4,4}',
'{0,0,0,0,0,0,7,4,4,4,4,4}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
204,4,1,
9,6,3,4,204,2,1);
---
--- C pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(205,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',6,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{78,106,141,185,221,258,508,661,708,664,601,533}',
'{0,0,0,0,0,0,7,4,4,4,4,4}',
'{0,0,0,0,0,0,7,4,4,4,4,4}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
205,4,1,
23,6,3,4,205,2,1);
---
--- He pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(206,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',5,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{110,146,189,225,262,246,330,323,273,225}',
'{0,0,0,0,0,4,2,2,2,2}',
'{0,0,0,0,0,4,2,2,2,2}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
206,4,1,
9,2,3,4,206,2,1);
---
--- He pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(207,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',5,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{110,146,189,225,262,266,342,322,276,228}',
'{0,0,0,0,0,4,2,2,2,2}',
'{0,0,0,0,0,4,2,2,2,2}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
207,4,1,
23,2,3,4,207,2,1);
---
--- S pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(208,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',6,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{80,107,142,186,221,259,1129,1286,1313,1251,1182,1080}', 
'{0,0,0,0,0,0,13,13,11,9,7,7}',
'{0,0,0,0,0,0,13,13,11,9,7,7}'); 
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
208,4,1,
9,16,3,4,208,2,1);
---
--- S pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS) 
VALUES
(209,1000,'Pion-Nucleus Total Cross-Sections from 88-MeV to 860-MeV',6,
'{"kinetic Energy T [MeV]","crosssection [mb]"}',
'{80,107,142,186,221,259,1342,1383,1389,1310,1219,1101}',
'{0,0,0,0,0,0,13,13,11,9,7,7}',
'{0,0,0,0,0,0,13,13,11,9,7,7}'); 
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
209,4,1,
23,16,3,4,209,2,1);
---
--- Allardyce reaction cross section
---
---
--- Sn120  pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(210,1000,'Pion reaction cross sections',3,
'{"kinetic Energy T [MeV]","reaction crosssection [mb]"}',
'{712,870,1228,1221,1254,1223}',
'{0,0,0,14,10,14}',
'{0,0,0,14,10,14}',
'{0,0,0,12,13,0}',
'{0,0,0,12,13,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
210,5,1,
9,287,7,4,210,2,1);
---
--- Sn120 pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(211,1000,'Pion reaction cross sections',3,
'{"kinetic Energy T [MeV]","reaction cross section [mb]"}',
'{712,870,1228,1241,1260,1258}',        
'{0,0,0,10,11,10}', 
'{0,0,0,10,11,10}', 
'{0,0,0,12,13,0}',
'{0,0,0,12,13,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
211,5,1,
23,287,7,4,211,2,1);
--
--- Pb208  pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(212,1000,'Pion reaction cross sections',3,
'{"kinetic Energy T [MeV]","reaction crosssection [mb]"}',
'{712,870,1228,1764,1762,1742}',
'{0,0,0,21,26,24}', 
'{0,0,0,21,26,24}', 
'{0,0,0,18,18,0}',
'{0,0,0,18,18,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
212,5,1,
9,288,7,4,212,2,1);
---
--- Pb208 pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(213,1000,'Pion reaction cross sections',3,
'{"kinetic Energy T [MeV]","reaction cross section [mb]"}',
'{712,870,1228,1759,1810,1806}',
'{0,0,0,40,9,16}',
'{0,0,0,40,9,16}',
'{0,0,0,18,18,0}',
'{0,0,0,18,18,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
213,5,1,
23,288,7,4,213,2,1);
--
--- Al  pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(214,1000,'Pion reaction cross sections',4,
'{"kinetic Energy T [MeV]","reaction crosssection [mb]"}',
'{712,870,1228,1447,429,451,437,435}',
'{0,0,0,0,11,3,4,3}',
'{0,0,0,0,11,3,4,3}',
'{0,0,0,0,4,5,0,0}',
'{0,0,0,0,4,5,0,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
214,5,1,
9,13,7,4,214,2,1);
---
--- Al pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(215,1000,'Pion reaction cross sections',4,
'{"kinetic Energy T [MeV]","reaction cross section [mb]"}',
'{712,870,1228,1447,433,454,444,444}',
'{0,0,0,0,6,3,3,4}',
'{0,0,0,0,6,3,3,4}',
'{0,0,0,0,4,5,0,0}',
'{0,0,0,0,4,5,0,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
215,5,1,
23,13,7,4,215,2,1);
--
--- C  pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(216,1000,'Pion reaction cross sections',6,
'{"kinetic Energy T [MeV]","reaction crosssection [mb]"}',
'{584,712,870,1228,1447,1865,263,246,257,248,243,224}',
'{0,0,0,0,0,0,3,3,2,3,3,2}', 
'{0,0,0,0,0,0,3,3,2,3,3,2}',
'{0,0,0,0,0,0,3,2,3,0,0,0}',
'{0,0,0,0,0,0,3,2,3,0,0,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
216,5,1,
9,6,7,4,216,2,1);
---
--- C pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(217,1000,'Pion reaction cross sections',6,
'{"kinetic Energy T [MeV]","reaction cross section [mb]"}',
'{584,712,870,1228,1447,1865,243,250,264,254,246,224}',
'{0,0,0,0,0,0,3,2,1,2,2,2}',
'{0,0,0,0,0,0,3,2,1,2,2,2}',
'{0,0,0,0,0,0,2,3,3,0,0,0}',
'{0,0,0,0,0,0,2,3,3,0,0,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
217,5,1,
23,6,7,4,217,2,1);
--
--- Ca  pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(218,1000,'Pion reaction cross sections',6,
'{"kinetic Energy T [MeV]","reaction crosssection [mb]"}',
'{584,712,870,1228,1447,1865,587,594,609,599,601,555}',
'{0,0,0,0,0,0,7,9,5,4,5,5}',
'{0,0,0,0,0,0,7,9,5,4,5,5}',
'{0,0,0,0,0,0,6,6,6,0,0,0}',
'{0,0,0,0,0,0,6,6,6,0,0,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
218,5,1,
9,20,7,4,218,2,1);
---
--- Ca pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(219,1000,'Pion reaction cross sections',6,
'{"kinetic Energy T [MeV]","reaction cross section [mb]"}',
'{584,712,870,1228,1447,1865,608,607,624,611,598,550}',
'{0,0,0,0,0,0,5,5,5,4,5,3}', 
'{0,0,0,0,0,0,5,5,5,4,5,3}', 
'{0,0,0,0,0,0,6,6,6,0,0,0}',
'{0,0,0,0,0,0,6,6,6,0,0,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
219,5,1,
23,20,7,4,219,2,1);
--
--- Ho  pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(220,1000,'Pion reaction cross sections',3,
'{"kinetic Energy T [MeV]","reaction crosssection [mb]"}',
'{712,870,1228,1543,1606,1544}',
'{0,0,0,28,15,26}',
'{0,0,0,28,15,26}',
'{0,0,0,15,16,0}',
'{0,0,0,15,16,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
220,5,1,
9,67,7,4,220,2,1);
---
--- Ho pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(221,1000,'Pion reaction cross sections',3,
'{"kinetic Energy T [MeV]","reaction cross section [mb]"}',
'{712,870,1228,1583,1607,1594}',
'{0,0,0,14,13,10}',
'{0,0,0,14,13,10}',
'{0,0,0,16,16,10}',
'{0,0,0,16,16,10}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
221,5,1,
23,67,7,4,221,2,1);
--
--- Ni  pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(222,1000,'Pion reaction cross sections',6,
'{"kinetic Energy T [MeV]","reaction cross section [mb]"}',
'{584,712,870,1228,1447,1865,741,742,749,752,750,707}',
'{0,0,0,0,0,0,8,9,5,5,6,8}',
'{0,0,0,0,0,0,8,9,5,5,6,8}',
'{0,0,0,0,0,0,7,7,7,0,0,0}',
'{0,0,0,0,0,0,7,7,7,0,0,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
222,5,1,
9,28,7,4,222,2,1);
---
--- Ni pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(223,1000,'Pion reaction cross sections',6,
'{"kinetic Energy T [MeV]","reaction cross section [mb]"}',
'{584,712,870,1228,1447,1865,764,764,772,771,758,710}',
'{0,0,0,0,0,0,6,6,3,5,4,4}',
'{0,0,0,0,0,0,6,6,3,5,4,4}',
'{0,0,0,0,0,0,7,7,7,0,0,0}',
'{0,0,0,0,0,0,8,8,8,0,0,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
223,5,1,
23,28,7,4,223,2,1);
--
--- Pb  pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(224,1000,'Pion reaction cross sections',6,
'{"kinetic Energy T [MeV]","reaction cross section [mb]"}',
'{584,712,870,1228,1447,1865,1754,1752,1772,1743,1736,1670}',
'{0,0,0,0,0,0,16,16,9,13,16,13}',
'{0,0,0,0,0,0,16,16,9,13,16,13}',
'{0,0,0,0,0,0,18,18,18,0,0,0}',
'{0,0,0,0,0,0,18,18,18,0,0,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
224,5,1,
9,82,7,4,224,2,1);
---
--- Pb pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(225,1000,'Pion reaction cross sections',6,
'{"kinetic Energy T [MeV]","reaction cross section [mb]"}',
'{584,712,870,1228,1447,1865,1806,1814,1808,1817,1802,1703}',
'{0,0,0,0,0,0,15,12,6,9,7,16}',
'{0,0,0,0,0,0,15,12,6,9,7,16}',
'{0,0,0,0,0,0,18,18,18,0,0,0}',
'{0,0,0,0,0,0,18,18,18,0,0,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
225,5,1,
23,82,7,4,225,2,1);
--
--- Sn  pi+
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(226,1000,'Pion reaction cross sections',6,
'{"kinetic Energy T [MeV]","reaction cross section [mb]"}',
'{584,712,870,1228,1447,1865,1187,1190,1231,1217,1208,1154}',
'{0,0,0,0,0,0,17,10,8,9,11,10}',
'{0,0,0,0,0,0,17,10,8,9,11,10}',
'{0,0,0,0,0,0,12,12,12,0,0,0}',
'{0,0,0,0,0,0,12,12,12,0,0,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
226,5,1,
9,50,7,4,226,2,1);
---
--- Sn pi-
---
INSERT INTO PUBLIC.DATATABLE(DTID,DTYPE,TITLE,NPOINTS,AXIS_TITLE,VAL,ERR_STAT_PLUS,ERR_STAT_MINUS,ERR_SYS_PLUS,ERR_SYS_MINUS) 
VALUES
(227,1000,'Pion reaction cross sections',6,
'{"kinetic Energy T [MeV]","reaction cross section [mb]"}',
'{584,712,870,1228,1447,1865,1221,1230,1249,1249,1233,1164}',
'{0,0,0,0,0,0,11,9,6,7,5,11}',
'{0,0,0,0,0,0,11,9,6,7,5,11}',
'{0,0,0,0,0,0,12,12,12,0,0,0}',
'{0,0,0,0,0,0,12,12,12,0,0,0}');
 INSERT INTO PUBLIC.RESULT(TRID,REFID,MCDTID,BEAM,TARGET,OBSERVABLE,REACTION,DATATABLEID,SCORE,ACCESS) 
VALUES
(
227,5,1,
23,50,7,4,227,2,1);
