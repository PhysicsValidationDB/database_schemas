--
-- this table contains the reference to the entry in the inspire database:
-- we use the inspire API to extract info like author, Journal etc from inspire
-- using the ReferenceUpload java application 
--
CREATE TABLE INSPIREREFERENCE(
   INSPIREID  INTEGER NOT NULL PRIMARY KEY 
);
ALTER TABLE INSPIREREFERENCE OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON  INSPIREREFERENCE TO G4VALWRITER;
GRANT SELECT ON INSPIREREFERENCE  TO G4VALREADER;
---
--- The following table for reference 
--- (e.g. data extracted from the inspire database is cashed here to guarantee fast access)
--
CREATE TABLE REFERENCE(
   REFID      SERIAL PRIMARY KEY,
   INSPIREID  INTEGER,
   AUTHORS    CHARACTER VARYING(100)[],  -- list of authors 
   TITLE      TEXT,                      -- title of the paper
   JOURNAL    CHARACTER VARYING(100),    -- name of the scientific journal
   ERN        CHARACTER VARYING(100),    -- electronic-resource-num
   PAGES      CHARACTER VARYING(50),     -- page numbers
   VOLUME     CHARACTER VARYING(50),     -- journal volume
   YEA        INTEGER,                   -- year of publication
   ABSTR      TEXT,                      -- abstract
   KEYWORDS   CHARACTER VARYING(100)[],  -- keywords 
   LINKURL    TEXT                      -- link to the e.g. inspire url for this paper 
);
ALTER TABLE REFERENCE OWNER TO g4valwriter;
GRANT SELECT, UPDATE, INSERT, DELETE ON  REFERENCE TO g4valwriter;
GRANT SELECT ON REFERENCE TO g4valreader;
---
--- stuff representing the data (e.g. 1D/2d Histograms, 1D/2D Graphs)
---
CREATE TABLE DATATYPES(
DTYPE INTEGER PRIMARY KEY,
DESCRIPTION CHARACTER VARYING(50) UNIQUE
);
ALTER TABLE DATATYPES OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON  DATATYPES TO G4VALWRITER;
GRANT SELECT ON  DATATYPES TO G4VALREADER;
---
--- Table representing the data (e.g. 1D/2d Histograms, 1D/2D Graphs)
---
CREATE TABLE DATATABLE(
       DTID           SERIAL PRIMARY KEY,
       DTYPE          INTEGER  REFERENCES DATATYPES(DTYPE),
       title          CHARACTER VARYING(100),
       npoints        INTEGER,
       NBINS          INTEGER[],
       axis_title     CHARACTER VARYING(100)[],
       val            REAL[],
       err_stat_plus  REAL[],
       err_stat_minus REAL[],
       err_sys_plus   REAL[],
       err_sys_minus  REAL[],
       bin_min        REAL[],
       bin_max        REAL[]
);
ALTER TABLE DATATABLE OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON  DATATABLE TO G4VALWRITER;
GRANT SELECT ON  DATATABLE TO G4VALREADER;
--
-- Name: scores; Type: TABLE; Schema: public; Owner: g4valwriter; Tablespace: 
--
CREATE TABLE SCORES (
    SCOREID  SERIAL PRIMARY KEY,
    SCORE CHARACTER VARYING(10),
    STYPE CHARACTER VARYING(10), 
    UNIQUE (SCORE,STYPE)
);
ALTER TABLE SCORES OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON SCORES  TO G4VALWRITER;
GRANT SELECT ON  SCORES TO G4VALREADER;
--
--  dictionary table defining the ACCESS of a test result
--
CREATE TABLE ACCESS (
    ACCESSID  SERIAL PRIMARY KEY,
    ACCESS CHARACTER VARYING(50) NOT NULL UNIQUE
);
ALTER TABLE ACCESS OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON ACCESS  TO G4VALWRITER;
GRANT SELECT ON  ACCESS TO G4VALREADER;

CREATE TABLE MCTOOL(
   MCID SERIAL  PRIMARY KEY,
   MCNAME       CHARACTER VARYING(20) NOT NULL UNIQUE
); 


ALTER TABLE  MCTOOL OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON MCTOOL  TO G4VALWRITER;
GRANT SELECT ON MCTOOL TO G4VALREADER;

CREATE TABLE MCDETAIL(
   MCDTID       SERIAL PRIMARY KEY,
   MCTID        INTEGER REFERENCES MCTOOL(MCID),
   VERSIONTAG   CHARACTER VARYING(50) NOT NULL,
   MODEL        CHARACTER VARYING(50) NOT NULL,
   UNIQUE (MCTID,VERSIONTAG,MODEL)
); 

ALTER TABLE   MCDETAIL OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON MCDETAIL  TO G4VALWRITER;
GRANT SELECT ON MCDETAIL TO G4VALREADER;

CREATE TABLE PARTICLE(
   PDGID  INTEGER PRIMARY KEY,
   PNAME  CHARACTER VARYING(50) UNIQUE
);
ALTER TABLE  PARTICLE OWNER TO g4valwriter;
GRANT SELECT, UPDATE, INSERT, DELETE ON PARTICLE  TO g4valwriter;
GRANT SELECT ON PARTICLE  TO g4valreader;

CREATE TABLE MATERIAL(
   MID       SERIAL PRIMARY KEY,
   Z         INTEGER,                                 -- z
   MNAME     CHARACTER VARYING(50) NOT NULL UNIQUE,   -- name of material
   DENSITY   REAL                  NOT NULL,          -- density in g/cm^3
   ION       REAL                  NOT NULL,          -- ionization potential in eV
   NCOMP     INTEGER,                                 -- number of compounds
   COMP      INTEGER[],                               -- which compound
   FRAC      REAL[]                                   -- fraction of compound
);
ALTER TABLE  MATERIAL OWNER TO g4valwriter;
GRANT SELECT, UPDATE, INSERT, DELETE ON MATERIAL  TO g4valwriter;
GRANT SELECT ON MATERIAL TO g4valreader;
---
--- Elements:
---

CREATE TABLE OBSERVABLE(
   OID SERIAL PRIMARY KEY,
   ONAME  CHARACTER VARYING(50) UNIQUE
);
ALTER TABLE  OBSERVABLE OWNER TO g4valwriter;
GRANT SELECT, UPDATE, INSERT, DELETE ON OBSERVABLE TO g4valwriter;
GRANT SELECT ON OBSERVABLE TO g4valreader;


CREATE TABLE REACTION(
   RID SERIAL PRIMARY KEY,
   RNAME  CHARACTER VARYING(50) UNIQUE
);
ALTER TABLE  REACTION OWNER TO g4valwriter;
GRANT SELECT, UPDATE, INSERT, DELETE ON REACTION  TO g4valwriter;
GRANT SELECT ON REACTION TO g4valreader;


---
--- The following table should evolve as we will deal with test beams/neutrino fluxes  etc. 
---
CREATE TABLE BEAM(
   BID SERIAL PRIMARY KEY,
   PARTICLE INTEGER REFERENCES PARTICLE(PDGID),     
   ENERGY REAL                              -- kinetic Energy in MeV
);
ALTER TABLE  BEAM OWNER TO g4valwriter;
GRANT SELECT, UPDATE, INSERT, DELETE ON BEAM  TO g4valwriter;
GRANT SELECT ON BEAM TO g4valreader;
---
--- The following table shows evolve as we will deal with test beam setups etc. 
---
CREATE TABLE BEAM2(
   BID SERIAL   PRIMARY KEY,
   BNAME        CHARACTER VARYING(50) UNIQUE,         -- name of the Beam or flux file  
   REFID        INTEGER REFERENCES REFERENCE(REFID),  -- reference where beam is described
   PARTICLEIDS  INTEGER[],                            -- Particle Content of Beam (each references PARTICLE(PDGID))
   MEANENERGY   REAL[],                               -- kinetic Energy in MeV for each component
   DATATABLEIDS INTEGER[]                             -- pointer to histograms with fluxes/particle contribution each REFERENCES DATATABLE(DTID),
                                                      -- can be null  
);
ALTER TABLE  BEAM2 OWNER TO g4valwriter;
GRANT SELECT, UPDATE, INSERT, DELETE ON BEAM2  TO g4valwriter;
GRANT SELECT ON BEAM2 TO g4valreader;
--
-- table representing dictionary of working group
--
CREATE TABLE WGROUPS (
    WGID SERIAL  PRIMARY KEY,                    -- working group id
    WGNAME      CHARACTER VARYING(50) UNIQUE     -- name of the working group             
);
ALTER TABLE WGROUPS OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON WGROUPS  TO G4VALWRITER;
GRANT SELECT ON WGROUPS TO G4VALREADER;
--
-- table representing a TEST and providing a description. 
--
CREATE TABLE TEST (
    TESTID      INTEGER NOT NULL PRIMARY KEY,            -- number matches geant 4 test number if applicable
    TESTNAME    CHARACTER VARYING(50) UNIQUE,            -- name of the test
    MCID        INTEGER REFERENCES MCTOOL(MCID),         -- what montecarlo (MCTOOL)  (e.g. Genie or Geant4)
    DESCRIPTION TEXT,                                    -- text describing the purpose of the test
    RESPONSIBLE CHARACTER VARYING(100)[],                -- names of persons responsible for the test
    WG          INTEGER REFERENCES WGROUPS(WGID),        -- working group that the test is associated with
    KEYWORDS    CHARACTER VARYING(50)[],                 -- keywords associated with the test
    REFERENCEID INTEGER[],                               -- array elements reference RID in the  REFERENCE table
    DEFAULTS    INTEGER[]                                -- references to the default results to be displayed
);
ALTER TABLE TEST OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON TEST  TO G4VALWRITER;
GRANT SELECT ON TEST TO G4VALREADER;
---
---  An imageblob is used to store images (jpeg, gif etc.) directly in form of a blob (Binary Large OBject (BLOB))
---  to allow the use of blobs in the postgres database you must:  
---  modify the following /postgresql.conf
---  # For Postgresql versions later than 9
---  lo_compat_privileges = on 
---
CREATE TABLE IMAGEBLOB (
    IBID SERIAL PRIMARY KEY,
    IMAGE OID NOT NULL,
    MTYPE CHARACTER VARYING(50) DEFAULT 'IMAGE/GIF'::CHARACTER VARYING NOT NULL,
    FNAME CHARACTER VARYING(50) DEFAULT 'IMAGE.GIF'::CHARACTER VARYING NOT NULL
);
ALTER TABLE IMAGEBLOB OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON IMAGEBLOB TO G4VALWRITER;
GRANT SELECT ON IMAGEBLOB TO G4VALREADER;
--
-- table representing RESULT (experimental or simulation)
-- it defines the meta data and links to a datatable or imageblob
--
CREATE TABLE RESULT (
   TRID         SERIAL PRIMARY KEY,
   TID          INTEGER REFERENCES TEST(TESTID),                         -- REFERENCES THE TEST THAT TEST RESULT IS ASSOCIATED WITH
   REFID        INTEGER REFERENCES REFERENCE(REFID),                     -- reference the Reference that the experimental data is associated with
   MCDTID       INTEGER REFERENCES MCDETAIL(MCDTID),                     -- REFERENCES the MC details (tool, version,model)
   BEAM         INTEGER REFERENCES BEAM(BID),                            -- REFERENCES PARTICLE TABLE (PDGID,NAME, ...)
   TARGET       INTEGER REFERENCES MATERIAL(MID),                        -- REFERENCES TABLE defining materials
   OBSERVABLE   INTEGER REFERENCES OBSERVABLE(OID),                      -- REFERENCES TABLE defininG OBSERVABLES (CROSS SECTION, MOMENTUM OF OUTGOING PARTICLES..)
   SECONDARY    INTEGER REFERENCES PARTICLE(PDGID),                      -- REFERENCES PARTICLE TABLE (PDGID,NAME, ...)
   REACTION     INTEGER REFERENCES REACTION(RID),                        -- REFERENCES REACTION TABLE (SCATTERING, PARTICLE PRODUCTION, CAPTURE, DECAY....)
   DATATABLEID  INTEGER REFERENCES DATATABLE(DTID),                      -- REFERENCES DATATABLE can be null
   IMAGEBLOBID  INTEGER REFERENCES IMAGEBLOB(IBID),                      -- REFERENCES IMAGEBLOB can be null
   SCORE        INTEGER REFERENCES SCORES(SCOREID),                      -- TEST SCORE AND TYPE
   ACCESS       INTEGER REFERENCES ACCESS(ACCESSID),                     -- controls who has access to the test result
   PARNAMES     CHARACTER VARYING(50)[],                                 -- additional parameters e.g. phi angle of outgoing particle 
   PARVALUES    CHARACTER VARYING(50)[],                                 -- value of additional parameter e.g 50 deg
 MODTIME      TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP       -- make the default value of modtime be the time at which the row is inserted
);
ALTER TABLE RESULT OWNER TO G4VALWRITER;
GRANT SELECT, UPDATE, INSERT, DELETE ON RESULT TO G4VALWRITER;
GRANT SELECT ON RESULT TO G4VALREADER;








